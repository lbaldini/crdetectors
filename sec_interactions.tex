\section{Interaction of radiation with matter}%
\label{sec:interactions}

This section does not contain more information (and in fact it contains a lot
less information) that any of the excellent references floating
around~\cite{PDG,Leo,Knoll}---except maybe for the fact that we made an
effort to put this information in the specific context of the review.


\subsection{Charged particles: energy losses}

Energy losses of charged particles are customarily discussed separately for
heavy (e.g., protons, alpha particles and nuclei) and light (e.g. electrons)
particles---the reason behind that being that radiation losses are, generally
speaking, negligible for the the former. Given that, in principle, all
particles---be they heavy or light---suffer both ionization and radiation
losses, here we take a slightly different approach, focusing our discussion,
at the top level, on the distinction between these two types of losses.


\subsubsection{Ionization losses}

Relativistic charged heavy particles passing through matter loose energy by
ionization at an average rate that is reasonably well described by the Bethe
equation over most of energies we are interested:
\begin{align}\label{eq:stopping_power_full}
  -\left<\frac{dE}{dx}\right>_{\rm ion} \!\!\!\!\!\!
  & = 4\pi N_Ar_e^2m_ec^2 z^2 \frac{Z}{A}\frac{1}{\beta^2}\times
  \nonumber\\
  & \left[\frac{1}{2}\ln \frac{2m_ec^2\beta^2\gamma^2T_{\rm max}}{I^2}
    - \beta^2 -\frac{\delta(\beta\gamma)}{2} \right]
\end{align}
(here $z$, $\beta$ and $\gamma$ refer to the projectile, while $Z$ and $A$
refer to the target material; see \cite{PDG} for a detailed description of
all the other terms in the equation).

Complicated as this might seem, the main features are readily evident if one
rewrites it as
\begin{align}\label{eq:stopping_power}
  -\left<\frac{dE}{dx}\right>_{\rm ion} \!\!\!\!\!\!
  \propto z^2 \frac{Z}{A}\frac{1}{\beta^2}
  \left[ \frac{1}{2} \ln(C_0 \beta^2\gamma^2) - \beta^2 +
    \text{corrections} \right].
\end{align}
The energy loss per unit length is proportional to the $Z/A$ ratio of the
target material and the \emph{square} of the charge $z$ of the incident
particle. When the latter is \emph{slow} ($\beta \ll 1$) the energy losses
decrease as $1/\beta^2 \propto 1/E$ as $\beta$ increases.
In the ultra-relativistic regime ($\beta \approx 1$) the losses increase
as $\ln \beta^2\gamma^2$ (this generally goes under the name of
\emph{relativistic rise} or \emph{logarithmic raise}). These basic facts are
customarily used, as we shall briefly see in the following, for charge
measurement and/or particle identification.

We should note that, since ionization losses are fundamentally due to
interactions between the incoming particle and the atomic electrons of the
medium, there are significant kinematic differences between the two cases of
heavy and light projectiles. In addition to that, quantum-mechanical effects due
to the fact that the projectile and the target are identical particles come
into play in case of electrons. But we are neglecting a whole lot of details
anyway and won't push this any further.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{stopping_power}
  \caption{Average energy loss $dE/dx$, as a function of $\beta\gamma$ for
    muons in silicon. The data points are taken from
    \url{http://pdg.lbl.gov/2012/AtomicNuclearProperties/MUON_ELOSS_TABLES/muonloss_014.dat}.}
  \label{fig:stopping_power}
\end{figure}

When plotted as a function of $\beta\gamma$ (i.e., the momentum) of the
incident particle, the energy-loss curve has the typical shape shown in
figure~\ref{fig:stopping_power}, with a relatively broad minimum at
$\beta\gamma \sim 3$, where $dE/dx$ (normalized to the density of the target)
is of the order of 1--2~MeV~g$^{-1}$~cm$^{2}$.
A particle with the energy corresponding to this minimum ionization is
customarily called a \emph{minimum ionizing particle} (MIP).


\subsubsection{Radiation losses and critical energy}

At sufficiently high energy any charged particle radiates. Radiation losses
are nearly proportional to the particle energy
\begin{align}\label{eq:rad_losses}
  -\left<\frac{dE}{dx}\right>_{\rm rad} \!\!\!\!\!\! \propto E,
\end{align}
and, since ionization losses only grow logarithmically, the two loss rates,
when seen as function of energy, are bound to cross each other at some point,
as shown in figure~\ref{fig:stopping_power}. The energy $E_c$ at which this
happens is called \emph{critical energy}. Since the constant of proportionality
in equation~\eqref{eq:rad_losses} scales as $1/m^2$, for any given material
$E_c$ is different for different particles---and it is much higher for heavier
particles. For reference, the critical energy in silicon is $\sim 580$~GeV
for muons and $\sim 40$~MeV for electrons (and in the tens to hundreds of TeV
for protons).

To all practical purposes radiation losses are mainly relevant for electrons
(and positrons) and it is customary to refer to the critical energy for
electrons as \emph{the} critical energy%
\footnote{We note explicitly that, in a given material, the critical energies
  for electrons and positrons are, in general, slightly different.}%
. A popular empirical approximation for $E_c$ as a function of the atomic
number of the material (for solids) is given by
\begin{align}
  E_c = \frac{710}{Z + 0.92}\ {\rm MeV}
\end{align}
and plotted in figure~\ref{fig:critical_energy}.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{critical_energy}
  \caption{Empirical parameterization of the electron critical energy as a
    function of the atomic number from~\cite{PDG}.}
  \label{fig:critical_energy}
\end{figure}


\subsubsection{Radiation length}%
\label{sec:radiation_length}

Above the critical energy (i.e. in the regime where radiation losses dominate)
equation~\eqref{eq:rad_losses} can readily integrated in a homogeneous material,
yielding the electron energy as a function of the distance $x$ traversed:
\begin{align}
  E(x) = E_0 \exp\left(-\frac{x}{X_0}\right).
\end{align}
The quantity $X_0$, representing the typical length over which an electron
looses all but $1/e$ of its energy due to \bremss, is called
\emph{radiation length} and is characteristic of the material.
The radiation length is conveniently expressed in g~cm$^{-2}$, i.e.,
factoring out the density of the medium. A popular parameterization is given by
\begin{align}\label{eq:param_radlen}
  X_0 = \frac{716 A}{Z(Z + 1) \ln(287/\sqrt{Z})}~{\rm g\ cm^{-2}}
\end{align}
and table~\ref{tab:exp_radlen} shows numerical values for a few materials of
interest (see also figure~\ref{fig:rad_int_len}). 

\begin{table}[htb!]
  \begin{tabular}{p{0.23\linewidth}p{0.23\linewidth}p{0.23\linewidth}%
      p{0.23\linewidth}}
    \hline
    Material & $X_0$~[g~cm$^{-2}$] & $\density$ [g~cm$^{-3}$] & $X_0$ [cm]\\
    \hline
    \hline
    Pb & 6.37 & 11.350 & 0.561\\
    BGO & 7.97 & 7.130 & 1.12\\
    CsI & 8.39 & 4.510 & 1.86\\
    W & 6.76 & 19.3 & 0.350\\
    C (graphite) & 42.70 & 2.210 & 19.3\\
    Si & 21.82 & 2.329 & 9.37\\
    Air & 36.62 & $1.2 \times 10^{-3}$ & 30,500\\
    \hline
  \end{tabular}
  \caption{Tabulated values of the radiation length for some materials of
    interest.}
  \label{tab:exp_radlen}
\end{table}

The radiation length is the natural scale for all the most relevant
electromagnetic phenomena we shall deal with in the following: multiple
scattering, electron \bremss, pair production and electromagnetic
showers. We shall customarily indicate with the letter $t = x/X_0$ any
distance measured in units of $X_0$.


\subsection{Multiple Coulomb scattering}%
\label{sec:inter_mcs}

A charged particle traversing a medium undergoes multiple Coulomb scatterings
on the atomic nuclei of the material. In the so-called gaussian approximation
the root mean square of the deviation angle projected in any of the planes
containing the incoming particle direction can be parameterized as
\begin{align}\label{eq:theta_ms}
  \thetamsp =
  \frac{0.0136~{\rm GeV}}{\beta c p}z\sqrt{t} (1 + 0.038 \ln t)~\text{rad},
\end{align}
where $\beta c$, $p$ and $z$ are the velocity, momentum and charge number
of the incoming particle, and $t$ is the thickness of the traversed
material in units of radiation lengths.

The deflection space angle being the sum in quadrature of the independent
deflections on two orthogonal planes, it is simply given by
\begin{align}
  \thetamss = \sqrt{2}\thetamsp.
\end{align}

Whether one or the other is more relevant depends on the problem at hand
(e.g., typically the pointing accuracy of a detector is parameterized in terms
of the space angle, but in a magnetic spectrometer the important figure is
really the deflection angle in the bending plane). In the following we shall use
both and we shall try and make clear which one we are referring to.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{multiple_scattering}
  \caption{Multiple scattering angle, as a function of the momentum of the
    incoming particle, for three sample values of $t$, for electrons and
    protons. The three values of $t$ corresponds to $300~\mu$m of silicon
    (the thickness of the AMS-02 silicon detectors) and the \Fermi-LAT thin
    and thick radiators.}
  \label{fig:multiple_scattering}
\end{figure}

Figure~\ref{fig:multiple_scattering} shows the (space) multiple scattering
deflection angles for three sample values of $t$, for electrons and protons, as
a function of their momentum. In the ultra-relativistic regime
$\beta \approx 1$ (i.e. above a few MeV for electrons and above a few GeV for
protons) the curves are indistinguishable and fall as $1/p$. The difference
below 1~GeV has implication for the momentum resolution in a magnetic
spectrometer and we shall come back to it in section~\ref{sec:ams_mdr}.


\subsection{High-energy photons}%
\label{sec:inter_gammas}

High-energy photons (above a few tens of MeV) interact with matter mainly by
pair production in the field of atomic nuclei. As it turns out, the
processes of electron \bremss\ and pair production are intimately
related to each other, and the mean free path for pair production for a
high-energy photon is given by
\begin{align}\label{eq:lambda_pair}
  \lambda_{\rm pair} = \frac{9}{7}X_0
\end{align}
(i.e., it is $9/7$ of the scale length over which a high-energy electron
looses all but $1/e$ of its energy).

It should be noted that, while pair production is a destructive process
(the gamma-ray no longer exists afterwards), \bremss\ only degrades the
electron energy. As we shall see in a second, these two physical processes are
at the base of the development of electromagnetic showers.


\subsubsection{More on the pair production}%
\label{sec:more_pair_production}

The $e^+/e^-$ pair production by high-energy gamma rays is the basic physical
process used in pair conversion telescopes. It is pretty much conventional
wisdom that the kinematic of the process is closed and the tracks of the
electron and the positron can in principle be combined to recover \emph{exactly}
the original photon direction. It turns out that, even neglecting the finite
detector resolution, this is not quite true, and there are a few subtleties
involved that we briefly mention in this section.

The average opening angle of the electron-positron pair scales as
\begin{align}
  \theta_{\rm open} \propto \frac{m_ec^2}{E},
\end{align}
where $E$ is the photon energy. Depending on $E$ and on the layout of the
tracking detectors, this angle might be too small to be resolved, in which case
the two tracks effectively overlap (i.e., from an experimental standpoint, one
really sees one track).

At low enough energy, where the opening angle is relatively large and the tracks
are well separated, in order to recover the original photon direction one
should in principle combine the momenta of the two particles
\emph{covariantly} which brings up the question of the track energies.
The differential distribution in the fractional electron (or positron) energy
is relatively flat%
\footnote{The distribution is actually slightly peaked at $0$ and $1$, and more
and more so the highest the energy.}
so that asymmetric (energy-wise) pairs are quite common---if you think about,
this implies that the average energy of the particle in the pair with the
\emph{highest} energy is approximately $3/4$ of the original photon energy.
Now, depending on the experimental setup, one might measure the single track
energies with good or bad resolution---or just not measure them at all. As one
might imagine, this has profound implications on the maximum attainable angular
resolution for pair conversion telescopes.

There is one last ingredient to the mix that we haven't mentioned yet, namely
the nucleus recoil (remember, typically gamma rays pair-produce in the field
of the nucleus). In practical applications this is largely irrelevant, as one
is usually limited by the multiple scattering in the low-energy regime, as
we shall see in section~\ref{sec:lat_psf}. When this is not true the nucleus
recoil (which is essentially impossible to measure) becomes the ultimate
limiting factor to the accuracy with which one can reconstruct the photon
direction. It is shown in~\cite{2014APh....59...18H} that this \emph{kinematic}
limit $\theta_{\rm kl}$ is approximately given by
\begin{align}
  \theta_{\rm kl} \sim 5^\circ \left( \frac{10~{\rm MeV}}{E} \right)
\end{align}
and this figure is possibly relevant when designed gamma-ray detectors
optimized for the low-energy end of the pair production regime.


\subsection{Electromagnetic showers}%
\label{sec:em_showers}

As explained in the previous section, high-energy electrons and photons
produce in matter secondary photons by \bremss\ and electron-positron
pairs by pair production. These secondaries, in turn, can produce other
particles with progressively lower energy and start an
\emph{electromagnetic shower} (or \emph{cascade}).
The process continue until the average energy of the electron component falls
below the critical energy of the material---at which point the rest of the
energy is released via ionization.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{toy_em_shower}
  \caption{Sketch of the development of an electromagnetic shower in the 
    simplest possible toy model.}
  \label{fig:toy_em_shower}
\end{figure}

A simple toy model for an electron-initiated shower is schematically
represented in figure~\ref{fig:toy_em_shower}, where after $t$ radiation lengths
of material, the cascade has developed in $2^t$ particles (a mix of electrons,
positrons and photons) with average energy $E_0/2^t$. Rough as this model is
(for one thing we neglect the stocasticity of the process altogether, along
with the $9/7$ in~\eqref{eq:lambda_pair} and the fact that the electron
can---and does---radiate multiple photons of different energy per radiation
length) it is able to reproduce one of the main features of electromagnetic
showers, namely the fact that the position of the shower maximum scales
logarithmically with the energy. In fact the stop condition reads
\begin{align}
  \frac{E_0}{2^{t_{\rm max}}} \sim E_c,
\end{align}
and hence
\begin{align}
  t_{\rm max} \sim \ln \left(\frac{E_0}{E_c}\right).
\end{align}

At a slightly higher level of sophistication, the longitudinal profile of an
electromagnetic shower can be effectively described as
\begin{align}\label{eq:long_profile}
  \frac{dE}{dt} = E_0 b \frac{(bt)^{a-1} e^{-bt}}{\Gamma(a)},
\end{align}
where $a$ and $b$ are parameters related to the nature of the incident particle
(electron or photon) and to the characteristics of the medium ($b = 0.5$
is a reasonable approximation in many cases of practical interest).
The position of the shower maximum occurs at
\begin{align}\label{eq:shower_max}
  t_{\rm max} = \frac{(a - 1)}{b} \approx
  \ln \left(\frac{E_0}{E_c}\right) + t_0
\end{align}
where $t_0 = -0.5$ for electrons and $t_0 = 0.5$ for photons. For the sake
of clarity, the way one typically uses these relation is to plug $E_0$, $E_c$
and $t_0$ in \eqref{eq:shower_max} to find $a$ (assuming $b = 0.5$) and then
use~\eqref{eq:long_profile} to describe the longitudinal profile of the shower.

\begin{figure}[htb]
  \includegraphics[width=\linewidth]{shower_profile}
  \caption{Average longitudinal profile of the shower generated by a
    $100$~GeV electron in a homogeneous slab of BGO.}
  \label{fig:shower_profile}
\end{figure}

Figure~\ref{fig:shower_profile} shows the average shower profile for
$100$~GeV in BGO. With a critical energy of $10.1$~MeV, the position of
the shower maximum is located, according to equation~\eqref{eq:shower_max},
at $8.7~X_0$.

We mention, in passing, that the transverse development of an electromagnetic
shower, mainly due to the multiple scattering of electrons and positrons away
from the shower axis, scales with the so-called the Moli\`ere radius,
that can be empirically parameterized as
\begin{align}
  R_M \approx \frac{21 X_0}{E_c~{[\rm MeV]}}~{\rm MeV}.
\end{align}


\subsection{Hadronic showers}%
\label{sec:had_showers}

While the development of electromagnetic showers is determined, as we have seen,
by two well-understood QED processes, the energy degradation of hadrons proceeds
through both strong and electromagnetic interactions in the medium.
The complexity of the hadronic and nuclear processes produce a multitude of
phenomena that make hadronic showers intrinsically more complicated than the
electromagnetic ones. In other words for hadronic showers there is no such a
thing as the toy model illustrated in figure~\ref{fig:toy_em_shower}.

As a matter of fact hadronic showers consist in general of two distinctly
different components: (i) an electromagnetic component due to $\pi^0$ and
$\eta$ generated in the absorption process and decaying into photons
(which in turn develop electromagnetic showers) before they have a chance to
undergo a new strong interaction; and (ii) a non-electromagnetic component,
which combines essentially everything else that takes place in the absorption
process.
Most importantly, the two components evolve with different length scales: the
radiation length $X_0$ for the first and the nuclear interaction length
$\lambda_I$ (which is at least on order of magnitude larger for $Z > 30$, as
shown in figure~\ref{fig:rad_int_len}) for the second. A useful
parameterization for $\lambda_I$ is
\begin{align}\label{eq:param_intlen}
  \lambda_I = 37.8 A^{0.312}~\text{g~cm}^{-2}
\end{align}
and the actual values for some relevant materials are shown in
table~\ref{tab:exp_intlen}.

\begin{figure}
  \includegraphics[width=\linewidth]{rad_int_len}
  \caption{Approximate dependence of the radiation length $X_0$ and the
    nuclear interaction length $\lambda_I$ on the atomic number $Z$ of the
    material. The parameterizations used are those in equations
    \eqref{eq:param_radlen} and \eqref{eq:param_intlen}, with the additional
    assumption $A = 2Z$.}
  \label{fig:rad_int_len}
\end{figure}

\begin{table}[htb!]
  \begin{tabular}{p{0.23\linewidth}p{0.23\linewidth}p{0.23\linewidth}%
      p{0.23\linewidth}}
    \hline
    Material & $\lambda_I$~[g~cm$^{-2}$] & $\density$ [g~cm$^{-3}$] &
    $\lambda_I$ [cm]\\
    \hline
    \hline
    Pb & 199.6 & 11.350 & 17.6 \\
    BGO & 159.1 & 7.130 & 22.3 \\
    CsI & 171.5 & 4.510 & 38.0\\
    W & 191.9 & 19.3 & 9.94\\
    C (graphite) & 85.8 & 2.210 & 38.8\\
    Si & 108.4 & 2.329 & 46.5\\
    Air & 90.1 & $1.2 \times 10^{-3}$ & 75,000\\
    \hline
  \end{tabular}
  \caption{Tabulated values of the nuclear interaction length for some materials
    of interest.}
  \label{tab:exp_intlen}
\end{table}

The fractional non-electromagnetic component of a hadronic shower $F_h$ (as
opposed to electromagnetic component $F_e = 1 - F_h$) decreases with energy and
is generally~\cite{PDG} parameterized as
\begin{align}
  F_h(E) = \left( \frac{E}{E_0} \right)^{k-1},
\end{align}
where typical values are $E_0 \approx 1$~GeV and $k \approx 0.8$ (roughly
speaking, the fraction of energy the non-electromagnetic component accounts
for is of the order of $50\%$ at $100$~GeV and $30\%$ at $1$~TeV).

In broad terms, hadronic showers tend to start developing at relatively large
depths in the material and they are typically larger and more irregular when
compared with electromagnetic showers. As we shall see in the following, all
these differences are customarily used in modern space-based imaging
calorimeters for particle identification---particularly to discriminate
electrons and photons against the much larger proton background.


\subsection{\cheren\ radiation}%
\label{sec:cherenckov_rad}

\cheren\ radiation is emitted when a charged particle moves in a medium at 
a speed greater than the speed of light \emph{in that medium}
\begin{align}
  \beta > \frac{1}{n}.
\end{align}
(Here $\beta$ refers to the incident particle and $n$ is the index of
refraction of the material.) In the ideal case of a non-dispersive medium, the
\cheren\ wave front form an acute angle with respect to the particle velocity
given by
\begin{align}
  \cos\theta_c = \frac{1}{n\beta}
\end{align}
as sketched in figure~\ref{fig:cherenkov_sketch}.

\begin{figure}[htb]
  \includegraphics[width=\linewidth]{cherenkov_sketch}
  \caption{Sketch of the geometry relevant for the \cheren\ effect.}
  \label{fig:cherenkov_sketch}
\end{figure}

The (double differential) spectrum of the photons produced per unit path length
and wavelength is given by
\begin{align}\label{eq:cherenkov_loss}
  \frac{d^2N}{dxd\lambda} &= \frac{2\pi\alpha z^2}{\lambda^2}
  \left(1 -  \frac{1}{\beta^2 n^2(\lambda)}\right) = \nonumber\\
  &= \frac{2\pi\alpha z^2}{\lambda^2}
  \left(1 - \frac{1 + \beta^2\gamma^2}{\beta^2\gamma^2n^2(\lambda)}\right),
\end{align}
where $z$ is the charge of the projectile, and the index of refraction $n$
is evaluated at the generic photon wavelength $\lambda$.
Equation~\eqref{eq:cherenkov_loss} cannot be readily translated into a detector
signal as, in practice, one has to convolve it with the response of the
transducer and integrate over the photon wavelengths of interest.
That all said, plotting~\eqref{eq:cherenkov_loss} for a reference value of
the index of refraction is a useful illustrative exercise.

\begin{figure}[htb]
  \includegraphics[width=\linewidth]{cherenkov_betagamma}
  \caption{Number of \cheren\ photons emitted per unit path length
    and wavelength as a function of the $\beta\gamma$ of the projectile,
    for a reference index of refraction $n = 1.5$.}
  \label{fig:cherenkov_betagamma}
\end{figure}

While, strictly speaking, the \cheren\ radiation is generally not important in
terms of energy losses, we shall see in the following that its basic properties
(the existence of a threshold and the dependence of $\theta_c$ and
$d^2N/dxd\lambda$ on the velocity of the particle) are customarily exploited
in high-energy physics for particle identification and velocity measurements.
In particular figure~\ref{fig:cherenkov_betagamma} shows that the steep slope
near the threshold potentially allows to achieve a good velocity or momentum
resolution in that region (while the amount of radiation saturates at higher
energies).


\subsection{Transition radiation}%
\label{sec:transition_rad}

Transition radiation is emitted when a ultra-relativistic particle crosses the
interface between two media with different indices of refraction.
As we shall see in the following, in practical implementations the useful
(i.e., detectable) photons are in the x-ray band, which requires the
incidence particle to have a relativistic $\gamma$ factor of the order of
$10^3$.

For a single interface the fractional energy emitted into x-rays above a given
energy $\hbar\omega_0$ is given by
\begin{align}\label{eq:tr_yield}
  F(\hbar\omega > \hbar\omega_0) = 
  \frac{\alpha z^2}{\pi}\left[
    \left( \ln\frac{\gamma\omega_p}{\omega_0} - 1\right)^2 +
    \frac{\pi}{12}
    \right],
\end{align}
where $\omega_p$ is the plasma frequency of the radiation. If $\gamma$ is
big enough it effectively scales as
\begin{align}
  F(\hbar\omega > \hbar\omega_0) \propto z^2 \ln\gamma.
\end{align}
This form of~\eqref{eq:tr_yield} highlights the two main features of the 
transition radiation---namely: it scales as $z^2$ with the charge of the
particle and it grows with $\gamma$.

Since, in practical situations, $F$ is small, transition radiation detectors
typically exploit multiple boundary crosses to enhance the signal. In this case 
interference effects lead to a saturation effect at a value $\gamma$ that,
depending on the actual design, ranges from a few~$10^3$ to $\sim 10^5$.
