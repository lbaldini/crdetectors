\section{In-flight calibration}

Space- and balloon-borne detectors are typically calibrated with particle beams
prior to launch in order to benchmark the Monte Carlo simulations used
to generate the instrument response function. While this is of primary
importance, one should recognize that that the space environment is different,
to many respects, than that in which ground tests are performed. In addition,
cosmic-ray and gamma-ray detector often aim at measuring fluxes and intensities
at much higher energies that those accessible by accelerators.

In this section we shall glance through the topic of in-flight instrument
calibration. There are two main aspects to this---namely the monitoring of the
performance stability in time and the assessment of the systematic
uncertainties associated to the instrument response functions
(e.g., effective area, point-spread function and absolute energy scale).
As we shall see in a second, the latter is generally much harder---and it goes
without saying that it is more critical for satellite (as opposed to balloon)
experiments, since, due the much larger exposure factors, many of the
measurements are systematic-limited.


\subsection{Time stability}

The stability in time of the basic instrument performance figures is usually
monitored both through dedicated calibration systems embedded in the
readout electronics (e.g., charge-injection circuits to measure the gain 
and noise of the front-end amplifiers, or laser systems to measure the 
alignment of tracking devices) and using the particle populations available in
orbit. In this latter respect minimum ionizing protons are a good example
of an abundant \emph{calibration source} with well-known properties that can be
used by large-acceptance detectors to monitor the performance stability
over relatively short time-scales.

Figure~\ref{fig:cal_light_yield} shows the relative light yield of the
\Fermi-LAT calorimeter, measured through the first four years of mission
by means of the path-length-corrected energy deposition of on-orbit minimum
ionizing protons~\cite{2013arXiv1304.5456B}. The slight ($\sim 1\%$ per year)
downward trend (due to the anticipated radiation damage of the CsI crystals) is
a good example of an effect that, if not corrected through time-dependent
calibrations, might produce observable consequences in the high-level science
analysis---first and foremost a drift of the absolute energy scale.
(On a related note, it is worth noticing that it's easier to use
minimum-ionizing proton to monitor the stability of the energy scale, rather
than calibrating it directly.)

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cal_light_yield}
  \caption{Relative light yield for the \Fermi-LAT calorimeter, inferred
    from the path-length-corrected energy deposition of on-orbit minimum
    ionizing protons, throughout the first four years of the mission
    (adapted from~\cite{2013arXiv1304.5456B}). The small downward trending is
    due to the (expected) radiation damage of the calorimeter crystals.}
  \label{fig:cal_light_yield}
\end{figure}

The on-orbit refinement and monitoring of the AMS-02 tracker alignment
described, e.g., in~\cite{ambrosi_icrc13_tkr_align} constitutes another
interesting topic of discussion germane to the main subject of this section.
As we know, by now, for magnetic spectrometers the tracker alignment is
crucial, as the high-energy rigidity resolution is hostage to the spatial
resolution in the bending plane. In the case of AMS-02 is impressive how the
instrument team managed to cope with the time-dependent displacements of the
outer tracker layers induced by temperature variation (up to hundreds of
$\mu$m on sub-hour time scales) and achieve a systematic error on the alignment
of the order of $\sim 3~\mu$m---significantly smaller than the hit resolution
of the position-sensitive detectors.


\subsection{Systematic uncertainties on the IRFs}

Measurements by modern, large-acceptance detectors are in many (possibly
most) cases systematic-limited---one has so many events at hand that the
statistical errors are just negligible over most of the instrumental phase
space. When that is the case the level of accuracy of our knowledge of the
detector performance figures (i.e., the systematic errors on the instrument
response functions) becomes, to some extent, more important than the absolute
values of the figures themselves. It is important, therefore, to devise methods
study those systematic errors in the environment where the instrument operates.


\subsubsection{Effective area and acceptance}

The effective area is typically studied and parameterized by means of detailed
Monte Carlo simulations and \emph{measured} with particle beams, when possible,
in discrete points of the phase space.

Given that there is no such thing as a source with a \emph{known} flux (this
is typically what one is trying to measure) it is not straightforward to study
the systematic errors on the effective area on orbit. There are ways around
this: one can select reasonably clean, signal-enriched event samples and
study the efficiency of any given selection criterium with respect to a
baseline; or try and measure the same thing with sub-samples of events---e.g.,
on-axis and off-axis events. We refer the reader to~\cite{2012ApJS..203....4A}
for a somewhat detailed discussion of such possible strategies in the context
of the analysis of \Fermi-LAT gamma-ray data, but we stress that this is a
difficult problem to discuss in abstract terms.


\subsubsection{Point-spread function}

The point-spread function is, to some extent, the easiest thing to
\emph{calibrate} in orbit (at least where it matters, i.e., in gamma rays),
in that bright gamma-ray point sources%
\footnote{In a sense, there is no such thing as a point source, but in
  practice the actual angular size for many gamma-ray sources is much
  (much) smaller than PSF one can realistically achieved.}
serve essentially the same purpose that a monochromatic particle beam would
serve for energy calibration: not only the measured photon directions are
distributed around the \emph{true} position according to the PSF (this is by
definition), but in many cases this true position is known%
\footnote{Technically, the position is measured at other wavelengths with
  a much greater accuracy than that achievable in gamma rays, which allows to
  calibrate not only the point-spread function, but also the absolute
  pointing accuracy in the sky.} 
from measurements at other wavelengths.

Bright pulsars are prototypical examples of sources that can be used for the
in-flight calibration of the PSF---in that case one can also take advantage the
phase information to select the calibration sample. One notable limitation is
due to the fact that most pulsars feature a spectral cutoff at $\sim$~GeV
energy, so that they cannot be readily used at very high energies.
Bright active galactic nuclei (and possibly a stack of them) are a viable
alternatives.
We refer the reader to~\cite{2012ApJS..203....4A,2013ApJ...765...54A} for a
thorough discussion of the in-flight calibration of the \Fermi-LAT PSF.


\subsubsection{Absolute energy scale}%
\label{sec:calib_energy_scale}

Unlike the point-spread function, the absolute energy scale is notoriously
difficult to calibrate in orbit due to the lack of sharp spectral features
\emph{at know energies} in the GeV-to-TeV range. As a matter of fact, the
discovery of such a feature (e.g., a line) would be a major one, but still
useless from the standpoint of the calibration of the energy scale.

Magnetic spectrometers with an electromagnetic calorimeter on board have the
advantage of measuring the energy \emph{and} the rigidity (which is to say,
they measure the energy twice and independently) for electrons and positrons
with a good resolution over a relatively wide energy range. The
energy-rigidity matching provides a good handle on the systematic uncertainties
on the absolute scales, but for purely calorimetric experiments life is
relatively harder.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cre_cutoff_fermi}
  \caption{Examples of simulated cosmic-ray electron count spectra in bins of
    McIlwain~$L$, representative of the \Fermi\ orbit---averaged over the
    \Fermi\-LAT field of view and folded with the \Fermi\-LAT energy resolution
    (adapted from~\cite{2012APh....35..346A}). The peaked shape is the result
    of the convolution of the power-law spectrum of primary cosmic-ray electrons
    with the screening effect of the geomagnetic field. The different peaks
    at different values of McIlwain~$L$, whose position can be reliably
    predicted through ray-tracing techniques, effectively provide a series of
    calibration points for the absolute energy scale.}
  \label{fig:cre_cutoff_fermi}
\end{figure}

We end this section by briefly mentioning the verification of the absolute
energy scale performed by the \Fermi-LAT~\cite{2012APh....35..346A} using the
rigidity cutoff induced by the geomagnetic field (see
section~\ref{sec:geomagnetic_cutoff}).
While the geomagnetic cutoff is a sharp feature in any given direction, it
gets smeared when averaged over the finite field of view of an instrument, as
shown in figure~\ref{fig:cre_cutoff_fermi}. Still, by selecting events
in bins of the McIlwain~$L$ variable, the convolution between the power-law
spectrum of primary cosmic-ray electrons and the shielding effect due to the
magnetic field results in narrowly-peaked spectra whose shape can be
predicted by means of ray-tracing techniques. We refer the reader
to~\cite{2012APh....35..346A} for more details about the method and the
results.
