\section{Design principles}

Not surprisingly, the question of how one should design a scientific instrument
(be it for space or not) is a tough one---and one that requires many different
inputs. The typical logical flow is to start with a precise idea of the science
targets, i.e., \emph{what we want to measure}. The science requirements
translate more or less obviously into corresponding requirements on the
detector performance, e.g., acceptance, point-spread function and energy
resolution. How to turn them into an actual instrument implementation
that fits into all the operational constraints (e.g., the weight and dimensions
that can be accommodated by the launcher and the power budget) is often the most
difficult part of the process---and one might have to iterate the process itself
several time for it to converge.

In this section we shall briefly glance through some of the basic aspects
of instrument design. (And no, even if you do read the section all the way
to its end you should not go out and try and design your own space detector
right away.)


\subsection{Sensitivity studies}

Now that we have a basic understanding of the main figures of merit describing
the instrument performance it is time to try and tie them to the actual things
that one aims at measuring.


\subsubsection{Observing time, exposure factor and exposure}

Up to now we haven't quite talked about the observing time, but it is rather
intuitive that the total number of events collected by a given instrument
is proportional to the the exposure factor
\begin{align}\label{eq:fexposure}
  \fexposure(E) = \accept(E) \times \obstime,
\end{align}
i.e., the product of the acceptance and the exposure factor (measured, e.g., in
m$^2$~sr~year). In passing, by \emph{``observing time''}, here, we really mean
the cumulative amount of time that the instrument is collecting science data.
When defining the observing time, care should be taken to account for all
the possible effects that reduce the overall duty-cycle: instrumental dead time,
time spent for calibrations and hardware problems (and, for instruments in
low-Earth orbit, the time spent in the SAA). And, since we are only concerned
with the orders of magnitude, we shall obviously neglect all that in the
following.

That all said, the exposure factor defined in~\eqref{eq:fexposure} is mainly
relevant for charged cosmic rays, whose flux is, to an good approximation,
isotropic. On the other hand gamma-ray astronomy is by its nature
\emph{dispersive}---you see a subset of the sky at a time%
\footnote{Strictly speaking this is true for charged CRs too, but in that case
  we are essentially looking at the same thing in any direction in the sky,
  while for gamma-rays we really look at different sources in different
  directions.}.
What matters is the exposure \exposure, or the \emph{integrated effective area}
in a given direction in the sky---measured in m$^2$~year.

This is where the observation strategy comes into play: depending on how large
is the field of view, aiming at a uniform exposure (i.e., scanning the sky)
vs. pointing a particular region can give very different answers.
Phrased in a different way, different (and mutually exclusive) observing
strategies potentially provide very different sensitivities for particular
science objectives, so this is really an important piece of the puzzle.

Even in \emph{sky-survey} mode, the exposure factor are the exposure are two
very different things: assuming that the observation strategy provides an
approximately uniform sky exposure on long time scales (which is the case,
e.g., for \Fermi), the two quantities are related to each other by
\begin{align}\label{eq:exposure_acc_obstime}
  \mathcal{E}(E) \sim \frac{\fexposure(E)}{4\pi} =
  \frac{\accept(E) \times \obstime}{4\pi}.
\end{align}
It follows that \emph{an overall exposure factor of 1~m$^2$~sr~year
and an exposure of 1~m$^2$~year in a given direction in the sky are
fundamentally different things: for an instrument with an acceptance of
1~m$^2$~sr it takes 1~year of observation to achieve the former and about
$12.5$~year for the latter}%
\footnote{Incidentally, when the time spent in the SAA and
the instrumental dead-time fraction are taken into account, 1~m$^2$~year
(or $3.16 \times 10^{11}$~cm$^2$~s) is representative of the average sky
exposure at high energy (i.e., above $\sim 10$~GeV) integrated by the
\Fermi-LAT in the course of the entire mission.}.


\subsubsection{Cosmic-ray and gamma-ray intensities}

We are now ready to attack some basic sensitivity studies. The first obvious
case is calculating the expected number of cosmic rays of a particular species
(e.g., protons or electrons) for a given detector acceptance and observing time.
One obvious follow-up question would be what is the maximum energy up to which
a given detector can study a given component of the cosmic radiation before
running out of statistics.

One possible way to go about this---given a model for the differential
intensity $\intensity(E)$ under study---is to calculate the integral number of
events $N_I$ above a given energy:
\begin{align}\label{eq:int_counts}
  N_I(E) = \int_{E}^{\infty}\!\!\!\!\fexposure(E') J(E') dE'.
\end{align}
(Check for yourself that all the physical units cancel out and the result is
a pure number.) Compared to the corresponding differential quantity this has the
advantage of depending on a single energy bound.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{int_limits}
  \caption{Accuracy of the integral spectra evaluation as a function of the
    upper extreme of integration, as given in equation~\ref{eq:int_limits}.}
  \label{fig:int_limits}
\end{figure}

\begin{figure*}[phtb]
  \includegraphics[width=0.49\linewidth]{cr_int_rate_protons}%
  \includegraphics[width=0.49\linewidth]{cr_int_rate_electrons}\\
  \bigskip
  \includegraphics[width=0.49\linewidth]{cr_int_rate_antiprotons}%
  \includegraphics[width=0.49\linewidth]{cr_int_rate_positrons}\\
  \bigskip
  \includegraphics[width=0.49\linewidth]{cr_int_rate_iron}%
    \includegraphics[width=0.49\linewidth]{cr_int_rate_allgamma}
  \caption{Integral number of events above a given energy for some interesting
    CR species, and for some representative values of the exposure factor.
    As a rule of thumb we can fairly say that, for each species, the energy for
    which a given curve drops below $\sim 10$ is the ultimate energy reach for
    an instrument integrating the corresponding exposure factor.
    Note that the axis ranges are all different. Also note that, for obvious 
    reasons, the spectrum extrapolations are quite uncertain for the
    positrons.}
  \label{fig:cr_projected_counts}
\end{figure*}

We note, in passing, that the upper extreme of integration in
equation~\eqref{eq:int_counts} is formally infinite, but given how steeply
cosmic-ray spectra are falling down, one hardly needs to get that far.
For a power law with index $\Gamma$ the relevant figure is the ratio
\begin{align}\label{eq:int_limits}
  r = \frac{\int_{E}^{kE}E'^{-\Gamma}dE'}{\int_{E}^{\infty}E'^{-\Gamma}dE'} =
  1 - \frac{1}{k^{\Gamma - 1}},
\end{align}
which is plotted in figure~\ref{fig:int_limits} for $\Gamma = 2.75$.
Integrating over a decade in energy is in practice enough to get us within a
few \% of the exact answer and a factor of $\sim 3$ in energy still provides an
answer accurate to some $\sim 15\%$. This implies that, in order to evaluate
the integral spectra above a given energy $E$, one needs a reliable estimation
of the corresponding differential intensities up to energy at least 3--4 times
higher.

If the exposure factor does not depend on energy, which is not a terrible
assumption for most high-energy instruments, at least under certain conditions,
the problem simplifies considerably
\begin{align}\label{eq:int_counts_constfexp}
  N_I(E) =  \fexposure \int_{E}^{\infty}\!\!\!\!J(E') dE',
\end{align}
as by integrating the differential intensity one effectively gets a 
\emph{universal} function that can be scaled by the exposure factor and plotted
for different values of \fexposure\ as shown in
figure~\ref{fig:cr_projected_counts}.

When looking at figure~\ref{fig:cr_projected_counts} the reader should be aware
that the we have used our na\"ive baseline models of cosmic-ray intensities,
and we have extrapolated them at high energy---which means that the numbers
you read off the plot cannot expected to be accurate to three significant
digits. That said, the plots allow to have a good idea of how many particles
one is expecting to detect in a given setup above a given energy---and, by
difference, in any given energy range.

The energy value for which the curve of interest in
figure~\ref{fig:cr_projected_counts} falls below, say, 10 events signal the
maximum energy reach that one might hope to reach for a given cosmic-ray
species with a given exposure factor. Depending on the situation this might
be realistic or no---e.g., a measurement with a magnetic spectrometer might be
dominated by the charge confusion well before reaching out that far. In other
words one should always keep in mind figure~\ref{fig:cr_ratios} when looking
at figure~\ref{fig:cr_projected_counts}.


%\subsubsection{Large-scale anisotropies}



\subsubsection{Gamma-ray source sensitivity}

As one might imagine, evaluating the source sensitivity for a given instrument
involves a fair number of ingredients---most notably the effective area and
point-spread function of the detector, the observation time toward a given
direction in the sky and the intensity of the isotropic and galactic diffuse
emission around the very same direction. In broad terms the detection
threshold (i.e., the minimum integral source flux above a given energy
$E_{\rm min}$ that is needed to detect the source itself at a given position and
in a given observation time $T_0$) is determined by the number of background
events---i.e., gamma rays from diffuse emission---into the solid angle
subtended by the (energy-dependent) \psf. It goes without saying that the
detection threshold depends on the position in the sky and is largest where
the galactic diffuse emission is most intense (e.g., on the galactic plane
and especially around the galactic center).

If we assume that the diffuse background is locally uniform, it is possible
to derive an approximate expression for the detection threshold. In the
following we shall outline a prescription described
in~\cite{2010ApJS..188..405A} in the context of the \Fermi-LAT first source
catalog.

The basic starting point is the maximum likelihood formalism customarily
used for gamma-ray source analysis---described, e.g.,
in~\cite{1996ApJ...461..396M}. The likelihood of observing $n_i$ counts
in the $i$-th bin (here the pixel index $i$ is running in space and energy)
where the model predicts $\lambda_i$ counts is given by the Poisson
probability
\begin{align}
  p_i = \frac{\lambda_i^{n_i} e^{-\lambda_i}}{n_i!}.
\end{align}
The full likelihood is the product of the (independent) probabilities for each
single pixels
\begin{align}
  L = \prod_i p_i = \prod_i \frac{\lambda_i^{n_i} e^{-\lambda_i}}{n_i!}
\end{align}
and the log-likelihood reads
\begin{align}
  \ln L = \sum_i \left( n_i\ln \lambda_i - \lambda_i - \ln n_i! \right).
\end{align}
The last term is irrelevant as it does not depend on the model, and the
quantity that we have to maximize is effectively
\begin{align}\label{eq:src_det_likelihood}
  \ln L = \sum_i \left( n_i\ln \lambda_i - \lambda_i \right).
\end{align}
Finally, the test statistics (roughly speaking, the square of the significance
of the detection) is given by (twice) the difference between the likelihood
with the source included in the model and the likelihood calculated under the
hypothesis of no source:
\begin{align}
  {\rm TS} = 2(\ln L^* - \ln L^0).
\end{align}

Assuming a locally uniform background we can rewrite the above expression 
in the form of an integral over the energy and the angular separation $\psi$
between the source position and the reconstructed direction. Let $S(E)$
be the spectrum of the source, $B(E)$ that of the background, $\psf(E, \psi)$
the spatial distribution of the events from a point source (at a given energy);
in addition, let us define the local source-to-background ratio
\begin{align}
  g(E, \psi) = \frac{S(E)\psf(E, \psi)}{B(E)}.
\end{align}
That all said, the probability density function for the number of events
detected will be
\begin{align}
  n*(E, \psi) & = T_0\aeff(E)\left( S(E)\psf(E,\psi) + B(E) \right) \nonumber\\
  & = T_0\aeff(E)B(E) (1 + g(E, \psi))
\end{align}
if the source is included and
\begin{align}
  n^0(E, \psi) = T_0\aeff(E)B(E) 
\end{align}
in case of no source (note that both have to be integrated in energy and
solid angle to get the actual number of events in the two hypotheses).
The corresponding \emph{differential} TS contribution per unit energy and
solid angle reads
\begin{align*}
  \frac{d{\rm TS}}{dEd\Omega} & =
  2 \left[ (n^*\ln n^* - n^*) - (n^* \ln n^0 - n^0) \right] = \\
  & = 2 \left[ n^*\ln \left(\frac{n^*}{n^0}\right) - (n^* - n^0) \right] = \\
  & = 2 T_0 \aeff(E)B(E) \times \\
  & \Big[(1 + g(E, \psi))  \ln(1 + g(E, \psi)) - g(E, \psi) \Big].
\end{align*}
Assuming a given spectral shape for $S(E)$, e.g., a power law with a given
index, and a model for the diffuse emission e.g., the GDE model that we have
introduced in section~\ref{sec:dge}%
\footnote{At high latitude you will also need a model for the isotropic
  background.},
one can integrate over the energy and the solid angle to get the actual TS and
calculate the value of the minimum required normalization of the source
spectrum---for a $5\sigma$ detection---by setting ${\rm TS} = 25$.

Complicated as it might seems, the previous expression simplifies considerably
in the background-dominated regime, i.e., when $g(E, \psi) \ll 1$ even for
$\psi = 0$%
\footnote{For \emph{typical} pair-conversion telescopes this is true at low
energies, where the \psf\ is comparatively poor and faint sources are always
background limited. On the other hand, at high energy the \psf\ can be narrow
enough that even for faint sources the detection threshold is determined by
the counting statistics.}%
. By expanding
\begin{align}
  (1 + x)\ln (1 + x) - x \sim (1 + x)\left(x - \frac{x^2}{2}\right) - x 
  \sim \frac{x^2}{2}
\end{align}
one gets the approximate relation
\begin{align}
  \frac{d{\rm TS}}{dEd\Omega} \sim
  \frac{T_0\aeff(E) S(E)^2 \psf(E, \psi)^2}{B(E)}.
\end{align}
Finally, by noting that
\begin{align}
  \int_0^\pi \psf(E, \psi) d\Omega \propto \frac{1}{\sigma(E)^2},
\end{align}
where $\sigma(E)$ is the angular resolution at a given energy $E$, one 
recognizes in the TS contribution per unit energy
\begin{align}
  \frac{d{\rm TS}}{dE} \propto
  \frac{T_0\aeff(E) S(E)^2}{B(E) \sigma(E)^2}
  \propto \frac{n_{\rm sig}^2}{n_{\rm bkg}^{\rm eff}}
\end{align}
the (square of) the number of signal events divided by the square root of the
number of background events within the solid angle subtended by the \psf\ of
the instrument.

We refer the reader to~\cite{2010ApJS..188..405A} for a more thorough
description of the procedure in the specific case of the \Fermi\ Large Area
Telescope.


%\subsubsection{Gamma-ray source localization}



\subsubsection{Search for spectral lines}

The detection of a high-energy spectral line on top of the continuum gamma-ray
emission (e.g., from the galactic center), is generally regarded as one of the
few instances of a potential observable phenomenon that would convincingly
point to the existence of new physics at work (e.g., two-body annihilation of
dark matter into photons). The topic has recently received a lot of attention
due to the claim~\cite{2012JCAP...07..054B,2012JCAP...08..007W} of a line-like
feature around 130~GeV in the publicly available \Fermi\ data.

From the standpoint of what we are interested in, the general subject of
the line search provides a good example to underline the interplay between
the acceptance and the energy resolution in this specific analysis.
In broad terms, the basic figure of merit $Q$ for such a line search is
\begin{align}
  Q = \frac{n_s}{\sqrt{n_b}},
\end{align}
where $n_s$ is the number of signal events and $n_b$ is the \emph{effective}
background, i.e. the number of background events integrated over the signal
p.d.f. At a fixed signal flux and detector acceptance, both the signal and
the background scale linearly with the exposure factor \fexposure, but the
width of the window over which the background is integrated is proportional to
the energy resolution, which implies that
\begin{align}\label{eq:line_sensitivity}
  Q \propto \sqrt{\frac{\fexposure}{\eres}}.
\end{align}
While a good energy resolution is surely desirable for this particular search,
equation~\eqref{eq:line_sensitivity} implies that the sensitivity increases
with a narrower energy dispersion only if one is not trading too much acceptance
for that.


\subsection{Operational constraints}

Delving into the details of the instrument design principles from the standpoint
of the operational constraints is obviously beyond the scope of the
write-up. Nonetheless we shall glance through some of the most obvious
aspects of the problem, with emphasis on space-based detectors.


\subsubsection{Weight, power and all that}

The first obvious constraints imposed by the choice of the launcher are on
the overall dimensions and weight of the instrument. The latter impacts
primarily the layout of the calorimeter, which typically accounts for a
significant fraction of the mass in most popular instrument designs.
In broad terms, once the calorimeter weight is fixed, one has the face a trade
off between the acceptance and the energy resolution---i.e., decide the
optimal compromise between the cross-sectional active surface and the depth.
It should be emphasized that for magnetic spectrometers the magnet is also
a fundamental contributor to the mass budget.

Power in space generally comes from solar panels---and is not unlimited.
Typical power budgets for space- and balloon-borne detectors are within
500--1000~W---rarely much more than that. In terms of detector implementation,
the limited power primarily impacts the tracking stage, which is typically the
one featuring the highest level of segmentation (as we mentioned in
section~\ref{sec:silicon_detectors}, silicon trackers with $10^5$--$10^6$
independent electronic channels are nowadays commonly operated in space).

The bandwidth for data down-link is expensive, too---pretty much as anything
else in space---with typical average figures ranging in the Mb/s. This has
profound implication on the overall data flow, requiring in general some
combination of zero suppression, event filtering and data compression (all
happening on board).


\subsubsection{Launch and space environment}

In contrast to ground-based detectors (e.g., experiments at accelerators),
space-based instruments must be designed, assembled and tested as to ensure
successful launch and on-orbit operation. The \emph{environmental}
verification process typically includes mechanical tests (both static and
dynamic), thermal tests (thermal and/or thermo-vacuum cycling) and
electromagnetic emission and susceptibility tests. While this general topic is
not trivial to discuss without reference to a specific instrument, we shall
briefly introduce some of the basic aspects of the problem. The reader is
referred, e.g., to~\cite{2008NIMPA.584..358B} for a thorough discussion of the 
environmental tests of the \Fermi-LAT tracker towers.

Vibration tests are aimed at verifying that the detector can sustain the
low-frequency dynamic load in the launch environment---i.e., vibrations induced
by the bearing structures (the satellite and the launcher) at the natural
frequencies of the latter (usually below 50~Hz). The \emph{transfer function}
of the instrument assembly (or any of its sub-parts) is typically measured by
means of a combination of a shaker and a series of accelerometers, that allow
to measure both the resonance frequencies and the $Q$ factors of the natural
modes (and verify that they do not change significantly in response to a
realistic simulation of the launch environment, which might indicate some kind
of structural damage).

In the space environment heat is exchanged between the detector and the ambient
essentially by \emph{radiation} (it goes without saying that convection plays
no role in vacuum, which means that you can't really use fans to dissipate
heat as you would do on the ground). Space-based detectors feature more or less
sophisticated thermal systems to ensure that the power generated by the
on-board electronics is efficiently radiated in the environment and to
mitigate the thermal gradients induced by changes of the orientation with
respect to the Sun (and Sun occultation by the Earth). Thermal and thermo-vacuum
tests serve the twofold purpose of validating the thermal model at the base
of the thermal control system and verifying that the detector is able to
operate (or survive) withing the maximum temperature excursion range expected
in orbit.

We note, in passing, that operating an instrument in vacuum is not as trivial
as one might expect, as in many designs composite structures (i.e.,
aggregates of several layers of different materials) are used in order to
maximize the mechanical stiffness within the mass budget---and the details
of manufacturing processes become critical.

There are many more considerations---connected with the environment---that
go into the design of a space detector (e.g., electromagnetic interference
effect and radiation damage) but we shall just pause here and move forward
to a different subject.


\subsection{Design trade-offs}

Trade-offs arise naturally when designing an instrument, as---even pretending
for a second that the operational constraints do not exist---it is impossible
to optimize all the performance metrics at the same time and it is necessary
to do compromises aimed at maximizing the overall sensitivity for a given
science target. In addition to that, different science targets may require
different (and often conflicting) optimizations, which makes the entire process
even more difficult.

In this section we shall elaborate on the concept of trade-off by looking
at a couple of (largely) academic examples.


\subsubsection{A case study: the tracking stage for a pair-conversion telescope}

Let's pretend for a second that your favorite space agency contacted you to
design the tracking stage for a pair-conversion telescope. The rules are: you
have a cubic space, with side $L = 1$~m, to fill up (completely) with
$n$ equally-spaced aggregates, each including a double-sided silicon-strip
detection plane with strip pitch $p$, immediately followed by a high-Z
converter with thickness $t$. (In other words, you have 1~m$^3$ available and
you get to pick your favorite choices for $n$, $p$ and $t$%
\footnote{This is a pretty bogus offer, as in real life you would presumably
  have control over the height of the tracker, as well, which in this case is
  fixed to $L$.}.) Last but not least, the agency is offering you a
non-negotiable power budget of 100~W.

There's a few performance metrics that we can calculate right away. Assuming
a 100\% detection and selection efficiency, for instance, the peak (i.e.,
high-energy) on-axis effective area can be estimated as
\begin{align}
  \aeffnorm = L^2 [1 - \exp(-nt)] = 1 - \exp(-nt)~{\rm m}^2.
\end{align}
In addition, following section~\ref{sec:lat_psf}, we can write the high-energy
68\% containment angle of the \psf\ as
\begin{align}
  \psf_{\rm HE} = \frac{\sqrt{2}p}{d(n/2)^{3/2}} = \frac{4p}{Ln^{1/2}} = 
  \frac{p~[\mu{\rm m}]}{250n^{1/2}}~{\rm mrad}
\end{align}
(note we are taking $n/2$ as the average number of planes crossed).
The corresponding \psf\ at 100~MeV reads
\begin{align}
  \psf_{100} = \sqrt{2} \frac{13.6}{50} \sqrt{t} = 385 \sqrt{t}~{\rm mrad}
\end{align}
(note we have neglected the extra logarithmic term in
equation~\eqref{eq:theta_ms}).

That all said, with three free parameters it is not trivial to imagine how one
would go about the optimization. One thing that we can easily calculate is the
overall number of channels%
\footnote{We note, in passing, that assembling and reading out 1~m long strips
  is not trivial (in reality one would probably implement this detector concept
  as an array of at least $2\times 2$ modules), but we shall happily neglect
  this, for the time being.}:
\begin{align}
  \text{\# channels} = \frac{2nL}{p}
\end{align}
(the factor 2 at the numerator accounts for the fact that, for each plane, we
have strips in both directions---i.e., the silicon detectors are double sided).
If we assume a power consumption of 250~$\mu$W per channel, the total necessary
power $P$ reads
\begin{align}
  P = \frac{500 n}{p~[\mu{\rm m}]}~W,
\end{align}
i.e., with 100~W available you can do, say, 10 layers at 50~$\mu$m pitch or
100 layers at 500~$\mu$m pitch (see the right axis in
figure~\ref{fig:telescope_optimization}). Good: this introduces a constraint
that our design parameters must satisfy
\begin{align}
  p = 5n~\mu{\rm m},
\end{align}
and effectively reduces the dimensionality of the problem from 3 to 2.
In fact we can now rewrite the high-energy \psf\ as
\begin{align}
  \psf_{\rm HE} = \frac{n^{1/2}}{50}~{\rm mrad}
\end{align}
(if you are surprised seeing that the \psf\ is worsening as one adds more
layers, remember that the power budget is forcing us to make the strip
pitch larger).

Figure~\ref{fig:telescope_optimization} summarizes this trade-off study in
the $n$--$t$ plane. In our setup the low-energy \psf\ depends only on the
thickness of the converters and the high-energy \psf\ depends only on the
number of layers (which in turn, at fixed total power, determines the strip
pitch). Hyper-surfaces at constant on-axis high-energy effective area are
represented by the dashed curves.

\begin{figure}[htb]
  \includegraphics[width=\linewidth]{telescope_optimization}
  \caption{Iso-performance curves in the $n$--$t$ phase space for our
  benchmark pair conversion telescope. The lines at constant $\psf_{100}$ are
  vertical, while those at constant $\psf_{\rm HE}$ are horizontal. The curves
  are iso-\aeff\ lines.}
  \label{fig:telescope_optimization}
\end{figure}

There's a few interesting remarks that we can do about
figure~\ref{fig:telescope_optimization}. Obviously one would like to choose
a small value of $t$ to optimize the \psf\ at low energy. In addition, in
our setup it is advantageous to limit the number of detection planes and
take advantage of the smaller strip pitch to optimize the high-energy \psf.
By choosing the bottom-left corner of the phase space one effectively
optimizes the angular resolution over the entire energy range. Unfortunately
this is also the region where the effective area is relatively low
(no wonder: you have a small number of thin conversion layers). In contrast,
increasing the effective area requires trading off some angular resolution,
at either low or high energies---or both.

There is several things that we left off, including cost considerations and
other performance metrics such as the field of view. Nonetheless, fictional
as it is, this exercise does illustrates some of the basic aspects of
instrument design.



