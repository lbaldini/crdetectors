#! /bin/env python

import os
import glob
import re

def exe(cmd):
    """
    """
    print('About to execute "%s"...' % cmd)
    os.system(cmd)
    print('Done.')


exe('rm -rf arxiv/*')
exe('rm -rf arxiv.tar.gz')
exe('mkdir arxiv')
exe('make')
exe('cp *.bib *.bbl *.tex Makefile arxiv')
exe('mkdir arxiv/figures')
for filePath in glob.glob('*.tex'):
    for line in open(filePath):
        if '\\includegraphics' in line:
            pdfBaseName = re.search('(?<={).*(?=})', line).group(0)
            pdfFilePath = 'figures/%s.pdf' % pdfBaseName
            exe('cp %s arxiv/figures' % pdfFilePath)
exe('cp -r tables arxiv')
exe('tar -zcvf crdetectors.tar.gz arxiv/*')
exe('rm -rf arxiv')
exe('make cleanall')
