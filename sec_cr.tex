\section{Galactic cosmic rays}%
\label{sec:cr}

In order to give some context for the following discussion, in this section we
briefly summarize some of the basic facts about galactic cosmic rays.


\subsection{The spectrum of galactic cosmic rays}%
\label{sec:gcr_spectra}

The energy spectrum of individual CR species has now been measured by
space- and balloon-borne detectors over some~7 decades in energy---at least
for the more abundant ones---and is largely dominated by protons, as
shown in figure~\ref{fig:cr_spectra_single_charge}.
Among the other singly-charged species, electrons amount to some
$10^{-2}$--$10^{-3}$ (depending on the energy) of the proton flux, and
positrons and antiprotons are even less abundant, the latter being
some $10^{-4}$ of the proton flux. We shall come back to these numbers in
section~\ref{sec:event_selection} when discussing the challenges one has to
face in separating the different species.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_spectra_single_charge}
  \caption{Spectra of the singly-charged components of the cosmic radiation.
    The data points are taken from~\cite{CRDB} and correspond to
    references~\cite{2009BRASP..73..564P, 2011ApJ...728..122Y, 2011Sci...332...69A, 2008Natur.456..362C, 2008PhRvL.101z1104A, 2010PhRvD..82i2004A, 2011PhRvL.106t1101A, 1996A&A...307..981R, 2012PhRvL.108a1103A, 1998ApJ...498..779B, 2013JETPL..96..621A}. For each species, the dashed line represents the weighted
  average of all the recent available measurements, and will be used in the
  following as the baseline for sensitivity estimates. For completeness,
  the model for the positron spectrum has been obtained by combining the
  $(e^{+} + e^{-})$ spectrum with the positron fraction measured by
  AMS-02~\cite{2013PhRvL.110n1102A}.}
  \label{fig:cr_spectra_single_charge}
\end{figure}

As it turns out, cosmic rays include all sort of nuclei. Helium nuclei,
amounting to some $10\%$ of the protons, constitute the second more abundant
component, and carbon and oxygen are also relatively abundant, as shown in
figure~\ref{fig:cr_spectra_metals}.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_spectra_metals}
  \caption{Spectra of some of the more abundant cosmic-ray species with $z > 1$,
    compared with the proton spectrum shown in
    figure~\ref{fig:cr_spectra_single_charge}.
    The data points are taken from~\cite{CRDB} and correspond to
    references~\cite{2009BRASP..73..564P, 2011ApJ...728..122Y, 2011Sci...332...69A, 2009ApJ...707..593A, 2011ApJ...742...14O, 1994ApJ...429..736B, 1980ApJ...239..712S, 1990A&A...233...96E, 2008ApJ...678..262A, 2005ApJ...628L..41D, 1981ApJ...248..847M, 1981ApJ...246.1014Y}.
    For each species, the dashed line represents the weighted
    average of all the recent available measurements, and will be used in the
    following as the baseline for sensitivity estimates.}
  \label{fig:cr_spectra_metals}
\end{figure}

For completeness, the dashed lines in
figures~\ref{fig:cr_spectra_single_charge} and \ref{fig:cr_spectra_metals}
represent weighted averages of all the recent available measurements for
each CR species, and we shall use them in the rest of this review for
sensitivity estimates. We shall be fairly liberal, within reason, in terms of
extrapolating differential and integral spectra at energies where there are
not yet measurements available.


\begin{table}[htb!]
  \begin{tabular}{p{0.3\linewidth}p{0.3\linewidth}p{0.3\linewidth}}
    \hline
    Z & Element & Relative\\
    & & abundance\\
    \hline
    \hline
    1 & H & 540 \\
    2 & He & 26\\
    3--5 & Li--Be & 0.4\\
    6--8 & C--O & 2.20\\
    9--10 & F--Ne & 0.3\\
    11--12 & Na--Mg & 0.22\\
    13--14 & Al--Si & 0.19\\
    15--16 & P--S & 0.03\\
    17--18 & Cl--Ar & 0.01\\
    19--20 & K--Ca & 0.02\\
    21--25 & Sc--Mn & 0.05\\
    26--28 & Fe--Ni & 0.12\\
    \hline
  \end{tabular}
  \caption{Relative abundances of several groups of cosmic-ray species at
    $10.6$~\Eknunits\ (adapted from~\cite{PDG}), normalized to that of oxygen
    ($3.29 \times 10^{-2}$~\intensityunitsn.}
  \label{tab:cr_abundances}
\end{table}


%\subsection{Cosmic-ray anisotropies}


\subsection{The cosmic-ray gamma-ray connection}

Though it is not very common to see cosmic-ray and gamma-ray differential
intensities overlaid on the same plot, cosmic rays and gamma rays are tightly
tied to each other.
The vast majority of celestial gamma rays in the GeV energy range are produced
by interactions of cosmic rays with the interstellar medium and with galactic
magnetic and radiation fields. The study of this galactic \emph{diffuse}
emission provides a prospective on the diffusion of cosmic rays in the
galaxy complementary to direct measurements---as a matter of fact, it is
the realization that cosmic-ray interactions were bound to produce gamma rays
that provided one of the earliest stimuli to the development of gamma-ray
astronomy.
On a slightly different note, since gamma rays do point back to their sources,
they provide a direct view on the likely sources of cosmic rays---supernova
remnnants (SNR).
Finally, as we shall see in the following, the two fields of investigation
share much in terms of experimental techniques.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_spectra_gamma}
  \caption{Energy spectrum of the all-sky gamma-ray intensity and of
    the extra-galactic gamma-ray background~\cite{2010PhRvL.104j1101A},
    compared with the most and least abundant singly charged CR species
    (i.e., protons and antiprotons). Cosmic-ray data are taken from~\cite{CRDB}
    and correspond to references~\cite{2009BRASP..73..564P, 2011ApJ...728..122Y, 2011Sci...332...69A, 2009ApJ...707..593A, 2011ApJ...742...14O, 1994ApJ...429..736B, 1980ApJ...239..712S, 1990A&A...233...96E, 2008ApJ...678..262A, 2005ApJ...628L..41D, 1981ApJ...248..847M, 1981ApJ...246.1014Y, 2013JETPL..96..621A}.}
  \label{fig:cr_spectra_gamma}
\end{figure}

Figure~\ref{fig:cr_spectra_gamma} shows the all-sky gamma-ray differential
intensity measured by the \Fermi-LAT, and makes it fairly obvious that celestial
gamma-rays constitute a tiny fraction of the cosmic radiation. Above $1$~GeV
the entire gamma-ray sky intensity is more than an order of magnitude weaker
than that of the rarest singly-charged species of the cosmic
radiation---antiprotons---and five to six orders of magnitude less abundant
than that of cosmic-ray protons.
It goes without saying that the relative paucity of gamma-ray fluxes
exacerbates the need for large instruments relative to charged species in the
same energy range.

The difficulties in separating gamma rays out of the bulk of the charged
cosmic-ray component are somewhat mitigated by the fact that efficient
anticoincidence detectors can be realized to distinguish between neutral and
charged particles and, at least for the analysis of gamma-ray sources,
spatial---and sometimes temporal---signatures can be exploited. Nonetheless
the measurement of the faint isotropic gamma-ray background may require
a proton rejection factor as high as $10^6$. We shall discuss this in somewhat
more details in section~\ref{sec:event_selection}.


\subsection{The gamma-ray sky}%
\label{sec:gamma_ray_sky}

In broad terms, the gamma-ray sky can be roughly subdivided in three main
components: the galactic diffuse emission (DGE), point and extended sources,
and the isotropic gamma-ray background (IGRB), sometimes referred as the
extra-galactic background. In the next three subsections we shall briefly
introduce these basic components.


\subsubsection{Galactic diffuse gamma-ray emission}%
\label{sec:dge}

As mentioned in the previous section, the vast majority of celestial gamma rays
above 1~GeV are produced by the interaction of charged cosmic rays with the
interstellar gas and radiation fields, resulting in a structured diffuse
emission, brighter along the galactic plane---and especially toward the
galactic center. The general subjects of characterizing and modeling the
galactic diffuse gamma-ray emission are way beyond the scope of this write-up
and we refer the reader to~\cite{2012ApJ...750....3A} and references therein
for an in-depth description of the state of the art.

In this section we limit ourselves to introduce the model of the galactic
diffuse emission that the \Fermi-LAT collaboration makes available
as one of the analysis components for point-source analysis of public
\Fermi-LAT data (as we shall use it in the following for sensitivity studies)%
\footnote{The model is publicly available at
  \url{http://fermi.gsfc.nasa.gov/ssc/data/access/lat/BackgroundModels.html}
  in the form of a fits file containing intensity maps binned in galactic
  coordinates in 30 energy slices from $\sim 60$~MeV to $\sim 500$~GeV. For
  completeness, the model used here is that named \texttt{gll\_iem\_v05.fit},
  though it is worth emphasizing that, from the standpoint of basic sensitivity
  studies such as the ones we are concerned about, the differences between
  different releases of the LAT diffuse model are hardly relevant.}.

Figure~\ref{fig:gde_model} shows an example of the spatial morphology of the
\Fermi-LAT diffuse model in the energy slice centered at $\sim 12$~GeV%
\footnote{For completeness, the model has been re-binned from the original
$0.125^\circ$ grid into a coarser $2^\circ$ grid.}. The prominent emission from
the galactic plane, brightest toward the galactic center, is clearly visible.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{gde_model}
  \caption{Map, in galactic coordinates, of the differential intensity of the
    LAT \texttt{gll\_iem\_v05.fit} model of the galactic diffuse emission in
    the energy slice centered at $\sim 12$~GeV.}
  \label{fig:gde_model}
\end{figure}

Figure~\ref{fig:gde_spectrum} shows the differential intensity for the
$2^\circ \times 2^\circ$ square around the galactic center (the GDE spectrum
in other part of our Galaxy, normalization aside, is fairly similar.)

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{gde_spectrum}
  \caption{Differential intensity of the galactic diffuse emission around the
    galactic center for LAT \texttt{gll\_iem\_v05.fit} model.
    (Note that, this being a \emph{model}---though tuned to the LAT data---it
    comes with no error bars.)}
  \label{fig:gde_spectrum}
\end{figure}


\subsection{Gamma-ray point sources}

Based on two years of sky survey, the second \Fermi-LAT source catalog
(2FGL~\cite{2012ApJS..199...31N}) is the deepest ever gamma-ray catalog
between 100~MeV and 100~GeV. It contains positional, variability and spectral
information for 1873 sources detected by the LAT in this energy range.

We refer the reader to the fellow LAT second pulsar
catalog~\cite{2013ApJS..208...17A}, the second catalog of active galactic
nuclei~\cite{2011ApJ...743..171A} and first catalog of sources above
10~GeV~\cite{2013ApJS..209...34A} for more information about specific source
populations.


\subsection{Gamma-ray isotropic background}

What remains after the galactic diffuse emission and the point and extended
sources have been subtracted from the gamma-ray all-sky intensity is generally
referred to as isotropic gamma-ray background,
or IGRB~\cite{2010PhRvL.104j1101A}. The IGRB is an observation-dependent
quantity, as its intensity depends on how many sources are resolved in a give
survey---and populations of faint sources below the detection threshold are
guaranteed to contribute to it. The sum of the IGRB and the extra-galactic
sources is, in some sense, a more fundamental quantity which is generally
referred to as extra-galactic background (EGB).

From the experimental point of view, the measurement of the IGRB is
challenging in that one has to discriminate an isotropic flux of gamma rays
against the much brighter charged-particle foreground without any spatial
or temporal signature to be exploited. We shall briefly come back to this
in section~\ref{sec:event_selection}.


\subsection{State of the art: modeling and measurements}

Beautiful as they are, figures~\ref{fig:cr_spectra_single_charge},
\ref{fig:cr_spectra_metals}, \ref{fig:cr_spectra_gamma}, \ref{fig:gde_model}
and \ref{fig:gde_spectrum} provide some of the most striking evidence for the
tremendous body of knowledge about cosmic and gamma rays accumulated over the
last century. On the theoretical side the progress has been no less
spectacular, and we refer the reader to~\cite{2013A&ARv..21...70B} for an
impressive, up-to-date and comprehensive review of the basic theoretical ideas
at the basis of the so-called supernova paradigm for the origin of galactic
cosmic rays.

In fact the discussion in the previous sections might very well give the
reader the warm and fuzzy feeling that there is little left to discover, while
nothing is farthest from truth. The aforementioned
review~\cite{2013A&ARv..21...70B} makes a honest effort at highlighting the
most important loose ends in our understanding of cosmic-ray production and
propagation, and \cite{2012ApJ...750....3A} provides a good summary of the
difficulties involved in a self-consistent modeling of the galactic diffuse
gamma-ray emission and the propagation of cosmic rays.

More importantly, at least from the prospective of this review, there are
substantial pieces of observational evidence that are, to date, either missing
or controversial. The feature in cosmic ray electron spectrum reported by the
ATIC and BETS experiments in 2008, while not confirmed, at the time of writing,
by at least three experiments (\Fermi, PaMeLa and H.E.S.S) is a good example of
an unexpected finding that has stirred the interest of the community
triggering literally hundreds of follow-up papers (see
figure~\ref{fig:cr_allelectron_spectrum}).

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_allelectron_spectrum}
  \caption{Compilation of some of the most recent measurements of the
    high-energy cosmic-ray $e^+ + e^-$ spectra. Data points are from~\cite{2010PhRvD..82i2004A, 2008PhRvL.101z1104A, 2009A&A...508..561A, 2008Natur.456..362C, 2008AdSpR..42.1670Y, 2011PhRvL.106t1101A}.}
  \label{fig:cr_allelectron_spectrum}
\end{figure}

More recently, the PaMeLa experiment has reported~\cite{2011Sci...332...69A} an
abrupt change of slope around $\sim 230$~GV in the proton and helium spectra,
whose extrapolation seem to nicely match the measurements at higher energies by
CREAM~\cite{2011ApJ...728..122Y} and ATIC~\cite{2009BRASP..73..564P}.
This tantalizing piece of evidence, if confirmed, would be of tremendous
interest, and is one of the notable examples of direct measurements where the
community is holding its breath for the results from the AMS-02 experiment
operating on the space station%
\footnote{Preliminary results from the AMS-02 experiment do not confirm
  the break reported by PaMeLa, but the issue is important enough that waiting
  for a refereed publication is in order before commenting further.}.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_proton_spectrum}
  \caption{Compilation of the measurements of the high-energy proton spectrum
    by the PaMeLa~\cite{2011Sci...332...69A}, CREAM~\cite{2011ApJ...728..122Y}
    and ATIC~\cite{2009BRASP..73..564P} experiments.}
  \label{fig:cr_proton_spectrum}
\end{figure}
