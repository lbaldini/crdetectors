MANUSCRIPT = crdetectors

all:
	make pdflatex

dvi:
	latex $(MANUSCRIPT)

bib:
	bibtex $(MANUSCRIPT)

ps:
	make dvi
	dvips $(MANUSCRIPT).dvi

pdf:
	make dvi
	make bib
	make dvi
	make bib
	make dvi
	make dvi
	dvipdf $(MANUSCRIPT).dvi

pdflatex:
	pdflatex $(MANUSCRIPT)
	make bib
	pdflatex $(MANUSCRIPT)
	pdflatex $(MANUSCRIPT)

clean:
	rm -f *.toc *.log *.dvi *aux *.bbl *.blg *.end *~ *.out

cleanall:
	make clean
	rm -f *.ps *.pdf
	cd macro; make cleanall
