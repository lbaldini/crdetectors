\section{Prelude}%
\label{sec:prelude}

This write-up was prepared for the ISAPP School \emph{``Multi-wavelength and 
  multi-messenger investigation of the visible and dark Universe} held in
Belgirate between 21 and 30 July 2014%
\footnote{The program of the school is available at
\url{https://agenda.infn.it/conferenceDisplay.py?confId=6968}.}.
As such the reader should not expect it to be accurate and/or terse as an
actual scientific publication. It is largely idiosyncratic and guaranteed to
contain errors, instead.

That said, this review might be expanded and/or improved in the future,
depending on whether people will find it useful or not. If you find a mistake
or you have suggestions, do not hesitate to inform the author---though you
should not expect him to be working on this project with a significant duty
cycle, nor necessarily proactive. (Note, however, that I am committed to fix
right away any instance where the work of individuals or research groups has
arguably been misrepresented.)


\subsection{Overview}

The main focus of this review is introducing the basic instrument performance
metrics, with emphasis on space-born%
\footnote{Among space detectors we shall focus exclusively on those in low-Earth
  orbit, i.e., orbiting around the Earth at an altitude of a few hundred~km.}
and balloon-borne cosmic-ray and gamma-ray detectors. While, in general,
instrument response functions can only be predicted accurately by means of
detailed Monte Carlo simulations, the main features can be typically understood
(say within a factor of two) by means of simple, back-of-envelope calculations
based on the micro-physics of the particle interaction in the detector. Such an
approach is effective in highlighting the connection between the underlying
design choices and the actual instrument sensitivity.

To give some context to the discussion, the current body of knowledge on
cosmic and gamma rays is briefly reviewed in historical prospective, and the
basics of interactions between radiation and matter, along with the main
characteristics of the near-Earth environment, are concisely covered.
A honest attempt has been made to provide a complete and up-to date list of
references to the relevant sources in the literature.

The organization of the material is not necessarily the most natural one, based
on the consideration that, if we were to write it down in strict logical order,
deriving each fact from the prime principles, it is unlikely that the average
reader would get past page 2.
Instead, we frequently switch back and forth and use concepts that, strictly
speaking, are only introduced later in the flow of the discussion. We put some
effort, though, in trying to provide explicit cross references between the
different bits in a sensible way.

In the attempt to make it useful and---more importantly---\emph{usable}, all the
material contained in this review is copylefted under the GPL General Public
License and available at
\url{https://bitbucket.org/lbaldini/crdetectors}.
This includes: the \LaTeX\ source code, the tables, high-resolution versions of
all the figures and the code (along with the necessary auxiliary data files)
used to generate them.


\section{Introduction}%
\label{sec:intro}

The discovery of cosmic rays (CR) is customarily attributed to Viktor Franz
Hess, who first demonstrated in a series of pioneering balloon flights performed
in 1911 and 1912 that the ionizing radiation in the atmosphere of the Earth was
of extraterrestrial origin%
\footnote{Sure enough putting an electroscope on a balloon is more germane
  to the subject of this review than immersing it under water. Nonetheless
  it is appropriate to mention here the (largely uncredited) work of Domenico
  Pacini around the same time. The reader is referred
  to~\cite{2011arXiv1103.4392D} and references therein for a historical account
  of the early developments of cosmic-ray studies.}.
More than 100~years after their discoveries, cosmic rays have been extensively
studied, both with space-based detectors and with ground observatories.
For reference, the cosmic-ray database described in~\cite{CRDB}, which we have
extensively used in the preparation of this review, contains some $\sim 1100$
data sets from $\sim 40$ different experiments at energies below
$\sim 10^{15}$~eV.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{cr_big_picture}
  \caption{All-particle energy spectrum of primary cosmic rays, compiled from
    some of the most recent available measurements. The dashed line extending
    from $\sim 10^{9}$ to $\sim 10^{14}$~eV has been obtained by summing up
    the energy spectra of the most abundant CR species (p, He, C, O and Fe),
    measured by several different space- and balloon-borne experiments.
    The vertical dashed line marks the maximum energy ($\sim 1$~PeV) that can
    be practically measured by a CR detector in orbit.}
  \label{fig:cr_big_picture}
\end{figure}

Figure~\ref{fig:cr_big_picture} provides a largely incomplete, and yet
impressive, account of the tremendous body of knowledge that we have
accumulated along the way. The dashed line extending from $\sim 10^{9}$ to
$\sim 10^{14}$~eV is a weighted sum of the energy spectra of the most abundant
CR species (p, He, C, O and Fe), as measured by several different space- and
balloon-borne experiments. The reader is referred to~\cite{PDG} and references
therein for a more comprehensive compilation of the available all-particle
ultra-high energy cosmic-ray (UHECR) spectral measurements.

The immediate overall picture is that of a seemingly featureless power law
extending over some $12$~orders of magnitude in energy and $33$ orders of
magnitude in flux. As a matter of fact, cosmic-ray spectra are customarily
weighted by some power of the energy (typically ranging from $2$ to $3$) in
order to make relatively subtle intrinsic features more prominent---which is
a fairly sensible thing to do, as those features are connected with the
underlying physics. If we did so in this case, we would see the so-called
\emph{knee} emerging at a few $10^{15}$~eV, a second knee at $\sim 10^{18}$~eV,
the \emph{ankle} even higher energies and a spectral steepening, possibly
to be interpreted as the GZK cutoff, around $10^{20}$~eV.

Here we shall take a slightly different perspective and start by noting that
$10^{16}$~eV is roughly the energy where the integral all-particle cosmic-ray
spectrum amounts to $\sim 1$~m$^{-2}$~sr$^{-1}$~year$^{-1}$. If we forget for a
second the steradians (we shall see in section~\ref{sec:planar_detector}
that this essentially gives an extra factor of $\pi$, which is largely
irrelevant for the purpose of our argument) this means that
\emph{far away from the Earth there is about $1$ particle per year above
  $10^{16}$~eV crossing  any $1$~m$^2$ plane surface}.
Now, $1$~m$^2$ is a large figure by any space-experiment standard---and
$1$~year is not short either. This basic consideration naturally set the
PeV scale as the upper limit to the energies than can be practically studied
in space. It is fortunate that, though the atmosphere of the Earth is opaque
to primary particles above $\sim 10$~eV, ultra-high-energy cosmic rays
can be effectively studied from the ground through a variety of
experimental techniques involving the detection of their secondary
products---most notably the water-\cheren\ and florescence techniques, both
exploited by the Pierre Auger Observatory~\cite{2004NIMPA.523...50A},
and the extensive air shower arrays \emph{a la}
KASKADE~\cite{2003NIMPA.513..490A} or ARGO-YBJ~\cite{2012NIMPA.661S..50A}.
This is also true for high-energy gamma rays (above $\sim 100$~GeV), with at
least three major advanced imaging \cheren\ telescopes operating at the
time of writing (H.E.S.S., MAGIC, VERITAS) and the HAWC water-\cheren\
gamma-ray observatory currently under construction.
We note, in passing, that the dichotomy between space-borne (or balloon-borne)
and ground-based experiments has an important implication in that, while the
former provide measurements of separate cosmic-ray species, for the latter it
is much harder to infer the chemical composition. Important as it is, the
synergy between ground- and space-base observatories will not be discussed
further in this write-up.

At the other extreme of the energy spectrum (say below a few~GeV), primary
cosmic rays are heavily influenced by the heliospheric environment and
reprocessed by the atmosphere and the magnetic field of the Earth. While the
study of very low-energy primaries and of the geomagnetically trapped radiation
is by no means less interesting than that of their higher-energy fellows, an
informed discussion would require a large additional body of background
information and is therefore outside the scope of the paper.

In broad terms, this review is focused on the seven energy decades between 
$\sim 100$~MeV and $1$~PeV---and particularly on the experimental techniques
that have been (and are being) exploited to study cosmic rays and gamma rays in
this energy range. The subject is well defined and homogeneous enough---both
in terms of science topics and as far as experimental techniques are
concerned---that it can be usefully discussed in a unified fashion.
