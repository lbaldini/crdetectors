\section{Historical overview}%
\label{sec:history}

As we mentioned in the previous section, the discovery of cosmic rays is
customarily credited to Victor Hess for his balloon flights in the summer of
1912~\cite{HessDiscovery}. In fact, around the same time, several different
scientists were carrying out investigations on the penetrating radiation with
Wulf electroscopes, including Pacini~\cite{Pacini}, Gockel and Wulf himself.
By 1915 such instruments had been flown on balloons up to more than 8000~m,
measuring a level of radiation much larger than that recorded by Hess in his
first flight. The evidence for the extraterrestrial origin of the radiation was
compelling.

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{hess_electroscope}
  \caption{Original measurements from one of the ascents performed by
    Viktor Hess (adapted from~\cite{HessDiscovery}). The rate of discharge
    increasing with altitude implies that the ionizing radiation responsible
    for it is coming from the outer space. (On an un-related note, no plot is
    included in the original article, which is by no means less beautiful.)}
  \label{fig:hess_electroscope}
\end{figure}


\subsection{The early days}%
\label{sec:history_early}

A vibrant and enlightening (though not necessarily unbiased) account of the
first 50~years of research on cosmic rays is given by Bruno Rossi~\cite{Rossi}.
Interestingly enough, the question of the \emph{nature} of the cosmic radiation
did not get much attention until the end of the 1920s. The most striking
known feature of cosmic rays was their high penetrating power and, for the 
first 15 years after their discovery, scientists implicitly assumed that
they were gamma rays---the most penetrating radiation known at the time%
\footnote{While the term \emph{comic rays} was apparently coined by Millikan in
  the the 1920s, prior to 1930 the penetrating radiation was customarily
  referred to as \emph{ultragammastrahlung}, or ultra-gamma radiation, in the
  German literature.}.
This points to one of the most prominent difficulties that had to be faced
in the early studies of the cosmic radiation: the fact that little or nothing
was known about the physical interaction processes experienced by high-energy
photons and charged particles. The first satisfactory theory of
electromagnetic showers (see section~\ref{sec:em_showers}), due to Bethe and
Heitler, was published in 1934~\cite{1934RSPSA.146...83B}; before that, people
could only assume that high-energy gamma rays only interacted with matter via
Compton scattering, whose cross section has been shown to decrease with energy
by Dirac~\cite{1926RSPSA.111..405D} and Klein and
Nishina~\cite{1929ZPhy...52..853K}.

Robert Millikan formulated the first complete theory of comic rays, based on
all the measurements of attenuation in the atmosphere and in water available at
the time~\cite{1928SciAm.139..136M}. He proposed that the primary cosmic
radiation was composed of gamma rays of well-defined energies, produced in the
interstellar space by the the fusion of hydrogen atoms in heavier
elements---and that the charged particles observed in the Earth atmosphere were
electrons produced via Compton scattering.
Though we know, a posteriori, that this idea did not pass the test of time,
the original papers are still an interesting reading.

The begin of the post-electroscope era, around 1929, largely relies on a few
fundamental technical breakthroughs: the development of the Geiger-M\"uller
tubes, the first practical implementations of the coincidence technique,
introduced by Bothe and Kolh\"orster~\cite{1929ZPhy...56..751B} and refined by
Bruno Rossi~\cite{1930Natur.125..636R}, and the introduction of \emph{imaging}
devices such as the bubble chamber (and, later, the cloud chambers and the
stacks of photographic emulsions sensitive to single charged particles).

It was thanks to different clever arrangements of Geiger tubes in coincidence
and shielding materials that an incredible amount of new information about
cosmic rays was made available in the 1930s. It soon became clear that some of
the particles observed could pass through very noticeable amounts of material,
which casted serious doubts on the interpretation of the primary component as
consisting of photons.

At about the same time, physicists realized that the interaction between
radiation and matter was much more complicated that they had anticipated.
In 1933 Blackett and Occhialini~\cite{1933RSPSA.139..699B} published the
results of the first observations performed with a cloud chamber triggered by
Geiger-M\"uller tubes, clearly showing the copious production of secondary
radiation and the new phenomenon of the \emph{showers}.
This, in turn, forced scientists to focus the attention on
the \emph{genetic} relation between the primary and the secondary components
of the cosmic radiation, and to consider seriously the hypothesis that most of
the particles observed near the surface were actually produced in the
atmosphere.

The idea that the magnetic field of the Earth could be used to shed light
on the nature of the cosmic radiation, and establish unambiguously whether
primary cosmic rays were photons or charged particles occurred early on in the
1920s. It was clear that, in the second case, they would be somewhat channeled
along the field lines and one would expect a larger intensity at the magnetic
poles compared to the equator. Searches for this \emph{latitude effect} were
carried out as soon as 1927, but it is fair to say that in
1930, when Bruno Rossi started the first quantitative analysis of the
problem, evidence for an influence of the geomagnetic field on the intensity
of the cosmic radiation were far from being compelling (the latitude effect
was only established in the 1930s thanks to a monumental measurement campaign
led by A.~H.~Compton).
Building on top of the work by the Norwegian geophysicist Carl~St\"ormer,
Rossi set the stage for the ray-tracing techniques which are nowadays
customarily used to study the motion of charged particles in a magnetic field
(see section~\ref{sec:raytracing}).
He predicted that, if the primary cosmic rays were charged, and predominantly
of one sign (either positive or negative), one should observe an East-West flux
asymmetry, which would be maximal around the geomagnetic equator (see
section~\ref{sec:east_west_effect}).
In 1934 Rossi~\cite{1930PhRv...36..606R} and two other groups independently
measured this \emph{East-West effect}.
It was an incontrovertible evidence that primary cosmic rays are
charged---and, even more, the sign of the effect allowed to predict the
prevalent sign of their charge.
\emph{``The results of these experiments [\ldots] confirm the view,
  supported by the early experiments of the writer, that cosmic
  rays consist chiefly of a charged corpuscular radiation with a
  continuous energy spectrum extending to very great energies.
  Moreover the new results on the azimuthal effect show that the
  charge is predominantly positive.
  It is however possible that, in addition to the positive particles,
  a smaller amount of other kind of rays (negative particles,
  photons, neutrons) is contained in the cosmic radiation. In fact,
  some results are rather difficult to explain by supposing that the
  cosmic radiation consists merely of positive particles.''}
These brief except from~\cite{1930PhRv...36..606R}, written in 1934, still
constitutes a substantially correct description of our current understanding of
cosmic rays%
\footnote{One should be careful, however, in not trying and read
  \emph{too much} in these few sentences. At the time the relation between the
  primary and secondary components of the cosmic radiation was far from being
  completely understood and the correctness of the conclusions rest on
  the fact that the products of the interactions with the atmosphere
  retain much of the angular information of the primaries.}.

On a slightly different note, we should emphasize that we deliberately left
out from this short summary at least two fundamental items. The first is the
deep connection between cosmic rays and the early stages of development of
particle physics, with the positron~\cite{1933PhRv...43..491A},
muon~\cite{1937PhRv...51..884N,1937PhRv...52.1003S} and
pion~\cite{1947Natur.160..453L} all being discovered in the
cosmic radiation. The other is the discovery of extensive air showers
\cite{1939RvMP...11..288A}, which originated an independent (and incredibly
prolific) line of research---that of the study of ultra-high-energy cosmic
rays from the ground.

All this said, it is fair to say that between 1940 and 1950 a complete and
coherent picture of the phenomena connected with the cosmic radiation
emerged, with most of the primary cosmic rays being protons and nuclei of
heavier elements and most of the particles observed near to the surface being
secondary products of their interaction with the atmosphere. As we shall see in
a second, the development of stratospheric balloons and the beginning of the
space age allowed to customarily observe the primary radiation at the
top of the atmosphere in the following decades.


\subsection{The latter days}

The launch of the Sputnik~I artificial satellite by the Soviet Union on October
4, 1975 signals the begin of the space era. Within six months the Sputnik~II
soviet satellite and the Explorer~I and Explorer~III American satellites
were launched---all the three of them were equipped with Geiger-M\"uller
counters with the aim of mapping the comic-ray intensity beyond the altitudes
reachable by balloons (the Explorer~I and Explorer~III were placed into an
elliptical orbit reaching out to some 2500~km).
One of the most interesting discoveries was that of the Van Allen
belts~\cite{1959JGR....64.1683V}---regions around the Earth where low-energy
charged particles trapped in the geomagnetic field make the cosmic-ray
intensity several orders of magnitude more intense than that on the surface.

As it turns out, the radiation belts are not directly relevant for instruments
in low-Earth orbit (which is the main topic of this write-up), with the notable 
exception of the \emph{South Atlantic Anomaly} that we shall briefly introduce
in section~\ref{sec:saa}. We shall glance through the basics of geomagnetically
trapped radiation (which, in general, \emph{is} relevant for the low-Earth
orbit environment) in section~\ref{sec:trapped_radiation}.


\subsubsection{Charged cosmic rays}

It is fair to say that, among the first \emph{modern} instruments for charged
cosmic-ray measurements are the pioneering magnetic spectrometers flown on
balloons in the 1960s and 1970s for the study of the positron
\cite{1969ApJ...158..771F,1975ApJ...198..493D,1974PhRvL..33...34B} and the
antiproton~\cite{1979ICRC....1..330B,1984ApL....24...75G} components of the
cosmic radiation. They generally had limited (at least by any modern standard)
energy range and particle identification capabilities---typically provided by a
\cheren\ detector.

On a related note, it is somewhat amusing to note how back in the 1980s the
apparent increase of the positron fraction above $\sim 5$~GeV that these early
measurements seemed to indicate was actively discussed, and both pulsars and
dark matter annihilation were already proposed as viable candidates for its
origin~\cite{1989ApJ...342..807B}. And then the history repeated itself two
decades later (this time for real) with the measurement published by the
PaMeLa collaboration~\cite{2009Natur.458..607A}.

At about the same time emulsion
chambers~\cite{2012ApJ...760..146K,2006JPhCS..47...31C} and calorimetric
experiments~\cite{1975ApJ...197..219M,1977PhRvL..38.1368H,1984ApJ...278..881T}
were used for the measurement of the all-electron and the proton and nuclei
spectra.

WiZard~\cite{1990NCimB.105..191G} was the name of the magnetic spectrometer
concept selected in the late 1980s for the Astromag facility planned to operate
on the U.S. Space Station Freedom---which never saw the light as originally
conceived and later evolved into the International Space Station. As this
project was abandoned, the WIZARD collaboration~\cite{2003NuPhS.122...66S}
started a long and incredibly successful campaign of balloon-borne
experiments---including MASS89, MASS91, TS93, Caprice94, Caprice97 and
Caprice98---which finally winded up in the PaMeLa space-based magnetic
spectrometer, currently in operation.
HEAT and BESS (see section~\ref{sec:unconventional_detectors}) constitute
two additional notable examples of magnetic spectrometers flown around the same
time. Generally speaking, the 1990s signal a dramatic leap forward in the
particle identification capabilities of the instruments, with transition
radiation detectors and advanced \cheren\ detectors effectively exploited, and
modern imaging calorimeters providing shower-topology information in addition
to the basic energy measurement.

At this point we are straight into the present, with the AMS-02 magnetic
spectrometer operating on the International Space Station---preceded by the
Shuttle flight of the path-finder AMS-01 in 1998---and, on the calorimetric
side of the panorama, the \emph{fabulous four} balloon-borne detectors: ATIC,
CREAM, TIGER and TRACER. One a related note, the record-breaking 161 days of
exposure integrated by CREAM in its six flights signal the exciting perspectives
nowadays made available by the development for Long Duration (LD) circumpolar
balloon flights.


\subsubsection{Gamma rays}%
\label{sec:history_latter_gamma}

The Explorer XI satellite, launched in 1961, carried a gamma-ray detector
on board, consisting of a crystal scintillator and a \cheren\ counter,
surrounded by an anti-coincidence shield~\cite{1962PhRvL...8..106K}.
While the satellite could not be actively pointed and the photon direction was
loosely determined by the solid angle defined by the geometry of the telescope,
Explorer XI performed the first observation of the gamma-ray sky and
in 1962 the beginning of gamma-ray astronomy was announced to the world on
Scientific American:
\emph{``An ingenious telescope in a satellite has provided the
  first view of the Universe at the shortest wavelength of the electromagnetic
  spectrum. This historic glimpse is supplied by just 22 gamma
  rays.''}~\cite{1962SciAm.206e..52K}.

\begin{table}[htb!]
    \begin{tabular}{p{0.20\linewidth}p{0.28\linewidth}p{0.20\linewidth}%
        p{0.25\linewidth}}
      \hline
      Experiment & Energy range & Date & $\gamma$ candidates\\
      \hline
      \hline
      Explorer XI & $>50$~MeV & 1961 & 22\\
      OSO-3 & $>50$~MeV & 1967--1968 & 621\\
      SAS-2 & 20~MeV--1~GeV & 1972--1973 & 13,000\\
      COS-B & 30~MeV--3~GeV & 1975--1983 & 200,000\\
      EGRET & 30~MeV--10~GeV & 1991--1999 & 1,500,000\\
      \Fermi-LAT & 20~MeV--$>1$~TeV & 2008--?? & 500,000,000\\
      \hline
    \end{tabular}
    \caption{Total number of gamma-ray candidates collected by specific
      space-born detectors through their missions. Note that the energy
      ranges for the various experiments are purely indicative.}
    \label{tab:gamma_stat}
\end{table}

A similar detector concept was flown in 1967 on board the third Orbiting Space
Observatory (OSO-3). OSO-3 operated continuously for 16 months (at which
point the last spacecraft tape recorder failed), performing a complete sky
survey and recording 621 photons above 50 MeV. Most notably, the experiment
demonstrated that celestial gamma-rays are anisotropically
distributed---concentrated in the direction of the galactic plane and,
particularly, toward the galactic center.

In 1969 and 1970 the array of military satellites VELA, launched by the United
States to monitor possible nuclear experiments carried out by the Soviet Union,
serendipitously discovered the transient flashes of gamma radiation
that generally go under the name of gamma-ray bursts (GRB).

It is generally acknowledged that SAS-II~\cite{1972NucIM..98..557D}, launched
on November 1972%
\footnote{Unfortunately a failure of the low voltage power supply
stopped the data collection on June 1973.},
provided the first detailed information about the gamma ray sky and effectively
demonstrated the ultimate promise of gamma-ray astronomy, showing that the
galactic plane radiation was strongly correlated with the galactic structural
features---not event mentioning the first detection of gamma-ray point sources,
most notably the Vela and Crab pulsars.
SAS-II was the first satellite entirely devoted to gamma-ray astrophysics, with
a gamma-ray telescope on board composed by spark chambers interleaved with
tungsten conversion foils%
\footnote{The energy information was (loosely) derived by the multiple
  scattering, measured by means of the tracking detectors.}
, and an anti-coincidence system featuring a set of plastic scintillator tiles
and directional \cheren\ detectors placed below the spark chambers. With a peak
effective area of $\sim 120$~cm$^2$ and a PSF of the order of a few degrees,
the gamma-ray detector on board SAS-II is effectively one of the first
incarnations of the pair conversion telescope concept (see
section~\ref{sec:pair_conversion_telescopes}).

In 1975 the European Space Agency launched the COS-B~\cite{1975SSI.....1..245B}
satellite, that operated successfully for 6 years and 8 months---well beyond
the original goal of two years.
The gamma-ray telescope on board COS-B was conceived following the heritage
of that on SAS-II, with the crucial addition of a $4.7~X_0$ calorimeter to
improve the energy measurement. It was sensitive to photons between 30~MeV and
several~GeV over a field of view of almost 2~sr, with a peak effective area of
some 50~cm$^2$.
Among the key science results from the COS-B mission are the first catalog of
gamma-ray sources (including 25 entries) and a complete map of the disc of the
milky way.

With a weight of approximately 17~tons, the Compton Gamma Ray Observatory
(CGRO), launched by NASA in 1991, is possibly the heaviest scientific payload
ever flown in low-Earth orbit. The Energetic Gamma Ray Experiment Telescope
(EGRET~\cite{1988SSRv...49...69K}) on board CGRO, a pair conversion telescope
with far superior sensitivity than any of its predecessors, made the first
complete survey of the gamma-ray sky in the energy range between 30~MeV and
$\sim 10$~GeV (detecting 271 discrete sources) and is at the base of the
last-generation instruments exploiting the silicon-strip technology such as
AGILE and the \Fermi-LAT~\cite{2009ApJ...697.1071A}.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{gamma_exp_stat}
  \caption{Total number of gamma-ray candidates collected by specific
    space-born detectors through their missions. The points on the $x$-axis
    are the average values between the mission start and stop times.}
  \label{fig:gamma_exp_stat}
\end{figure}

Table~\ref{tab:gamma_stat} summarizes the number of gamma-ray candidates
collected by the gamma-ray detectors listed in this section through the
duration of the corresponding mission. While the numbers surely give a sense
of the continuous advance in performance, there are good reasons (primarily
geometrical dimensions and weight) to presume that it is going to be hard to
keep up with this sort of Moore's law (see figure~\ref{fig:gamma_exp_stat})
over the next few years.


\subsection{Instrument concepts}

\begin{figure*}[htbp]
  \includegraphics[width=\textwidth]{detectors}
  \caption{Schematic view of some of the most recent space- or balloon-borne
    cosmic-ray and gamma-ray detectors (all the dimensions are meant to be
    in scale). Only the detector subsystems are sketched (note that the magnets
    for PaMeLa and AMS-02 are not represented). For the \Fermi-LAT: the
    anti-coincidence detector (ACD), the tracker (TKR) and the calorimeter
    (CAL). For AMS-02: the transition radiation detector (TRD), the time of
    flight (TOF), the tracker layers, the electromagnetic calorimeter (ECAL)
    and the ring imaging \cheren\ detector (RICH). For CREAM-II: the
    timing charge detector (TCD), the \cheren\ detector (CD), the silicon
    charge detector (SCD) and the calorimeter (CAL). For PaMeLa: the time of
    flight scintillators (S1--S3), the tracker (TKR), the calorimeter (CAL)
    and the neutron detector. For each instrument the relevant combination
    of sub-detectors determining whether a given event is reconstructable
    (and hence the field of view) is also sketched. Trivial as it is, we also
    note that the calorimeters are made of different material and therefore
    the relative thickness does not necessarily reflect the depth in terms
    of radiation lengths (this information is included in
    table~\ref{tab:pres_fut_instruments}).}
  \label{fig:detectors}
\end{figure*}

\input{tables/cr_irfs}

The brief summary of the history of cosmic-ray measurements we have outlined
in the previous two sections is also an illustration of the basic detector
concepts and experimental techniques that have been exploited over the last
$\sim 50$~year. The dichotomy between magnetic spectrometers and calorimetric
experiments is a fundamental one and we shall elaborate a little bit more
on it in the next section. We postpone a (much) more in-depth discussion of
some related technical aspects to sections~\ref{sec:techniques} and
\ref{sec:irfs}. Schematic views of a few actual instruments, either recent or in
operation, are shown (in scale!) in figure~\ref{fig:detectors}.


\subsubsection{Spectrometers and calorimeters}

Most modern comic-ray detectors fall in either of the two categories:
magnetic spectrometers or calorimetric experiments. Strictly speaking all
the advanced magnetic spectrometers feature an electromagnetic calorimeter
for energy measurement, so the basic difference between the two is really the
presence of the magnet.

The key feature of magnetic spectrometers is their ability of distinguishing
the charge sign---e.g., separating electrons and positrons or protons and
antiprotons. In addition, they are typically equipped with additional
sub-detectors (\cheren\ detectors and/or transition radiation detectors) aimed
at particle identification, e.g., for isotopical composition studies (see
section~\ref{sec:tof}). This all comes at a cost, in that the magnet is a
passive element (and typically a heavy one) contributing to the mass budget and
limiting the field of view (not even mentioning the potential issue of the
background of secondary particles). Both effect conspire to make the acceptance
of this kind of instrument relatively smaller.

Calorimetric experiments, on the other hand, typically feature a larger
acceptance and energy reach---and are best suited for measuring, e.g., the
inclusive $e^+ + e^-$ spectrum or the proton and nuclei spectra up to the
highest energies---but cannot readily separate charges (see, however,
section~\ref{sec:geomeg_charge_sep} for yet another twist to the story).
Modern electromagnetic imaging calorimeters, be they homogeneous or sampling,
provide excellent electron/hadron discrimination and are typically instrumented
with some kind of external active layer for the measurement of the absolute
value of the charge (e.g., to distinguish between singly-charged particles and
heavier nuclei). Since flying an accelerator-type hadronic calorimeter in space
is impractical due to mass constraints (see section~\ref{sec:had_cal}),
a fashionable alternative to measure the energy for hadrons is that of 
exploiting a passive low-Z target (as done, e.g., in the CREAM and ATIC
detectors) to promote a nuclear interaction and then recover the energy from
the electromagnetic component of the shower.


\subsubsection{Pair-conversion telescopes}%
\label{sec:pair_conversion_telescopes}

In the basic scheme laid out in the previous section gamma-ray pair conversion
telescopes are essentially calorimetric experiments featuring a dedicated
tracker-converter stage in which foils of high-$Z$ materials are interleaved
with position sensitive detection planes. The basic detection principle is
easy: the conversion foils serve the purpose to promote the conversion of
high-energy (say above $\sim 20$~MeV) gamma rays into an electron-positron pair
which is in turn tracked to recover the original photon direction.
The pair is then absorbed into the calorimeter for the measurement of the
gamma-ray energy. Last but not least, pair conversion telescopes feature some
kind of anti-coincidence detector for the rejection of the charged-particle
background that, as we have seen, outnumbers the signal by several orders of
magnitude in typical low-Earth orbit.

As mentioned in section~\ref{sec:history_latter_gamma},
EGRET~\cite{1988SSRv...49...69K} on-board the CGRO mission, AGILE and the
\Fermi-LAT~\cite{2009ApJ...697.1071A} are prototypical examples of
pair-conversion telescopes.


\subsubsection{Unconventional (or just old-fashioned) implementations}%
\label{sec:unconventional_detectors}

While most of the detectors we shall consider in the following are built around
either a magnetic spectrometer \emph{a la}~AMS-02 or an electromagnetic
calorimeter, there exist less conventional implementations that deserve to be
briefly mentioned, here.

Among the magnetic spectrometers, BESS (see, e.g., \cite{2002AdSpR..30.1253Y})
is a notable example where the instruments features a thin superconducting
solenoid magnet enabling a large geometrical acceptance with a horizontally
cylindrical configuration (note that in this case the particles go
\emph{through} the magnet!). Several different versions of the instrument
underwent a long and very successful campaign of balloon flight for the
measurement (and monitoring in time) of the
antiproton~\cite{2008PhLB..670..103B} and proton and helium
spectra~\cite{2000ApJ...545.1135S}.

With its unrivaled imaging granularity, the emulsion chamber technique
played a prominent role in the early days of calorimetric experiments, as it
readily allowed to assemble large---and yet relatively simple---detectors.
In this case it is the data analysis process that is significantly more
difficult to scale up with the available statistics with respect to that of
modern \emph{digital} detectors. Reference~\cite{2012ApJ...760..146K}, e.g,
contains a somewhat detailed summary of more than 30~years of observations of
high-energy cosmic-ray electrons---in several balloon flights from 1968 to
2001---with a detector setup consisting of a stack of nuclear emulsion plates,
X-ray films, and lead/tungsten plates. Electromagnetic showers were detected by
a naked-eye scan of the X-ray films and energies were determined by counting
the number of shower tracks in each emulsion plate within a 100~$\mu$m wide
cone around the shower axis. At even higher energies, a nice account of the
emulsion chambers (e.g., JACEE~\cite{1986NIMPA.251..583B} and
RUNJOB~\cite{2005ApJ...628L..41D}) flown on balloons with the aim of measuring
the cosmic-ray chemical composition near the \emph{knee} is given
in~\cite{2006JPhCS..47...31C}.

\begin{figure}[!hbt]
  \includegraphics[width=\linewidth]{bremss_id}
  \caption{Schematic diagram of a magnetic spectrometer equipped for the
    \bremss-identification technique (adapted from~\cite{1974PhRvL..33...34B}).}
  \label{fig:bremss_id}
\end{figure}

We close this section by mentioning the \bremss-identification technique,
used, e.g., in ~\cite{1974PhRvL..33...34B}, which strikes the author as one of
the neatest and most clever attempts to overcome the limited particle
identification capabilities of the early detectors.
The basic idea, illustrated in figure~\ref{fig:bremss_id}, is that of using
a thin radiator and select the electrons and positrons producing
a \bremss\ photon---identified as an additional individual shower in the
\emph{calorimeter} (or really, in the \emph{shower detector}). Though the
overall fraction of useful signal events is somewhat reduced, this detector
concept provides a clear and distinct signature (that is very hard to mimic
for heavy particles) allowing proton rejection factors of the order of $10^5$.


\subsubsection{Different instrument concepts}

All the cosmic-ray detectors we have mentioned so far exploit nuclear
interactions to measure the energy of protons and heavier nuclei. In contrast
to calorimetric measurements, it is possible, at least in principle, to
measure the cosmic-ray charge and energy through their electromagnetic
interaction only. The main advantage is that this concept makes it possible
to realize instruments with very large geometrical aperture (of the order of
several m$^2$~sr before selection cuts), as only low-density material are
necessary. On the other hand, the main drawback is that the quality of the
energy measurement is largely non uniform across the energy range covered---and
poor-to-non existent in some significant portions of the phase space.

The TRACER balloon-borne detector~\cite{2011ApJ...742...14O} is possibly the
most notable example of this type of instrument concept. By a clever combination
of scintillators, \cheren\ detectors, transition radiation detectors and a
$dE/dx$ array (see sections~\ref{sec:interactions} and~\ref{sec:techniques}
for more details), TRACER was able to measure the charge and the energy of
cosmic-ray nuclei with $Z > 5$ between $\sim 1$~GeV and a few~TeV.
Figure~\ref{fig:tracer_signal} shows the response functions, normalized by
$Z^2$, for the three basic TRACER sub-detectors: the charge is measured by the
$dE/dx$ array---with the \cheren\ detectors breaking the degeneracy between the
two parts of the curve on the two sides of the minimum---while the energy is
recovered from the \cheren\ detectors near the \cheren\ threshold, from the
$dE/dx$ array in the intermediate range (admittedly with limited resolution),
and from the TRD at very high energy.

\begin{figure}[!hbt]
  \includegraphics[width=\linewidth]{tracer_signal}
  \caption{Response functions, normalized by $Z^2$ for the three basic
    TRACER sub-detectors (adapted from~\cite{2011ApJ...742...14O}).
    Much more details about the basic physical interaction processes and
    how these can be exploited in actual particle detectors can be found in
    sections~\ref{sec:interactions} and~\ref{sec:techniques}.}
  \label{fig:tracer_signal}
\end{figure}

On a completely different subject, we briefly mention the
\emph{Compton telescope} concept, which constitutes one of the possible
experimental approaches for studying gamma rays below $\sim 20$~MeV---where
Compton scattering is the interaction physical process with largest cross
section.
The basic idea is that of measuring both the (first) Compton interaction
point through the scattered electron and the direction of the scattered
photon by absorbing it. This, in turn, allows to kinematically constraint the
direction in the sky of the original gamma-ray in the so called
Compton cone. As it turns out, any practical implementation of this seemingly
simple concept is quite challenging, for many different reasons.
It is not by coincidence that the last Compton telescope flow in space was
COMPTEL~\cite{1993ApJS...86..657S} on-board the CGRO, totaling $\sim 50$~cm$^2$
on-axis effective area---a somewhat meager figure when considering that the
total weight of the instrument was of the order of 1~ton.
