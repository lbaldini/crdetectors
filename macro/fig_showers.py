#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __constants__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend
from gGraphErrors import gGraphErrors


# From Review of Particle Physics, July 2004, pag 250.
#
# dE/dt = E_0 * b * (b*t)^(a-1)*exp(-b*t) / (Gamma(a))
# par     0     1    1 x   2         1             2
# t = x/X0
#
# b = 0.5
# t_max = (a-1)/b = ln(y) + C
# y = E/Ec
# C = -0.5 for electrons
# C = +0.5 for photons 


X_MAX = 30

SHOWER_FORMULA = '[0]*[1]*([1]*x)**([2]-1)*exp(-[1]*x)/TMath::Gamma([2])'
SHOWER_TF1 = gF1('fshower', SHOWER_FORMULA, 0, X_MAX)
SHOWER_TF1.SetParameter(1, 0.5)


CONT_ENERGIES = [1, 10, 100, 1000, 10000]


def setupFunction(energy, criticalEnergy = BGO_EC, gamma = False,
                  maximum = None):
    """
    """
    SHOWER_TF1.SetParameter(0, energy)
    if gamma:
        c = 0.5
    else:
        c = -0.5
    y = 1000*float(energy)/criticalEnergy
    a = 1 + SHOWER_TF1.GetParameter(1)*(math.log(y) + c)
    SHOWER_TF1.SetParameter(2, a)
    SHOWER_TF1.GetXaxis().SetTitle('t [X_{0}]')
    SHOWER_TF1.GetYaxis().SetTitle('dE/dt [GeV X_{0}^{-1}]')
    if maximum is not None:
        SHOWER_TF1.SetMaximum(maximum)

def getIntegral(energy, numPoints = 100):
    """
    """
    setupFunction(energy)
    g = gGraphErrors('g%d' % energy)
    store(g)
    for i in range(numPoints):
        t = X_MAX*i/float(numPoints - 1)
        cont = SHOWER_TF1.Integral(0, t)/energy
        g.SetNextPoint(t, cont)
    g.GetXaxis().SetRangeUser(0, X_MAX)
    g.GetXaxis().SetTitle('t [X_{0}]')
    g.GetYaxis().SetTitle('Shower containment')
    return g


c1 = gCanvas('shower_profile')
energy = 100
setupFunction(energy, maximum = 10)
f = SHOWER_TF1.DrawCopy()
f.GetXaxis().SetTitle('t [X_{0}]')
f.GetYaxis().SetTitle('dE/dt [GeV X_{0}^{-1}]')
c1.annotate(0.5, 0.82, '%d GeV electron in BGO' % energy)
c1.Update()
c1.save()


c2 = gCanvas('shower_containment')
emin = CONT_ENERGIES[0]
emax = CONT_ENERGIES[-1]
for i, energy in enumerate(CONT_ENERGIES):
    g = getIntegral(energy)
    if i == 0:
        g.Draw('ac')
    else:
        g.Draw('csame')
c2.annotate(0.2, 0.6, '%d GeV' % emin)
c2.annotate(0.5, 0.38, '%d TeV' % (emax/1000.))
c2.Update()
c2.save()
