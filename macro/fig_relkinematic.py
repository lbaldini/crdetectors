#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import __utils__
import numpy
import math

from __ROOT__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend
from gGraphErrors import gGraphErrors


""" First a quick test of the formulae.
"""
m = 0.938
beta = 0.95
gamma = 1./math.sqrt(1 - beta**2)
p = m*beta*gamma
E = math.sqrt(m**2 + p**2)
Ek = E - m
print m, beta, gamma, gamma*beta, p, E, Ek

print p/m, math.sqrt(E**2/m**2 - 1), math.sqrt((Ek**2 + 2*Ek*m)/m**2)
print p/math.sqrt(m**2 + p**2), math.sqrt(1 - m**2/E**2),\
    math.sqrt(Ek**2 + 2*Ek*m)/(Ek + m)
print math.sqrt(1 + p**2/m**2), E/m, 1 + Ek/m


beta = gF1('beta', 'x/sqrt([0]**2 + x**2)', 0.001, 1000)
gamma = gF1('gamma', 'sqrt(1 + x**2/[0]**2)', 0.001, 1000)
n = 1.05
threshold = gF1('threshold', '[0]', 0.001, 1000)
threshold.SetParameter(0, 1./n)
threshold.SetLineStyle(7)

c1 = gCanvas('beta_vs_r', Logx = True)
frame1 = gH1F('h1', 'h1', 100, 0.1, 1000, Minimum = 0, Maximum = 1.1,
              XTitle = 'R [GV]', YTitle = '#beta')
frame1.Draw()
beta.SetParameter(0, 0.000512)
beta.DrawCopy('same')
x = 1
c1.annotate(x, beta.Eval(x) + 0.02, 'e^{#pm}', ndc = False, align = 21)
beta.SetParameter(0, 0.938)
beta.DrawCopy('same')
c1.annotate(x, beta.Eval(x) + 0.06, 'p', ndc = False, align = 21)
threshold.Draw('same')
c1.annotate(100, 1./n - 0.02, 'n = %.2f' % n, ndc = False, align = 23)
c1.Update()
c1.save()

c2 = gCanvas('gamma_vs_r', Logx = True, Logy = True)
frame2 = gH1F('h2', 'h2', 100, 0.1, 1000, Minimum = 1, Maximum = 1e4,
              XTitle = 'R [GV]', YTitle = '#gamma')
frame2.Draw()
gamma.SetParameter(0, 0.000512)
gamma.DrawCopy('same')
x = 1
c2.annotate(x, 1.15*gamma.Eval(x), 'e^{#pm}', ndc = False, align = 21)
gamma.SetParameter(0, 0.938)
gamma.DrawCopy('same')
c2.annotate(x, 1.2*gamma.Eval(x), 'p', ndc = False, align = 21)
c2.Update()
c2.save()
