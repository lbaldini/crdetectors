#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend


X_MIN = 0.25
X_MAX = 1e5

INPUT_FILE = open('data/muon_loss.txt')
for i in range(10):
    INPUT_FILE.next()

gion = gGraphErrors('gion', 'Ionization')
grad = gGraphErrors('grad', 'Radiation')
gtot = gGraphErrors('gtot', 'Total')

dedxmin = 1e6
for line in INPUT_FILE:
    data = line.split()
    p = float(data[1])
    gammabeta = p/105.65839
    ion = float(data[2])
    rad = float(data[6])
    tot = float(data[7])
    if tot < dedxmin:
        dedxmin = tot
        xmin = gammabeta
    if gammabeta > X_MIN and gammabeta < X_MAX:
        gion.SetNextPoint(gammabeta, ion)
        grad.SetNextPoint(gammabeta, rad)
        gtot.SetNextPoint(gammabeta, tot)


gtot.GetXaxis().SetTitle('#beta#gamma')
gtot.SetMinimum(1)
gtot.GetYaxis().SetTitle('-dE/dx [MeV g^{-1} cm^{2}]')
gion.SetLineStyle(7)
grad.SetLineStyle(3)

c = gCanvas('stopping_power', Logx = True, Logy = True)
gtot.Draw('al')
gion.Draw('lsame')
grad.Draw('lsame')
l = gLegend(0.2, 0.9)
for g in [gtot, gion, grad]:
    l.AddEntry(g, g.GetTitle(), 'l')
l.Draw()
c.annotate(0.7, 0.88, 'Muons in Si')
a = ROOT.TArrow(xmin, 2*dedxmin, xmin, dedxmin, 0.02, '->')
a.Draw()
c.annotate(xmin, 2.2*dedxmin, 'Min dE/dx', ndc = False, align = 22)
c.Update()
c.save()
