#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __geometry2d__ import *
from gCanvas import gCanvas
from gPhysicalCanvas import gPhysicalCanvas
from gF1 import gF1


R_EARTH = 6371.
ALTITUDE = 565.
TOP_ATM = 50.
R_ALT = R_EARTH + ALTITUDE
R_ALT2 = R_EARTH + 2*ALTITUDE
R_TOP_ATM = R_EARTH + TOP_ATM

XSIZE = 3800.
YSIZE = 2000.
TXT_SIZE = 1.6


c = gPhysicalCanvas('earth_limb', XSIZE, YSIZE)
xc = 0.
yc = -R_EARTH - 0.25*YSIZE
circle(xc, yc, R_EARTH)
circle(xc, yc, R_TOP_ATM, LineStyle = 7)
#circle(xc, yc, R_ALT, LineStyle = 7)
theta = -0.2
st = math.sin(theta)
ct = math.cos(theta)

xdet = xc + R_ALT*st
ydet = yc + R_ALT*ct
xint = xc + R_TOP_ATM*math.sin(math.acos(R_TOP_ATM/R_ALT) + theta)
yint = yc + R_TOP_ATM*math.cos(math.acos(R_TOP_ATM/R_ALT) + theta)

line(xc + R_EARTH*st, yc + R_EARTH*ct, xc + R_ALT2*st, yc + R_ALT2*ct)
marker(xdet, ydet, 20)
line(xc + R_ALT*st, yc + R_ALT*ct, xint, yint, curly = True)
l = 2000
marker(xint, yint, 20)
line(xint, yint, xint + l*ct, yint + l*st)

annotate(xc + R_ALT2*st, yc + R_ALT2*ct + 20, 'Local zenith', TXT_SIZE,
         align = 21)
annotate(xdet - 50, ydet, 'Detector', TXT_SIZE, align = 32)
annotate(xint - 150, yint + 100, 'p-air interaction', TXT_SIZE, align = 11)
arc(xdet, ydet, 100, -10, 100)
arc(xdet, ydet, 125, -10, 100)
annotate(xdet + 300, ydet + 200, '#theta_{z} = #theta_{limb}', TXT_SIZE)

c.Update()
ROOT.gStyle.SetPaperSize(28, 80)
c.save()


""" And now print the viewing angle.
"""
print 90. + math.degrees(math.acos((R_EARTH+TOP_ATM)/(R_EARTH+ALTITUDE)))
print 90. + math.degrees(math.acos((R_EARTH)/(R_EARTH+ALTITUDE)))


""" Parameters from the first limb paper.
"""
"""
DELTA_OMEGA = 2*math.pi*(math.cos(math.radians(66)) -\
                         math.cos(math.radians(70)))
SPECTRUM = gF1('limb_spec', '[0]*((1000*x)**(-[1]))', 0.1, 1000000)
SPECTRUM.SetParameters(DELTA_OMEGA*7.4e9, 2.79)

print 250*3600*SPECTRUM.Integral(1, 1000000)
print 365*24*3600*SPECTRUM.Integral(1000, 1000000)

c1 = gCanvas('limb_spec', Logx = True, Logy = True)
SPECTRUM.Draw()
c1.Update()
"""
