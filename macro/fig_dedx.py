#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import __utils__
import numpy
import math

from __ROOT__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend
from gGraphErrors import gGraphErrors


X_MIN = 0.2
X_MAX = 1e5

INPUT_FILE = open('data/muon_loss.txt')
for i in range(10):
    INPUT_FILE.next()

gion = gGraphErrors('gion', 'Ionization')
for line in INPUT_FILE:
    data = line.split()
    p = float(data[1])
    gammabeta = p/105.65839
    ion = float(data[2])
    if gammabeta > X_MIN and gammabeta < X_MAX:
        gion.SetNextPoint(gammabeta, ion)

"""
Parameters:
[0]: normalization
[1]: z
[2]: log coefficient
"""
formula = '[0]*[1]**2*(1 + 1./x**2)*(0.5*log([2]*x**2) - x**2/(1 + x**2))'
f = gF1('bb', formula, X_MIN, X_MAX)
f.SetParameter(0, 1)
f.SetParLimits(0, 0.1, 1.5)
f.FixParameter(1, 1)
f.SetParameter(2, 1)
gion.Fit('bb')

c1 = gCanvas('c1', Logx = True)
gion.Draw('ap')
c1.Update()

grid = numpy.logspace(-1, 3, 100)
frame = gH1F('h', 'h', 100, 0.1, 1000, Minimum = 0, Maximum = 30,
             XTitle = 'R [GV]', YTitle = '-dE/dx [MeV g^{-1} cm^{2}]')
gp = gGraphErrors('gp')
f.FixParameter(1, 1)
for r in grid:
    y = f.Eval(r*1/0.938)
    gp.SetNextPoint(r, y)
ghe = gGraphErrors('ghe')
f.FixParameter(1, 2)
for r in grid:
    y = f.Eval(r*2/(4*0.938))
    ghe.SetNextPoint(r, y)

c2 = gCanvas('dedxcharge', Logx = True)
frame.Draw()
gp.Draw('same')
ghe.Draw('same')
x = 100
c2.annotate(x, gp.Eval(x) + 0.5, 'p', ndc = False, align = 21)
c2.annotate(x, ghe.Eval(x) + 0.5, '^{4}He', ndc = False, align = 21)
c2.Update()
c2.save()
