#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy

from __logging__ import logger
from __ROOT__ import *
from gGraphErrors import gGraphErrors


class gGDEModel:

    """ Basic interface to the ROOT file containing the diffuse galactic
    emission model.
    """

    def __init__(self, filePath = 'data/gll_iem_v05_rebin.root'):
        """ Constructor.
        """
        logger.info('Initializing GDE model...')
        self.__InputFile = ROOT.TFile(filePath)
        nslices = len(self.__InputFile.GetListOfKeys())
        self.__HistogramList = []
        self.__EnergyList = []
        for i in range(nslices):
            hname = 'gll_iem_%d' % i
            h = self.__InputFile.Get(hname)
            e = float(h.GetTitle().split()[0])
            logger.info('Loading %s at %.3f GeV' % (h.GetName(), e))
            self.__HistogramList.append(h)
            self.__EnergyList.append(e)
        logger.info('Done, model data ready.')

    def rebin(self, n = 8, outputFilePath = None):
        """
        """
        logger.info('Rebinning diffuse model (n = %d)...' % n)
        for h in self.__HistogramList:
            h.RebinX(n)
            h.RebinY(n)
            h.Scale(1./(n*n))
        if outputFilePath is not None:
            logger.info('Writing rebinned model to %s...' % outputFilePath)
            outputFile = ROOT.TFile(outputFilePath, 'RECREATE')
            for h in self.__HistogramList:
                h.Write()
            outputFile.Close()

    def energy(self, ebin):
        """
        """
        return self.__EnergyList[ebin]

    def histogram(self, ebin):
        """
        """
        return self.__HistogramList[ebin]

    def spectrum(self, lonbin, latbin, epower = None, fweight = None):
        """
        """
        name = 'spectrum_%d_%d' % (lonbin, latbin)
        g = gGraphErrors(name)
        for h, energy in zip(self.__HistogramList, self.__EnergyList):
            val = h.GetBinContent(lonbin, latbin)
            if epower is not None:
                val *= (energy**epower)
            if fweight is not None:
                val *= fweight.Eval(energy)
            g.SetNextPoint(energy, val)
        g.GetXaxis().SetTitle('Energy [GeV]')
        ytitle = h.GetZaxis().GetTitle()
        if epower is not None:
            ytitle = 'E^{%.2f} #times %s' % (epower, ytitle)
        g.GetYaxis().SetTitle(ytitle)
        return g



if __name__ == '__main__':
    from gCanvas import gCanvas
    gde = gGDEModel()
   
    c1 = gCanvas('c1', Logz = True)
    c1.colz()
    h = gde.histogram(20)
    h.Draw('cont4z')
    c1.Update()

    c2 = gCanvas('c2', Logx = True, Logy = True)
    spec = gde.spectrum(180, 90, 2.)
    spec.Draw('acp')
    c2.Update()
