#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import __utils__

from __ROOT__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gLegend import gLegend

amseres = gF1('ams', 'sqrt((10.4**2)/x + 1.4**2)', 5, 500)
amseres.SetTitle('AMS-02')
f = ROOT.TFile('data/perfPlots_P7SOURCE_V6.root')
fermieres = f.Get('gEdispEnergy_P7SOURCE_V6Total')
fermieres = __utils__.gscale(fermieres, 100, 0.001)
fermieres.SetLineStyle(7)
fermieres.SetMarkerStyle(0)
fermieres.SetTitle('Fermi-LAT')

for g in [amseres, fermieres]:
    g.SetMinimum(0)
    g.SetMaximum(20)
    g.GetXaxis().SetTitle('Energy [GeV]')
    g.GetYaxis().SetTitle('On-axis energy resolution [%]')

c = gCanvas('calorimeter_eres', Logx = True)
fermieres.Draw('ac')
amseres.Draw('same')
l = gLegend(0.65, 0.85, [fermieres, amseres])
l.Draw()
c.Update()
c.save()
