#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __geometry2d__ import *
from gCanvas import gCanvas
from gPhysicalCanvas import gPhysicalCanvas

TXT_SIZE = 1.5


c = gPhysicalCanvas('bremss_id', 2, 2)
box(0, 0.68, 1, 0.02, FillStyle = 3004, FillColor = ROOT.kBlack)
box(0, 0.65, 1, 0.02)
box(0, 0.15, 1, 0.02)
box(0, -0.35, 1, 0.02)
box(0, -0.60, 1, 0.3)
line(0, 0.85, 0, 0.68)
line(0, 0.68, 0, -0.6, LineStyle = 7)
r = 4
arc(r, 0.69, r, 180, 198.8)
circle(0, 0.17, 0.57, LineWidth = 1, LineStyle = 7)
annotate(0, 0.85, 'Incident electron', TXT_SIZE, align = 21)
marker(0, -0.6, MarkerSize = 1.5)
marker(0.215, -0.6, MarkerSize = 1.5)
annotate(0, -0.65, '#gamma', TXT_SIZE, align = 23)
annotate(0.215, -0.65, 'e^{-}', TXT_SIZE, align = 23)
annotate(-0.6, 0.25, 'Region with high B', TXT_SIZE)
annotate(0.52, 0.69, 'Bremsstrahlung radiator', 1.05, align = 12)
annotate(0.52, 0.64, 'Spark chamber 1', 1.05, align = 12)
annotate(0.52, 0.15, 'Spark chamber 2', 1.05, align = 12)
annotate(0.52, -0.35, 'Spark chamber 3', 1.05, align = 12)
annotate(0.52, -0.60, 'Shower detector', 1.05, align = 12)
c.Update()
c.save()
