#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy

from __ROOT__ import *
from __pdg__ import *
from gResidualCanvas import gResidualCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1
from gGraphErrors import gGraphErrors


radlen = gF1('radlen', '716*2*x/(x*(x + 1)*log(287./sqrt(x)))', 1, 100)
intlen = gF1('intlen', '37.8*(2*x)**0.312', 1, 100)
intlen.SetLineStyle(7)
ratio = gGraphErrors('ratio')
for x in numpy.logspace(0, 2, 100):
    ratio.SetNextPoint(x, intlen.Eval(x)/radlen.Eval(x))

frame1 = gH1F('h1', 'h1', 100, 1, 100, XTitle = 'Z',
              YTitle = 'X_{0}, #lambda_{I} [g cm^{-2}]',
              Minimum = 3, Maximum = 300)
frame2 = gH1F('h2', 'h2', 100, 1, 100, XTitle = 'Z', YTitle = 'Ratio',
              Minimum = 0.3, Maximum = 50)


def drawElement(symbol, z, pad = 3):
    """
    """
    val = ratio.Eval(z)
    a = ROOT.TArrow(z, pad*val, z, val, 0.02, '->')
    a.Draw()
    store(a)
    if pad < 1:
        align = 23
    else:
        align = 21
    c1.annotate(z, pad*val, '%s' % symbol, ndc = False, align = align)


c1 = gResidualCanvas('rad_int_len', Logx = True, Logy = True)
c1.drawPlots(frame1, frame2)
c1.cd(1)
radlen.Draw('same')
intlen.Draw('same')
x = 20
c1.annotate(x, 1.1*radlen.Eval(x), 'X_{0}', ndc = False)
c1.annotate(x, 1.2*intlen.Eval(x), '#lambda_{I}', ndc = False)
c1.cd(2)
ratio.Draw('l,same')
drawElement('Pb', 82, pad = 0.2)
drawElement('CsI', 54, pad = 0.2)
drawElement('Si', 14)
drawElement('C', 6)
drawElement('W', 74, pad = 0.05)
c1.Update()
c1.save()
