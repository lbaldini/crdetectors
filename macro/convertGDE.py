#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import pyfits
import numpy

from __logging__ import logger
from __constants__ import SECS_PER_YEAR
from __ROOT__ import *





def convert(inputFilePath, outputFilePath = None):
    """ Convert to ROOT a DGE model fit file.

    The mapping between pixels x,y and coordinates L,B is
    L = (x - CRPIX1)*CDELT1 + CRVAL1
    B = (y - CRPIX2)*CDELT2 + CRVAL2
    where by FITS convention x and y are numbered starting from 1.

    According to the values of the CRPIX, CDELT, and CRVAL keywords in the
    header, the step size is 0.125 deg in both coordinates. For L, the
    coordinates of the pixels range from -179.9375 to +179.9375 deg and for
    b the range is -90 to +90 deg. These are understood to correspond to the
    centers of the pixels.  The reason for an odd number in b is so that we
    have a row of pixels exactly at b = 0. An early model for the diffuse
    emission did not have this feature and because the Galactic diffuse
    intensity peaks in the plane there was some concern that this would mean
    that important structure was effectively being smoothed over. There's no
    solid angle at +-90 deg latitude, so the entire rows for those pixels are
    useless but formally they need to be there.

    Each plane of the cube is the diffuse intensity (ph cm-2 s-1 sr-1 MeV-1)
    at a specific energy, the energies listed in the ENERGIES extension.

    Note that we convert all the energies in GeV and the Intensity
    to m^{-2} year^{-1} sr^{-1}
    """
    logger.info('Opening input file %s...' % inputFilePath)
    inputFile = pyfits.open(inputFilePath)
    inputFile.info()
    logger.info('PRIMARY table header:\n%s' % inputFile['PRIMARY'].header)
    logger.info('ENERGIES table header:\n%s' % inputFile['ENERGIES'].header)
    data = inputFile['PRIMARY'].data

    def kw(key):
        """
        """
        return inputFile['PRIMARY'].header[key]

    logger.info('Retrieving binning....')
    nlonbins = kw('NAXIS1')
    lonbinning = [(x - kw('CRPIX1') - 0.5)*kw('CDELT1') + kw('CRVAL1') \
                  for x in range(1, nlonbins + 1)]
    lonbinning.append(lonbinning[-1] + kw('CDELT1'))
    lonbinning = numpy.array(lonbinning)
    logger.info('Longitude binning (%d bins):\n%s' % (nlonbins, lonbinning))
    nlatbins = kw('NAXIS2')
    latbinning = [(x - kw('CRPIX2') - 0.5)*kw('CDELT2') + kw('CRVAL2') \
                  for x in range(1, nlatbins + 1)]
    latbinning.append(latbinning[-1] + kw('CDELT2'))
    latbinning = numpy.array(latbinning)
    logger.info('Latitude binning (%d bins):\n%s' % (nlatbins, latbinning))
    evalues = 0.001*numpy.array([x[0] for x in inputFile['ENERGIES'].data])
    logger.info('Energy values:\n%s' % evalues)

    logger.info('Filling maps...')
    hdict = {}
    for k, e in enumerate(evalues):
        logger.info('Filling slice %d at energy %f GeV' % (k, e))
        h = ROOT.TH2F('gll_iem_%d' % k, '%f GeV' % e, nlonbins, lonbinning,
                      nlatbins, latbinning)
        h.SetXTitle('Longitude [#circ]')
        h.SetYTitle('Latitude [#circ]')
        h.SetZTitle('Intensity [m^{-2} year^{-1} sr^{-1} GeV^{-1}]')
        hdict[h.GetName()] = h
        for i in range(nlonbins):
            for j in range(nlatbins):
                val = 1000.*SECS_PER_YEAR*10000.*data[k][j][i]
                h.SetBinContent(i + 1, j + 1, val)
    logger.info('Opening output file %s...' % outputFilePath)
    outputFile = ROOT.TFile(outputFilePath, 'RECREATE')
    logger.info('Writing diffuse model to file..')
    for h in hdict.values():
        h.Write()
    outputFile.Close()
    logger.info('Done, bye.')



if __name__ == '__main__':
    inputFilePath = 'data/gll_iem_v05.fit'
    outputFilePath = inputFilePath.replace('.fit', '.root')
    convert(inputFilePath, outputFilePath)
