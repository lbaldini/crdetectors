#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from gF1 import gF1
from gGraphErrors import gGraphErrors


DATA = [('Explorer XI', 1961, 1961, 22, 0.065),
        ('OSO-8', 1967, 1968, 621, 1.33),
        ('SAS-2', 1972, 1973, 13000, 0.66),
        ('COS-B', 1975, 1983, 200000, 6.66),
        ('EGRET', 1991, 1999, 1500000, 6.),
        ('Fermi-LAT', 2008, 2018, 500000000, 10.)
    ]


c = gCanvas('gamma_exp_stat', Logy = True)
h = gH1F('frame', 'frame', 1000, 1955, 2020, Minimum = 1, Maximum = 10e9,
         XTitle = 'Year', YTitle = 'Number of #gamma-ray candidates')
f = gF1('f', '[0]**((x - [1])/[2])', 1955, 2020)
f.SetParameters(10, 1955, 6)
f.SetLineStyle(7)
g = gGraphErrors('stat')
h.Draw()
for i, (name, start, stop, evts, duration) in enumerate(DATA):
    x = 0.5*(start + stop)
    y = evts
    g.SetNextPoint(x, y, dy = 1)
    c.annotate(x, 2*y, name, ndc = False, align = 21, size = SMALLER_TEXT_SIZE)
g.Draw('psame')
f.Draw('same')
c.Update()
c.save()
