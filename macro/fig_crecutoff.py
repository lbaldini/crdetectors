#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1
from __logging__ import logger


INPUT_FILE = ROOT.TFile('data/cre_cutoff_fermi.root')

def getCountHistogram(index):
    h = INPUT_FILE.Get('Tracer_%s' % index)
    #h.Scale(250000./h.GetEntries())
    return h

def getCountGraph(index):
    h = getCountHistogram(index)
    g = ROOT.TGraph()
    xmax = None
    ymax = None
    for i in range(1, h.GetNbinsX() +  1):
        x = h.GetBinCenter(i)/1000.
        y = h.GetBinContent(i)
        g.SetPoint(i, x, y)
        if ymax is None or y > ymax:
            ymax = y
            xmax = x
    return (g, xmax, ymax)


MC_ILWAIN_L_ID_LIST = [0, 14, 2, 28, 42, 56]
logger.info('Populating McIlwainL intervals...')
MC_ILWAIN_L_INTERVAL_DICT = {}
for (i, index) in enumerate(MC_ILWAIN_L_ID_LIST):
    minL = float('1.%d' % index)
    try:
        maxL = float('1.%d' % MC_ILWAIN_L_ID_LIST[i+1])
    except IndexError:
        maxL = 1.8
    interval = (minL, maxL)
    logger.info('Interval %d: %s' % (i, interval))
    MC_ILWAIN_L_INTERVAL_DICT[index] = interval
logger.info('Done.')

def getMcIlwainInterval(index):
    return MC_ILWAIN_L_INTERVAL_DICT[index]


c = gCanvas('cre_cutoff_fermi', Logx = True)
h = ROOT.TH1F('hFrame', 'Frame', 1000, 3, 100)
h.SetXTitle('Energy [GeV]')
h.SetYTitle('Counts [GeV^{-1}]')
h.SetMaximum(50)
h.Draw()
for index in [0, 28, 56]:
    (g, x, y) = getCountGraph(index)
    g.Draw('c,same')
    store(g)
    c.annotate(1.06*x, y + 1, 'L = %.2f-%.2f' % getMcIlwainInterval(index),
               ndc = False, align = 31, size = SMALL_TEXT_SIZE)
c.Update()
c.save()
