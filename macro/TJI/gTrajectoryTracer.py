#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import ROOT
import os
import math
import commands
import copy

import tji2010x as tj

from __particles__ import getParticle

TJI_ROOT = os.path.dirname(os.path.abspath(__file__))
    
EARTH_RADIUS      = 6371.2  # km
DEFAULT_MAX_STEPS = 100000   # Runge-Kutta integration subroutine parameter
RAD_TO_DEG        = 180./math.pi
DEG_TO_RAD        = math.pi/180.
NORM_FONT_SIZE    = 0.042
BIG_FONT_SIZE     = 0.060


class StepPoint(ROOT.TVector3):
    
    def __init__(self, r, theta, phi):
        ROOT.TVector3.__init__(self, 1, 0, 0)
        self.SetMag(r)
        self.SetTheta(theta)
        self.SetPhi(phi)
        self.Height = EARTH_RADIUS*(self.Mag() - 1)

    def __sub__(self, other):
        # Need to circumvent the fact that the "-" operator is not exposed
        # to python, while "-=" is. There might be a better workaround this.
        v = copy.copy(self)
        v -= other
        return v

    def __str__(self):
        return 'r = %.5s, h = %6.0f km, theta = %1.3f deg, phi = %1.3f deg' %\
            (self.Mag(), self.Height, RAD_TO_DEG*self.Theta(),
             RAD_TO_DEG*self.Phi())



class TrajectoryPoint:

    def __init__(self, latitude, longitude, altitude, zenith, azimuth):
        self.Latitude          = latitude
        self.Longitude         = longitude
        self.Altitude          = altitude
        self.ZenithAngle       = zenith
        self.AzimuthAngle      = azimuth

    def getRadius(self):
        return (self.Altitude + EARTH_RADIUS)/EARTH_RADIUS

    def getTheta(self):
        return (self.Latitude + 90.)*DEG_TO_RAD

    def getPhi(self):
        return self.Longitude*DEG_TO_RAD

    def __str__(self):
        return 'lat = %.2f, lon = %.2f, h = %.0f km, zen = %.2f, azi = %.2f' %\
            (self.Latitude, self.Longitude, self.Altitude, self.ZenithAngle,
             self.AzimuthAngle)


class TrajectoryEndPoint(TrajectoryPoint):
    
    def __init__(self, escaped, latitude, longitude, altitude,
                 zenith, azimuth, minDistance, maxDistance, numSteps):
        TrajectoryPoint.__init__(self, latitude, longitude, altitude, zenith,
                                 azimuth)
        self.ParticleEscaped = escaped > 0
        self.MinDistance     = minDistance
        self.MaxDistance     = maxDistance
        self.MinAltitude     = (self.MinDistance - 1)*EARTH_RADIUS
        self.MaxAltitude     = (self.MaxDistance - 1)*EARTH_RADIUS
        self.NumSteps        = numSteps

    def __str__(self):
        return '%s\nEscaped = %s (in %d steps), alt. range = %.0f--%.0f km' %\
            (TrajectoryPoint.__str__(self), self.ParticleEscaped,
             self.NumSteps, self.MinAltitude, self.MaxAltitude)



class ParticleTrace:
    
    def __init__(self, parent, energy, startPoint, endPoint = None):
        self.ParentTracer = parent
        self.Energy       = energy
        self.StartPoint   = startPoint
        self.EndPoint     = endPoint

    def setEndPoint(self, endPoint):
        self.EndPoint = endPoint

    def getParticleName(self):
        return self.ParentTracer.Particle.Name

    def getParticleId(self):
        return self.ParentTracer.Particle.Id

    def __str__(self):
        return '%.2f GeV %s (pid = %d)\nStart point: %s\nEndPoint: %s' %\
            (self.Energy, self.getParticleName(), self.getParticleId(),
             self.StartPoint, self.EndPoint)



class ParticleTrajectory(ParticleTrace, ROOT.TPolyLine3D):
    
    def __init__(self, parent, energy, startPoint, lineWidth = 2):
        ParticleTrace.__init__(self, parent, energy, startPoint)
        ROOT.TPolyLine3D.__init__(self)
        self.StartStep = StepPoint(self.StartPoint.getRadius(),
                                   self.StartPoint.getTheta(),
                                   self.StartPoint.getPhi())
        self.StepList = [self.StartStep]
        self.SetLineWidth(lineWidth)
        self.EarthSphere = ROOT.TGeoSphere(0, 1)
        self.LabelManager = ROOT.TLatex()
        self.GridLineHor = ROOT.TLine(-0.9, 0, 0.9, 0)
        self.GridLineHor.SetLineStyle(2)
        self.GridLineVer = ROOT.TLine(0, -0.9, 0, 0.9)
        self.GridLineVer.SetLineStyle(2)
        self.EndPoint = None

    def particleEscaped(self):
        try:
            return self.EndPoint.ParticleEscaped
        except:
            return False

    def completed(self):
        return self.EndPoint is not None

    def setColor(self, color):
        self.SetLineColor(color)

    def reset(self):
        self.StepList = [self.StartStep]
        lineWidth = self.GetLineWidth()
        ROOT.TPolyLine3D.__init__(self)
        self.SetLineWidth(lineWidth)
        self.EndPoint = None
    
    def addPoint(self, r, theta, phi, threshold = 0.01):
        step = StepPoint(r, theta, phi)
        (x, y, z) = (step.X(), step.Y(), step.Z())
        # The ray-tracing routine has the ability of auto-restart itself
        # (with a finer step) under certain conditions (essentially when the
        # accuracy is not enough). Need to catch when this happen and
        # restart all the relevant data structures.
        #
        # Might need some fine tuning, here.
        try:
            if (step - self.StartStep).Mag()/\
                    (step - self.StepList[-1]).Mag() < threshold:
                self.reset()
        except:
            self.reset()
        self.StepList.append(step)
        self.SetNextPoint(x, y, z)

    def getEndDirection(self):
        # This is meant to calculate the final (asymptotic) particle direction
        # (just before the particle is lost either at the infinity or at the
        # top of the atmosphere).
        #
        # It should in principle be close enough to the latitude/longitude
        # pair stored in the end point (as returned by the non-verbose version
        # of the code).
        d = self.StepList[-2] - self.StepList[-1]
        zenithAngle = RAD_TO_DEG*d.Theta() - 90.
        azimuthAngle = RAD_TO_DEG*d.Phi() - 180.
        return (zenithAngle, azimuthAngle)

    def __str__(self):
        text = 'Start point: %s\n\n' % self.StartPoint
        for i, point in enumerate(self.StepList):
            text += '[Point %2d]: %s\n' % (i, point)
        text += '\nEnd point: %s\n' % self.EndPoint
        text += 'End direction: zen = %.2f, azi = %.2f' %\
            self.getEndDirection()
        return text



class TrajectoryTracer:

    def __init__(self, particleName, escapeRadius = 20, startAltitude = 565,
                 stopAltitude = 20, color = ROOT.kBlack,
                 maxSteps = DEFAULT_MAX_STEPS):
        self.Particle = getParticle(particleName)
        self.EscapeRadius = escapeRadius
        self.StartAltitude = startAltitude
        self.StopAltitude = stopAltitude
        self.Color = color
        self.MaxSteps = maxSteps
        self.__TrajectoryPool = []
    
    def getTrajectory(self, energy, startLatitude = 0, startLongitude = 0,
                      startZenith = 0, startAzimuth = 0):
        startPoint = TrajectoryPoint(startLatitude, startLongitude,
                                     self.StartAltitude, startZenith,
                                     startAzimuth)
        t = ParticleTrajectory(self, energy, startPoint)
        opt = '-p "%s" -a %f -r %f -R %f -m %d -e %f -c "%f,%f" -Z %f -A %f' %\
            (self.Particle.NickName, self.StartAltitude, self.StopAltitude,
             self.EscapeRadius, self.MaxSteps, energy, startLatitude,
             startLongitude, startZenith, startAzimuth)
        cmd = 'cd %s; python tji2010x_verbose.py %s' % (TJI_ROOT, opt)
        print('About to execute "%s"...' % cmd)
        output = commands.getoutput(cmd).split('\n')
        for line in output:
            if line.startswith('TrajectoryEndPoint'):
                try:
                    t.setEndPoint(eval(line))
                except NameError:
                    t.reset()
            else:
                (r, theta, phi) = [float(item) for item in line.split()]
                t.addPoint(r, theta, phi)
        t.setColor(self.Color)
        self.__TrajectoryPool.append(t)
        return t

    def getTrajectoryEndPoint(self, energy, startLatitude = 0,
                              startLongitude = 0, startZenith = 0,
                              startAzimuth = 0):
        rigidity = self.Particle.getRigidity(energy)
        tj.inittj(self.Particle.Charge, self.StopAltitude, self.EscapeRadius,
                  self.MaxSteps, self.MaxSteps)
        (escaped, endLatitude, endLongitude, endAltitude, endZenith,
         endAzimuth, minDistance, maxDistance, numSteps) =\
         tj.tji95x(startLatitude, startLongitude ,self.StartAltitude,
                   rigidity, startZenith, startAzimuth, 0, 0)
        return TrajectoryEndPoint(escaped, endLatitude, endLongitude,
                                  endAltitude, endZenith, endAzimuth,
                                  minDistance, maxDistance, numSteps)

    def getTrace(self, energy, startLatitude = 0, startLongitude = 0,
                 startZenith = 0, startAzimuth = 0):
        startPoint = TrajectoryPoint(startLatitude, startLongitude,
                                     self.StartAltitude, startZenith,
                                     startAzimuth)
        endPoint = self.getTrajectoryEndPoint(energy, startLatitude,
                                              startLongitude, startZenith,
                                              startAzimuth)
        return ParticleTrace(self, energy, startPoint, endPoint)

    def getVerticalEnergyCutoff(self, latitude = 0, longitude = 0,
                                precision = 0.01):
        # Being very naive, here. The code does not take into account the
        # penumbral regions, which might be adequate, depending on the
        # application.
        minEnergy = 0
        maxEnergy = 50
        while (maxEnergy - minEnergy) > precision:
            energy = (maxEnergy + minEnergy)/2.0
            endPoint = self.getTrajectoryEndPoint(energy, latitude, longitude,
                                                  0, 0)
            if endPoint.ParticleEscaped:
                maxEnergy = energy
            else:
                minEnergy = energy
        return energy

    def getVerticalRigidityCutoff(self, latitude = 0, longitude = 0,
                                  precision = 0.01):
        # See comment above.
        energy = self.getVerticalEnergyCutoff(latitude, longitude, precision)
        return self.Particle.getRigidity(energy)

    def getEnergyCutoff(self, latitude = 0, longitude = 0, zenith = 0,
                        azimuth = 0, precision = 0.01):
        """
        """
        minEnergy = 0
        maxEnergy = 50
        while (maxEnergy - minEnergy) > precision:
            energy = (maxEnergy + minEnergy)/2.0
            endPoint = self.getTrajectoryEndPoint(energy, latitude, longitude,
                                                  zenith, azimuth)
            if endPoint.ParticleEscaped:
                maxEnergy = energy
            else:
                minEnergy = energy
        return energy

    def getRigidityCutoff(self, latitude = 0, longitude = 0, zenith = 0,
                        azimuth = 0, precision = 0.01):
        """
        """
        energy = self.getEnergyCutoff(latitude, longitude, zenith, azimuth,
                                      precision)
        return self.Particle.getRigidity(energy)


        

if __name__ == '__main__':
    tracer1 = TrajectoryTracer('e-', ROOT.kRed)
    tracer2 = TrajectoryTracer('e+', ROOT.kBlue)
    trajectories = []
    for (i, energy) in enumerate([5, 8, 10, 15, 20]):
        t1 = tracer1.getTrajectory(energy)
        t2 = tracer2.getTrajectory(energy)
        t1.drawView('Side', 'same'*(i != 0))
        t2.drawView('Side', 'same')
        t1.SetLineColor(ROOT.kRed)
        t2.SetLineColor(ROOT.kBlue)
        trajectories.append(t1)
        trajectories.append(t2)
    t1 = tracer1.getTrajectory(tracer1.getVerticalEnergyCutoff()*1.01)
    t1.SetLineStyle(2)
    t1.SetLineColor(ROOT.kRed)
    t1.drawView('Side', 'same')
    t2 = tracer2.getTrajectory(tracer2.getVerticalEnergyCutoff()*1.01)
    t2.SetLineStyle(2)
    t2.SetLineColor(ROOT.kBlue)
    t2.drawView('Side', 'same')
