C
C........+.........+.........+.........+.........+.........+.........+..
C     Multi-platform COSMIC-RAY TRAJECTORY PROGRAM
C     FORTRAN 77     transportable version
C     Read in control card; LAT, LON, RIG, ZENITH, AZIMUTH, DELPC, INDO
C          Then calculate        INDO    trajectories
C               Starting at      PC
C               Incrementing at  DELPC   intervals
C     Includes conversion from Geodetic to Geocentric coordinates
C     Includes re-entrant albedo calculations
C     Uses subroutine SINGLTJE to do trajectory calculations
C     Magnetic field - IGRF 1995 (order 10)                          ###
C........+.........+.........+.........+.........+.........+.........+..
C     Restrictions: Cannot run over N or S pole; will get BETA blowup
C........+.........+.........+.........+.........+.........+.........+..
C     Mod History
CLast Mod 21 Dec 00  Make all intrinsic function double precision for PC
C     Mod 20 Dec 00  Insert 8 character format 1000 with AZ & ZE
C     Mod 17 Feb 99  set limit to 600000
C     Mod 17 Feb 99  if (ymax.lt.6.6) IFATE = 3
C     Mod    Aug 97  Adjust step size to minimize beta problems
C     Mod    Jan 97  High latitude step size adjust, introduce AHLT
C     Mod    Jun 96  EDIF limit set to 1.0e-5
C     Mod    Jun 96  IERRPT formats, Boundary and look ahead 
C     Mod    Feb 96  Standard reference TJ1V line check
C     Mod    Dec 94  Print out start and end times of PC run
C     **************************************************************
C          Timing estimates base on COMPAQ Digital FORTRAN
C     Will run on PIII PC at 850 MHZ        55000 steps/sec (Real*8)
C     Will run on PIII PC at 700 MHZ        39000 steps/sec (Real*8)
C     Will run on PIII PC at 550 MHZ        32000 steps/sec (Real*8)
C     Will run on PII  PC at 400 MHZ        23000 steps/sec (Real*8)
C     **************************************************************
C     *  TAPE*       Monitor program operation
C     *  TAPE1       Trajectory control cards
C     *  TAPE7       80  character line (card image)  output
C     *  TAPE8       132 character line printer output
C     *  TAPE16      Diagnostic output for trouble shooting
C     *              Normally turned off (open statement commented out)
C     **************************************************************
C........+.........+.........+.........+.........+.........+.........+..
C     Programmer - Don F. Smart; FORTRAN77
C     Note - The programming adheres to the conventional FORTRAN
C            default standard that variables beginning with
C            'i','j','k','l','m',or 'n' are integer variables
C            Variables beginning with "c" are character variables
C            All other variables are real
C........+........+.........+.........+.........+.........+.........+..
C            Do not mix different type variables in same common block
C            Some computers do not allow this
C........+.........+.........+.........+.........+.........+.........+..
C

C........+.........+.........+.........+.........+.........+.........+..
C
     

      SUBROUTINE INITTJ(CS,RH,DOUT,FSTP,LMT)

      IMPLICIT INTEGER (I-N)
      IMPLICIT REAL * 8(A-B)
      IMPLICIT REAL * 8(D-H)
      IMPLICIT REAL * 8(O-Z)

      REAL RH,DOUT,FSTP
      INTEGER LMT,CS
      

C
C........+.........+.........+.........+.........+.........+.........+..
C
      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,B
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
      COMMON /TRIG/   PI,RAD,PIO2
      COMMON /GEOID/  ERADPL, ERECSQ
      COMMON /SNGLR/  SALT,DISOUT,GCLATD,GDLATD,GLOND,GDAZD,GDZED,
     *                RY1,RY2,RY3,RHT,TSTEP,ICHRSIGN
      COMMON /SNGLI/  LIMIT,NTRAJC,IERRPT
      COMMON /OUTPUT/ FDIR1,FDIR2,FDIR3,ASLON,ASLAT,ASRAD,AMINDIST,
     *                AMAXDIST,NSTEPS
      COMMON /FSTEP/  FSTEP,INIT
    

Cf2py intent(in) CS
Cf2py intent(in) RHT
Cf2py intent(in) DOUT
Cf2py intent(in) FSTP
Cf2py intent(in) LMT
   
      DISOUT = DOUT
      RHT=RH
      FSTEP = FSTP
      LIMIT = LMT
      ICHRSIGN = CS
      INIT = 1

      END
C
C........+.........+.........+.........+.........+.........+.........+..
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ User defined program control 
C........+.........+.........+.........+.........+.........+.........+..

      SUBROUTINE TJI95X(LAT, LON, ISALT, RIG, ZENITH, AZIMUTH,IDO,IFLIP,
     *                  IRESULT,LATOUT,LONOUT,ALTOUT,ZENOUT,AZIOUT,
     *                  MNDST,MXDST, NSTP)

      
      IMPLICIT INTEGER (I-N)
      IMPLICIT REAL * 8(A-B)
      IMPLICIT REAL * 8(D-H)
      IMPLICIT REAL * 8(O-Z)

      REAL LAT, LON, ISALT, RIG, ZENITH, AZIMUTH
      REAL LATOUT,LONOUT,ALTOUT,MNDST,MXDST,ZENOUT,AZIOUT
      INTEGER NSTP,IRESULT

      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,B
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
      COMMON /TRIG/   PI,RAD,PIO2
      COMMON /GEOID/  ERADPL, ERECSQ
      COMMON /SNGLR/  SALT,DISOUT,GCLATD,GDLATD,GLOND,GDAZD,GDZED,
     *                RY1,RY2,RY3,RHT,TSTEP,ICHRSIGN
      COMMON /SNGLI/  LIMIT,NTRAJC,IERRPT
      COMMON /OUTPUT/ FDIR1,FDIR2,FDIR3,ASLON,ASLAT,ASRAD,AMINDIST,
     *                AMAXDIST,NSTEPS
      COMMON /FSTEP/  FSTEP,INIT

Cf2py intent(in) LAT
Cf2py intent(in) LON
Cf2py intent(in) RIG
Cf2py intent(in) ZENITH
Cf2py intent(in) AZIMUTH
Cf2py intent(in) ISALT
Cf2py intent(in) IDO
Cf2py intent(in) IFLIP
Cf2py intent(out) IRESULT
Cf2py intent(out) LATOUT
Cf2py intent(out) LONOUT
Cf2py intent(out) ALTOUT
Cf2py intent(out) ZENOUT
Cf2py intent(out) AZIOUT
Cf2py intent(out) MNDST
Cf2py intent(out) MXDST
Cf2py intent(out) NSTP

      DELPC=0.1
      INDO=1

      if (INIT.ne.1) THEN
         write(6,*) "ERROR in tji95x: tji95x not initialized."
	 write(6,*) "Call inittj first"
	 STOP
       ENDIF	 

C      FSTEP = 4.0E08
C      LIMIT = 600000
C
C........+.........+.........+.........+.........+.........+.........+..
C     /\ FSTEP  is total number of steps before run is terminated
C        LIMIT  is       max number of steps before trajectory declared F
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Define program constants
C........+.........+.........+.........+.........+.........+.........+..
C        DISOUT is       radial distance for trajectory termination
C        ERAD   is       average earth radius
C        NTRAJC is       number of trajectory computed in this run
C        RHT    is       top of atmosphere for re-entrant trajectory
C        TSTEP  is       number of steps executed in this run
C........+.........+.........+.........+.........+.........+.........+..
C
      NTRAJC = 0
      TSTEP = 0.0
C
C      DISOUT = 25.0
C      RHT    = 20.0
      ERAD   = 6371.2
      VEL    = 2.99792458E5/ERAD
C
C........+.........+.........+.........+.........+.........+.........+..
C      "VEL" is light velocity in earth radii per second
C      Light speed defined as    299792458 m/s
C       Ref: E. R. Cohne AND B. N. Taylor, "The Fundamental Physical
C            Constants, Physics Today P11, August 1987.
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Define essential trigonometric values
C........+.........+.........+.........+.........+.........+.........+..
C
      PI   = ACOS(-1.0)
      RAD  = 180.0/PI
      PIO2 = PI/2.0
C
     
      GDLATD=LAT
      GLOND=LON
      PC=RIG
      GDZED=ZENITH
      GDAZD=AZIMUTH
      IERRPT=0
      INDEX=0

C.......+.........+.........+.........+.........+.........+.........+..
C     \/ Start at top of atmosphere (20 km above surface of oblate earth)
C        Coding is relic of past when ISALT was read in
C.......+.........+.........+.........+.........+.........+.........+..
C
      IF (ISALT.LE.0) SALT = 20.0
      IF (ISALT.GT.0) SALT = ISALT
C
      KNT = 0
      IDELPC = DELPC*1000.0+0.0001
      INDXPC = PC*1000.0+0.0001
C
C.......+.........+.........+.........+.........+.........+.........+..
C     For trajectories from Earth
C         convert from Geodetic coordinates to Geocentric coordinates
C                Geodetic   coordinates used for input
C                Geocentric coordinates used for output
C        All calculation are done in Geocentric coordinates!
C     \/ Conversion from Geodetic to Geocentric coordinates
C.......+.........+.........+.........+.........+.........+.........+..
C
      CALL GDGC (TCD, TSD,IDO)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Remember positron of initial point on trajectory
C                 in Geocentric coordinates
C        Y(1) is distance in earth radii from geocenter
C             Start with height above geoid and convert to earth radii
C                   The initial values of Y(1), Y(2), and Y(3) are
C                   calculated in subroutine GDGC
C        Coordinate reference system
C            Y(1) = R      = vertical
C            Y(2) = THETA  = south
C            Y(3) = PHI    = east
C........+.........+.........+.........+.........+.........+.........+..
C
      RY2 = Y(2)
      RY3 = Y(3)
      RY1 = Y(1)
C
      GDAZ = GDAZD/RAD
      GDZE = GDZED/RAD
      TSGDZE = SIN(GDZE)
      TCGDZE = COS(GDZE)
      TSGDAZ = SIN(GDAZ)
      TCGDAZ = COS(GDAZ)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Get Y1, Y2, Y3 components in Geodetic coordinates
C         Azimuth is measured clockwise from the north
C         in R, THETA, PHI coordinates, in the THETA-PHI plane
C         The angle is 180 - AZD
C........+.........+.........+.........+.........+.........+.........+..
C
      Y1GD =  TCGDZE
      Y2GD = -TSGDZE*TCGDAZ
      Y3GD =  TSGDZE*TSGDAZ
      
            
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ The small angle delta at the point in space between the
C        downward Geodetic   direction and the
C        downward Geocentric direction is given by
C        DELTA = Geocentric co-latitude + Geodetic latitude  - 90 (deg)
C
C        We are looking up
C           The rotation from Geodetic vertical to Geocentric Vertical
C               Is always rotation toward the equator
C
C     \/ Convert from Geodetic to Geocentric Components for Y1, Y2,
C........+.........+.........+.........+.........+.........+.........+..
C
      IF (IDO.eq.1) THEN
          Y1GC =  Y1GD*TCD+Y2GD*TSD
          Y2GC = -Y1GD*TSD+Y2GD*TCD
          Y3GC =  Y3GD
      ENDIF
      IF (IDO.eq.0) THEN
          Y1GC =  Y1GD
          Y2GC =  Y2GD
          Y3GC =  Y3GD
      ENDIF
      	   

      IF (IFLIP.eq.1) THEN 
          Y1GC =  -Y1GC
          Y2GC =  -Y2GC
          Y3GC =  -Y3GC
      ENDIF
C
C      WRITE (*,1060) GDZED,GDZE,GDAZD,GDAZ,TSGDZE,TCGDZE,TSGDAZ,TCGDAZ
C      WRITE (*,1060) Y1GD,Y2GD,Y3GD,Y1GC,Y2GC,Y3GC
C 1060 FORMAT (' 1050',8F15.5)
C
C........+.........+.........+.........+.........+.........+.........+..
C     ***************************************************
C     Main control of trajectory calculations begins here
C     Trajectories are calculated in subroutine SINGLTJ
C     ***************************************************
C
C     PC     =  rigidity IN GV
C     INDXPC =  index of rigidity in MV (integer)
C     IRSLT  =  trajectory result
C               IRSLT     +1     allowed
C               IRSLT      0     failed
C               IRSLT     -1     re-entrant 
C........+.........+.........+.........+.........+.........+.........+..
C
C
      CALL SINGLTJ (PC,IRSLT,INDXPC,Y1GC,Y2GC,Y3GC)
C
      KNT = KNT+1
C
C        +.........+.........+.........+.........+.........+.........+..
C        \/ Check termination conditions
C        +.........+.........+.........+.........+.........+.........+..
C

      LONOUT = ASLON
      LATOUT = ASLAT
      ALTOUT = ASRAD
      MNDST  = AMINDIST
      MXDST  = AMAXDIST
      NSTP = NSTEPS
      IRESULT= IRSLT

       
C      ZZ1GD = FDIR1*TCD -FDIR2*TSD
C      ZZ2GD = FDIR1*TSD +FDIR2*TCD
C      ZZ3GD = FDIR3

      ZZ1GD = FDIR1
      ZZ2GD = FDIR2
      ZZ3GD = FDIR3


      ZZGDZE= ATAN2(SQRT(ZZ2GD*ZZ2GD+ZZ3GD*ZZ3GD),ZZ1GD)
      ZZGDAZ= PI-ATAN2(ZZ3GD,ZZ2GD)
 
      ZENOUT=ZZGDZE*RAD
      AZIOUT=ZZGDAZ*RAD

C
C........+.........+.........+.........+.........+.........+.........+..
C        ************************
C        End of main control loop
C        ************************
C     /\ Go read in next control card
C........+.........+.........+.........+.........+.........+.........+..
C
C
C........+.........+.........+.........+.........+.........+.........+..
C     ******************************
C     End of trajectory calculations
C     ******************************
C........+.........+.........+.........+.........+.........+.........+..
C

C
C........+.........+.........+.........+.........+.........+.........+..
C     Y(1) is R coordinate         Y(2) is THETA coordinate
C     Y(3) is PHI coordinate       Y(4) is V(R)
C     Y(5) is V(THETA)             Y(6) is V(PHI)
C     F(1) is R dot                F(2) is THETA dot
C     F(3) is PHI dot              F(4) is R dot dot
C     F(5) is THETA dot dot        F(6) is PHI dot dot
C     BR   is B(R)                 BT   is B(THETA)
C     BP   is B(PHI)               B    is magnitude of magnetic field
C........+.........+.........+.........+.........+.........+.........+..
C
C     ierrpt vlu  Program Format Variables printed out
C     IERRPT = 1  "MAIN"   1070  Input to SINGLTJ 
C     IERRPT = 1  SINGLTJ  2000  Input to SINGLTJ 
C     IERRPT = 2  SINGLTJ  2070  PC,BETA,KBF,RCKBETA,NSTEP,TBETA,Y,H
C     IERRPT = 4  SINGLTJ  2090  Y,F,ACCER,H,NSTEP
C     IERRPT = 3  SINGLTJ  2100  H,HCK,Y(1),DELACC,PC,NSTEP
C     IERRPT = 3  SINGLTJ  2110  H,HCK,Y(1),RFA,   PC,NSTEP
C     IERRPT = 3  SINGLTJ  2120  H,HCK,Y(1),NAMX,F(ICK),ICK,FOLD(ICK),
C                                ICK,PC,STEP
C     IERRPT = 4  SINGLTJ  2130  Y(1),DISCK,PVEL,H,HSNEK,HOLD,NSTEP
C     IERRPT = 4  SINGLTJ  2140  Y(1),DISCK,PVEL,H,      HOLD,NSTEP
C
      END
      SUBROUTINE GDGC (TCD, TSD,IDO)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Convert from Geodetic to Geocentric coordinates
C        Adopted from NASA ALLMAG
C         GDLATD = Geodetic   latitude (in degrees)
C         GCLATD = Geocentric latitude (in degrees)
C         GDCLT  = Geodetic co-latitude
C         ERPLSQ is earth radius AT poles   squared = 40408585 (km sq)
C         EREQSQ is earth radius AT equator squared = 40680925 (km sq)
C         ERADPR is earth polar      radius = 6356.774733 (km)
C         ERADER is earth equatorial radius = 6378.160001 (km)
C         ERAD   is earth average    radius = 6371.25     (km)
C         ERADFL is flattening factor = 1.0/298.25
C         ERADFL =  (ERADEQ - factor)/ERADEQ
C         ERECSQ is eccentricity squared = 0.00673966
C         ERECSQ =  EREQSQ/ERPLSQ - 1.0
C........+.........+.........+.........+.........+.........+.........+..
C
CLast Mod 15 Jan 97  Common block SNGLR & SNGLI
C     Mod    Feb 96  Standard reference TJ1V line check
C
C........+.........+.........+.........+.........+.........+.........+..
C     Programmer - Don F. Smart; FORTRAN77
C     Note - The programming adheres to the conventional FORTRAN
C            default standard that variables beginning with
C            'i','j','k','l','m',or 'n' are integer variables
C            Variables beginning with "c" are character variables
C            All other variables are real
C........+........+.........+.........+.........+.........+.........+..
C            Do not mix different type variables in same common block
C            Some computers do not allow this
C........+.........+.........+.........+.........+.........+.........+..
C
      IMPLICIT INTEGER (I-N)
      IMPLICIT REAL * 8(A-B)
      IMPLICIT REAL * 8(D-H)
      IMPLICIT REAL * 8(O-Z)
C
C........+.........+.........+.........+.........+.........+.........+..
C
      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,B
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
      COMMON /TRIG/   PI,RAD,PIO2
      COMMON /GEOID/  ERADPL, ERECSQ
      COMMON /SNGLR/  SALT,DISOUT,GCLATD,GDLATD,GLOND,GDAZD,GDZED,
     *                RY1,RY2,RY3,RHT,TSTEP,ICHRSIGN
C
C........+.........+.........+.........+.........+.........+.........+..
C
      ERPLSQ = 40408585.0
      EREQSQ = 40680925.0
      ERADPL = SQRT(ERPLSQ)
      ERECSQ = EREQSQ/ERPLSQ - 1.0
C
      GDCLT = PIO2-GDLATD/RAD
      TSGDCLT = SIN(GDCLT)
      TCGDCLT = COS(GDCLT)
      ONE = EREQSQ*TSGDCLT*TSGDCLT
      TWO = ERPLSQ*TCGDCLT*TCGDCLT
      THREE = ONE+TWO
      RHO = SQRT(THREE)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Get geocentric distance from geocenter in kilometers
C........+.........+.........+.........+.........+.........+.........+..
C
      DISTKM = SQRT(SALT*(SALT+2.0*RHO)+(EREQSQ*ONE+ERPLSQ*TWO)/THREE)
C
C........+.........+.........+.........+.........+.........+.........+..
C     TCD and TSD are sine and cosine of the angle the Geodetic vertical
C         must be rotated to form the Geocentric vertical
C........+.........+.........+.........+.........+.........+.........+..
C
      TCD = (SALT+RHO)/DISTKM
      TSD = (EREQSQ-ERPLSQ)/RHO*TCGDCLT*TSGDCLT/DISTKM
      TCY2 = TCGDCLT*TCD-TSGDCLT*TSD
      TSY2 = TSGDCLT*TCD+TCGDCLT*TSD
C
      if (IDO.eq.1) THEN
	 Y(2) = ACOS(TCY2)
	 Y(3) = GLOND/RAD
	 Y(1) = DISTKM/ERAD
      ENDIF

      if (IDO.eq.0) THEN
	 Y(2) = GDCLT
	 Y(3) = GLOND/RAD
	 Y(1) = SALT/ERAD+1.
      ENDIF
C
      GCLATD = (PIO2-Y(2))*RAD
C
C     WRITE (*,1200) GDLATD,GDCLT,TSGDCLT,TCGDCLT,ONE,TWO,THREE,RHO
C1200 FORMAT (' 1200',8F15.5)
C     WRITE (*,1200) DISTKM,TCD,TSD,TCY2,TSY2,GCLATD
C
      RETURN
      END
      SUBROUTINE SINGLTJ (PC,IRSLT,INDXPC,Y1GC,Y2GC,Y3GC)
C
C........+.........+.........+.........+.........+.........+.........+..
C     Cosmic-ray trajectory calculations subroutine
C                calculates cosmic ray trajectory at rigidity  PC
C........+.........+.........+.........+.........+.........+.........+..
C        PC     = rigidity in GV
C        IRSLT  = trajectory result
C        INDXPC = index of rigidity in mv (integer)
C                 Y1GC,Y2GC,Y3GC are initial geocentric coorinates
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Step size optimization & look ahead for potential BETA problems
C             monitor accelerating terms and reduce step length 
C                     if large increase occurs
C        Restart at smaller step size if BETA error occurs
C........+.........+.........+.........+.........+.........+.........+..
C     Restrictions: Cannot run over N or S pole; will get BETA blowup
C........+.........+.........+.........+.........+.........+.........+..
CLast Mod 17 Feb 99  if (ymax.lt.6.6) IFATE = 3
C     Mod 18 Jan 97  Patch high latitude beta problem
C     Mod    Jan 97  High latitude step size adjust, introduce AHLT
C     Mod    Jun 96  EDIF limit set to 1.0e-5
C     Mod    Jun 96  IERRPT formats, Boundary and look ahead 
C     Mod    FEB 96  standard reference TJ1V (line check 17 Feb)

C........+.........+.........+.........+.........+.........+.........+..
C     Programmer - Don F. Smart; FORTRAN77
C     Note - The programming adheres to the conventional FORTRAN
C            default standard that variables beginning with
C            'i','j','k','l','m',or 'n' are integer variables
C            Variables beginning with "c" are character variables
C            All other variables are real
C........+........+.........+.........+.........+.........+.........+..
C            Do not mix different type variables in same common block
C            Some computers do not allow this
C........+.........+.........+.........+.........+.........+.........+..
C
      IMPLICIT INTEGER (I-N)
      IMPLICIT REAL * 8(A-B)
      IMPLICIT REAL * 8(D-H)
      IMPLICIT REAL * 8(O-Z)
C
C........+.........+.........+.........+.........+.........+.........+..
C
      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,B
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
      COMMON /TRIG/   PI,RAD,PIO2
      COMMON /GEOID/  ERADPL, ERECSQ
      COMMON /SNGLR/  SALT,DISOUT,GCLATD,GDLATD,GLOND,GDAZD,GDZED,
     *                RY1,RY2,RY3,RHT,TSTEP,ICHRSIGN
      COMMON /SNGLI/  LIMIT,NTRAJC,IERRPT
      COMMON /OUTPUT/ FDIR1,FDIR2,FDIR3,ASLON,ASLAT,ASRAD,AMINDIST,
     *                AMAXDIST,NSTEPS

C
C........+.........+.........+.........+.........+.........+.........+..
C
      DIMENSION P(6),Q(6),R(6),S(6),YB(6),FOLD(6),YOLD(6)
C
C........+.........+.........+.........+.........+.........+.........+..
C
      CHARACTER*1 CF,CR
      CHARACTER*6 CNAME
C
      DATA CF,CR / 'F','R'/
      DATA CNAME / '  I95 '/                                            ###
C
C........+.........+.........+.........+.........+.........+.........+..
C
      IF (IERRPT.GT.0) WRITE (16,2000) PC,INDXPC,RY1,RY2,RY3
 2000 FORMAT (' SINGLTJ ',F8.3,I8,3F8.4)
C
      BETAST = 2.0
      LSTEP = 0
      KBF = 0
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Runge-Kutta constants
C........+.........+.........+.........+.........+.........+.........+..
C
      RC1O6 = 1.0/6.0
      SR2 = SQRT(2.0)
      TMS2O2 = (2.0-SR2)/2.0
      TPS2O2 = (2.0+SR2)/2.0
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Initialize Runge-Kutta variables to zero
C........+.........+.........+.........+.........+.........+.........+..
C
  100 DO 110 I = 1, 6
         YB(I) = 0.0
         S(I) = 0.0
         R(I) = 0.0
         Q(I) = 0.0
         P(I) = 0.0
         F(I) = 0.0
  110 CONTINUE
C
      NMAX = 0
      NMIN = 0
      NSTEP = 0
      NSTEPT = 0
C
      TAU = 0.0
      TU100 = 0.0
      YMAX = RY1
      YMIN = DISOUT
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Define initial point at start of trajectory
C........+.........+.........+.........+.........+.........+.........+..
C
      Y(1) = RY1
      Y(2) = RY2
      Y(3) = RY3
      GRNDKM = (ERADPL/SQRT(1.0-ERECSQ*TSY2SQ))
      Y10 = (RHT+GRNDKM)/ERAD
      R1200KM = (ERAD+1200.0)/ERAD
C
C........+.........+.........+.........+.........+.........+.........+..
C     Rigidity = momentum/charge
C                use oxygen 16 as reference isotope
C     Constants used from Handbook of Physics (7-170)
C                         1 amu = 0.931141  GeV
C........+.........+.........+.........+.........+.........+.........+..
C
      ANUC = 16.0
      ZCHARGE = ICHRSIGN*8.0
C
      EMCSQ = 0.931141
      TENG = SQRT((PC*ZCHARGE)**2+(ANUC*EMCSQ)**2)
      EOMC = -8987.566297*ZCHARGE/TENG
      GMA = SQRT(((PC*ZCHARGE)/(EMCSQ*ANUC))**2+1.0)
      BETA = SQRT(1.0-1.0/(GMA*GMA))
      PVEL = VEL*BETA
      HMAX = 1.0/PVEL
      disck = disout - 1.1*hmax*pvel
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Set max step length ("HMAX") to 1 earth radii
C        PVEL  is particle velocity in earth radii per second
C        DISCK is check for approaching termination boundary
C                 (within 1.1 steps)
C........+.........+.........+.........+.........+.........+.........+..
C
      EDIF = BETA*1.0E-4
      if (edif.lt.1.0-5)  edif = 1.0e-5
      if (beta.lt.0.1)    edif = 1.0e-4

C
      Y(4) = BETA*Y1GC
      Y(5) = BETA*Y2GC
      Y(6) = BETA*Y3GC
C
       azd = gdazd
       zed = gdzed
      IAZ = AZD+0.01
      IZE = ZED+0.01

C      write (6,*) "IN: ",Y(1),Y(2),Y(3),Y(4),Y(5),Y(6)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Set HSTART to about 1 % of the time to complete one gyro-radius
C                            in a 1 Gauss field
C        H = [(2.0*PI*33.333*PC)/(BETA*C)]/0.01
C            if restart after BETA error, set HCK to small value
C        Introduce   AHLT  to control step size at high lat (beta problem)
C                    HCK   - reduce step size when large acceleration
C                    HOLD  - last step size used
C                    HCNG  - only allow 20% max growth in step size
C                    HSNEK - attempt to approach boundary quickly
C        Problem at z=90 at high lat
C                add zen angle in deg to reduce first step
C........+.........+.........+.........+.........+.........+.........+..
C
      PTCY2 = ABS(TCY2)
      AHLT = (1.0 + PTCY2)**2
      HSTART = 6.0E-6*PC/(BETA*AHLT + ZED*PTCY2)
      IF (HSTART.LT.1.0E-6) HSTART = 1.0E-6
      HOLD = HSTART
      HCK  = HSTART
      HCNG = HSTART
C
C     WRITE (16, 2010)  HMAX,HOLD,HCK,HCNG,Y(4),Y(5),Y(6),PVEL, NSTEP
C2010 FORMAT (' 2010 ',18X, 4F9.6, 3F9.4, F9.4,9X,15X,I6)
C
C........+.........+.........+.........+.........+.........+.........+..
C     Start Runge-Kutta
C      \/\/\/\/\/\/\/\/
C        \/\/\/\/\/\/
C          \/\/\/\/
C            \/\/
C             \/
C........+.........+.........+.........+.........+.........+.........+..
C     Change in step size criteria, Aug 97
C            remove cos VxB step size, causes problems in tight loops
C            step size is now only a function of B and BETA
C........+.........+.........+.........+.........+.........+.........+..
C
  130 IF (HCK.LT.1.0E-6) HCK = 1.0E-6
      CALL FGRAD
      HB = 1.6E-5*PC/(B*BETA)
      H = HB/BETAST
C
      IF (KBF.GT.0)  H=H/(FLOAT(KBF*2))
      IF (H.GT.HMAX) H = HMAX
      IF (H.GT.HCNG) H = HCNG
      IF (H.GT.HCK)  H = HCK
C
      DO 140 I = 1, 6
         S(I) = H*F(I)
         P(I) = 0.5*S(I)-Q(I)
         YB(I) = Y(I)
         Y(I) = Y(I)+P(I)
         R(I) = Y(I)-YB(I)
         Q(I) = Q(I)+3.0*R(I)-0.5*S(I)
  140 CONTINUE
C
      CALL FGRAD
C
      DO 150 I = 1, 6
         S(I) = H*F(I)
         P(I) = TMS2O2*(S(I)-Q(I))
         YB(I) = Y(I)
         Y(I) = Y(I)+P(I)
         R(I) = Y(I)-YB(I)
         Q(I) = Q(I)+3.0*R(I)-TMS2O2*S(I)
  150 CONTINUE
C
      CALL FGRAD
C
      DO 160 I = 1, 6
         S(I) = H*F(I)
         P(I) = TPS2O2*(S(I)-Q(I))
         YB(I) = Y(I)
         Y(I) = Y(I)+P(I)
         R(I) = Y(I)-YB(I)
         Q(I) = Q(I)+3.0*R(I)-TPS2O2*S(I)
  160 CONTINUE
C
      CALL FGRAD
C
      DO 170 I = 1, 6
         S(I) = H*F(I)
         P(I) = RC1O6*(S(I)-2.0*Q(I))
         YB(I) = Y(I)
         Y(I) = Y(I)+P(I)
         R(I) = Y(I)-YB(I)
         Q(I) = Q(I)+3.0*R(I)-0.5*S(I)
  170 CONTINUE
C
C........+.........+.........+.........+.........+.........+.........+..
C             /\
C            /\/\
C          /\/\/\/\
C        /\/\/\/\/\/\
C      /\/\/\/\/\/\/\/\
c      One Runge-Kutta
c      step completed
C........+.........+.........+.........+.........+.........+.........+..
C
      NSTEP = NSTEP+1
      NSTEPT = NSTEPT + 1
      TAU = TAU+H
      HOLD = H
      HCNG = H*1.2
      HCK  = HCNG
      IF (Y(3).lt.-PI) Y(3)=Y(3)+2*PI
      IF (Y(3).gt.PI) Y(3)=Y(3)-2*PI
C      IF (Y(1).lt.1.3) THEN
C         write(6,*) H,Y(1),Y10
C         H=HSTART
C	 HCNG=HSTART
C	 HCK=HSTART
C      ENDIF	 
     
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Emergency diagnostic printout if desired
C........+.........+.........+.........+.........+.........+.........+..
C     WRITE (16, 2030)     H,  Y(1),Y(2),Y(3),   PVEL,B, NSTEP
C     WRITE (16, 2040)  HB,H,HMAX,HOLD,HCK,HCNG,Y(4),Y(5),Y(6),
C    *                  PVEL,B,NSTEP
C2030 FORMAT (' 2030 ',  9X,    F9.6, 36X,    3F9.5,F9.4,F9.5,18X,I6)
C2040 FORMAT (' 2040 ',        6F9.6,         3F9.5,F9.4,F9.5,18X,I6)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Check for altitude less than 100 km
C        if less than 120 km, compute exact altitude above oblate earth
C           and sum time trajectory is below 100 km altitude.
C        set re-entrant altitude at  RHT  km above oblate earth
C               computed from international reference ellipsoid
C     ...+.........+.........+.........+.........+.........+.........+..
C
C       WRITE(6,*) "  R,TH,PHI = ",Y(1),Y(2),Y(3)
C       WRITE(6,*) "  DIR = ",Y(4),Y(5),Y(6)
      

      IF (Y(1).LT.R1200KM) THEN
         TSY2SQ = SIN(Y(2))**2
         GRNDKM = (ERADPL/SQRT(1.0-ERECSQ*TSY2SQ))
         R1000KM = (1000.0+GRNDKM)/ERAD
         R1200KM = (1200.0+GRNDKM)/ERAD
         IF (Y(1).LT.R1000KM) TU100 = TU100+H
         PSALT = Y(1)*ERAD-GRNDKM
         Y10 = (RHT+GRNDKM)/ERAD
      ELSE
         PSALT = (Y(1)-1.)*ERAD
      
      ENDIF

C
      IF (NSTEP.GT.5)  THEN
          IF (Y(1).LT.Y10) THEN
             IF (IERRPT.GT.2)  WRITE (16, 2045) PSALT, Y(1), Y10 
             IRT = -1
             GO TO 260
          ENDIF
      ENDIF
 2045 FORMAT (' 2045 PSALT,Y(1),Y10',F10.6,1PE14.6,E14.6)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Begin error checks
C        (1) Check for unacceptable changes in BETA
C........+.........+.........+.........+.........+.........+.........+..
C
      RCKBETA = SQRT(Y(4)*Y(4)+Y(5)*Y(5)+Y(6)*Y(6))
      TBETA = BETA-RCKBETA
      IF (ABS(TBETA).GT.EDIF) THEN
         KBF = KBF+1
         BETAST = BETAST + AHLT
         EDIF = 2.0*EDIF
         IF (RCKBETA.GT.(1.0+EDIF)) THEN
             BETAST = BETAST+FLOAT(KBF)*(1.0+AHLT)
C             WRITE (*,2050) KBF,BETA,RCKBETA
C             WRITE (*,2060) Y,H,PC,NSTEP
         ENDIF
C
C         WRITE ( *,2070)  PC,BETA,KBF,RCKBETA,NSTEP,TBETA,Y,H
C
C        +.........+.........+.........+.........+.........+.........+..
C        \/ Check for irrecoverable beta error
C           if KBF > 4, set fate to failed and start next rigidity
C        +.........+.........+.........+.........+.........+.........+..
C
         IF (KBF.lt.5) THEN
            GO TO 100
         ELSE
            IRT = 0
            PATH = -PVEL*TAU
            ISALT = SALT+0.0001
C            WRITE (*,2080)
            GO TO 280
         ENDIF
      ENDIF
C
 2050 FORMAT (' 2050 ','KBF=',i5,5x,' error, the velocity of the ',
     *   'particle (BETA) has exceeded the velocity of light'/
     *   '          BETA at start of trajectory was ',F10.7/
     *   '          BETA now equals                 ',F10.7/
     *   '          reduce step size and try again    ')
 2060 FORMAT (' 2060 ','Y,H,PC,NSTEP=',8F12.6,I10)
 2070 FORMAT (' 2070 ','ERROR, Particle BETA changed;',
     *        ' PC = ',F10.6,' GV'/
     *     6x,' Beta at start was ',F10.7,17X,'KBF=',I6/
     *     6x,' Beta new equals   ',F10.7,10X,'step number',I6/
     *     6x,' Beta difference   ',F10.7/
     *     6x,' step length reduced and trajectory recalculated '/,
     *     6x,'Y=',6F12.6,6X,' H=',F15.7)
 2080    FORMAT (' 2080 ','Irrecoverable BETA error problem'/6x,
     *      ' Set fate to Failed & make path length negative'/6x,
     *      ' Terminate this trajectory & continue with program')
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/ Continue status checks
C       (2) Compute composite acceleration
C     ...+.........+.........+.........+.........+.........+.........+..
C
      ACCER = SQRT(F(4)*F(4)+F(5)*F(5)+F(6)*F(6))
C
      IF (IERRPT.GT.3) WRITE (16,2090) Y, F, ACCER, H, NSTEP
 2090 FORMAT (' Y,F,A,H ',f7.4,5f7.3,1x,1pe8.1,5e8.1,1x,e8.1, e9.2,I6)
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/ Continue status checks, make adjustment latitude dependant
C        (3) Monitor change in composite acceleration
C            If composite acceleration (new-old) change > 5
C            If composite acceleration (new/old) ratio  > 2
C               change step size to a smaller value
C    ....+.........+.........+.........+.........+.........+.........+..
C
      IF (NSTEP.GE.2) THEN
         IF (ACCER.GT.ACCOLD) THEN
            DELACC = ACCER-ACCOLD
            IF (DELACC.GT.5.0)  THEN
               HCK = HCK/(1.0+AHLT)
               IF (IERRPT.GT.2) WRITE (16,2100) 
     *                          H,HCK,y(1),DELACC,PC,NSTEP
               RFA = ACCER/ACCOLD
               IF (RFA.GT.2.0) THEN
                  HCK = HCK/(1.0+AHLT)
                  IF (IERRPT.GT.2) WRITE (16,2110)
     *                             H,HCK,Y(1),RFA,PC,NSTEP
               ENDIF
            ENDIF
         ENDIF
C
 2100    FORMAT (' 2100 ','H-REDUCE',2x,'H=',F8.6,2x,'HCK=',F8.6,2x,
     *    'Y(1)=',f7.4,2X,'DELACC=',F6.2,4X,'PC=',F8.3,4x,'NSTEP=',I8)
 2110    FORMAT (' 2110 ','H-REDUCE',2x,'H=',F8.6,2x,'HCK=',F8.6,2x,
     *    'Y(1)=',f7.4,4X,' RFA=',F6.2,4X,'PC=',F8.3,4x,'NSTEP=',I8)
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/ Continue status checks, make adjustment latitude dependant
C        (4) Monitor change in acceleration components
C            If change in any acceleration component is more than
C               a factor of 3, reduce step length
C     ...+.........+.........+.........+.........+.........+.........+..
C
         DO 200 ICK = 4, 6
            AFOLD = ABS(FOLD(ICK))
            IF (AFOLD.GT.3.0) THEN
               RFCK = ABS(F(ICK)/AFOLD)
               IF (RFCK.GT.3.0) THEN
                  HCK = HCK/(1.0+AHLT)
                  IF (IERRPT.GT.2)  THEN
                      WRITE (16,2120)  H,HCK,Y(1),NMAX,ICK,F(ICK),
     &                       ICK,FOLD(ICK),PC,NSTEP
                  ENDIF
               ENDIF
            ENDIF
  200    CONTINUE
      ENDIF
C
 2120 FORMAT (' 2120 ','H-reduce',2X,'H=',F8.6,2X,'HCK=',F8.6,2X,
     *        'Y(1)=',F7.4,2X,'NAMX=',I4,2X,'F(',I1,')=',F6.2,2X, 
     *        'FOLD(',I1,')=',F6.2,2X,'PC=',F6.3,2X,'NSTEP=',I6)
C
      ACCOLD = ACCER
C
C........+.........+.........+.........+.........+.........+.........+..
C     /\ Error checks complete
C
C     \/ Find if a max or a min has occurred
C........+.........+.........+.........+.........+.........+.........+..
C
      IF (NSTEP.GT.1) THEN
         IF (YOLD(4).LE.0.0.AND.Y(4).GT.0.0) NMIN = NMIN+1
         IF (YOLD(4).GE.0.0.AND.Y(4).LT.0.0) NMAX = NMAX+1
      ENDIF
C
      IF (Y(1).GT.YMAX) YMAX = Y(1)
      IF (Y(1).LT.YMIN) YMIN = Y(1)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Check for termination conditions
C         Allowed    - radial distance exceeded disout
C         Failed     - number of steps exceeded
C         Re-entrant - trajectory is below "top" of atmosphere
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/ (1) Check for step limit exceeded
C     ...+.........+.........+.........+.........+.........+.........+..
C
      IF (NSTEP.GE.LIMIT) THEN
         IRT = 0
         GO TO 260
      ENDIF
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/  (2) Check if y(1) within 1.1 max step lengths of disout.
C                   if so, reduce step size and
C                          approach boundary at smaller step
C     ...+.........+.........+.........+.........+.........+.........+..
C
      IF (Y(1).GT.DISCK) THEN
         DISTR = ABS(DISOUT - Y(1))
         HSNEK = DISTR/PVEL
         HCNG = HCNG/2.0
         HCK  = HCK/2.0
         IF (HSNEK .LT. HCNG)  HCNG = HSNEK 
         IF (HSNEK .LT. HCK)   HCK  = HSNEK 
         DISCK = DISOUT - DISTR/2.0
         IF (DISCK.GE.DISOUT)  THEN
            DISCK = 24.999
             GO TO  210
         ENDIF
         IF (H.LT.1.0E-5 .OR. HCK.LT.1.0E-5 .OR. HCNG.LT.1.0E-5)  THEN
            H    = 1.0E-5
            HCK  = 1.0E-5
            HCNG = 1.0E-5
         ENDIF
C
         IF (IERRPT.GT.3) WRITE (16,2130) Y(1),DISCK,PVEL,H,HSNEK,NSTEP
C
  210    IF (Y(1).GT.DISOUT) THEN
            IF (H.LE.1.0E-5)  THEN
               IRT = 1
               GO TO 260
            ENDIF
            TAU = TAU - H
            DO 220 I = 1, 6
               Y(I) = YOLD(I)
               F(I) = FOLD(I)
  220       CONTINUE
         endif
         GO TO 130
      ENDIF
 2130 FORMAT (' 2130 ',2X,'Y(1),DISCK,PVEL,H,HSNEK', 
     *        4X,1PE12.6,4X,E12.6,4X,E12.6,4X,2E9.2,22X,I6)
C
C     +.........+.........+.........+.........+.........+.........+..
c     \/ Have penetrated boundary if you are here.
c          if large step size, go back one step and
c             reduce step length (and adjust "TAU")
C     +.........+.........+.........+.........+.........+.........+..
C
  230    IF (Y(1).GT.DISOUT) THEN
C
         IF (IERRPT.GT.3)  WRITE (16, 2140) y(1),disck,pvel,H,nstep
C
         if (h.lt.1.0e-5 .or. hck.lt.1.0e-5 .or. hcng.lt.1.0e-5)  then
            IRT = 1
            go to 260
         else
            hck  = hck/2.0
            hcng = hcng/2.0
            TAU = TAU - H
            DO 240 I = 1, 6
               Y(I) = YOLD(I)
               F(I) = FOLD(I)
  240       CONTINUE
            GO TO 130
         ENDIF
      ENDIF
C
 2140 FORMAT (' 2140 ',2x,'y(1),disck,pvel,H', 
     *        4x,1pe12.6,4x,e12.6,4x,e12.6,4x,e9.2,27x,i6)
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Store values of  Y  and  F  as  FOLD & YOLD
C........+.........+.........+.........+.........+.........+.........+..
C
      DO 250 I = 1, 6
         YOLD(I) = Y(I)
         FOLD(I) = F(I)
  250 CONTINUE
C
      GO TO 130
C
C........+.........+.........+.........+.........+.........+.........+..
C*********************************************************************
C        **********          **********          **********
C                  **********          **********
C                           **********
C                 TRAJECTORY COMPLETE IF YOU ARE HERE
C........+.........+.........+.........+.........+.........+.........+..
C
  260 CONTINUE
C
      IF (Y(1).GE.DISOUT)  IRT = 1
      PATH  = PVEL*TAU
      ISALT = SALT+0.0001
      LSTEP = BETAST - 1.9
C
C........+.........+.........+.........+.........+.........+.........+..
C     \/ Write out results
C        IRT     +1     ALLOWED          (FATE = 0)
C        IRT      0     FAILED           (FATE = 2)
C        IRT     -1     RE-ENTRANT       (FATE = 1)
C........+.........+.........+.........+.........+.........+.........+..
C
      IF (IRT.GT.-2) THEN
C         write (6,*) "OUT:",Y(1),Y(2),Y(3),Y(4),Y(5),Y(6)

         TCY2 = COS(Y(2))
         TSY2 = SIN(Y(2))
         YDA5 = Y(5)*TCY2+Y(4)*TSY2
         ATRG1 = Y(4)*TCY2-Y(5)*TSY2
         ATRG2 = SQRT(Y(6)*Y(6)+YDA5*YDA5)
C  Extrapolation to infinity. 
C  That is not what we need for the moon case
         FASLAT = 0.0
         IF (ATRG1.NE.0.0.AND.ATRG2.NE.0.0) FASLAT = 
     *                                      ATAN2(ATRG1,ATRG2)*RAD
         FASLON = Y(3)*RAD
         IF (Y(6).NE.0.0.AND.YDA5.NE.0.0) FASLON = (Y(3)+ATAN2(Y(6),
     *                                    YDA5))*RAD
         IF (FASLON.LT.-180.0)    FASLON = FASLON+360.0
         IF (FASLON.GT.180.0)  FASLON = FASLON-360.0
C
C End extrapolation.....
         RENLAT = (PIO2-Y(2))*RAD
         RENLON = Y(3)*RAD
         IFATE = 0
	 ASLON = RENLON
	 ASLAT = RENLAT
C	 write (6,*) FASLON,FASLAT,Y(1),Y(2),Y(3),Y(4),Y(5),Y(6)
	 ASRAD = (Y(1)-1.)*ERAD
	 ASRAD = PSALT
	 AMAXDIST = YMAX
	 AMINDIST = YMIN	 
	 NSTEPS  = NSTEP            
	 FDIR1 = Y(4)/BETA
	 FDIR2 = Y(5)/BETA
	 FDIR3 = Y(6)/BETA	 
      ENDIF
C
C      IF (IRT.LE.0) THEN
C         RENLAT = (PIO2-Y(2))*RAD
C         RENLON = Y(3)*RAD
C
C         IFATE = 1
C	 ASLON = RENLON
C	 ASLAT = RENLAT
C	 ASRAD = (Y(1)-1.)*ERAD
C	 AMAXDIST = YMAX
C	 AMINDIST = YMIN	 
C	 NSTEPS  = NSTEP
C	 FDIR1 = Y(4)/BETA
C	 FDIR2 = Y(5)/BETA
C	 FDIR3 = Y(6)/BETA	 
C      ENDIF
C
  280 IF (IRT.EQ.0) THEN
C
         IFATE = 2
         IF (YMAX.LT.6.6) IFATE = 3
      ENDIF
C
      NTRAJC = NTRAJC+1
      TSTEP = TSTEP+FLOAT(NSTEP)
C
C     ...+.........+.........+.........+.........+.........+.........+..
C     \/ Comment out to reduce IO
C     ...+.........+.........+.........+.........+.........+.........+..
C
C     WRITE (*,2210) PC, ZED, AZD, NSTEP, IFATE
C2210 FORMAT (1H+, 22X, 3F7.2,7x,2I6)
C
      IRSLT = IRT
      RETURN
      END
      SUBROUTINE FGRAD
C
C........+.........+.........+.........+.........+.........+.........+..
C     Mod    Feb 96  standard reference TJ1V (line check 17 Feb)
C     Mod 27 Jan 1999  Change MAGNEW to NEWMAG95                        ###
C........+.........+.........+.........+.........+.........+.........+..
C     Programmer   -  Don F. Smart; FORTRAN77
C     Note -  The programming adheres to the conventional FORTRAN
C             default standard that variables beginning with
C            'i','j','k','l','m',or 'n' are integer variables
C             Variables beginning with "c" are character variables
C             All other variables are real
C........+.........+.........+.........+.........+.........+.........+..
C        Do not mix different type variables in same common block
C           Some computers do not allow this
C........+.........+.........+.........+.........+.........+.........+..
C
      IMPLICIT INTEGER (I-N)
      IMPLICIT REAL * 8(A-B)
      IMPLICIT REAL * 8(D-H)
      IMPLICIT REAL * 8(O-Z)
C
C........+.........+.........+.........+.........+.........+.........+..
C
      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,B
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
C
C........+.........+.........+.........+.........+.........+.........+..
C
      F(1) = VEL*Y(4)
      F(2) = VEL*Y(5)/Y(1)
      TSY2 = SIN(Y(2))
      TCY2 = COS(Y(2))
      F(3) = VEL*Y(6)/(Y(1)*TSY2)
      SQY6 = Y(6)*Y(6)/Y(1)
      Y5OY1 = Y(5)/Y(1)
      TAY2 = TSY2/TCY2
C      CALL MAGNEW95                                                     ###
      CALL MAGN2010
      F(4) = EOMC*(Y(5)*BP-Y(6)*BT)+VEL*(Y(5)*Y5OY1+SQY6)
      F(5) = EOMC*(Y(6)*BR-Y(4)*BP)+VEL*(SQY6/TAY2-Y5OY1*Y(4))
      F(6) = EOMC*(Y(4)*BT-Y(5)*BR)-VEL*((Y5OY1*Y(6))/TAY2+Y(4)*Y(6)/
     *       Y(1))
      RETURN
C
C........+.........+.........+.........+.........+.........+.........+..
C     Y(1) is R coordinate         Y(2) is THETA coordinate
C     Y(3) is PHI coordinate       Y(4) is V(R)
C     Y(5) is V(THETA)             Y(6) is V(PHI)
C     F(1) is R dot                F(2) is THETA dot
C     F(3) is PHI dot              F(4) is R dot dot
C     F(5) is THETA dot dot        F(6) is PHI dot dot
C     BR   is B(R)                 BT   is B(THETA)
C     BP   is B(PHI)               B    is magnitude of magnetic field
C........+.........+.........+.........+.........+.........+.........+..
C
      END
      SUBROUTINE MAGN2010
c
c........+.........+.........+.........+.........+.........+.........+..
c     Compute Magnetic field 
c     Derived from NASA (NSSDC) Routine  
c             Modified for 13 order field
c     Coefficients for IGRF-11 Epoch 2010 loaded into this subroutine   ! ###
c     Coefficients obtained from program CNGMAGR11                      ! ###
c........+.........+.........+.........+.........+.........+.........+..
cLastMod 11 Jul 2010  IGRF-11 Epoch 2010 Coefficients loaded            ! ###
C    Mod 14 Sep 2004  Testing Cutoffs
C    Mod 13 Sep 2004  Line check and corrections
c    Mod    Nov 1980  Arguments in labeled common
c........+.........+.........+.........+.........+.........+.........+..
c     Programmer   -  Don F. Smart; FORTRAN77
c     Note -  The programming adheres to the conventional FORTRAN
c             default standard that variables beginning with
c            'I','J','K','L','M',or 'N' are integer variables
c             Variables beginning with "C" are character variables
c             All other variables are REAL*8
c........+.........+.........+.........+.........+.........+.........+..
c
      IMPLICIT INTEGER*4(I-N)                                           ! PC
      IMPLICIT REAL*8(A-B)
      IMPLICIT REAL*8(D-H)
      IMPLICIT REAL*8(O-Z)
c
c........+.........+.........+.........+.........+.........+.........+..
c
      COMMON /WRKVLU/ F(6),Y(6),ERAD,EOMC,VEL,BR,BT,BP,BB
      COMMON /WRKTSC/ TSY2,TCY2,TSY3,TCY3
      COMMON /DCOEF2/ G10,G11,G20,G21,G22,H11,H21,H22
c
c........+.........+.........+.........+.........+.........+.........+..
c
      DIMENSION     G(14,14),BM(14)
c
c........+.........+.........+.........+.........+.........+.........+..
c     \/ Load in data constants if this is the first time called
c        Otherwise, skip to evaluation of magnetic field
c        Designed so the maximum order of expansion can be specified
c........+.........+.........+.........+.........+.........+.........+..
c     GAUSS NORMALIZED SCHMIDT COEFFICIENTS ORDERED FOR FAST COMPUTATION
c........+.........+.........+.........+.........+.........+.........+..
c
      IF (JDATA.EQ.77) GO TO 9920
c
      NMAX= 14
c
c........+.........+.........+.........+.........+.........+.........+..
CARDS FOR FORTRAN   
C     2010.00       G & H COEF IN CNGMAGR11 FORMAT     IGRF-11
      DATA(G(N, 1),N=1,14)/   0.000000E+00,
     1   0.294965E+05,   0.359490E+04,  -0.334925E+04,  -0.399262E+04,
     2   0.181991E+04,  -0.105105E+04,  -0.215573E+04,  -0.122164E+04,
     3  -0.512789E+03,   0.360852E+03,  -0.103335E+04,   0.138641E+04,
     4   0.253921E+03/
      DATA(G(N, 2),N=1,14)/  -0.494510E+04,
     1   0.158590E+04,  -0.524119E+04,   0.712281E+04,  -0.447699E+04,
     2  -0.363150E+04,  -0.129675E+04,   0.266022E+04,  -0.549656E+03,
     3  -0.119759E+04,   0.153270E+04,   0.699580E+03,   0.179405E+03,
     4   0.155716E+04/
      DATA(G(N, 3),N=1,14)/   0.468987E+04,
     1   0.498311E+03,  -0.144505E+04,  -0.238518E+04,  -0.651926E+03,
     2  -0.153935E+04,  -0.113576E+04,   0.136116E+03,   0.813194E+03,
     3  -0.369410E+03,  -0.189623E+03,   0.859001E+03,  -0.238539E+03,
     4  -0.464256E+03/
      DATA(G(N, 4),N=1,14)/   0.491429E+03,
     1  -0.487415E+03,   0.424378E+03,  -0.501379E+03,   0.746928E+03,
     2   0.664517E+03,   0.140874E+04,  -0.927671E+03,   0.236092E+03,
     3   0.439809E+03,   0.181808E+03,  -0.524749E+03,  -0.649221E+03,
     4  -0.513254E+03/
      DATA(G(N, 5),N=1,14)/  -0.158493E+04,
     1   0.826451E+03,  -0.343867E+03,   0.228656E+03,  -0.663340E+02,
     2   0.361842E+03,   0.124962E+03,  -0.172885E+03,   0.516009E+03,
     3  -0.174765E+03,   0.233742E+02,   0.119757E+03,   0.340841E+03,
     4   0.393648E+03/
      DATA(G(N, 6),N=1,14)/  -0.454446E+03,
     1  -0.145174E+04,   0.555804E+03,  -0.221853E+00,  -0.707875E+02,
     2   0.540202E+01,  -0.304813E+02,  -0.642144E+02,  -0.172035E+03,
     3   0.417768E+03,  -0.184789E+03,  -0.792118E+02,  -0.300619E+03,
     4  -0.765466E+03/
      DATA(G(N, 7),N=1,14)/   0.393185E+03,
     1  -0.660535E+03,  -0.612714E+03,   0.361790E+03,  -0.721312E+01,
     2  -0.368760E+02,   0.523249E+02,  -0.387492E+01,  -0.748310E+02,
     3   0.139183E+02,   0.123960E+02,   0.752941E+02,   0.208299E+02,
     4   0.135464E+03/
      DATA(G(N, 8),N=1,14)/   0.205014E+04,
     1   0.613969E+03,  -0.135157E+03,  -0.307488E+03,  -0.432213E+02,
     2   0.670845E+02,   0.220068E+01,  -0.317157E+01,   0.353463E+02,
     3  -0.632816E+02,  -0.440950E+02,  -0.198417E+02,  -0.585269E+02,
     4  -0.213710E+03/
      DATA(G(N, 9),N=1,14)/  -0.730641E+03,
     1   0.112165E+04,  -0.492893E+03,   0.465210E+03,  -0.247671E+03,
     2  -0.487431E+02,   0.270737E+02,  -0.106540E+01,   0.231881E+01,
     3   0.217054E+02,  -0.253660E+02,  -0.409681E+02,   0.234108E+02,
     4   0.285582E+02/
      DATA(G(N,10),N=1,14)/   0.261177E+04,
     1  -0.126034E+04,  -0.106218E+04,   0.405905E+03,   0.249313E+03,
     2  -0.139183E+03,  -0.165738E+02,   0.157623E+02,  -0.426335E+01,
     3   0.615140E+01,   0.265478E+01,  -0.176298E+01,   0.102173E+02,
     4  -0.272292E+02/
      DATA(G(N,11),N=1,14)/  -0.681201E+03,
     1   0.210692E+02,  -0.776818E+03,  -0.514232E+03,   0.532192E+03,
     2   0.413201E+02,   0.801727E+02,   0.163652E+02,   0.530957E+01,
     3   0.492711E+01,   0.166216E+01,  -0.217628E+01,  -0.188649E+01,
     4   0.000000E+00/
      DATA(G(N,12),N=1,14)/  -0.466386E+02,
     1  -0.695382E+03,   0.196781E+03,   0.431125E+03,  -0.142581E+03,
     2   0.376471E+02,   0.124011E+03,   0.295880E+02,   0.185113E+02,
     3   0.516866E+01,   0.104396E+01,  -0.220392E+01,   0.222519E+01,
     4  -0.401473E+01/
      DATA(G(N,13),N=1,14)/   0.717622E+03,
     1  -0.238539E+03,  -0.142829E+04,   0.121729E+04,  -0.167011E+03,
     2  -0.124979E+03,   0.000000E+00,  -0.585269E+01,  -0.766298E+01,
     3   0.848922E+01,   0.556297E+00,  -0.454214E+00,   0.000000E+00,
     4   0.851652E+00/
      DATA(G(N,14),N=1,14)/   0.138414E+04,
     1  -0.464256E+03,  -0.218133E+04,   0.590472E+03,   0.835053E+03,
     2   0.451545E+02,  -0.133569E+03,  -0.142791E+02,  -0.340365E+02,
     3  -0.113554E+02,   0.200736E+01,   0.141942E+01,   0.445394E+00,
     4   0.167023E+00/
      data jmag/ 0/,mgnmax/    14/,gsum/  -0.153063E+06/
      data bm/   0.100461E+06,
     1   0.100461E+06,   0.405608E+05,   0.254751E+05,   0.134128E+05,
     2   0.694107E+04,   0.372912E+04,   0.220961E+04,   0.118415E+04,
     3   0.714057E+03,   0.358455E+03,   0.186548E+03,   0.940565E+02,
     4   0.397991E+02/
C     2010.00       G & H COEF IN CNGMAGR11 FORMAT     IGRF-11
c
c   ******************************************************************
c   *    The array G contains Gauss normalized Schmidt coefficients
c   *    The array G contains both the G and H coefficients
c   *     G(1,1) = 0.0
c   *Schmidt G(N,M) Corresponds to -G(NN+1,MM+1) Gauss normalized coef
c   *Schmidt H(N,M) Corresponds to -G( MI ,NN+1) Gauss normalized coef
c   *                            where MI = M
c   ******************************************************************
c     \/ Derive Schmidt coefficients from the Gauss normalized
c        coefficients used in MAGNEW
c........+.........+.........+.........+.........+.........+.........+..
c
      SR3 = DSQRT(3.0D0)
      SR3O2 = SR3/2.0D0
      G10 = -G(2,1)
      G11 = -G(2,2)
      G20 = -G(3,1)/1.5
      G21 = -G(3,2)/SR3
      G22 = -G(3,3)/SR3O2
      H11 = -G(1,2)
      H21 = -G(1,3)/SR3
      H22 = -G(2,3)/SR3O2
c
      JDATA = 77
c
 9920 CONTINUE
      P21 = TCY2
      P22 = TSY2
      AR   = 1.0/Y(1)
c
c........+.........+.........+.........+.........+.........+.........+..
c     \/ N= 2
c........+.........+.........+.........+.........+.........+.........+..
c
      DP22 =  P21
      TSY3 =  DSIN(Y(3))
      TCY3 =  DCOS(Y(3))
      TSP2 =  TSY3
      TCP2 =  TCY3
      DP21 = -P22
      AOR  =  AR*AR*AR
      RC2  =  G(2,2)*TCP2+G(1,2)*TSP2
      BR   = -(AOR+AOR) *(G(2,1)*P21+RC2*P22)
      BT   =       AOR  *(G(2,1)*DP21+RC2*DP22)
      BP =         AOR  *(G(1,2)*TCP2-G(2,2)*TSP2)*P22
c
C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.3)  GO TO  9999
C     \/                                                      N =  3
C........+.........+.........+.........+.........+.........+.........+..

 9930 TSP3 = (TSP2+TSP2)*TCP2
      TCP3 = (TCP2+TSP2)*(TCP2-TSP2)
      P31  =  P21*P21-0.333333333
      P32  =  P21*P22
      P33  =  P22*P22
      DP31 = -P32-P32
      DP32 =  P21*P21-P33
      DP33 = -DP31
      AOR  = AOR*AR
      RC2  = G(3,2)*TCP2 + G(1,3)*TSP2
      RC3  = G(3,3)*TCP3 + G(2,3)*TSP3
      BR   = BR - 3.0*AOR*(G(3,1)*P31 +RC2 *P32+RC3*P33)
      BT   = BT +     AOR*(G(3,1)*DP31+RC2*DP32+RC3*DP33)
      BP   = BP -    AOR*((G(3,2)*TSP2-G(1,3)*TCP2)*P32
     $          +     2.0*(G(3,3)*TSP3-G(2,3)*TCP3)*P33)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.4)  GO TO  9999
C     \/                                                      N =  4
C........+.........+.........+.........+.........+.........+.........+..

 9940 TSP4 = TSP2*TCP3+TCP2*TSP3
      TCP4 = TCP2*TCP3-TSP2*TSP3
      P41  = P21          *P31-0.26666666 *P21
      DP41 = P21*DP31+DP21*P31-0.26666666*DP21
      P42  = P21          *P32-0.20000000 *P22
      DP42 = P21*DP32+DP21*P32-0.20000000*DP22
      P43  = P21 *P33
      DP43 = P21*DP33+DP21*P33
      P44  = P22 *P33
      DP44 = 3.0 *P43
      AOR  = AOR*AR
      RC2  = G(4,2)*TCP2  + G(1,4)*TSP2
      RC3  = G(4,3)*TCP3  + G(2,4)*TSP3
      RC4  = G(4,4)*TCP4  + G(3,4)*TSP4
      BR   = BR - 4.0*AOR *(G(4,1) *P41+RC2 *P42+RC3* P43+RC4  *P44)
      BT   = BT +     AOR *(G(4,1)*DP41+RC2*DP42+RC3*DP43+RC4*DP44)
      BP   = BP -     AOR*((G(4,2)*TSP2-G(1,4)*TCP2) *P42
     2          +      2.0*(G(4,3)*TSP3-G(2,4)*TCP3) *P43
     3          +      3.0*(G(4,4)*TSP4-G(3,4)*TCP4) *P44)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE. 5)  GO TO  9999
C     \/                                                      N =  5
C........+.........+.........+.........+.........+.........+.........+..

 9950 TSP5 = (TSP3+TSP3)*TCP3
      TCP5 = (TCP3+TSP3)*(TCP3-TSP3)
      P51  = P21          *P41-0.25714285 *P31
      DP51 = P21*DP41+DP21*P41-0.25714285*DP31
      P52  = P21          *P42-0.22857142 *P32
      DP52 = P21*DP42+DP21*P42-0.22857142*DP32
      P53  = P21          *P43-0.14285714 *P33
      DP53 = P21*DP43+DP21*P43-0.14285714*DP33
      P54  = P21 *P44
      DP54 = P21*DP44+DP21*P44
      P55  = P22 *P44
      DP55 = 4.0 *P54
      AOR  = AOR*AR
      RC2  = G(5,2)*TCP2+G(1,5)*TSP2
      RC3  = G(5,3)*TCP3+G(2,5)*TSP3
      RC4  = G(5,4)*TCP4+G(3,5)*TSP4
      RC5  = G(5,5)*TCP5+G(4,5)*TSP5
      BR  = BR - 5.0*AOR*(G(5,1)*P51+RC2*P52+RC3 *P53+RC4* P54+RC5* P55)
      BT  = BT + AOR*(G(5,1)  *DP51+RC2*DP52+RC3*DP53+RC4*DP54+RC5*DP55)
      BP  = BP - AOR*((G(5,2)*TSP2-G(1,5)*TCP2) *P52
     2         +  2.0*(G(5,3)*TSP3-G(2,5)*TCP3) *P53
     3         +  3.0*(G(5,4)*TSP4-G(3,5)*TCP4) *P54
     4         +  4.0*(G(5,5)*TSP5-G(4,5)*TCP5) *P55)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.6)  GO TO  9999
C     \/                                                      N =  6
C........+.........+.........+.........+.........+.........+.........+..

 9960 TSP6 = TSP2*TCP5+TCP2*TSP5
      TCP6 = TCP2*TCP5-TSP2*TSP5
      P61  = P21          *P51-0.25396825 *P41
      DP61 = P21*DP51+DP21*P51-0.25396825*DP41
      P62  = P21          *P52-0.23809523 *P42
      DP62 = P21*DP52+DP21*P52-0.23809523*DP42
      P63  = P21          *P53-0.19047619 *P43
      DP63 = P21*DP53+DP21*P53-0.19047619*DP43
      P64  = P21          *P54-0.11111111 *P44
      DP64 = P21*DP54+DP21*P54-0.11111111*DP44
      P65  = P21 *P55
      DP65 = P21*DP55+DP21*P55
      P66  = P22 *P55
      DP66 = 5.0 *P65
      AOR  = AOR*AR
      RC2  = G(6,2)*TCP2+G(1,6)*TSP2
      RC3  = G(6,3)*TCP3+G(2,6)*TSP3
      RC4  = G(6,4)*TCP4+G(3,6)*TSP4
      RC5  = G(6,5)*TCP5+G(4,6)*TSP5
      RC6  = G(6,6)*TCP6+G(5,6)*TSP6
      BR   = BR - 6.0*AOR*(G(6,1) *P61+RC2 *P62+RC3 *P63+RC4 *P64
     &                                +RC5 *P65+RC6 *P66)
      BT   = BT +     AOR*(G(6,1)*DP61+RC2*DP62+RC3*DP63+RC4*DP64 
     &                                +RC5*DP65+RC6*DP66)
      BP   = BP -     AOR*((G(6,2)*TSP2-G(1,6)*TCP2)*P62
     2          +      2.0*(G(6,3)*TSP3-G(2,6)*TCP3)*P63
     3          +      3.0*(G(6,4)*TSP4-G(3,6)*TCP4)*P64
     4          +      4.0*(G(6,5)*TSP5-G(4,6)*TCP5)*P65
     5          +      5.0*(G(6,6)*TSP6-G(5,6)*TCP6)*P66)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.7)  GO TO  9999
C     \/                                                    N =  7
C........+.........+.........+.........+.........+.........+.........+..

 9970 TSP7 = (TSP4+TSP4)*TCP4
      TCP7 = (TCP4+TSP4)*(TCP4-TSP4)
      P71  = P21 *P61         -0.25252525 *P51
      DP71 = P21*DP61+DP21*P61-0.25252525*DP51
      P72  = P21 *P62         -0.24242424 *P52
      DP72 = P21*DP62+DP21*P62-0.24242424*DP52
      P73  = P21 *P63         -0.21212121 *P53
      DP73 = P21*DP63+DP21*P63-0.21212121*DP53
      P74  = P21 *P64         -0.16161616 *P54
      DP74 = P21*DP64+DP21*P64-0.16161616*DP54
      P75  = P21 *P65         -0.09090909 *P55
      DP75 = P21*DP65+DP21*P65-0.09090909*DP55
      P76  = P21 *P66
      DP76 = P21*DP66+DP21*P66
      P77  = P22 *P66
      DP77 = 6.0 *P76
      AOR  = AOR*AR
      RC2  = G(7,2)*TCP2+G(1,7)*TSP2
      RC3  = G(7,3)*TCP3+G(2,7)*TSP3
      RC4  = G(7,4)*TCP4+G(3,7)*TSP4
      RC5  = G(7,5)*TCP5+G(4,7)*TSP5
      RC6  = G(7,6)*TCP6+G(5,7)*TSP6
      RC7  = G(7,7)*TCP7+G(6,7)*TSP7
      BR   = BR - 7.0*AOR*(G(7,1) *P71+RC2 *P72+RC3 *P73+RC4 *P74
     &                                +RC5 *P75+RC6 *P76+RC7 *P77)
      BT   = BT +     AOR*(G(7,1)*DP71+RC2*DP72+RC3*DP73+RC4*DP74
     &                                +RC5*DP75+RC6*DP76+RC7*DP77)
      BP   = BP -     AOR*((G(7,2)*TSP2-G(1,7)*TCP2)*P72
     2          +      2.0*(G(7,3)*TSP3-G(2,7)*TCP3)*P73
     3          +      3.0*(G(7,4)*TSP4-G(3,7)*TCP4)*P74
     4          +      4.0*(G(7,5)*TSP5-G(4,7)*TCP5)*P75
     5          +      5.0*(G(7,6)*TSP6-G(5,7)*TCP6)*P76
     6          +      6.0*(G(7,7)*TSP7-G(6,7)*TCP7)*P77)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.8)  GO TO  9999
C                                                           N =  8
C........+.........+.........+.........+.........+.........+.........+..

 9980 TSP8 = TSP2*TCP7+TCP2*TSP7
      TCP8 = TCP2*TCP7-TSP2*TSP7
      P81  = P21          *P71-0.25174825 *P61
      DP81 = P21*DP71+DP21*P71-0.25174825*DP61
      P82  = P21          *P72-0.24475524 *P62
      DP82 = P21*DP72+DP21*P72-0.24475524*DP62
      P83  = P21          *P73-0.22377622 *P63
      DP83 = P21*DP73+DP21*P73-0.22377622*DP63
      P84  = P21          *P74-0.18881118 *P64
      DP84 = P21*DP74+DP21*P74-0.18881118*DP64
      P85  = P21          *P75-0.13986013 *P65
      DP85 = P21*DP75+DP21*P75-0.13986013*DP65
      P86  = P21          *P76-0.07692307 *P66
      DP86 = P21*DP76+DP21*P76-0.07692307*DP66
      P87  = P21          *P77
      DP87 = P21*DP77+DP21*P77
      P88  = P22          *P77
      DP88 = 7.0          *P87
      AOR  = AOR*AR
      RC2  = G(8,2)*TCP2+G(1,8)*TSP2
      RC3  = G(8,3)*TCP3+G(2,8)*TSP3
      RC4  = G(8,4)*TCP4+G(3,8)*TSP4
      RC5  = G(8,5)*TCP5+G(4,8)*TSP5
      RC6  = G(8,6)*TCP6+G(5,8)*TSP6
      RC7  = G(8,7)*TCP7+G(6,8)*TSP7
      RC8  = G(8,8)*TCP8+G(7,8)*TSP8
      BR   = BR-8.0*AOR*(G(8,1)*P81+RC2 *P82+RC3 *P83+RC4 *P84
     &                             +RC5 *P85+RC6 *P86+RC7 *P87+RC8 *P88)
      BT   = BT +  AOR*(G(8,1)*DP81+RC2*DP82+RC3*DP83+RC4*DP84
     &                             +RC5*DP85+RC6*DP86+RC7*DP87+RC8*DP88)
      BP   = BP -  AOR*((G(8,2)*TSP2-G(1,8)*TCP2)*P82
     2          +   2.0*(G(8,3)*TSP3-G(2,8)*TCP3)*P83
     3          +   3.0*(G(8,4)*TSP4-G(3,8)*TCP4)*P84
     4          +   4.0*(G(8,5)*TSP5-G(4,8)*TCP5)*P85
     5          +   5.0*(G(8,6)*TSP6-G(5,8)*TCP6)*P86
     6          +   6.0*(G(8,7)*TSP7-G(6,8)*TCP7)*P87
     7          +   7.0*(G(8,8)*TSP8-G(7,8)*TCP8)*P88)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE. 9)  GO TO  9999
C     \/                                                      N =  9
C........+.........+.........+.........+.........+.........+.........+..

 9990 TSP9 = (TSP5+TSP5)*TCP5
      TCP9 = (TCP5+TSP5)*(TCP5-TSP5)
      P91  = P21          *P81-0.25128205 *P71
      DP91 = P21*DP81+DP21*P81-0.25128205*DP71
      P92  = P21          *P82-0.24615384 *P72
      DP92 = P21*DP82+DP21*P82-0.24615384*DP72
      P93  = P21          *P83-0.23076923 *P73
      DP93 = P21*DP83+DP21*P83-0.23076923*DP73
      P94  = P21          *P84-0.20512820 *P74
      DP94 = P21*DP84+DP21*P84-0.20512820*DP74
      P95  = P21          *P85-0.16923076 *P75
      DP95 = P21*DP85+DP21*P85-0.16923076*DP75
      P96  = P21          *P86-0.12307692 *P76
      DP96 = P21*DP86+DP21*P86-0.12307692*DP76
      P97  = P21          *P87-0.0666666 *P77
      DP97 = P21*DP87+DP21*P87-0.0666666*DP77
      P98  = P21          *P88
      DP98 = P21*DP88+DP21*P88
      P99  = P22          *P88
      DP99 = 8.0          *P98
      AOR  = AOR*AR
      RC2  = G(9,2)*TCP2+G(1,9)*TSP2
      RC3  = G(9,3)*TCP3+G(2,9)*TSP3
      RC4  = G(9,4)*TCP4+G(3,9)*TSP4
      RC5  = G(9,5)*TCP5+G(4,9)*TSP5
      RC6  = G(9,6)*TCP6+G(5,9)*TSP6
      RC7  = G(9,7)*TCP7+G(6,9)*TSP7
      RC8  = G(9,8)*TCP8+G(7,9)*TSP8
      RC9  = G(9,9)*TCP9+G(8,9)*TSP9
      BR   = BR - 9.0*AOR*(G(9,1)*P91+RC2 *P92+RC3 *P93+RC4 *P94
     &                               +RC5 *P95+RC6 *P96+RC7 *P97+RC8*P98
     &                               +RC9 *P99)
      BT   = BT + AOR*(G(9,1)   *DP91+RC2*DP92+RC3*DP93+RC4*DP94
     &                               +RC5*DP95+RC6*DP96+RC7*DP97
     &                               +RC8*DP98+RC9*DP99)
      BP   = BP - AOR*((G(9,2)*TSP2-G(1,9)*TCP2)*P92
     2          +  2.0*(G(9,3)*TSP3-G(2,9)*TCP3)*P93
     3          +  3.0*(G(9,4)*TSP4-G(3,9)*TCP4)*P94
     4          +  4.0*(G(9,5)*TSP5-G(4,9)*TCP5)*P95
     5          +  5.0*(G(9,6)*TSP6-G(5,9)*TCP6)*P96
     6          +  6.0*(G(9,7)*TSP7-G(6,9)*TCP7)*P97
     7          +  7.0*(G(9,8)*TSP8-G(7,9)*TCP8)*P98
     8          +  8.0*(G(9,9)*TSP9-G(8,9)*TCP9)*P99)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.10)  GO TO  9999
C     \/                                                      N = 10
C........+.........+.........+.........+.........+.........+.........+..

 9100 TSP10  = TSP2*TCP9+TCP2*TSP9
      TCP10  = TCP2*TCP9-TSP2*TSP9
      P101   = P21          *P91-0.25098039 *P81
      DP101  = P21*DP91+DP21*P91-0.25098039*DP81
      P102   = P21          *P92-0.24705882 *P82
      DP102  = P21*DP92+DP21*P92-0.24705882*DP82
      P103   = P21          *P93-0.23529411 *P83
      DP103  = P21*DP93+DP21*P93-0.23529411*DP83
      P104   = P21          *P94-0.21568627 *P84
      DP104  = P21*DP94+DP21*P94-0.21568627*DP84
      P105   = P21          *P95-0.18823529 *P85
      DP105  = P21*DP95+DP21*P95-0.18823529*DP85
      P106   = P21          *P96-0.15294117 *P86
      DP106  = P21*DP96+DP21*P96-0.15294117*DP86
      P107   = P21          *P97-0.10980392 *P87
      DP107  = P21*DP97+DP21*P97-0.10980392*DP87
      P108   = P21          *P98-0.05882352 *P88
      DP108  = P21*DP98+DP21*P98-0.05882352*DP88
      P109   = P21          *P99
      DP109  = P21*DP99+DP21*P99
      P1010  = P22          *P99
      DP1010 = 9.0          *P109
      AOR    = AOR*AR
      RC2    = G(10,2)*TCP2+G(1,10)*TSP2
      RC3    = G(10,3)*TCP3+G(2,10)*TSP3
      RC4    = G(10,4)*TCP4+G(3,10)*TSP4
      RC5    = G(10,5)*TCP5+G(4,10)*TSP5
      RC6    = G(10,6)*TCP6+G(5,10)*TSP6
      RC7    = G(10,7)*TCP7+G(6,10)*TSP7
      RC8    = G(10,8)*TCP8+G(7,10)*TSP8
      RC9    = G(10,9)*TCP9+G(8,10)*TSP9
      RC10   = G(10,10)*TCP10+G(9,10)*TSP10
      BR = BR -10.0*AOR*(G(10,1)*P101+RC2 *P102+RC3* P103+RC4  *P104
     &                               +RC5 *P105+RC6* P106+RC7  *P107
     &                               +RC8 *P108+RC9* P109+RC10 *P1010)
      BT = BT + AOR*(G(10,1)*DP101   +RC2*DP102+RC3*DP103+RC4 *DP104
     &                               +RC5*DP105+RC6*DP106+RC7 *DP107
     &                               +RC8*DP108+RC9*DP109+RC10*DP1010)
      BP = BP - AOR*((G(10, 2) *TSP2 -G(1,10)*TCP2) *P102
     2         + 2.0*(G(10, 3) *TSP3 -G(2,10)*TCP3) *P103
     3         + 3.0*(G(10, 4) *TSP4 -G(3,10)*TCP4) *P104
     4         + 4.0*(G(10, 5) *TSP5 -G(4,10)*TCP5) *P105
     5         + 5.0*(G(10, 6) *TSP6 -G(5,10)*TCP6) *P106
     6         + 6.0*(G(10, 7) *TSP7 -G(6,10)*TCP7) *P107
     7         + 7.0*(G(10, 8) *TSP8 -G(7,10)*TCP8) *P108
     8         + 8.0*(G(10, 9) *TSP9 -G(8,10)*TCP9) *P109
     9         + 9.0*(G(10,10) *TSP10-G(9,10)*TCP10)*P1010)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.11)  GO TO  9999
C     \/                                                      N = 11
C........+.........+.........+.........+.........+.........+.........+..

 9110 TSP11  = (TSP6+TSP6)*TCP6
      TCP11  = (TCP6+TSP6)*(TCP6-TSP6)
      P111   = P21 *P101          -0.25077399 *P91
      DP111  = P21*DP101+DP21*P101-0.25077399*DP91
      P112   = P21 *P102          -0.24767801 *P92
      DP112  = P21*DP102+DP21*P102-0.24767801*DP92
      P113   = P21 *P103          -0.23839009 *P93
      DP113  = P21*DP103+DP21*P103-0.23839009*DP93
      P114   = P21 *P104          -0.22291021 *P94
      DP114  = P21*DP104+DP21*P104-0.22291021*DP94
      P115   = P21 *P105          -0.20123839 *P95
      DP115  = P21*DP105+DP21*P105-0.20123839*DP95
      P116   = P21 *P106          -0.17337461 *P96
      DP116  = P21*DP106+DP21*P106-0.17337461*DP96
      P117   = P21 *P107          -0.13931888 *P97
      DP117  = P21*DP107+DP21*P107-0.13931888*DP97
      P118   = P21 *P108          -0.09907120 *P98
      DP118  = P21*DP108+DP21*P108-0.09907120*DP98
      P119   = P21 *P109          -0.05263157 *P99
      DP119  = P21*DP109+DP21*P109-0.05263157*DP99
      P1110  = P21 *P1010
      DP1110 = P21*DP1010+DP21*P1010
      P1111  = P22 *P1010
      DP1111 = 10.0 *P1110
      AOR    = AOR*AR
      RC2  = G(11, 2)*TCP2 +G( 1,11)*TSP2
      RC3  = G(11, 3)*TCP3 +G( 2,11)*TSP3
      RC4  = G(11, 4)*TCP4 +G( 3,11)*TSP4
      RC5  = G(11, 5)*TCP5 +G( 4,11)*TSP5
      RC6  = G(11, 6)*TCP6 +G( 5,11)*TSP6
      RC7  = G(11, 7)*TCP7 +G( 6,11)*TSP7
      RC8  = G(11, 8)*TCP8 +G( 7,11)*TSP8
      RC9  = G(11, 9)*TCP9 +G( 8,11)*TSP9
      RC10 = G(11,10)*TCP10+G( 9,11)*TSP10
      RC11 = G(11,11)*TCP11+G(10,11)*TSP11
      BR = BR -11.0*AOR*(G(11,1) *P111+RC2 *P112+RC3  *P113+RC4  *P114
     &                                +RC5 *P115+RC6  *P116+RC7  *P117
     &                                +RC8 *P118+RC9  *P119+RC10 *P1110
     &                                +RC11*P1111)
      BT = BT + AOR*(G(11,1)*DP111    +RC2 *DP112+RC3*DP113+RC4 *DP114
     &                                +RC5 *DP115+RC6*DP116+RC7 *DP117
     &                                +RC8 *DP118+RC9*DP119+RC10*DP1110
     &                                +RC11*DP1111)
      BP = BP - AOR*((G(11, 2) *TSP2 -G( 1,11)*TCP2) *P112
     2        +  2.0*(G(11, 3) *TSP3 -G( 2,11)*TCP3) *P113
     3        +  3.0*(G(11, 4) *TSP4 -G( 3,11)*TCP4) *P114
     4        +  4.0*(G(11, 5) *TSP5 -G( 4,11)*TCP5) *P115
     5        +  5.0*(G(11, 6) *TSP6 -G( 5,11)*TCP6) *P116
     6        +  6.0*(G(11, 7) *TSP7 -G( 6,11)*TCP7) *P117
     7        +  7.0*(G(11, 8) *TSP8 -G( 7,11)*TCP8) *P118
     8        +  8.0*(G(11, 9) *TSP9 -G( 8,11)*TCP9) *P119
     9        +  9.0*(G(11,10)*TSP10 -G( 9,11)*TCP10)*P1110
     &        + 10.0*(G(11,11)*TSP11 -G(10,11)*TCP11)*P1111)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.12)  GO TO  9999
C     \/                                                      N = 12
C........+.........+.........+.........+.........+.........+.........+..

 9120 TSP12  = TSP2*TCP11+TCP2*TSP11
      TCP12  = TCP2*TCP11-TSP2*TSP11
      P121   = P21 *P111            -0.25062656 *P101
      DP121  = P21*DP111 +DP21*P111 -0.25062656*DP101
      P122   = P21 *P112            -0.24812030 *P102
      DP122  = P21*DP112 +DP21*P112 -0.24812030*DP102
      P123   = P21 *P113            -0.24060150 *P103
      DP123  = P21*DP113 +DP21*P113 -0.24060150*DP103
      P124   = P21 *P114            -0.22807017 *P104
      DP124  = P21*DP114 +DP21*P114 -0.22807017*DP104
      P125   = P21 *P115            -0.21052631 *P105
      DP125  = P21*DP115 +DP21*P115 -0.21052631*DP105
      P126   = P21 *P116            -0.18796992 *P106
      DP126  = P21*DP116 +DP21*P116 -0.18796992*DP106
      P127   = P21 *P117            -0.16040100 *P107
      DP127  = P21*DP117 +DP21*P117 -0.16040100*DP107
      P128   = P21 *P118            -0.12781954 *P108
      DP128  = P21*DP118 +DP21*P118 -0.12781954*DP108
      P129   = P21 *P119            -0.09022556 *P109
      DP129  = P21*DP119 +DP21*P119 -0.09022556*DP109
      P1210  = P21 *P1110           -0.04761904 *P1010
      DP1210 = P21*DP1110+DP21*P1110-0.04761904*DP1010
      P1211  = P21 *P1111
      DP1211 = P21*DP1111+DP21*P1111
      P1212  = P22            *P1111
      DP1212 = 11.0           *P1211
      AOR  = AOR*AR
      RC2  = G(12, 2)*TCP2 +G( 1,12)*TSP2
      RC3  = G(12, 3)*TCP3 +G( 2,12)*TSP3
      RC4  = G(12, 4)*TCP4 +G( 3,12)*TSP4
      RC5  = G(12, 5)*TCP5 +G( 4,12)*TSP5
      RC6  = G(12, 6)*TCP6 +G( 5,12)*TSP6
      RC7  = G(12, 7)*TCP7 +G( 6,12)*TSP7
      RC8  = G(12, 8)*TCP8 +G( 7,12)*TSP8
      RC9  = G(12, 9)*TCP9 +G( 8,12)*TSP9
      RC10 = G(12,10)*TCP10+G( 9,12)*TSP10
      RC11 = G(12,11)*TCP11+G(10,12)*TSP11
      RC12 = G(12,12)*TCP12+G(11,12)*TSP12
      BR   = BR - 12.0*AOR*(G(12,1)*P121+RC2 *P122 +RC3 *P123 +RC4 *P124
     &                                  +RC5 *P125 +RC6 *P126 +RC7 *P127
     &                                  +RC8 *P128 +RC9 *P129+RC10*P1210
     &                                  +RC11*P1211+RC12*P1212)
      BT = BT +AOR*(G(12,1)*DP121+RC2*DP122+RC3*DP123+RC4 *DP124
     &                           +RC5*DP125+RC6*DP126+RC7 *DP127
     &                           +RC8*DP128+RC9*DP129+RC10*DP1210
     &                           +RC11*DP1211+RC12 *DP1212)
      BP = BP - AOR*((G(12, 2)*TSP2 -G( 1,12)*TCP2) *P122
     2        +  2.0*(G(12, 3)*TSP3 -G( 2,12)*TCP3) *P123
     3        +  3.0*(G(12, 4)*TSP4 -G( 3,12)*TCP4) *P124
     4        +  4.0*(G(12, 5)*TSP5 -G( 4,12)*TCP5) *P125
     5        +  5.0*(G(12, 6)*TSP6 -G( 5,12)*TCP6) *P126
     6        +  6.0*(G(12, 7)*TSP7 -G( 6,12)*TCP7) *P127
     7        +  7.0*(G(12, 8)*TSP8 -G( 7,12)*TCP8) *P128
     8        +  8.0*(G(12, 9)*TSP9 -G( 8,12)*TCP9) *P129
     9        +  9.0*(G(12,10)*TSP10-G( 9,12)*TCP10)*P1210
     &        + 10.0*(G(12,11)*TSP11-G(10,12)*TCP11)*P1211
     1        + 11.0*(G(12,12)*TSP12-G(11,12)*TCP12)*P1212)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.13)  GO TO  9999
C     \/                                                      N = 13
C........+.........+.........+.........+.........+.........+.........+..

 9130 TSP13  = (TSP7+TSP7)*TCP7
      TCP13  = (TCP7+TSP7)*(TCP7-TSP7)
      P131   = P21            *P121 -0.25051759 *P111
      DP131  = P21*DP121 +DP21*P121 -0.25051759*DP111
      P132   = P21            *P122 -0.24844720 *P112
      DP132  = P21*DP122 +DP21*P122 -0.24844720*DP112
      P133   = P21            *P123 -0.24223602 *P113
      DP133  = P21*DP123 +DP21*P123 -0.24223602*DP113
      P134   = P21            *P124 -0.23188405 *P114
      DP134  = P21*DP124 +DP21*P124 -0.23188405*DP114
      P135   = P21            *P125 -0.21739130 *P115
      DP135  = P21*DP125 +DP21*P125 -0.21739130*DP115
      P136   = P21            *P126 -0.19875776 *P116
      DP136  = P21*DP126 +DP21*P126 -0.19875776*DP116
      P137   = P21            *P127 -0.17598343 *P117
      DP137  = P21*DP127 +DP21*P127 -0.17598343*DP117
      P138   = P21            *P128 -0.14906832 *P118
      DP138  = P21*DP128 +DP21*P128 -0.14906832*DP118
      P139   = P21            *P129 -0.11801242 *P119
      DP139  = P21*DP129 +DP21*P129 -0.11801242*DP119
      P1310  = P21            *P1210-0.08281573 *P1110
      DP1310 = P21*DP1210+DP21*P1210-0.08281573*DP1110
      P1311  = P21            *P1211-0.04347826 *P1111
      DP1311 = P21*DP1211+DP21*P1211-0.04347826*DP1111
      P1312  = P21            *P1212
      DP1312 = P21*DP1212+DP21*P1212
      P1313  = P22            *P1212
      DP1313 = 12.0           *P1312
      AOR = AOR*AR
      RC2  = G(13, 2)*TCP2 +G( 1,13)*TSP2
      RC3  = G(13, 3)*TCP3 +G( 2,13)*TSP3
      RC4  = G(13, 4)*TCP4 +G( 3,13)*TSP4
      RC5  = G(13, 5)*TCP5 +G( 4,13)*TSP5
      RC6  = G(13, 6)*TCP6 +G( 5,13)*TSP6
      RC7  = G(13, 7)*TCP7 +G( 6,13)*TSP7
      RC8  = G(13, 8)*TCP8 +G( 7,13)*TSP8
      RC9  = G(13, 9)*TCP9 +G( 8,13)*TSP9
      RC10 = G(13,10)*TCP10+G( 9,13)*TSP10
      RC11 = G(13,11)*TCP11+G(10,13)*TSP11
      RC12 = G(13,12)*TCP12+G(11,13)*TSP12
      RC13 = G(13,13)*TCP13+G(12,13)*TSP13
      BR = BR -13.0*AOR*(G(13,1)*P131+RC2  *P132 +RC3 *P133 +RC4 *P134
     &                               +RC5  *P135 +RC6 *P136 +RC7 *P137
     &                               +RC8  *P138 +RC9 *P139 +RC10*P1310
     &                               +RC11 *P1311+RC12*P1312+RC13*P1313)
      BT = BT + AOR*(G(13,1)   *DP131+RC2 *DP132+RC3 *DP133 +RC4 *DP134
     &                               +RC5 *DP135+RC6 *DP136 +RC7 *DP137
     &                               +RC8 *DP138+RC9 *DP139 +RC10*DP1310
     &                               +RC11*DP1311+RC12*DP1312
     &                               +RC13*DP1313)
      BP = BP - AOR*((G(13, 2)*TSP2 -G( 1,13)*TCP2) *P132
     2        +  2.0*(G(13, 3)*TSP3 -G( 2,13)*TCP3) *P133
     3        +  3.0*(G(13, 4)*TSP4 -G( 3,13)*TCP4) *P134
     4        +  4.0*(G(13, 5)*TSP5 -G( 4,13)*TCP5) *P135
     5        +  5.0*(G(13, 6)*TSP6 -G( 5,13)*TCP6) *P136
     6        +  6.0*(G(13, 7)*TSP7 -G( 6,13)*TCP7) *P137
     7        +  7.0*(G(13, 8)*TSP8 -G( 7,13)*TCP8) *P138
     8        +  8.0*(G(13, 9)*TSP9 -G( 8,13)*TCP9) *P139
     9        +  9.0*(G(13,10)*TSP10-G( 9,13)*TCP10)*P1310
     &        + 10.0*(G(13,11)*TSP11-G(10,13)*TCP11)*P1311
     1        + 11.0*(G(13,12)*TSP12-G(11,13)*TCP12)*P1312
     2        + 12.0*(G(13,13)*TSP13-G(12,13)*TCP13)*P1313)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.14)  GO TO  9999
C                                                           N = 14
C........+.........+.........+.........+.........+.........+.........+..

 9140 TSP14  = TSP2*TCP13+TCP2*TSP13
      TCP14  = TCP2*TCP13-TSP2*TSP13
      P141   = P21            *P131 -0.25043478 *P121
      DP141  = P21*DP131+DP21 *P131 -0.25043478*DP121
      P142   = P21            *P132 -0.24869565 *P122
      DP142  = P21*DP132+DP21 *P132 -0.24869565*DP122
      P143   = P21            *P133 -0.24347826 *P123
      DP143  = P21*DP133+DP21 *P133 -0.24347826*DP123
      P144   = P21            *P134 -0.23478260 *P124
      DP144  = P21*DP134+DP21 *P134 -0.23478260*DP124
      P145   = P21            *P135 -0.22260869 *P125
      DP145  = P21*DP135+DP21 *P135 -0.22260869*DP125
      P146   = P21            *P136 -0.20695652 *P126
      DP146  = P21*DP136+DP21 *P136 -0.20695652*DP126
      P147   = P21            *P137 -0.18782608 *P127
      DP147  = P21*DP137+DP21 *P137 -0.18782608*DP127
      P148   = P21            *P138 -0.16521739 *P128
      DP148  = P21*DP138+DP21 *P138 -0.16521739*DP128
      P149   = P21            *P139 -0.13913043 *P129
      DP149  = P21*DP139+DP21 *P139 -0.13913043*DP129
      P1410  = P21            *P1310-0.10956521 *P1210
      DP1410 = P21*DP1310+DP21*P1310-0.10956521*DP1210
      P1411  = P21            *P1311-0.07652173 *P1211
      DP1411 = P21*DP1311+DP21*P1311-0.07651273*DP1211
      P1412  = P21            *P1312-0.04000000 *P1212
      DP1412 = P21*DP1312+DP21*P1312-0.04000000*DP1212
      P1413  = P21            *P1313
      DP1413 = P21*DP1313+DP21*P1313
      P1414  = P22            *P1313
      DP1414 = 13.0           *P1413
      AOR = AOR*AR
      RC2  = G(14, 2)*TCP2 +G( 1,14)*TSP2
      RC3  = G(14, 3)*TCP3 +G( 2,14)*TSP3
      RC4  = G(14, 4)*TCP4 +G( 3,14)*TSP4
      RC5  = G(14, 5)*TCP5 +G( 4,14)*TSP5
      RC6  = G(14, 6)*TCP6 +G( 5,14)*TSP6
      RC7  = G(14, 7)*TCP7 +G( 6,14)*TSP7
      RC8  = G(14, 8)*TCP8 +G( 7,14)*TSP8
      RC9  = G(14, 9)*TCP9 +G( 8,14)*TSP9
      RC10 = G(14,10)*TCP10+G( 9,14)*TSP10
      RC11 = G(14,11)*TCP11+G(10,14)*TSP11
      RC12 = G(14,12)*TCP12+G(11,14)*TSP12
      RC13 = G(14,13)*TCP13+G(12,14)*TSP13
      RC14 = G(14,14)*TCP14+G(13,14)*TSP14
      BR = BR -14.0*AOR*(G(14,1)*P141+RC2  *P142+ RC3 *P143+RC4 *P144
     &                               +RC5  *P145+ RC6 *P146+RC7 *P147
     &                               +RC8  *P148+ RC9 *P149+RC10*P1410
     &                               +RC11 *P1411+RC12*P1412 
     &                               +RC13 *P1413+RC14*P1414)
      BT = BT + AOR*(G(14,1)*DP141+RC2 *DP142 +RC3 *DP143 +RC4 *DP144
     &                            +RC5 *DP145 +RC6 *DP146 +RC7 *DP147
     &                            +RC8 *DP148 +RC9 *DP149 +RC10*DP1410
     &                            +RC11*DP1411+RC12*DP1412+RC13*DP1413
     &                            +RC14*DP1414)
      BP= BP - AOR*((G(14,2)*TSP2-G( 1,14)*TCP2)*P142
     2       +               2.0*(G(14, 3)*TSP3 -G( 2,14)*TCP3) *P143
     3       +               3.0*(G(14, 4)*TSP4 -G( 3,14)*TCP4) *P144
     4       +               4.0*(G(14, 5)*TSP5 -G( 4,14)*TCP5) *P145
     5       +               5.0*(G(14, 6)*TSP6 -G( 5,14)*TCP6) *P146
     6       +               6.0*(G(14, 7)*TSP7 -G( 6,14)*TCP7) *P147
     7       +               7.0*(G(14, 8)*TSP8 -G( 7,14)*TCP8) *P148
     8       +               8.0*(G(14, 9)*TSP9 -G( 8,14)*TCP9) *P149
     9       +               9.0*(G(14,10)*TSP10-G( 9,14)*TCP10)*P1410
     &       +              10.0*(G(14,11)*TSP11-G(10,14)*TCP11)*P1411
     1       +              11.0*(G(14,12)*TSP12-G(11,14)*TCP12)*P1412
     2       +              12.0*(G(14,13)*TSP13-G(12,14)*TCP13)*P1413
     3       +              13.0*(G(14,14)*TSP14-G(13,14)*TCP14)*P1414)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.15)  GO TO  9999
C     \/                                                      N = 15
C........+.........+.........+.........+.........+.........+.........+..

 9150 TSP15  = (TSP8+TSP8)*TCP8
      TCP15  = (TCP8+TSP8)*(TCP8-TSP8)
      P151   = P21            *P141  -0.25037037 *P131
      DP151  = P21*DP141+DP21 *P141  -0.25037037*DP131
      P152   = P21            *P142  -0.24888888 *P132
      DP152  = P21*DP142+DP21 *P142  -0.24888888*DP132
      P153   = P21            *P143  -0.24444444 *P133
      DP153  = P21*DP143+DP21 *P143  -0.24444444*DP133
      P154   = P21            *P144  -0.23703703 *P134
      DP154  = P21*DP144+DP21 *P144  -0.23703703*DP134
      P155   = P21            *P145  -0.22666666 *P135
      DP155  = P21*DP145+DP21 *P145  -0.22666666*DP135
      P156   = P21            *P146  -0.21333333 *P136
      DP156  = P21*DP146+DP21 *P146  -0.21333333*DP136
      P157   = P21            *P147  -0.19703703 *P137
      DP157  = P21*DP147+DP21 *P147  -0.19703703*DP137
      P158   = P21            *P148  -0.17777777 *P138
      DP158  = P21*DP148+DP21 *P148  -0.17777777*DP138
      P159   = P21            *P149  -0.15555555 *P139
      DP159  = P21*DP149+DP21 *P149  -0.15555555*DP139
      P1510  = P21            *P1410 -0.13037037 *P1310
      DP1510 = P21*DP1410+DP21*P1410 -0.13037037*DP1310
      P1511  = P21            *P1411 -0.10222222 *P1311
      DP1511 = P21*DP1411+DP21*P1411 -0.10222222*DP1311
      P1512  = P21            *P1412 -0.07111111 *P1312
      DP1512 = P21*DP1412+DP21*P1412 -0.07111111*DP1312
      P1513  = P21            *P1413 -0.03703703 *P1313
      DP1513 = P21*DP1413+DP21*P1413 -0.03703703*DP1313
      P1514  = P21            *P1414 
      DP1514 = P21*DP1414+DP21*P1414
      P1515  = P22            *P1414 
      DP1515 = 14.0           *P1514 
      AOR  = AOR*AR 
      RC2  = G(15, 2) *TCP2+G( 1,15)*TSP2
      RC3  = G(15, 3) *TCP3+G( 2,15)*TSP3
      RC4  = G(15, 4) *TCP4+G( 3,15)*TSP4
      RC5  = G(15, 5) *TCP5+G( 4,15)*TSP5
      RC6  = G(15, 6) *TCP6+G( 5,15)*TSP6
      RC7  = G(15, 7) *TCP7+G( 6,15)*TSP7
      RC8  = G(15, 8) *TCP8+G( 7,15)*TSP8
      RC9  = G(15, 9) *TCP9+G( 8,15)*TSP9
      RC10 = G(15,10)*TCP10+G( 9,15)*TSP10
      RC11 = G(15,11)*TCP11+G(10,15)*TSP11
      RC12 = G(15,12)*TCP12+G(11,15)*TSP12
      RC13 = G(15,13)*TCP13+G(12,15)*TSP13
      RC14 = G(15,14)*TCP14+G(13,15)*TSP14
      RC15 = G(15,15)*TCP15+G(14,15)*TSP15
      BR = BR -15.0*AOR*(G(15,1)*P151+RC2 *P152 +RC3 *P153 +RC4 *P154
     &                               +RC5 *P155 +RC6 *P156 +RC7 *P157
     &                               +RC8 *P158 +RC9 *P159 +RC10*P1510
     &                               +RC11*P1511+RC12*P1512+RC13*P1513
     &                               +RC14*P1514+RC15*P1515)
      BT = BT +AOR*(G(15,1)*DP151+RC2 *DP152 +RC3 *DP153 +RC4 *DP154
     &                           +RC5 *DP155 +RC6 *DP156 +RC7 *DP157
     &                           +RC8 *DP158 +RC9 *DP159 +RC10*DP1510
     &                           +RC11*DP1511+RC12*DP1512+RC13*DP1513
     &                           +RC14*DP1514+RC15*DP1515)
      BP = BP - AOR*((G(15,2)*TSP2-G( 1,15)*TCP2) *P152
     2        +               2.0*(G(15, 3)*TSP3 -G( 2,15)*TCP3) *P153
     3        +               3.0*(G(15, 4)*TSP4 -G( 3,15)*TCP4) *P154
     4        +               4.0*(G(15, 5)*TSP5 -G( 4,15)*TCP5) *P155
     5        +               5.0*(G(15, 6)*TSP6 -G( 5,15)*TCP6) *P156
     6        +               6.0*(G(15, 7)*TSP7 -G( 6,15)*TCP7) *P157
     7        +               7.0*(G(15, 8)*TSP8 -G( 7,15)*TCP8) *P158
     8        +               8.0*(G(15, 9)*TSP9 -G( 8,15)*TCP9) *P159
     9        +               9.0*(G(15,10)*TSP10-G( 9,15)*TCP10)*P1510
     &        +              10.0*(G(15,11)*TSP11-G(10,15)*TCP11)*P1511
     1        +              11.0*(G(15,12)*TSP12-G(11,15)*TCP12)*P1512
     2        +              12.0*(G(15,13)*TSP13-G(12,15)*TCP13)*P1513
     3        +              13.0*(G(15,14)*TSP14-G(13,15)*TCP14)*P1514
     4        +              14.0*(G(15,15)*TSP15-G(14,15)*TCP15)*P1515)
      IF (NMAX.LE.16)  GO TO  9999

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.16)  GO TO  9999
C     \/                                                      N = 16
C........+.........+.........+.........+.........+.........+.........+..

 9160 TSP16   = TSP2*TCP15+TCP2*TSP15
      TCP16   = TCP2*TCP15-TSP2*TSP15
      P161    = P21            *P151  -0.25031928 *P141
      DP161   = P21*DP151+DP21 *P151  -0.25031928*DP141
      P162    = P21            *P152  -0.24904214 *P142
      DP162   = P21*DP152+DP21 *P152  -0.24904214*DP142
      P163    = P21            *P153  -0.24521072 *P143
      DP163   = P21*DP153+DP21 *P153  -0.24521072*DP143
      P164    = P21            *P154  -0.23882503 *P144
      DP164   = P21*DP154+DP21 *P154  -0.23882503*DP144
      P165    = P21            *P155  -0.22988505 *P145
      DP165   = P21*DP155+DP21 *P155  -0.22988505*DP145
      P166    = P21            *P156  -0.21839080 *P146
      DP166   = P21*DP156+DP21 *P156  -0.21839080*DP146
      P167    = P21            *P157  -0.20434227 *P147
      DP167   = P21*DP157+DP21 *P157  -0.20434227*DP147
      P168    = P21            *P158  -0.18773946 *P148
      DP168   = P21*DP158+DP21 *P158  -0.18773946*DP148
      P169    = P21            *P159  -0.16858237 *P149
      DP169   = P21*DP159+DP21 *P159  -0.16858237*DP149
      P1610   = P21            *P1510 -0.14687100 *P1410
      DP1610  = P21*DP1510+DP21*P1510 -0.14687100*DP1410
      P1611   = P21            *P1511 -0.12260536 *P1411
      DP1611  = P21*DP1511+DP21*P1511 -0.12260536*DP1411
      P1612   = P21            *P1512 -0.09578544 *P1412
      DP1612  = P21*DP1512+DP21*P1512 -0.09578544*DP1412
      P1613   = P21            *P1513 -0.06641123 *P1413
      DP1613  = P21*DP1513+DP21*P1513 -0.06641123*DP1413
      P1614   = P21            *P1514 -0.03448275 *P1414
      DP1614  = P21*DP1514+DP21*P1514 -0.03448275*DP1414
      P1615   = P21            *P1515
      DP1615  = P21*DP1515+DP21*P1515
      P1616   = P22            *P1515
      DP1616  = 15.0           *P1615
      AOR  = AOR*AR
      RC2  = G(16, 2)*TCP2+ G( 1,16)*TSP2
      RC3  = G(16, 3)*TCP3+ G( 2,16)*TSP3
      RC4  = G(16, 4)*TCP4+ G( 3,16)*TSP4
      RC5  = G(16, 5)*TCP5+ G( 4,16)*TSP5
      RC6  = G(16, 6)*TCP6+ G( 5,16)*TSP6
      RC7  = G(16, 7)*TCP7+ G( 6,16)*TSP7
      RC8  = G(16, 8)*TCP8+ G( 7,16)*TSP8
      RC9  = G(16, 9)*TCP9+ G( 8,16)*TSP9
      RC10 = G(16,10)*TCP10+G( 9,16)*TSP10
      RC11 = G(16,11)*TCP11+G(10,16)*TSP11
      RC12 = G(16,12)*TCP12+G(11,16)*TSP12
      RC13 = G(16,13)*TCP13+G(12,16)*TSP13
      RC14 = G(16,14)*TCP14+G(13,16)*TSP14
      RC15 = G(16,15)*TCP15+G(14,16)*TSP15
      RC16 = G(16,16)*TCP16+G(15,16)*TSP16
      BR = BR -16.0*AOR*(G(16,1)*P161+RC2 *P162 +RC3 *P163 +RC4 *P164
     &                               +RC5 *P165 +RC6 *P166 +RC7 *P167
     &                               +RC8 *P168 +RC9 *P169 +RC10*P1610
     &                               +RC11*P1611+RC12*P1612+RC13*P1613
     &                               +RC14*P1614+RC15*P1615+RC16*P1616)
      BT = BT +AOR*(G(16,1)*DP161+RC2 *DP162 +RC3 *DP163 +RC4 *DP164
     &                           +RC5 *DP165 +RC6 *DP166 +RC7 *DP167
     &                           +RC8 *DP168 +RC9 *DP169 +RC10*DP1610
     &                           +RC11*DP1611+RC12*DP1612+RC13*DP1613
     &                           +RC14*DP1614+RC15*DP1615+RC16*DP1616)
      BP = BP - AOR*((G(16,2)*TSP2-G( 1,16) *TCP2)*P162
     2        +               2.0*(G(16, 3) *TSP3-G( 2,16) *TCP3)*P163
     3        +               3.0*(G(16, 4) *TSP4-G( 3,16) *TCP4)*P164
     4        +               4.0*(G(16, 5) *TSP5-G( 4,16) *TCP5)*P165
     5        +               5.0*(G(16, 6) *TSP6-G( 5,16) *TCP6)*P166
     6        +               6.0*(G(16, 7) *TSP7-G( 6,16) *TCP7)*P167
     7        +               7.0*(G(16, 8) *TSP8-G( 7,16) *TCP8)*P168
     8        +               8.0*(G(16, 9) *TSP9-G( 8,16) *TCP9)*P169
     9        +               9.0*(G(16,10)*TSP10-G( 9,16)*TCP10)*P1610
     &        +              10.0*(G(16,11)*TSP11-G(10,16)*TCP11)*P1611
     1        +              11.0*(G(16,12)*TSP12-G(11,16)*TCP12)*P1612
     2        +              12.0*(G(16,13)*TSP13-G(12,16)*TCP13)*P1613
     3        +              13.0*(G(16,14)*TSP14-G(13,16)*TCP14)*P1614
     4        +              14.0*(G(16,15)*TSP15-G(14,16)*TCP15)*P1615
     5        +              15.0*(G(16,16)*TSP16-G(15,16)*TCP16)*P1616)
      IF (NMAX.LE.17)  GO TO  9999

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.17)  GO TO  9999
C                                                           N = 17
C........+.........+.........+.........+.........+.........+.........+..

 9170 TSP17  = (TSP9+TSP9)*TCP9
      TCP17  = (TCP9+TSP9)*(TCP9-TSP9)
      P171   = P21            *P161  -0.25027808 *P151
      DP171  = P21*DP161+DP21 *P161  -0.25027808*DP151
      P172   = P21            *P162  -0.24916573 *P152
      DP172  = P21*DP162+DP21 *P162  -0.24916573*DP152
      P173   = P21            *P163  -0.24582869 *P153
      DP173  = P21*DP163+DP21 *P163  -0.24582869*DP153
      P174   = P21            *P164  -0.24026696 *P154
      DP174  = P21*DP164+DP21 *P164  -0.24026696*DP154
      P175   = P21            *P165  -0.23248053 *P155
      DP175  = P21*DP165+DP21 *P165  -0.23248053*DP155
      P176   = P21            *P166  -0.22246941 *P156
      DP176  = P21*DP166+DP21 *P166  -0.22246941*DP156
      P177   = P21            *P167  -0.21023359 *P157
      DP177  = P21*DP167+DP21 *P167  -0.21023359*DP157
      P178   = P21            *P168  -0.19577308 *P158
      DP178  = P21*DP168+DP21 *P168  -0.19577308*DP158
      P179   = P21            *P169  -0.17908787 *P159
      DP179  = P21*DP169+DP21 *P169  -0.17908787*DP159
      P1710  = P21            *P1610 -0.16017797 *P1510
      DP1710 = P21*DP1610+DP21*P1610 -0.16017797*DP1510
      P1711  = P21            *P1611 -0.13904338 *P1511
      DP1711 = P21*DP1611+DP21*P1611 -0.13904338*DP1511
      P1712  = P21            *P1612 -0.11568409 *P1512
      DP1712 = P21*DP1612+DP21*P1612 -0.11568409*DP1512
      P1713  = P21            *P1613 -0.09010011 *P1513
      DP1713 = P21*DP1613+DP21*P1613 -0.09010011*DP1513
      P1714  = P21            *P1614 -0.06229143 *P1514
      DP1714 = P21*DP1614+DP21*P1614 -0.06229143*DP1514
      P1715  = P21            *P1615 -0.03225806 *P1515
      DP1715 = P21*DP1615+DP21*P1615 -0.03225806*DP1515
      P1716  = P21            *P1616
      DP1716 = P21*DP1616+DP21*P1616 
      P1717  = P22            *P1616
      DP1717 = 16.0           *P1716
      AOR  = AOR*AR
      RC2  = G(17, 2)*TCP2 +G( 1,17)*TSP2
      RC3  = G(17, 3)*TCP3 +G( 2,17)*TSP3
      RC4  = G(17, 4)*TCP4 +G( 3,17)*TSP4
      RC5  = G(17, 5)*TCP5 +G( 4,17)*TSP5
      RC6  = G(17, 6)*TCP6 +G( 5,17)*TSP6
      RC7  = G(17, 7)*TCP7 +G( 6,17)*TSP7
      RC8  = G(17, 8)*TCP8 +G( 7,17)*TSP8
      RC9  = G(17, 9)*TCP9 +G( 8,17)*TSP9
      RC10 = G(17,10)*TCP10+G( 9,17)*TSP10
      RC11 = G(17,11)*TCP11+G(10,17)*TSP11
      RC12 = G(17,12)*TCP12+G(11,17)*TSP12
      RC13 = G(17,13)*TCP13+G(12,17)*TSP13
      RC14 = G(17,14)*TCP14+G(13,17)*TSP14
      RC15 = G(17,15)*TCP15+G(14,17)*TSP15
      RC16 = G(17,16)*TCP16+G(15,17)*TSP16
      RC17 = G(17,17)*TCP17+G(16,17)*TSP17
      BR   = BR -17.0*AOR*(G(17,1)*P171+RC2 *P172 +RC3* P173+RC4  *P174
     &                                 +RC5 *P175 +RC6* P176+RC7  *P177
     &                                 +RC8 *P178 +RC9* P179+RC10 *P1710
     &                                 +RC11*P1711+RC12*P1712+RC13*P1713
     &                                 +RC14*P1714+RC15*P1715+RC16*P1716
     &                                 +RC17*P1717)
      BT = BT + AOR*(G(17,1)*DP171+RC2 *DP172 +RC3 *DP173 +RC4 *DP174
     &                            +RC5 *DP175 +RC6 *DP176 +RC7 *DP177
     &                            +RC8 *DP178 +RC9 *DP179 +RC10*DP1710
     &                            +RC11*DP1711+RC12*DP1712+RC13*DP1713
     &                            +RC14*DP1714+RC15*DP1715+RC16*DP1716
     &                            +RC17*DP1717)
      BP = BP - AOR*((G(17,2)*TSP2-G( 1,17) *TCP2) *P172
     2        +               2.0*(G(17, 3) *TSP3-G( 2,17) *TCP3)*P173
     3        +               3.0*(G(17, 4) *TSP4-G( 3,17) *TCP4)*P174
     4        +               4.0*(G(17, 5) *TSP5-G( 4,17) *TCP5)*P175
     5        +               5.0*(G(17, 6) *TSP6-G( 5,17) *TCP6)*P176
     6        +               6.0*(G(17, 7) *TSP7-G( 6,17) *TCP7)*P177
     7        +               7.0*(G(17, 8) *TSP8-G( 7,17) *TCP8)*P178
     8        +               8.0*(G(17, 9) *TSP9-G( 8,17) *TCP9)*P179
     9        +               9.0*(G(17,10)*TSP10-G( 9,17)*TCP10)*P1710
     &        +              10.0*(G(17,11)*TSP11-G(10,17)*TCP11)*P1711
     1        +              11.0*(G(17,12)*TSP12-G(11,17)*TCP12)*P1712
     2        +              12.0*(G(17,13)*TSP13-G(12,17)*TCP13)*P1713
     3        +              13.0*(G(17,14)*TSP14-G(13,17)*TCP14)*P1714
     4        +              14.0*(G(17,15)*TSP15-G(14,17)*TCP15)*P1715
     5        +              15.0*(G(17,16)*TSP16-G(15,17)*TCP16)*P1716
     6        +              16.0*(G(17,17)*TSP17-G(16,17)*TCP17)*P1717)

C........+.........+.........+.........+.........+.........+.........+..
      IF (NMAX.LE.18)  GO TO  9999
C     \/                                                      N = 18
C........+.........+.........+.........+.........+.........+.........+..

 9180 TSP18  = TSP2*TCP17+TCP2*TSP17
      TCP18  = TCP2*TCP17-TSP2*TSP17
      P181   = P21            *P171  -0.25024437 *P161
      DP181  = P21*DP171+DP21 *P171  -0.25024437*DP161
      P182   = P21            *P172  -0.24926686 *P162
      DP182  = P21*DP172+DP21 *P172  -0.24926686*DP162
      P183   = P21            *P173  -0.24633431 *P163
      DP183  = P21*DP173+DP21 *P173  -0.24633431*DP163
      P184   = P21            *P174  -0.24144672 *P164
      DP184  = P21*DP174+DP21 *P174  -0.24144672*DP164
      P185   = P21            *P175  -0.23460410 *P165
      DP185  = P21*DP175+DP21 *P175  -0.23460410*DP165
      P186   = P21            *P176  -0.22580645 *P166
      DP186  = P21*DP176+DP21 *P176  -0.22580645*DP166
      P187   = P21            *P177  -0.21505376 *P167
      DP187  = P21*DP177+DP21 *P177  -0.21505376*DP167
      P188   = P21            *P178  -0.20234604 *P168
      DP188  = P21*DP178+DP21 *P178  -0.20234604*DP168
      P189   = P21            *P179  -0.18768328 *P169
      DP189  = P21*DP179+DP21 *P179  -0.18768328*DP169
      P1810  = P21            *P1710 -0.17106549 *P1610
      DP1810 = P21*DP1710+DP21*P1710 -0.17106549*DP1610
      P1811  = P21            *P1711 -0.15249266 *P1611
      DP1811 = P21*DP1711+DP21*P1711 -0.15249266*DP1611
      P1812  = P21            *P1712 -0.13196480 *P1612
      DP1812 = P21*DP1712+DP21*P1712 -0.13196480*DP1612
      P1813  = P21            *P1713 -0.10948191 *P1613
      DP1813 = P21*DP1713+DP21*P1713 -0.10948191*DP1613
      P1814  = P21            *P1714 -0.08504398 *P1614
      DP1814 = P21*DP1714+DP21*P1714 -0.08504398*DP1614
      P1815  = P21            *P1715 -0.05865102 *P1615
      DP1815 = P21*DP1715+DP21*P1715 -0.05865102*DP1615
      P1816  = P21            *P1716 -0.03030303 *P1616
      DP1816 = P21*DP1716+DP21*P1716 -0.03030303*DP1616
      P1817  = P21            *P1717
      DP1817 = P21*DP1717+DP21*P1717 
      P1818  = P22            *P1717
      DP1818 = 17.0           *P1817
      AOR  = AOR*AR
      RC2  = G(18,2) *TCP2 +G( 1,18)*TSP2
      RC3  = G(18,3) *TCP3 +G( 2,18)*TSP3
      RC4  = G(18,4) *TCP4 +G( 3,18)*TSP4
      RC5  = G(18,5) *TCP5 +G( 4,18)*TSP5
      RC6  = G(18,6) *TCP6 +G( 5,18)*TSP6
      RC7  = G(18,7) *TCP7 +G( 6,18)*TSP7
      RC8  = G(18,8) *TCP8 +G( 7,18)*TSP8
      RC9  = G(18,9) *TCP9 +G( 8,18)*TSP9
      RC10 = G(18,10)*TCP10+G( 9,18)*TSP10
      RC11 = G(18,11)*TCP11+G(10,18)*TSP11
      RC12 = G(18,12)*TCP12+G(11,18)*TSP12
      RC13 = G(18,13)*TCP13+G(12,18)*TSP13
      RC14 = G(18,14)*TCP14+G(13,18)*TSP14
      RC15 = G(18,15)*TCP15+G(14,18)*TSP15
      RC16 = G(18,16)*TCP16+G(15,18)*TSP16
      RC17 = G(18,17)*TCP17+G(16,18)*TSP17
      RC18 = G(18,18)*TCP18+G(17,18)*TSP18
      BR = BR - 18.0*AOR*(G(18,1)*P181+RC2 *P182+ RC3 *P183 +RC4 *P184
     &                                +RC5 *P185+ RC6 *P186 +RC7 *P187
     &                                +RC8 *P188+ RC9 *P189 +RC10*P1810
     &                                +RC11*P1811+RC12*P1812+RC13*P1813
     &                                +RC14*P1814+RC15*P1815+RC16*P1816
     &                                +RC17*P1817+RC18*P1818)
      BT = BT + AOR*(G(18,1)*DP181+RC2 *DP182 +RC3 *DP183 +RC4 *DP184
     &                            +RC5 *DP185 +RC6 *DP186 +RC7 *DP187
     &                            +RC8 *DP188 +RC9 *DP189 +RC10*DP1810
     &                            +RC11*DP1811+RC12*DP1812+RC13*DP1813
     &                            +RC14*DP1814+RC15*DP1815+RC16*DP1816
     &                            +RC17*DP1817+RC18*DP1818)
      BP = BP - AOR*((G(18, 2)*TSP2*-G( 1,18)*TCP2) *P182
     2        +  2.0*(G(18, 3)*TSP3*-G( 2,18)*TCP3) *P183
     3        +  3.0*(G(18, 4)*TSP4*-G( 3,18)*TCP4) *P184
     4        +  4.0*(G(18, 5)*TSP5*-G( 4,18)*TCP5) *P185
     5        +  5.0*(G(18, 6)*TSP6*-G( 5,18)*TCP6) *P186
     6        +  6.0*(G(18, 7)*TSP7*-G( 6,18)*TCP7) *P187
     7        +  7.0*(G(18, 8)*TSP8*-G( 7,18)*TCP8) *P188
     8        +  8.0*(G(18, 9)*TSP9*-G( 8,18)*TCP9) *P189
     9        +  9.0*(G(18,10)*TSP10-G( 9,18)*TCP10)*P1810
     &        + 10.0*(G(18,11)*TSP11-G(10,18)*TCP11)*P1811
     1        + 11.0*(G(18,12)*TSP12-G(11,18)*TCP12)*P1812
     2        + 12.0*(G(18,13)*TSP13-G(12,18)*TCP13)*P1813
     3        + 13.0*(G(18,14)*TSP14-G(13,18)*TCP14)*P1814
     4        + 14.0*(G(18,15)*TSP15-G(14,18)*TCP15)*P1815
     5        + 15.0*(G(18,16)*TSP16-G(15,18)*TCP16)*P1816
     6        + 16.0*(G(18,17)*TSP17-G(16,18)*TCP17)*P1817
     7        + 17.0*(G(18,18)*TSP18-G(17,18)*TCP18)*P1818)

c........+.........+.........+.........+.........+.........+.........+..
c     \/ CONVERT TO UNITS OF GAUSS
c........+.........+.........+.........+.........+.........+.........+..
c
 9999 BP = BP/P22*1.0E-5
      BT = BT*1.0E-5
      BR = BR*1.0E-5
      BB = SQRT(BR*BR+BT*BT+BP*BP)

      RETURN
C
C........+.........+.........+.........+.........+.........+.........+..
C     Y(1) is R coordinate         Y(2) is THETA coordinate
C     Y(3) is PHI coordinate       Y(4) is V(R)
C     Y(5) is V(THETA)             Y(6) is V(PHI)
C     F(1) is R dot                F(2) is THETA dot
C     F(3) is PHI dot              F(4) is R dot dot
C     F(5) is THETA dot dot        F(6) is PHI dot dot
C     BR   is B(R)                 BT   is B(THETA)
C     BP   is B(PHI)               B    is magnitude of magnetic field
C........+.........+.........+.........+.........+.........+.........+..
C
      END
