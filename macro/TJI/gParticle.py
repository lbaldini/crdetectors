#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math


class gParticle:
    
    """ Small utility class describing a particle.
    """

    def __init__(self, name, nickname, identifier, mass, charge):
        """ Constructor.
        """
        self.Name = name
        self.NickName = nickname
        self.Identifier = identifier
        self.Mass = mass
        self.Charge = charge

    def getRigidity(self, energy):
        """ Return the particle rigidity at a given energy.
        """
        return math.sqrt(energy**2 - self.Mass**2)/abs(self.Charge)

    def getEnergy(self, rigidity):
        """ Return the particle energy at a given rigidity.
        """
        return math.sqrt((rigidity*self.Charge)**2 + self.Mass**2)

    def __str__(self):
        """ String formatting.
        """
        return '%s (m = %.3e GeV, Z = %d)' % (self.Name, self.Mass, self.Charge)



if __name__ == '__main__':
    p = gParticle('proton', 'p+', -2212, 0.938, 1)
    print(p)
