#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import tji2010x_verbose as tj

from __particles__ import getParticle


from optparse import OptionParser

parser = OptionParser()
parser.add_option('-p', '--particle-type', dest = 'p', default = 'e-',
                  help = 'particle to trace')
parser.add_option('-e', '--energy', dest = 'e', type = float, default = 10,
                  help = 'particle energy')
parser.add_option('-c', '--coordinates', dest = 'c', default = '0,0',
                  help = 'starting coordinates')
parser.add_option('-a', '--start-altitude', dest = 'a', type = float,
                  default = 565,
                  help = 'starting altitude')
parser.add_option('-r', '--stop-altitude', dest = 'r', type = float,
                  default = 20,
                  help = 'altitude at which the tracing is stopped')
parser.add_option('-R', '--esc-radius', dest = 'R', type = float, default = 20,
                  help = 'escape radius')
parser.add_option('-m', '--max-steps', dest = 'm', type = int,
                  default = 1000000, help = 'maximum number of steps')
parser.add_option('-Z', '--zenith', dest = 'Z', type = float, default = 0,
                  help = 'starting zenith angle')
parser.add_option('-A', '--azimuth', dest = 'A', type = float, default = 0,
                  help = 'starting azimuth angle')
(opts, args) = parser.parse_args()


particle = getParticle(opts.p)
if particle is None:
    sys.exit('Particle %s not defined. Abort.' % opts.p)
(latitude, longitude) = [float(item) for item in opts.c.split(',')]
rigidity = particle.getRigidity(opts.e)

tj.inittj(particle.Charge, opts.r, opts.R, opts.m, opts.m)
(escaped, endLatitude, endLongitude, endAltitude, endZenith,
 endAzimuth, minDistance, maxDistance, numSteps) =\
 tj.tji95x(latitude, longitude, opts.a, rigidity, opts.Z, opts.A, 0, 0) 
escaped = escaped == 1

print 'TrajectoryEndPoint(%s, %s, %s, %s, %s, %s, %s, %s, %s)' %\
    (escaped, endLatitude, endLongitude, endAltitude, endZenith,
     endAzimuth, minDistance, maxDistance, numSteps)
