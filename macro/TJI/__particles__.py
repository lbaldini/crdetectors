#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from gParticle import gParticle

PARTICLE_DICT = {}


def addParticle(particle):
    """ Add a particle to the dictionary.
    """
    PARTICLE_DICT[particle.NickName] = particle

def getParticle(nickname):
    """ Return a particle by nickname.
    """
    try:
        return PARTICLE_DICT[nickname]
    except KeyError:
        return None


PROTON = gParticle('proton', 'p+', -2212, 0.938, 1)
ANTIPROTON = gParticle('antiproton', 'p-', 2212, 0.938, -1)
ELECTRON = gParticle('electron', 'e-', 11, 5.11e-4, -1)
POSITRON = gParticle('positron', 'e+', -11, 5.11e-4, 1)


addParticle(PROTON)
addParticle(ANTIPROTON)
addParticle(ELECTRON)
addParticle(POSITRON)


if __name__ == '__main__':
    for key, value in PARTICLE_DICT.items():
        print('%s: %s' % (key, value))
