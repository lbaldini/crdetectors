#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity
from gLegend import gLegend


c1 = gVertCanvas('cr_spectra_uhecr', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 0.5e9, 1e20, Minimum = 1e-30, Maximum = 1e4,
          XTitle = 'Energy [eV]',
          YTitle = 'Flux [m^{-2} s^{-1} sr^{-1} GeV^{-1}]')
h1.Draw()

c1.Update()
c1.save()
