#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from math import sin, cos, pi



class gDipoleField:

    """ Small graphic class representing a dipole field.
    """

    def __init__(self, shells = [1.2, 1.5, 2., 3.], **kwargs):
        """ Constructor.
        """
        self.__LineColor = kwargs.get('LineColor', ROOT.kBlack)
        self.__LineWidth = kwargs.get('LineWidth', 2)
        self.__LineStyle = kwargs.get('LineStyle', 1)
        self.__ShellList = []
        for L in shells:
            self.addShell(L)

    def addShell(self, L, numPoints = 200):
        """ Add an L-shell at a given McIlwain L.
        """
        shell1 = ROOT.TPolyLine3D()
        shell2 = ROOT.TPolyLine3D()
        for shell in [shell1, shell2]:
            shell.SetLineColor(self.__LineColor)
            shell.SetLineWidth(self.__LineWidth)
            shell.SetLineStyle(self.__LineStyle)
        for i in range(numPoints + 1):
            theta = pi*i/numPoints
            r = L*sin(theta)**2
            x = r*sin(theta)
            y = 0.
            z = r*cos(theta)
            if (x**2 + y**2 + z**2) > 1:
                shell1.SetNextPoint(x, y, z)
                shell2.SetNextPoint(-x, y, z)
        self.__ShellList.append(shell1)
        self.__ShellList.append(shell2)

    def Draw(self):
        """ Draw the field lines.
        """
        for shell in self.__ShellList:
            shell.Draw()
