#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __constants__ import SECS_PER_YEAR
from gCanvas import gCanvas
from gH1F import gH1F

import __utils__


INPUT_ROOT_FILE = ROOT.TFile('data/cr_models.root')
LAT_PRD_FILE = ROOT.TFile('data/lat_cre_anisotropies.root')

protons = INPUT_ROOT_FILE.Get('gcomb_protons_int')
electrons = INPUT_ROOT_FILE.Get('gcomb_all_electrons_int')
positrons = INPUT_ROOT_FILE.Get('gcomb_positrons_int')
antiprotons = INPUT_ROOT_FILE.Get('gcomb_antiprotons_int')
iron = INPUT_ROOT_FILE.Get('gcomb_Fe_int')
allgamma = INPUT_ROOT_FILE.Get('gcomb_allgamma_int')
egb = INPUT_ROOT_FILE.Get('gcomb_egb_int')


N_SIGMA = 2.
CAL_GEO_FACTORS = [0.001, 0.01, 0.1, 1., 10., 100.]
CAL_GEO_FACTORS_LABELS = str(CAL_GEO_FACTORS).strip('][').split(', ')
SPEC_GEO_FACTORS = [0.001, 0.01, 0.1, 1.,]
SPEC_GEO_FACTORS_LABELS = str(SPEC_GEO_FACTORS).strip('][').split(', ')

_x = ROOT.Double()
_y = ROOT.Double()


c1 = gCanvas('cr_dipole_protons', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 9.99, 1e5, Minimum = 1e-5, Maximum = 1,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Minimum dipole anisotropy #delta')
h1.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(protons, ef)
    for j in range(g.GetN()):
        g.GetPoint(j, _x, _y)
        g.SetPoint(j, _x, N_SIGMA*(2./_y)**0.5)
    g.Draw('csame')
    store(g)
    x = 1e3
    y = 1.1*g.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c1.annotate(x, y, text, ndc = False, angle = 30, size = SMALLER_TEXT_SIZE)
c1.annotate(0.15, 0.83, 'Protons')
c1.Update()
c1.save()


c2 = gCanvas('cr_dipole_electrons', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 1000, 9.99, 1e3, Minimum = 1e-4, Maximum = 1,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Minimum dipole anisotropy #delta')
h2.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(electrons, ef)
    for j in range(g.GetN()):
        g.GetPoint(j, _x, _y)
        g.SetPoint(j, _x, 2*(2./_y)**0.5)
    g.Draw('csame')
    store(g)
    x = 1e2
    y = 1.1*g.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c2.annotate(x, y, text, ndc = False, angle = 23, size = SMALLER_TEXT_SIZE)
c2.annotate(0.15, 0.83, 'Electrons + positrons')
g = LAT_PRD_FILE.Get('GraphUL')
g.Draw('lpsame')
c2.Update()
c2.save()
