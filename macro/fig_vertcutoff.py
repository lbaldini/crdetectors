#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gEarthSphere import gEarthSphere
from TJI.gTrajectoryTracer import TrajectoryTracer


EARTH = gEarthSphere(equpadding = 2.6)
LAT = 0.
LON = 0.
ALT = 500.
ENERGIES = [25, 20, 15, 10, 7.5, 5]

tneg = TrajectoryTracer('e-', startAltitude = ALT)
tpos = TrajectoryTracer('e+', startAltitude = ALT)
negCutoff = 1.0002*tneg.getVerticalEnergyCutoff()
posCutoff = 1.0002*tpos.getVerticalEnergyCutoff()

c = gCanvas('vertical_cutoff')
EARTH.DrawTop(zoom = 0.6)
t = tneg.getTrajectory(negCutoff, LAT, LON)
t.SetLineStyle(2)
t.Draw()
t = tpos.getTrajectory(posCutoff, LAT, LON)
t.SetLineStyle(2)
t.Draw()
c.Update()
for energy in ENERGIES:
    t = tneg.getTrajectory(energy, LAT, LON)
    t.Draw()
    t = tpos.getTrajectory(energy, LAT, LON)
    t.Draw()
    c.Update()
c.save()
