#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from gF1 import gF1


ACCELERATOR = 0x1
SPACE = 0x2
BALLOON = 0x3

MARKER_DICT = {ACCELERATOR: 20,
               SPACE: 25,
               BALLOON: 24}


class gSiliconDetector:
    
    """ Small convenience class.
    """
    
    def __init__(self, name, surface, numChannels, startDate,
                 location = ACCELERATOR, power = None, reference = None):
        self.Name = name
        self.Surface = surface
        self.NumChannels = numChannels
        self.StartDate = startDate
        self.Location = location
        self.Power = power
        self.Reference = reference
    
    def draw(self, xexpr = 'NumChannels', yexpr = 'Surface'):
        """
        """
        markerStyle = MARKER_DICT[self.Location]
        x = eval('self.%s' % xexpr)
        y = eval('self.%s' % yexpr)
        if x is None or y is None:
            print('Could not draw %s (%s vs %s)' % (self.Name, xexpr, yexpr))
            return 
        self.Marker = ROOT.TMarker(x, y, markerStyle)
        self.Marker.Draw()
        store(self.Marker)
        text = self.Name
        xpad = 1.10
        ypad = 1.05
        ROOT.gPad.annotate(x*xpad, y*ypad, text, ndc = False,
                           size = SMALLER_TEXT_SIZE)
        



DETECTOR_DICT = {}


def add(name, surface, numChannels, startDate, location = ACCELERATOR,
        power = None, reference = None):
    detector = gSiliconDetector(name, surface, numChannels, startDate,
                                location, power, reference)
    DETECTOR_DICT[name] = detector

# Ground
#add('NA1', 1979)
#add('NA11', 1981)
#add('NA14', 1982)
add('MarkII',
    0.05,
    18432,
    1990,
    ACCELERATOR)
#add('DELPHI', 1990)
#add('ALEPH', 0.49, 95088, 1996, 200)
#add('OPAL', 1991)
#add('CDF', 1992)
add('L3',
    0.25,
    73000,
    1993,
    ACCELERATOR,
    200)
add('CLEO III',
    0.64,
    125000,
    1998,
    ACCELERATOR,
    450)
add('BaBar',
    0.95,
    150000,
    1999,
    ACCELERATOR)
add('CDF run II',
    5.8,
    722432,
    2001,
    ACCELERATOR,
    3.0e3)
add('D0 run II',
    3.0,
    792576,
    2001,
    ACCELERATOR)
add('ALICE',
    0.21 + 1.31 + 5.0,
    9.8e6 + 133e3 + 2.6e6,
    2009,
    ACCELERATOR,
    1.35e3 + 30 + 1.06e3 + 1.75e3 + 850 + 1.15e3)
add('CMS',
    198.0,
    9.3e6,
    2009,
    ACCELERATOR)
add('ATLAS',
    61.1,
    6.3e6,
    2009,
    ACCELERATOR)


add('AMS-01',
    2.0,
    63488,
    1998,
    SPACE,
    45)
add('PaMeLa',
    0.134,
    36864,
    63,
    SPACE,
    37)
add('Agile',
    3.5,
    36864,
    2007,
    SPACE,
    15)
add('Fermi-LAT',
    73.0,
    884736,
    2008,
    SPACE,
    160)
add('AMS-02',
    6.2,
    196608,
    2011,
    SPACE,
    140)


add('ATIC',
    1.0,
    4480,
    2001,
    BALLOON)
add('CREAM-II',
    0.62,
    4992,
    2005,
    BALLOON,
    136)


def draw(detector):
    """
    """
    if detector.Location == ACCELERATOR:
        markerStyle = 20
    elif detector.Location == SPACE:
        markerStyle = 25
    elif detector.Location == BALLOON:
        markerStyle = 24
    else:
        print('Unknown location "%s", skipping...' % detector.Location)
    x = detector.NumChannels
    y = detector.Surface
    m = ROOT.TMarker(x, y, markerStyle)
    store(m)
    m.Draw()
    text = detector.Name
    xpad = 1.10
    ypad = 1.05
    ROOT.gPad.annotate(x*xpad, y*ypad, text, ndc = False,
                       size = SMALLER_TEXT_SIZE)


c1 = gCanvas('si_detector_size', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 100, 2e3, 5e7, Minimum = 0.01, Maximum = 500,
          XTitle = 'Number of readout channels',
          YTitle = 'Silicon area [m^{2}]')
h1.Draw()
f1 = gF1('f1', '[0]*x', 2e3, 5e7, LineColor = ROOT.kGray + 1, LineWidth = 1,
         LineStyle = 2)
for chanDensity in [1e3, 1e4, 1e5, 1e6, 1e7]:
    f1.SetParameter(0, 1./chanDensity)
    f1.DrawCopy('same')
for detector in DETECTOR_DICT.values():
    detector.draw()
c1.Update()
c1.save()


c2 = gCanvas('si_detector_power', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 100, 2e3, 5e7, Minimum = 1, Maximum = 2e4,
          XTitle = 'Number of readout channels',
          YTitle = 'Power consumption [W]')
h2.Draw()
f2 = gF1('f2', '[0]*x', 2e3, 5e7, LineColor = ROOT.kGray + 1, LineWidth = 1,
         LineStyle = 2)
for powerDensity in [1e-5, 1e-4, 1e-3, 1e-2]:
    f2.SetParameter(0, powerDensity)
    f2.DrawCopy('same')
for detector in DETECTOR_DICT.values():
    detector.draw(yexpr = 'Power')
c2.Update()
c2.save()
