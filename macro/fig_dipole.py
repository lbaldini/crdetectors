#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gDipoleField import gDipoleField
from gEarthSphere import gEarthSphere



EARTH = gEarthSphere(rotpadding = 1.8, equpadding = 3.5)
SHELLS = [1.2, 1.5, 2., 3.]
FIELD = gDipoleField(SHELLS)

c = gCanvas('dipole_field')
EARTH.DrawFront(zoom = 0.8)
FIELD.Draw()
EARTH.Axis0.Draw()
EARTH.Axis180.Draw()
for shell in SHELLS:
    x = shell/3.25
    y = 0.
    dx = 0.05
    dy = 0.04
    box = ROOT.TBox(x - dx, y - dy, x + dx, y + dy)
    box.SetFillColor(ROOT.kWhite)
    box.Draw()
    store(box)
    c.annotate(x, 0, '%.1f' % shell, ndc = False, align = 22,
               size = SMALLER_TEXT_SIZE)
c.Update()
c.save()
