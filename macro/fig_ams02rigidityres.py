#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import __csv__
import __pdg__

from __ROOT__ import *
from gCanvas import gCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1


AMS_SC_MDR = 2180. # MV
AMS_PM_MDR = 2140. # MV
B_INNER = 0.14 # T

# From Stefano Di Falco
Z_TKR_LAYERS = [159.05,53.04,29.22,25.24,1.70,-2.29,-25.25,-29.24,-136.00]
# Convert to m.
for i, z in enumerate(Z_TKR_LAYERS):
    Z_TKR_LAYERS[i] = z/100.
L_INNER = Z_TKR_LAYERS[1] - Z_TKR_LAYERS[-2]
L_MAGNET = L_INNER*4./3.
L_1 = Z_TKR_LAYERS[0] - Z_TKR_LAYERS[-2]
L_9 = Z_TKR_LAYERS[1] - Z_TKR_LAYERS[-1]
L_19 = Z_TKR_LAYERS[0] - Z_TKR_LAYERS[-1]

BL = B_INNER*L_MAGNET
BP_INNER = B_INNER*L_INNER**2
BP_1 = BP_INNER*L_1/L_INNER
BP_9 = BP_INNER*L_9/L_INNER*(L_MAGNET/L_INNER)**2
BP_19 = BP_INNER*L_19/L_INNER*(L_MAGNET/L_INNER)**2

SIGMA_HIT = 0.01
TKR_PLANE_X0 = 0.5/9

for quantity in ['L_INNER', 'L_19', 'L_MAGNET', 'B_INNER',
                 'L_19/L_INNER*(L_MAGNET/L_INNER)**2']:
    print('%s: %s' % (quantity, eval(quantity)))


ginner = __csv__.read('data/ams02_res_inner.csv', 'ginner', 'Inner tracker',
                      MarkerStyle = 24)
g1 = __csv__.read('data/ams02_res_1.csv', 'g1', 'Inner tracker + L1',
                  MarkerStyle = 26)
g9 = __csv__.read('data/ams02_res_9.csv', 'g9', 'Inner tracker + L9',
                  MarkerStyle = 25)
g19 = __csv__.read('data/ams02_res_19.csv', 'g19', 'Inner tracker + L1 + L9',
                   MarkerStyle = 20)

c1 = gCanvas('ams02_mdr', Logx = True)
emin = 0.5
emax = 8000.
h1 = gH1F('h1', 'h1', 100, emin, emax, XTitle = 'Rigidity [GV]',
          YTitle = 'Rigidity resolution #sigma_{R}/R',
          Minimum = 0, Maximum = 1.5)
h1.Draw()
ginner.Draw('psame')
#g1.Draw('psame')
#g9.Draw('psame')
g19.Draw('psame')

beta = __pdg__.getBetaFormula(__pdg__.PROTON_MASS)
exprMS = '0.0136*sqrt([0])*(1 + 0.038*log([0]))/(%s*0.3*[1])' % beta
exprHit = '(x*[2]/(37.5*[3]))'
expr = 'sqrt((%s)**2 + (%s)**2)' % (exprHit, exprMS)
f = gF1('f', expr, emin, emax)
for bp in [BP_INNER, BP_19]:
    f.SetParameters(TKR_PLANE_X0, BL, SIGMA_HIT, bp)
    f.DrawCopy('same')
unity = gF1('unity', '1', emin, emax)
unity.Draw('same')
l = gLegend()
l.AddEntry(ginner, options = 'p')
l.AddEntry(g19, options = 'p')
l.Draw()
c1.Update()
c1.save()
