#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy
import pyfits

from __ROOT__ import *
from __logging__ import logger
from __constants__ import SECS_PER_YEAR
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend


CATALOG = pyfits.open('data/gll_psch_v07.fit')['LAT_Point_Source_Catalog']
REFERENCE_EXPOSURE = SECS_PER_YEAR*10000.

data = CATALOG.data
numSources = len(data)


c1 = gCanvas('1fhlcounts', Logy = True)
h1 = ROOT.TH2F('h1', 'h1', 100, 0.5, 5.5, 100, 9.99, 10000)
h1.SetXTitle('Spectral index')
h1.SetYTitle('Integral flux > 10 GeV [m^{-2} year^{-1}]')
h1.Draw()
for entry in data:
    flux = entry['Flux']
    index = entry['Spectral_Index']
    counts = flux*REFERENCE_EXPOSURE
    lon = entry['GLON']
    lat = entry['GLAT']
    if abs(lat) > 10:
        mtype = 24
    else:
        mtype = 20
    m = ROOT.TMarker(index, counts, mtype)
    store(m)
    m.Draw()
m1 = ROOT.TMarker(0.21, 0.90, 24)
m1.SetNDC(True)
m1.Draw()
c1.annotate(0.24, 0.88, '|b| > 10')
m2 = ROOT.TMarker(0.21, 0.85, 20)
m2.SetNDC(True)
m2.Draw()
c1.annotate(0.24, 0.83, '|b| < 10')
c1.Update()
c1.save()

c2 = gCanvas('1fhlcounts_vhe', Logy = True)
h2 = ROOT.TH2F('h2', 'h2', 100, 0.5, 5.5, 100, 0.099, 1000)
h2.SetXTitle('Spectral index')
h2.SetYTitle('Integral flux > 100 GeV [m^{-2} year^{-1}]')
h2.Draw()
n = 0
for entry in data:
    flux = entry['Flux100_500GeV']
    index = entry['Spectral_Index']
    counts = flux*REFERENCE_EXPOSURE
    if counts > 0:
        lon = entry['GLON']
        lat = entry['GLAT']
        if abs(lat) > 10:
            mtype = 24
        else:
            mtype = 20
        m = ROOT.TMarker(index, counts, mtype)
        store(m)
        m.Draw()
        n += 1
logger.info('Sources with measurement above 100 GeV: %d' % n)
m1.Draw()
c1.annotate(0.24, 0.88, '|b| > 10')
m2.Draw()
c1.annotate(0.24, 0.83, '|b| < 10')
c2.Update()
c2.save()
