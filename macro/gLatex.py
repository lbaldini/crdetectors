#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gRootObject import gRootObject


class gLatex(ROOT.TLatex, gRootObject):

    """ Wrapper around the ROOT.TLatex object.
    """

    def __init__(self, x, y, text, **kwargs):
        """ Constructor.
        """
        ROOT.TLatex.__init__(self, x, y, text)
        gRootObject.init(self, **kwargs)

    def shift(self, dx, dy):
        """ Shift the TLatex object.
        """
        self.SetX(self.GetX() + dx)
        self.SetY(self.GetY() + dy)
    


if __name__ == '__main__':
    l = gLatex(0.5, 0.5, 'test', TextColor = ROOT.kBlue)
    l.Draw()
