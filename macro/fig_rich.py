#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __pdg__ import *
from __geometry2d__ import *
from gCanvas import gCanvas
from gPhysicalCanvas import gPhysicalCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend


c1 = gPhysicalCanvas('rich_sketch', 2, 1.7)
box(-0.5, 0.5, 0.8, 0.015)
c1.annotate(-0.35, 0.48, 'Radiator', align = 13, ndc = False)
box(-0.5, -0.5, 0.8, 0.015)
c1.annotate(-0.35, -0.52, 'Detection plane', align = 13, ndc = False)
line(-0.5, 0.75, -0.5, -0.75, LineStyle = 7)
c1.annotate(-0.5, 0.79, 'Charged particle', ndc = False, align = 21)
line(-0.5, 0.5, -0.675, -0.5 + 0.015/2)
line(-0.5, 0.5, -0.325, -0.5 + 0.015/2)
c1.annotate(-0.6, 0.4, '#theta_{c}', ndc = False)
x0 = 0.5
y0 = 0.
n = 15
r = 0.2
dr = 0.075
pitch = 0.025
l = 0.75*pitch
for i in range(-n, n + 1):
    x = x0 + i*pitch
    for j in range(-n, n + 1):
        y = y0 + j*pitch
        b = box(x, y, l, l, LineWidth = 1, LineColor = ROOT.kGray + 1)
        if abs((x - x0)**2 + (y - y0)**2 - r**2) < dr**2:
            b.SetFillColor(ROOT.kBlack)
            b.Draw()
line(-0.1, -0.5, x0 - (n + 0.5)*pitch, y0 - (n + 0.5)*pitch, LineWidth = 1,
     LineStyle = 7)
line(-0.1, -0.5, x0 + (n + 0.5)*pitch, y0 - (n + 0.5)*pitch, LineWidth = 1,
     LineStyle = 7)
c1.Update()
c1.save()
