#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gEarthSphere import gEarthSphere
from TJI.gTrajectoryTracer import TrajectoryTracer


EARTH = gEarthSphere(rotpadding = 1.5)
LAT = 0.
LON = -60.
ALT = 500.
ENERGY = 12

tracer = TrajectoryTracer('p+', startAltitude = ALT)

c = gCanvas('ray_tracing')
EARTH.DrawTop(zoom = 0.9)
t1 = tracer.getTrajectory(ENERGY, LAT, LON, 0, 0)
t1.Draw()
c.Update()
c.save()
