#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


""" Collection of useful text labels.
"""

def axisTitle(label, units):
    """
    """
    return '%s [%s]' % (label, units)


LABEL_ENERGY = 'E'
UNITS_ENERGY = 'GeV'
AXIS_TITLE_ENERGY = axisTitle('Energy', UNITS_ENERGY)

LABEL_KIN_ENERGY = 'E_{k}'
UNITS_KIN_ENERGY = 'GeV'
AXIS_TITLE_KIN_ENERGY = axisTitle('Kinetic energy', UNITS_KIN_ENERGY)

LABEL_KIN_ENERGY_N = '#varepsilon_{k}'
UNITS_KIN_ENERGY_N = 'GeV/A'
AXIS_TITLE_KIN_ENERGY_N = axisTitle('Kinetic energy/nucleon',
                                    UNITS_KIN_ENERGY_N)

LABEL_FLUX = 'F'
UNITS_FLUX = 'm^{-2} s^{-1} GeV^{-1}'

LABEL_INTENSITY = 'J'
UNITS_INTENSITY = 'm^{-2} s^{-1} GeV^{-1} sr^{-1}'
UNITS_INTENSITY_N = 'm^{-2} s^{-1} (GeV/A)^{-1} sr^{-1}'




def axisTitleFlux(xlabel = LABEL_ENERGY):
    """
    """
    return '%s(%s) [%s]' % (LABEL_FLUX, xlabel, UNITS_FLUX)


def axisTitleIntensity(xlabel = LABEL_ENERGY):
    """
    """
    if xlabel == LABEL_KIN_ENERGY_N:
        units = UNITS_INTENSITY_N
    else:
        units = UNITS_INTENSITY
    return '%s(%s) [%s]' % (LABEL_INTENSITY, xlabel, units)
