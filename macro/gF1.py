#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gRootObject import gRootObject



class gF1(ROOT.TF1, gRootObject):

    """ Wrapper around the ROOT.TF1 object.
    """

    DEFAULT_OPTIONS = {'LineWidth': 2,
                       'Npx'      : 1000
                       }

    def __init__(self, name, formula, xmin = 0, xmax = 1, **kwargs):
        """ Constructor.
        """
        ROOT.TF1.__init__(self, name, formula, xmin, xmax)
        gRootObject.init(self, **kwargs)



if __name__ == '__main__':
    f = gF1('f', '[0] + [1]*x', 0, 100)
    f.SetParameters(0, 1)
    f.Draw()
