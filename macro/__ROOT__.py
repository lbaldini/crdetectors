import os
import sys
import ROOT

# Top-level settings.
STYLE_NAME  = 'EEE'
STYLE_TITLE = 'ROOT style for EEE'
PALETTE = 'Default'
TEXT_FONT = 43
TEXT_SIZE = 31
BIG_TEXT_SIZE = 1.5*TEXT_SIZE
LABEL_TEXT_SIZE = 0.9*TEXT_SIZE
LEGEND_TEXT_SIZE = 0.9*TEXT_SIZE
SMALL_TEXT_SIZE = 0.8*TEXT_SIZE
SMALLER_TEXT_SIZE = 0.7*TEXT_SIZE
SMALLEST_TEXT_SIZE = 0.52*TEXT_SIZE
CANVAS_DEF_WIDTH  = 840
CANVAS_DEF_HEIGHT = 680
CANVAS_RIGHT_MARGIN = 0.04
CANVAS_TOP_MARGIN = 0.04
CANVAS_LEFT_MARGIN = 0.125*TEXT_SIZE/31.
CANVAS_BOTTOM_MARGIN = 0.122*TEXT_SIZE/31.

# Create a new style using the functionalities implemented in Eric's package.
STYLE = ROOT.TStyle(STYLE_NAME, STYLE_TITLE)
STYLE.SetPadColor(ROOT.kWhite)
STYLE.SetPadTickX(1)
STYLE.SetPadTickY(1)
STYLE.SetPadBorderMode(0)
STYLE.SetFrameFillColor(ROOT.kWhite)
STYLE.SetTitleFillColor(ROOT.kWhite)
STYLE.SetCanvasColor(ROOT.kWhite)
STYLE.SetStatColor(ROOT.kWhite)
STYLE.SetLineWidth(2)
STYLE.SetHistLineWidth(2)
STYLE.SetMarkerStyle(6)
STYLE.SetFrameBorderMode(0)
STYLE.SetCanvasBorderMode(0)
STYLE.SetTitleBorderSize(0)
STYLE.SetOptTitle(0)
STYLE.SetOptStat(0000)
STYLE.SetLegendBorderSize(0)
STYLE.SetPadRightMargin(CANVAS_RIGHT_MARGIN)
STYLE.SetPadTopMargin(CANVAS_TOP_MARGIN)
STYLE.SetPadLeftMargin(CANVAS_LEFT_MARGIN)
STYLE.SetPadBottomMargin(CANVAS_BOTTOM_MARGIN)
STYLE.SetStatBorderSize(0)
STYLE.SetStatFont(TEXT_FONT)
STYLE.SetStatFontSize(TEXT_SIZE)
STYLE.SetGridColor(ROOT.kGray + 1)
STYLE.SetGridStyle(2)
STYLE.SetStatStyle(0)
STYLE.SetMarkerStyle(1)
STYLE.SetCanvasDefW(CANVAS_DEF_WIDTH)
STYLE.SetCanvasDefH(CANVAS_DEF_HEIGHT)

# Text Font and Precision
# The text font code is combination of the font number and the precision.
#
#   Text font code = 10*fontnumber + precision
#
# Font numbers must be between 1 and 14.
#
# The precision can be:
# 0 fast hardware fonts (steps in the size)
# 1 scalable and rotatable hardware fonts (see below)
# 2 scalable and rotatable hardware fonts
# 3 scalable and rotatable hardware fonts. Text size is given in pixels. 
STYLE.SetTextFont(TEXT_FONT)
STYLE.SetTextSize(TEXT_SIZE)
STYLE.SetTitleFont(TEXT_FONT, 'XYZ')
STYLE.SetTitleSize(TEXT_SIZE, 'XYZ')
STYLE.SetLabelFont(TEXT_FONT, 'XYZ')
STYLE.SetLabelSize(LABEL_TEXT_SIZE, 'XYZ')
STYLE.SetTitleYOffset(1.28)
STYLE.SetTitleXOffset(1.25)
STYLE.SetTitleOffset(1.0, 'Z')
# Colors.
STYLE.SetPalette(1)

# Apply the style
ROOT.gROOT.SetStyle(STYLE_NAME)
ROOT.gROOT.ForceStyle()

ROOT_OBJECT_POOL = []

def store(rootObject):
    ROOT_OBJECT_POOL.append(rootObject)

def setupBox(box, x, y, numRows, width = 0.25):
    """ Setup a stat box or a fit box with a given number of rows.
    (x, y) is the top-left corner of the box.
    """
    box.SetX1NDC(x)
    box.SetX2NDC(x + width)
    box.SetY1NDC(y - numRows*TLegend.ROW_SPACING)
    box.SetY2NDC(y)
    ROOT.gPad.Modified()
    ROOT.gPad.Update()

def annotateContours(h, numLevels, x1 = 0.3, x2 = 0.7, y1 = 0.0, y2 = 1.0,
                     fmt = '%.2f', numSteps = 500, color = ROOT.kBlack,
                     debug = False, dxbox = 0.02):
    """ Annotate the contour levels for a 2-dimensional histogram.
    """
    # Retrieve the axes bounds.
    xmin, xmax  = h.GetXaxis().GetXmin(), h.GetXaxis().GetXmax()
    ymin, ymax  = h.GetYaxis().GetXmin(), h.GetYaxis().GetXmax()
    zmin, zmax  = h.GetMinimum(), h.GetMaximum()
    yoff = 0.005*(ymax - ymin)
    dxbox = dxbox*(xmax - xmin)
    dybox = 0.014*(ymax - ymin)
    zprev = zmin
    if ROOT.gPad.GetLogz():
        zstep = math.log10(zmax/zmin)/numLevels
    else:
        zstep = float(zmax - zmin)/numLevels
    #znext = zmin + zstep
    # Sweep the pad diagonally from bottom-left to top right and find contours.
    for i in xrange(numSteps):
        # First calculate the coordinated in NDC...
        x = x1 + i/float(numSteps)*(x2 - x1)
        y = y1 + i/float(numSteps)*(y2 - y1)
        # ...then switch to actual histogram coordinates...
        if ROOT.gPad.GetLogx():
            x = xmin**(1 + x*math.log10(xmax/xmin)/math.log10(xmin))
        else:
            x = xmin + x*(xmax - xmin)
        y = ymin + y*(ymax - ymin)
        # ...finally evaluate the histogram value and check whether we crossed
        # the next z countour.
        z = h.Interpolate(x, y)
        if ROOT.gPad.GetLogz() and z > 0:
            zdisc = int(math.log10(z/zmin)/zstep)
        else:
            zdisc = zmin + zstep*int((z - zmin)/zstep)
        if debug:
            m = ROOT.TMarker(x, y, 1)
            m.Draw()
            store(m)
        if zdisc != zprev:
            if ROOT.gPad.GetLogz():
                # Only use one significant figure.
                z = float('%.0e' % z)
            if i != 0:
                box = ROOT.TBox(x - dxbox, y - dybox, x + dxbox, y + dybox)
                box.SetFillColor(ROOT.kWhite)
                box.Draw()
                store(box)
                ROOT.gPad.annotate(x, y, fmt % z, ndc = False, align = 22,
                                   size = SMALLEST_TEXT_SIZE, color = color)
            zprev = zdisc
            if debug:
                m = ROOT.TMarker(x, y, 20)
                m.Draw()
                store(m)
