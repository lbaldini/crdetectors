#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity, getEGBIntensity



CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')


def printDataSets():
    """
    """
    import pickle
    from CRDB.__settings__ import CRDB_PICKLE_FILE_PATH
    CR_DATASET_LIST = pickle.load(open(CRDB_PICKLE_FILE_PATH))
    CR_DATASET_LIST.sort()
    for quant in ['H', '1H-bar', 'POSITRON', 'ELECTRON',
                  'ELECTRON+POSITRON', 'He', 'C', 'N', 'O', 
                  'POSITRON/ELECTRON+POSITRON', 'Fe']:
        logger.info('Searching datasets for %s...' % quant)
        for ds in CR_DATASET_LIST:
            if ds.Quantity == quant:
                logger.info(ds)


H_DATASETS = [142, 492, 873]
HBAR_DATASETS = [878]
POS_DATASETS = [607, 655, 659]
ELE_DATASETS = [662, 137, 134, 880]
ALL_ELE_DATASETS = [141, 610, 609, 880, 983]
HE_DATASETS = [143, 493, 875]
POS_FRAC_DATASETS = [140]
C_DATASETS = [145, 497, 909, 415, 310, 617]
N_DATASETS = [499, 587, 618, 311, 222, 196]
O_DATASETS = [910, 501, 899, 148, 416, 588, 619, 223]
FE_DATASETS = [911, 508, 906, 152, 892, 291, 526]



COMBINED_SPECTRA = []


def drawCombo(name, uids, **kwargs):
    """
    """
    s = gCombinedSpectrum(name, uids, **kwargs)
    s.Draw('lsame')
    for uid in uids:
        g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
        g.SetMarkerStyle(kwargs.get('markerStyle', 24))
        g.Draw('psame')
        store(g)
    ROOT.gPad.Update()
    COMBINED_SPECTRA.append(s)
    return s



c2 = gCanvas('positron_frac', Logx = True)
h2 = gH1F('frame2', 'frame', 1000, 0.1, 1000, Minimum = 0, Maximum = 0.5,
          XTitle = 'Energy [GeV]', YTitle = 'Positron fraction')
h2.Draw()
pos_frac = drawCombo('positron_fraction', POS_FRAC_DATASETS, fitLeverArm = 15,
                     extrArm = 2)
c2.Update()


c1 = gCanvas('combined_spectra', Logx = True, Logy = True)
h1 = gH1F('frame1', 'frame', 1000, 0.1, 1000000, Minimum = 1e-13, Maximum = 1e4,
          XTitle = 'Kinetic energy [GeV/n]',
          YTitle = 'Flux [m^{-2} s^{-1} s^{-1} GeV/n^{-1}]')
h1.Draw()
spec_p = drawCombo('protons', H_DATASETS, markerStyle = 24)
spec_He = drawCombo('helium', HE_DATASETS, markerStyle = 20)
spec_C = drawCombo('C', C_DATASETS, markerStyle = 30)
spec_N = drawCombo('N', N_DATASETS, markerStyle = 29, fitLeverArm = 25)
spec_O = drawCombo('O', O_DATASETS, markerStyle = 31)
spec_Fe = drawCombo('Fe', FE_DATASETS, markerStyle = 28)
spec_alle = drawCombo('all_electrons', ALL_ELE_DATASETS, markerStyle = 25)
spec_pos = __utils__.gmul('gcomb_positrons', spec_alle, pos_frac, LineStyle = 7)
spec_pos.Draw('lsame')
COMBINED_SPECTRA.append(spec_pos)
for uid in POS_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(26)
    g.Draw('psame')
    store(g)
spec_pbar = drawCombo('antiprotons', HBAR_DATASETS, markerStyle = 21)
spec_allgamma = getAllSkyGammaIntensity()
spec_allgamma.SetName('gcomb_allgamma')
spec_allgamma2 = spec_allgamma.Clone('all_gamma_temp')
spec_allgamma2.Draw('psame')
spec_allgamma.extrapolatePL(10, 10)
COMBINED_SPECTRA.append(spec_allgamma)
spec_allgamma.Draw('lsame')
spec_egb = getEGBIntensity()
spec_egb.SetName('gcomb_egb')
spec_egb2 = spec_egb.Clone('egb_temp')
spec_egb2.Draw('psame')
spec_egb.extrapolatePL(30, 10)
COMBINED_SPECTRA.append(spec_egb)
spec_egb.Draw('lsame')
c1.Update()


outputFilePath = 'data/cr_models.root'
outputFile = ROOT.TFile(outputFilePath, 'RECREATE')
logger.info('Opening output file %s...' % outputFilePath)
for g in COMBINED_SPECTRA:
    gint = g.getIntegralGraphPL()
    gint.Draw('lsame')
    logger.info('Writing %s...' % g.GetName())
    g.Write()
    logger.info('Writing %s...' % gint.GetName())
    gint.Write()
logger.info('Done.')
outputFile.Close()



#printDataSets()
