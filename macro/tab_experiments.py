#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __logging__ import logger



EXPERIMENTS = []


class gExperiment:

    def __init__(self, name, location, startDate, url = None):
        """
        """
        self.Name = name
        self.Location = location
        self.StartDate = startDate
        self.Url = url

    def latexRow(self):
        """
        """
        if self.Url is None:
            url = ''
        else:
            url = '\\url{%s}' % self.Url
        return '%s & %s & %s & %s \\\\' %\
            (self.Name, self.Location, self.StartDate, url)
 
    def __cmp__(self, other):
        """
        """
        if self.Name >= other.Name:
            return 1
        return -1

    def __str__(self):
        """
        """
        return '%s (%s, %s)' % (self.Name, self.Location, self.StartDate)



def add(name, location, startDate, url = None):
    """
    """
    experiment = gExperiment(name, location, startDate, url)
    logger.info('Adding %s...' % experiment)
    EXPERIMENTS.append(experiment)


add('ACE', 'Satellite', 1997,
    'http://www.srl.caltech.edu/ACE/CRIS_SIS/index.html')
add('AESOP', 'Balloon', 1994,
    'http://www.bartol.udel.edu/gp/balloon')
add('AGILE', 'Satellite', 2007, 'http://agile.rm.iasf.cnr.it/')
add('ALICE', 'Balloon', 1987,
    'http://ida1.physik.uni-siegen.de/alice.html')
add('AMS-01', 'Shuttle', 1998,
    'http://ams.cern.ch/AMS/Welcome.html')
add('AMS-02', 'ISS', '2011',
    'http://www.ams02.org')
add('ATIC', 'Balloon', 2000,
    'http://atic.phys.lsu.edu/index.html')
add('BESS', 'Balloon', 1993,
    'http://astrophysics.gsfc.nasa.gov/astroparticles/programs/bess/BESS.html')
add('BETS', 'Balloon', 1997, None)
add('CAPRICE', 'Balloon', 1994,
    'http://ida1.physik.uni-siegen.de/caprice.html')
add('CREAM', 'Balloon', 2004, 'http://cream.uchicago.edu')
add('CRISIS', 'Balloon', 1977, None)
add('CRN', 'Shuttle', 1985, None)
add('CRRES', 'Satellite', 1990,
    'http://nssdc.gsfc.nasa.gov/nmc/spacecraftDisplay.do?id=1990-065A')
add('EGRET', 'Satellite', 1991,
    'https://heasarc.gsfc.nasa.gov/docs/cgro/egret/')
add('Fermi', 'Satellite', 2008, 'http://fermi.gsfc.nasa.gov')
add('HEAO', 'Satellite', 1979,
    'http://heasarc.gsfc.nasa.gov/docs/heao3/heao3_about.html')
add('HEAT', 'Balloon', 1994, None)
add('HEIST', 'Balloon', 1988, None)
add('IMAX', 'Balloon', 1992, 'http://ida1.physik.uni-siegen.de/imax.html')
add('IMP', 'Satellite', 1963,
    'http://imagine.gsfc.nasa.gov/docs/sats_n_data/missions/imp7.html')
add('ISEE', 'Satellite', 1978,
    'http://heasarc.gsfc.nasa.gov/docs/heasarc/missions/isee3.html')
add('ISOMAX', 'Balloon', 1998,
    'http://astrophysics.gsfc.nasa.gov/astroparticles/programs/isomax')
add('JACEE', 'Balloon', 1979, 'http://marge.phys.washington.edu/jacee')
add('MASS', 'Balloon', 1989, 'http://ida1.physik.uni-siegen.de/mass2.html')
add('MUBEE', 'Balloon', 1975, None)
add('OSO-3', 'Satellite', 1967,
    'http://heasarc.gsfc.nasa.gov/docs/heasarc/missions/oso3.html')
add('OGO5', 'Satellite', 1968,
    'http://imagine.gsfc.nasa.gov/docs/sats_n_data/missions/ogo.html')
add('PAMELA', 'Satellite', 2006, 'http://pamela.roma2.infn.it/index.php')
add('Pioneer', 'Satellite', 1967,
    'http://nssdc.gsfc.nasa.gov/nmc/spacecraftDisplay.do?id=1972-012A')
add('RICH', 'Balloon', 1997, None)
add('RUNJOB', 'Balloon', 1995, 'http://runjob.boom.ru')
add('SAS-2', 'Satellite', 1972,
    'http://heasarc.gsfc.nasa.gov/docs/sas2/sas2.html')
add('SMILI', 'Balloon', 1989, None)
add('SOKOL', 'Satellite', 1984, None)
add('TRACER', 'Balloon', 2003, 'http://tracer.uchicago.edu')
add('Trek', 'ISS', 1991, None)
add('TS', 'Balloon', 1993, None)
add('Ulysses', 'Satellite', 1990,
    'http://sci.esa.int/science-e/www/area/index.cfm?fareaid=11')
add('Voyager', 'Satellite', 1977, 'http://voyager.jpl.nasa.gov')


EXPERIMENTS.sort()


outputFilePath = '../tables/cr_experiments.tex'
logger.info('Opening output file %s...' % outputFilePath)
outputFile = open(outputFilePath, 'w')

def _write(text, endline = True):
    """ Convenience local function to write to the output file.
    """
    if endline:
        text = '%s\n' % text
    outputFile.write(text)

for experiment in EXPERIMENTS:
    _write(experiment.latexRow())

logger.info('Closing output file %s...' % outputFilePath)
outputFile.close()
