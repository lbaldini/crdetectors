#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from gGraphErrors import gGraphErrors

ALL_SKY_FILE_PATH = 'data/all_sky_gamma_intensity.txt'
EGB_FILE_PATH = 'data/egb_gamma_intensity.txt'


def getAllSkyGammaIntensity():
    """
    """
    g = gGraphErrors('all_sky_gamma')
    f = open(ALL_SKY_FILE_PATH)
    for i in range(3):
        f.next()
    for line in f:
        if line.startswith('='):
            break
        line = line.strip('\ \n')
        line = line.replace(' ', '').replace('-', '&')
        emin, emax, flux, counts = [float(item) for item in line.split('&')]
        emean = math.sqrt(emin*emax)
        flux /= emean**2
        flux *= 1e7
        emean /= 1000.
        g.SetNextPoint(emean, flux)
    return g


def getEGBIntensity():
    """
    """
    g = gGraphErrors('egb_gamma')
    f = open(EGB_FILE_PATH)
    for line in f:
        line = line.strip('\ \n')
        emin, emax, flux = [float(item) for item in line.split()]
        flux *= 1e4
        flux /= (emax - emin)
        print emin, emax, flux
        emean = math.sqrt(emin*emax)
        g.SetNextPoint(emean, flux)
    return g



if __name__ == '__main__':
    from gCanvas import gCanvas
    c = gCanvas('c', Logx = True, Logy = True)
    g1 = getAllSkyGammaIntensity()
    g1.Draw('ap')
    g2 = getEGBIntensity()
    g2.Draw('psame')
    c.Update()
