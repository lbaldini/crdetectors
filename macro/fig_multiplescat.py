#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __constants__ import *
from __pdg__ import * 
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend


P_MIN = 0.1
P_MAX = 1000

THICKNESS_LIST = [0.03/SI_X0, 0.03, 0.18]


frame = gH1F('h', 'h', 100, P_MIN, P_MAX, XTitle = 'p [GeV/c]',
             YTitle = '#theta_{MS}^{space} [#circ]',
             Minimum = 1e-4, Maximum = 10)

fe = gF1('fe', getMSFormula(ELECTRON_MASS, True), P_MIN, P_MAX)
fe.SetLineStyle(7)
fe.SetTitle('Electrons')
fp = gF1('fp', getMSFormula(PROTON_MASS, True), P_MIN, P_MAX)
fp.SetTitle('Protons')


c1 = gCanvas('multiple_scattering', Logx = True, Logy = True)
frame.Draw()
for thickness in THICKNESS_LIST:
    fe.SetParameter(0, thickness)
    fe.DrawCopy('same')
    fp.SetParameter(0, thickness)
    fp.DrawCopy('same')
    x = 10
    t = thickness*100
    if t > 1:
        t = '%.1f' % t
    else:
        t = '%.2f' % t
    c1.annotate(x, 1.15*fe.Eval(x), '%s%% X_{0}' % t, ndc = False,
                size = SMALL_TEXT_SIZE, angle = -30)
l = gLegend(0.6, 0.78, entries = [fe, fp])
l.Draw()
c1.Update()
c1.save()
