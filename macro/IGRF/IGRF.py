#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import igrf


EARTH_RADIUS = 6371.2


class IGRF(dict):

    """ Small data structure encapsulating the B field model from IGRF.

    TODO: we still might want to add the invariant latitute, invariant radius
    and invariant lambda.
    """

    def __init__(self):
        """ Constructor.
        """
        igrf.initize()

    def compute(self, lat, lon, alt = 0, year = 2010.0):
        """ Compute the model.
        """
        if year < 1990 or year > 2015:
            print('The model is only valid through 1990--2015.')
            raise ValueError
        igrf.initize()
        self['dipole_moment'] = igrf.feldcof(year)
        self['B_north'], self['B_east'], self['B_down'],\
            self['B_abs'] = igrf.feldg(lat, lon, alt)
        self['mc_ilwain_L'], icode, bab1 = igrf.shellg(lat, lon, alt,
                                                       self['dipole_moment'])
        val, self['B_equator'], rr0 = igrf.findb0(0.05, 0.001)
        self['B_B0'] = self['B_abs']/self['B_equator']
        self['rigidity_cutoff'] = 0.25*self['dipole_moment']*EARTH_RADIUS*\
                                  3e-2/(self['mc_ilwain_L']**2)




if __name__ == '__main__':
    model = IGRF()
    model.compute(0, 0, 0, 2010.)
    print model
