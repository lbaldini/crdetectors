#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __logging__ import logger
from __ROOT__ import *
from __geometry2d__ import *
from gPhysicalCanvas import gPhysicalCanvas


""" Overall style.
"""
FOV_LINE_STYLE = 2


def cal(xc, yc, width, height):
    """ Specialized CAL box.
    """
    box(xc, yc, width, height, FillStyle = 3004, FillColor = ROOT.kBlack)


""" The Large Area Telescope.
"""
LAT_TOWER_PITCH = 37.45
LAT_CAL_TOP = -4.70
LAT_CAL_BOTTOM = -21.80
LAT_TKR_BOTTOM = 0.0
LAT_TKR_PLANE_WIDTH = 9.02*4
LAT_CAL_LOG_LENGTH = 32.60
LAT_TKR_LAYER_Z = [4.23, 4.48, 7.43, 7.69, 10.64, 10.90, 13.95, 14.21, 17.26,
                   17.52, 20.57, 20.83, 23.88, 24.14, 27.11, 27.37, 30.34,
                   30.60, 33.57, 33.82, 36.80, 37.05, 40.03, 40.29, 43.26,
                   43.52, 46.49, 46.74, 49.72, 49.98, 52.95, 53.21, 56.18,
                   56.44, 59.41, 59.67]
LAT_TKR_LAYER_Z = [0.5*(LAT_TKR_LAYER_Z[i] + LAT_TKR_LAYER_Z[i+1]) for i in \
                   range(0, 35, 2)]
LAT_ACD_TOP_Z = 77.20
LAT_ACD_TOP_TILE_WIDTH = 35.0
LAT_ACD_TILE_THICKNESS = 1.5
LAT_ACD_TILE_OVERLAP = 2.0
LAT_ACD_ROW0_TILE_HEIGHT = 36.6
LAT_ACD_ROW1_TILE_HEIGHT = 21.9
LAT_ACD_ROW2_TILE_HEIGHT = 17.0
LAT_ACD_ROW3_TILE_HEIGHT = 15.45
LAT_ACD_WIDTH = 170.787

def drawLAT(x0 = 0., y0 = 0.):
    """ Draw the LAT.
    """
    # TKR and CAL.
    yc = y0 + 0.5*(LAT_CAL_TOP + LAT_CAL_BOTTOM)
    for i in range(4):
        xc = x0 + LAT_TOWER_PITCH*(i - 1.5)
        cal(xc, yc, LAT_CAL_LOG_LENGTH, LAT_CAL_TOP - LAT_CAL_BOTTOM)
        for y in LAT_TKR_LAYER_Z:
            hline(xc, y0 + y, LAT_TKR_PLANE_WIDTH, LineWidth = 1)
    annotate(x0, y0 + LAT_CAL_BOTTOM - 8, 'CAL')
    # ACD
    for i in range(5):
        xc = x0 + (LAT_ACD_TOP_TILE_WIDTH - LAT_ACD_TILE_OVERLAP)*(i - 2)
        yc = y0 + LAT_ACD_TOP_Z - LAT_ACD_TILE_THICKNESS*(2 - abs(i - 2))
        box(xc, yc, LAT_ACD_TOP_TILE_WIDTH, LAT_ACD_TILE_THICKNESS)
    for xc in [x0 - 0.5*LAT_ACD_WIDTH, x0 + 0.5*LAT_ACD_WIDTH]:
        yc = y0 + LAT_CAL_TOP + 0.5*LAT_ACD_ROW3_TILE_HEIGHT
        box(xc, yc, LAT_ACD_TILE_THICKNESS, LAT_ACD_ROW3_TILE_HEIGHT)
        yc += 0.5*(LAT_ACD_ROW3_TILE_HEIGHT + LAT_ACD_ROW2_TILE_HEIGHT)
        box(xc, yc, LAT_ACD_TILE_THICKNESS, LAT_ACD_ROW3_TILE_HEIGHT)
        yc += 0.5*(LAT_ACD_ROW2_TILE_HEIGHT + LAT_ACD_ROW1_TILE_HEIGHT)
        box(xc, yc, LAT_ACD_TILE_THICKNESS, LAT_ACD_ROW1_TILE_HEIGHT)
        yc += 0.5*(LAT_ACD_ROW1_TILE_HEIGHT + LAT_ACD_ROW0_TILE_HEIGHT)
        box(xc, yc, LAT_ACD_TILE_THICKNESS, LAT_ACD_ROW0_TILE_HEIGHT)
    # More annotations.
    annotate(x0, y0 + LAT_TKR_LAYER_Z[-1] + 40, 'Fermi-LAT', size = 1.1)
    annotate(x0, y0 + 0.5*(LAT_TKR_LAYER_Z[0] + LAT_TKR_LAYER_Z[-1]), 'TKR')
    annotate(x0 - 1.85*LAT_TOWER_PITCH, y0 + LAT_TKR_LAYER_Z[-1] + 10, 'ACD')
    line(x0, y0 + LAT_ACD_TOP_Z + 10, x0, y0 + LAT_CAL_BOTTOM - 20,
         LineStyle = FOV_LINE_STYLE, LineWidth = 1, LineColor = ROOT.kGray + 1)
    hquote(x0 - 1.5*LAT_TOWER_PITCH - 0.5*LAT_CAL_LOG_LENGTH,
           x0 + 1.5*LAT_TOWER_PITCH + 0.5*LAT_CAL_LOG_LENGTH,
           y0 + LAT_CAL_BOTTOM - 25)
    vquote(y0 + LAT_CAL_BOTTOM, y0 + LAT_TKR_LAYER_Z[-1],
           x0 + 0.5*LAT_ACD_WIDTH + 20)
    line(x0 - 1.5*LAT_TOWER_PITCH - 0.5*LAT_CAL_LOG_LENGTH, y0 + LAT_CAL_BOTTOM,
         x0 + 1.5*LAT_TOWER_PITCH + 0.5*LAT_CAL_LOG_LENGTH, y0 + \
         LAT_TKR_LAYER_Z[2], 0, 20, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 1.5*LAT_TOWER_PITCH + 0.5*LAT_CAL_LOG_LENGTH, y0 + LAT_CAL_BOTTOM,
         x0 - 1.5*LAT_TOWER_PITCH - 0.5*LAT_CAL_LOG_LENGTH, y0 + 
         LAT_TKR_LAYER_Z[2], 0, 20, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = LAT_TKR_LAYER_Z[2] - LAT_CAL_BOTTOM
    dx = 3*LAT_TOWER_PITCH + LAT_CAL_LOG_LENGTH
    theta = math.atan(dx/dy)
    yc = y0 + LAT_CAL_BOTTOM +\
         (1.5*LAT_TOWER_PITCH + 0.5*LAT_CAL_LOG_LENGTH)/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 75, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 68, '#pm 75#circ')


""" AMS-02

This is essentially taken from an event display, with a few quotes taken
here and there. I believe it is accurate to 5--10%.
"""
AMS_Z_TKR_LAYERS = [159.05,53.04,29.22,25.24,1.70,-2.29,-25.25,-29.24,-136.00]
AMS_YSCALE_FACTOR = (AMS_Z_TKR_LAYERS[0] - AMS_Z_TKR_LAYERS[-1])/14.8
AMS_XSCALE_FACTOR = 0.929*AMS_YSCALE_FACTOR
AMS_TKR_WIDTH = [AMS_XSCALE_FACTOR*w for w in\
                 [7.0, 6.9, 5.5, 5.5, 5.5, 5.5, 5.5, 5.5, 5.1]]
AMS_ECAL_WIDTH = AMS_XSCALE_FACTOR*3.7 # this is 68.5 cm
AMS_ECAL_THICKNESS = AMS_YSCALE_FACTOR*0.9
AMS_ECAL_TOP = -AMS_YSCALE_FACTOR*7.1
AMS_TRD_TOP = 155.
AMS_TRD_THICKNESS = AMS_YSCALE_FACTOR*3.9
AMS_TRD_TOP_WIDTH = AMS_XSCALE_FACTOR*12.
AMS_TRD_BOT_WIDTH = AMS_XSCALE_FACTOR*8.6
AMS_RICH_TOP = -75.
AMS_RICH_THICKNESS = AMS_YSCALE_FACTOR*2.9
AMS_RICH_TOP_WIDTH = AMS_XSCALE_FACTOR*6.7
AMS_RICH_BOT_WIDTH = AMS_XSCALE_FACTOR*7.5
AMS_TOF_TOP_Z = 60.
AMS_TOF_TOP_THICKNESS = 1.
AMS_TOF_TOP_WIDTH = AMS_XSCALE_FACTOR*7.3
AMS_TOF_BOT_Z = -68.
AMS_TOF_BOT_WIDTH = AMS_XSCALE_FACTOR*7.5
AMS_TOF_BOT_THICKNESS = 1.

def drawAMS02(x0 = 0., y0 = 0.):
    """ Draw AMS-02.
    """
    # TKR
    for i, y in enumerate(AMS_Z_TKR_LAYERS):
        hline(x0, y0 + y, AMS_TKR_WIDTH[i], LineWidth = 1)
        annotate(x0 + 0.5*AMS_TKR_WIDTH[i], y0 + y, ' Tracker %d' % (i + 1),
                 align = 12, size = 0.65)
    # ECAL
    cal(x0, y0 + AMS_ECAL_TOP - 0.5*AMS_ECAL_THICKNESS, AMS_ECAL_WIDTH,
        AMS_ECAL_THICKNESS)
    annotate(x0, y0 + AMS_ECAL_TOP - 0.5*AMS_ECAL_THICKNESS - 15, 'ECAL')
    # TRD
    trapezoid(x0, y0 + AMS_TRD_TOP - 0.5*AMS_TRD_THICKNESS, AMS_TRD_TOP_WIDTH,
              AMS_TRD_BOT_WIDTH, AMS_TRD_THICKNESS)
    annotate(x0, y0 + AMS_TRD_TOP - 0.5*AMS_TRD_THICKNESS, 'TRD')
    # RICH
    trapezoid(x0, y0 + AMS_RICH_TOP - 0.5*AMS_RICH_THICKNESS,
              AMS_RICH_TOP_WIDTH, AMS_RICH_BOT_WIDTH, AMS_RICH_THICKNESS)
    annotate(x0, y0 + AMS_RICH_TOP - 0.5*AMS_RICH_THICKNESS, 'RICH')
    # TOF
    box(x0, y0 + AMS_TOF_TOP_Z, AMS_TOF_TOP_WIDTH, AMS_TOF_TOP_THICKNESS)
    annotate(x0 + 0.5*AMS_TOF_TOP_WIDTH, y0 + AMS_TOF_TOP_Z, ' TOF', align = 12)
    box(x0, y0 + AMS_TOF_BOT_Z, AMS_TOF_BOT_WIDTH, AMS_TOF_BOT_THICKNESS)
    annotate(x0 + 0.5*AMS_TOF_BOT_WIDTH, y0 + AMS_TOF_BOT_Z, ' TOF', align = 12)
    # More annotations.
    annotate(x0, y0 + AMS_Z_TKR_LAYERS[0] + 20, 'AMS-02', size = 1.1)
    line(x0, y0 + AMS_Z_TKR_LAYERS[0] + 10, x0, y0 + AMS_Z_TKR_LAYERS[-1] - 40,
         LineStyle = FOV_LINE_STYLE, LineWidth = 1, LineColor = ROOT.kGray + 1)
    vquote(y0 + AMS_Z_TKR_LAYERS[0], y0 + AMS_Z_TKR_LAYERS[-1],
           x0 + 0.5*AMS_TRD_TOP_WIDTH + 10)
    hquote(x0 - 0.5*AMS_ECAL_WIDTH, x0 + 0.5*AMS_ECAL_WIDTH,
           y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS - 23)
    line(x0 - 0.5*AMS_ECAL_WIDTH, y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS,
         x0 + 0.5*AMS_TKR_WIDTH[0], y0 + AMS_Z_TKR_LAYERS[0],
         0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 0.5*AMS_ECAL_WIDTH, y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS,
         x0 - 0.5*AMS_TKR_WIDTH[0], y0 + AMS_Z_TKR_LAYERS[0],
         0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = AMS_Z_TKR_LAYERS[0] - AMS_ECAL_TOP + AMS_ECAL_THICKNESS
    dx = 0.5*(AMS_ECAL_WIDTH + AMS_TKR_WIDTH[0])
    theta = math.atan(dx/dy)
    yc = y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS +\
         0.5*AMS_ECAL_WIDTH/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 150, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 140, '#pm 15#circ')
    line(x0 - 0.5*AMS_ECAL_WIDTH, y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS,
         x0 + 0.5*AMS_TKR_WIDTH[1], y0 + AMS_Z_TKR_LAYERS[1],
         0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 0.5*AMS_ECAL_WIDTH, y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS,
         x0 - 0.5*AMS_TKR_WIDTH[1], y0 + AMS_Z_TKR_LAYERS[1],
         0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = AMS_Z_TKR_LAYERS[1] - AMS_ECAL_TOP + AMS_ECAL_THICKNESS
    dx = 0.5*(AMS_ECAL_WIDTH + AMS_TKR_WIDTH[1])
    theta = math.atan(dx/dy)
    yc = y0 + AMS_ECAL_TOP - AMS_ECAL_THICKNESS + \
         0.5*AMS_ECAL_WIDTH/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 130, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 120, '#pm 25#circ')



""" CREAM-II

This is taken from ApJ 707 (2009) 593--603
"""
CREAM_XSCALE = 120./4.25
CREAM_YSCALE = 120./4.1
CREAM_TCD_WIDTH = 120.
CREAM_TCD_Z = 120.
CREAM_TCD_THICKNESS = 1.
CREAM_CD_WIDTH = CREAM_YSCALE*3.9
CREAM_CD_Z = CREAM_XSCALE*3.4
CREAM_CD_THICKNESS = 1.
CREAM_SCD_WIDTH = CREAM_XSCALE*2.8
CREAM_SCD_TOP_Z = CREAM_YSCALE*1.25
CREAM_SCD_BOT_Z = CREAM_YSCALE*1.15
CREAM_CT1_TOP_WIDTH = CREAM_XSCALE*2.7
CREAM_CT1_BOT_WIDTH = CREAM_XSCALE*2.3
CREAM_CT1_TOP = CREAM_YSCALE*1.025
CREAM_CT1_BOT = CREAM_YSCALE*0.7
CREAM_CT2_TOP_WIDTH = CREAM_XSCALE*2.2
CREAM_CT2_BOT_WIDTH = CREAM_XSCALE*1.8
CREAM_CT2_TOP = CREAM_YSCALE*0.65
CREAM_CT2_BOT = CREAM_YSCALE*0.35
CREAM_CAL_THICKNESS = 120./4.1*0.3
CREAM_CAL_WIDTH = CREAM_XSCALE*1.77
CREAM_S3_Z = 0
CREAM_S3_WIDTH = CREAM_CAL_WIDTH

def drawCREAMII(x0 = 0., y0 = 0.):
    """ Draw CREAM-II
    """
    # TCD
    box(x0, y0 + CREAM_TCD_Z, CREAM_TCD_WIDTH, CREAM_TCD_THICKNESS)
    annotate(x0 + 0.5*CREAM_TCD_WIDTH, y0 + CREAM_TCD_Z, ' TCD', align = 12)
    # CD
    box(x0, y0 + CREAM_CD_Z, CREAM_CD_WIDTH, CREAM_CD_THICKNESS)
    annotate(x0 + 0.5*CREAM_CD_WIDTH, y0 + CREAM_CD_Z, ' CD', align = 12)
    # SCD
    hline(x0, y0 + CREAM_SCD_TOP_Z, CREAM_SCD_WIDTH, LineWidth = 1)
    hline(x0, y0 + CREAM_SCD_BOT_Z, CREAM_SCD_WIDTH, LineWidth = 1)
    annotate(x0 + 0.5*CREAM_SCD_WIDTH, y0 + CREAM_SCD_TOP_Z + 0.5,
             ' SCD top', align = 12, size = 0.65)
    annotate(x0 + 0.5*CREAM_SCD_WIDTH, y0 + CREAM_SCD_BOT_Z - 0.5,
             ' SCD bottom', align = 12, size = 0.65)
    # C target
    trapezoid(x0, y0 + 0.5*(CREAM_CT1_TOP + CREAM_CT1_BOT),
              CREAM_CT1_TOP_WIDTH, CREAM_CT1_BOT_WIDTH,
              CREAM_CT1_TOP - CREAM_CT1_BOT)
    trapezoid(x0, y0 + 0.5*(CREAM_CT2_TOP + CREAM_CT2_BOT),
              CREAM_CT2_TOP_WIDTH, CREAM_CT2_BOT_WIDTH,
              CREAM_CT2_TOP - CREAM_CT2_BOT)
    annotate(x0, y0 + 0.5*(CREAM_CT1_TOP + CREAM_CT1_BOT), 'Carbon target',
             size = 0.95)
    # S3 hodoscope
    # TBD
    # CAL
    cal(x0, y0 + 0.5*CREAM_CAL_THICKNESS, CREAM_CAL_WIDTH, CREAM_CAL_THICKNESS)
    annotate(x0, y0 - 9, 'CAL')
    # More annotation
    annotate(x0, y0 + CREAM_TCD_Z + 15, 'CREAM-II', size = 1.1)
    vline(x0, y0 + 0.5*CREAM_TCD_Z, 140, LineStyle = FOV_LINE_STYLE,
          LineWidth = 1, LineColor = ROOT.kGray + 1)
    hquote(x0 - 0.5*CREAM_CAL_WIDTH, x0 + 0.5*CREAM_CAL_WIDTH, y0 - 20)
    vquote(y0, y0 + CREAM_TCD_Z, x0 + 0.5*CREAM_TCD_WIDTH + 30)
    line(x0 - 0.5*CREAM_CAL_WIDTH, y0, x0 + 0.5*CREAM_TCD_WIDTH, y0 + \
         CREAM_TCD_Z, 0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 0.5*CREAM_CAL_WIDTH, y0, x0 - 0.5*CREAM_TCD_WIDTH, y0 + \
         CREAM_TCD_Z, 0, 25, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = CREAM_TCD_Z
    dx = 0.5*(CREAM_CAL_WIDTH + CREAM_TCD_WIDTH)
    theta = math.atan(dx/dy)
    yc = y0 + 0.5*CREAM_CAL_WIDTH/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 75, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 68, '#pm 35#circ')
    line(x0 - 0.5*CREAM_CAL_WIDTH, y0, x0 + 0.5*CREAM_SCD_WIDTH, y0 + \
         CREAM_SCD_TOP_Z, 0, 40, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 0.5*CREAM_CAL_WIDTH, y0, x0 - 0.5*CREAM_SCD_WIDTH, y0 + \
         CREAM_SCD_TOP_Z, 0, 40, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = CREAM_SCD_TOP_Z
    dx = 0.5*(CREAM_CAL_WIDTH + CREAM_SCD_WIDTH)
    theta = math.atan(dx/dy)
    yc = y0 + 0.5*CREAM_CAL_WIDTH/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 75, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 67, '#pm 60#circ')



""" PaMeLa
"""
PAMELA_SCALE = 15./64
PAMELA_TKR_PLANE_WIDTH = 15.
PAMELA_TKR_PLANE_SPACING = 8.9
PAMELA_CAL_WIDTH = 24.
PAMELA_CAL_THICKNESS = 18.
PAMELA_CAL_TOP = -7.
PAMELA_N_WIDTH = 156*PAMELA_SCALE
PAMELA_N_THICKNESS = 15.
PAMELA_N_TOP = PAMELA_CAL_TOP - PAMELA_CAL_THICKNESS - 7.
PAMELA_S_THICKNESS = 1.
PAMELA_S_SEP = 3.
PAMELA_S1_WIDTH = 190*PAMELA_SCALE
PAMELA_S1_Z = 328*PAMELA_SCALE
PAMELA_S2_WIDTH = 17.
PAMELA_S2_Z = 47.9
PAMELA_S3_WIDTH = PAMELA_S2_WIDTH
PAMELA_S3_Z = -3.7
PAMELA_S4_WIDTH = PAMELA_S1_WIDTH
PAMELA_S4_Z = -28.

def drawPamela(x0 = 0., y0 = 0.):
    """
    """
    # TKR
    for i in range(6):
        hline(x0, y0 + i*PAMELA_TKR_PLANE_SPACING, PAMELA_TKR_PLANE_WIDTH,
              LineWidth = 1)
    annotate(x0 + PAMELA_TKR_PLANE_WIDTH, y0 + 22, ' TKR', align = 12)
    # CAL
    cal(x0, y0 + PAMELA_CAL_TOP - 0.5*PAMELA_CAL_THICKNESS, PAMELA_CAL_WIDTH,
        PAMELA_CAL_THICKNESS)
    annotate(x0, y0 + PAMELA_CAL_TOP - 0.5*PAMELA_CAL_THICKNESS, 'CAL')
    # Neutron detector
    box(x0, y0 + PAMELA_N_TOP - 0.5*PAMELA_N_THICKNESS, PAMELA_N_WIDTH,
        PAMELA_N_THICKNESS)
    annotate(x0, y0 + PAMELA_N_TOP - PAMELA_N_THICKNESS - 8,
             'Neutron detector')
    # Scintillators.
    box(x0, y0 + PAMELA_S1_Z + 0.5*PAMELA_S_SEP, PAMELA_S1_WIDTH,
        PAMELA_S_THICKNESS)
    box(x0, y0 + PAMELA_S1_Z - 0.5*PAMELA_S_SEP, PAMELA_S1_WIDTH,
        PAMELA_S_THICKNESS)
    annotate(x0 + 0.5*PAMELA_S1_WIDTH, y0 + PAMELA_S1_Z, ' S1', align = 12)
    box(x0, y0 + PAMELA_S2_Z + 0.5*PAMELA_S_SEP, PAMELA_S2_WIDTH,
        PAMELA_S_THICKNESS)
    box(x0, y0 + PAMELA_S2_Z - 0.5*PAMELA_S_SEP, PAMELA_S2_WIDTH,
        PAMELA_S_THICKNESS)
    annotate(x0 + 0.5*PAMELA_S2_WIDTH, y0 + PAMELA_S2_Z, ' S2', align = 12)
    box(x0, y0 + PAMELA_S3_Z + 0.5*PAMELA_S_SEP, PAMELA_S3_WIDTH,
        PAMELA_S_THICKNESS)
    box(x0, y0 + PAMELA_S3_Z - 0.5*PAMELA_S_SEP, PAMELA_S3_WIDTH,
        PAMELA_S_THICKNESS)
    annotate(x0 + 0.5*PAMELA_S3_WIDTH, y0 + PAMELA_S3_Z, ' S3', align = 12)
    box(x0, y0 + PAMELA_S4_Z, PAMELA_S4_WIDTH, PAMELA_S_THICKNESS)
    annotate(x0 + 0.5*PAMELA_S4_WIDTH, y0 + PAMELA_S4_Z, ' S4', align = 12)
    # More annotations
    annotate(x0, y0 + 100, 'PaMeLa', size = 1.1)
    hquote(x0 - 0.5*PAMELA_CAL_WIDTH, x0 + 0.5*PAMELA_CAL_WIDTH,
           y0 + PAMELA_CAL_TOP - PAMELA_CAL_THICKNESS - 40)
    vquote(y0 + PAMELA_S1_Z, y0 + PAMELA_N_TOP - PAMELA_N_THICKNESS,
           x0 + 0.5*PAMELA_CAL_WIDTH + 30)
    vline(x0, y0 + 16, 140, LineStyle = FOV_LINE_STYLE,
          LineWidth = 1, LineColor = ROOT.kGray + 1)
    line(x0 - 0.5*PAMELA_CAL_WIDTH, y0 + PAMELA_CAL_TOP - PAMELA_CAL_THICKNESS,
         x0 + 0.5*PAMELA_TKR_PLANE_WIDTH, y0 + 5*PAMELA_TKR_PLANE_SPACING,
         0, 40, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    line(x0 + 0.5*PAMELA_CAL_WIDTH, y0 + PAMELA_CAL_TOP - PAMELA_CAL_THICKNESS,
         x0 - 0.5*PAMELA_TKR_PLANE_WIDTH, y0 + 5*PAMELA_TKR_PLANE_SPACING,
         0, 40, LineStyle = FOV_LINE_STYLE, LineWidth = 1,
         LineColor = ROOT.kGray + 1)
    dy = 5*PAMELA_TKR_PLANE_SPACING - PAMELA_CAL_TOP + PAMELA_CAL_THICKNESS
    dx = 0.5*(PAMELA_CAL_WIDTH + PAMELA_TKR_PLANE_WIDTH)
    theta = math.atan(dx/dy)
    yc = y0 + PAMELA_CAL_TOP - PAMELA_CAL_THICKNESS + \
         0.5*PAMELA_CAL_WIDTH/math.tan(theta)
    deg = math.degrees(theta)
    circle(x0, yc, 52, 90 - deg, 90 + deg, LineStyle = FOV_LINE_STYLE,
           LineWidth = 1, LineColor = ROOT.kGray + 1)
    annotate(x0, yc + 43, '#pm 15#circ')


c = gPhysicalCanvas('detectors', 550, 450, wpx = 1200)
drawLAT(-180, 100)
drawAMS02(140, 20)
drawCREAMII(-180, -140)
drawPamela(0, -30)
c.Update()
ROOT.gStyle.SetPaperSize(28, 80)
c.save()
