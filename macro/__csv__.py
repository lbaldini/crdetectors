#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys

from __ROOT__ import *
from gGraphErrors import gGraphErrors


def read(filePath, name, title = None, **kwargs):
    """ Read a csv file and return a ROOT graph.
    """
    if not filePath.endswith('.csv'):
        sys.exit('Please provide a .csv file. Abort.')
    title = title or name
    g = gGraphErrors(name, title, **kwargs)
    for line in open(filePath):
        x, y = [float(item) for item in line.strip('\n').split(',')]
        g.SetNextPoint(x, y)
    return g
