#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1


PSF_100_VALUES = [3, 4, 5, 6]
PSF_HE_VALUES = [0.0050, 0.0075, 0.010]
AEFF_VALUES = [0.4, 0.6, 0.8, 0.95, 0.99]
LINE_WIDTH = 1
LINE_STYLE = 7
LINE_COLOR = ROOT.kGray + 1

N_MIN = 9.999
N_MAX = 100
PITCH_MIN = N_MIN*5
PITCH_MAX = N_MAX*5

c = gCanvas('telescope_optimization', Logx = True, Logy = False,
            Gridx = False, Gridy = False, RightMargin = 0.12, Ticky = False)
h = gH1F('h', 'h', 100, 0.01, 0.1, Minimum = N_MIN, Maximum = N_MAX,
         XTitle = 't [X_{0}]', YTitle = 'n')
h.GetXaxis().SetMoreLogLabels()
h.Draw()
xmaxNdc = h.GetXaxis().GetXmax()
yminNdc = h.GetMinimum()
ymaxNdc = h.GetMaximum()
rightAxis = ROOT.TGaxis(xmaxNdc, yminNdc, xmaxNdc, ymaxNdc,
                        PITCH_MIN, PITCH_MAX, 210, 'SL+')
rightAxis.SetTitle('Strip pitch [#mum]')
rightAxis.SetTitleOffset(1.3)
rightAxis.SetLabelFont(TEXT_FONT)
rightAxis.SetLabelSize(LABEL_TEXT_SIZE)
rightAxis.SetTitleFont(TEXT_FONT)
rightAxis.SetTitleSize(TEXT_SIZE)
rightAxis.Draw()

for psf in PSF_100_VALUES:
    t = (math.radians(psf)/0.385)**2
    l = ROOT.TLine(t, 10, t, 100)
    l.SetLineColor(LINE_COLOR)
    l.SetLineStyle(LINE_STYLE)
    l.SetLineWidth(LINE_WIDTH)
    store(l)
    l.Draw()
    c.annotate(t, 11, 'PSF_{100} = %d#circ' % psf, ndc = False, align = 21,
               size = SMALLEST_TEXT_SIZE)

for psf in PSF_HE_VALUES:
    n = (math.radians(psf)*50000)**2
    l = ROOT.TLine(0.01, n, 0.1, n)
    l.SetLineColor(LINE_COLOR)
    l.SetLineStyle(LINE_STYLE)
    l.SetLineWidth(LINE_WIDTH)
    store(l)
    l.Draw()
    c.annotate(0.013, 1.025*n, 'PSF_{HE} = %.4f#circ' % psf, ndc = False,
               align = 21, size = SMALLEST_TEXT_SIZE)

faeff = gF1('faeff', '[0]/x', 0.01, 0.1)
faeff.SetLineColor(LINE_COLOR)
faeff.SetLineWidth(LINE_WIDTH)
faeff.SetLineStyle(LINE_STYLE)
for aeff in AEFF_VALUES:
    par0 = -math.log(1 - aeff)
    faeff.SetParameter(0, par0)
    faeff.DrawCopy('same')
    x = 0.05*aeff
    y = faeff.Eval(x)
    print x, y
    c.annotate(x, y, 'A_{eff} = %.1f m^{2}' % aeff, ndc = False, align = 22,
               size = SMALLEST_TEXT_SIZE)
c.annotate(0.18, 0.85, '100 W')
c.Update()
c.save()
