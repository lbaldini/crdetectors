#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __logging__ import logger
from __ROOT__ import *
from __geometry2d__ import *
from gPhysicalCanvas import gPhysicalCanvas

""" Overall style.
"""
FOV_LINE_STYLE = 2
TEXT_SIZE = 1.3


def drawdd(x0 = 0, y0 = 0, side = 100, csep = 7, maxtheta = 125):
    """
    """
    box(x0, y0, side, side)
    y = y0 - 0.5*side*(1 - math.tan(math.radians(maxtheta - 90)))
    line(x0, y0 - 1.1*side, x0, y0 + 1.1*side, LineStyle = FOV_LINE_STYLE,
         LineWidth = 1, LineColor = ROOT.kGray + 1)
    circle(x0, y, 110, 90 - maxtheta, 90 + maxtheta,
           LineStyle = FOV_LINE_STYLE, LineWidth = 1,
           LineColor = ROOT.kGray + 1)
    annotate(x0, y0 + 88, '#pm %d#circ' % maxtheta, size = TEXT_SIZE)
    annotate(x0, y0 + 118, 'Local zenith', size = TEXT_SIZE)
    r = 160.
    dx = r*math.cos(math.radians(114. - 90.))
    dy = r*math.sin(math.radians(114. - 90.))
    line(x0, y, x0 + dx, y - dy, LineStyle = 7)
    line(x0, y, x0 - dx, y - dy, LineStyle = 7)
    line(x0, y0 + 0.5*side, x0 + dx, y0 + 0.5*side - dy, LineStyle = 7)
    line(x0, y0 + 0.5*side, x0 - dx, y0 + 0.5*side - dy, LineStyle = 7)
    dx = r*math.cos(math.radians(112. - 90.))
    dy = r*math.sin(math.radians(112. - 90.))
    line(x0, y, x0 + dx, y - dy, LineStyle = 7)
    line(x0, y, x0 - dx, y - dy, LineStyle = 7)
    line(x0, y0 + 0.5*side, x0 + dx, y0 + 0.5*side - dy, LineStyle = 7)
    line(x0, y0 + 0.5*side, x0 - dx, y0 + 0.5*side - dy, LineStyle = 7)
    line(x0 + 0.5*side + csep, y0 - 0.5*side - csep, x0 + 0.5*side + csep,
         y0 + 0.5*side + 2*csep)
    line(x0 - 0.5*side - csep, y0 - 0.5*side - csep, x0 - 0.5*side - csep,
         y0 + 0.5*side + 2*csep)
    line(x0 - 0.5*side - 0.9*csep, y0 + 0.5*side + csep,
         x0 + 0.5*side + 0.9*csep, y0 + 0.5*side + csep)
    annotate(x0, y0 + 0.1*side, 'Calorimeter', size = TEXT_SIZE)
    annotate(x0, y0 + 0.5*side + 2*csep, 'Charge detector', size = TEXT_SIZE)
    annotate(x0 + 1.25*side, y0 - 0.35*side, 'Limb', size = TEXT_SIZE)
    annotate(x0 - 1.25*side, y0 - 0.35*side, 'Limb', size = TEXT_SIZE)


c = gPhysicalCanvas('dish_washer', 300, 220)
drawdd(0, -20)
c.Update()
c.save()
