#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy
import random

from __ROOT__ import *
from __logging__ import logger
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend
from gGraphErrors import gGraphErrors


EMIN_TRUE = 20
EMAX_TRUE = 500000
EMIN_MEAS = 50
EMAX_MEAS = 5000
BINNING = numpy.logspace(math.log10(EMIN_MEAS), math.log10(EMAX_MEAS), 20)

SPECTRUM = gF1('spec', '[0]*x**(-[1])', EMIN_TRUE, EMAX_TRUE)
SPECTRUM.SetParameters(1, 2.75)
CONST = gF1('const', '[0]', EMIN_TRUE, EMAX_TRUE)
CONST.SetParameter(0, 1)
QUAD = gF1('quad', '1 + [0]*x**2', 0, 1)
QUAD.SetParameter(0, 1)

H_TRUE = ROOT.TH1F('hetrue', 'hetrue', len(BINNING) - 1, BINNING)
H_MEAS = ROOT.TH1F('hemeas', 'hemeas', len(BINNING) - 1, BINNING)
H_MEAS.Sumw2()

def generate(gamma = 2.75, eres = 0.3, nevents = 5000000):
    """
    """
    SPECTRUM.SetParameters(1, gamma)
    H_TRUE.Reset()
    H_MEAS.Reset()
    for i in range(nevents):
        etrue = SPECTRUM.GetRandom()
        emeas = random.gauss(etrue, etrue*eres)
        #emeas = random.uniform(etrue - etrue*eres*3**0.5, etrue +
        #                       etrue*eres*3**0.5)
        H_TRUE.Fill(etrue)
        H_MEAS.Fill(emeas)
    H_MEAS.Divide(H_TRUE)
    H_MEAS.Fit('const', 'N')
    H_MEAS.Draw()
    raw_input()
    return CONST.GetParameter(0)


g = ROOT.TGraph()
g.SetMarkerStyle(20)
g.SetLineStyle(7)
for gamma in [3.]:
    for eres in [0.2]:#[0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]:
        val = generate(gamma, eres)
        g.SetPoint(g.GetN(), eres, val)
        g.Draw('alp')
        ROOT.gPad.Update()
g.Fit('quad')
ROOT.gPad.Update()


"""
gamma = 2.75 -> 0.664*x**2
gamma = 3.00 -> 0.990*x**2
gamma = 3.50 -> 1.840*x**2

Uniform eisp
gamma = 3.00, sigma = 0.2 -> 4%
"""
