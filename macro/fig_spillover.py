#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import random
import numpy
import os

from __ROOT__ import *
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1
from __logging__ import logger


ROOT_FILE_PATH = 'data/spillover.root'
RMIN_TRUE = 20
RMAX_TRUE = 500000
RMIN_MEAS = 50
RMAX_MEAS = 5000
NUM_EVENTS = 100000000
MDR = 1000
BINNING = numpy.logspace(math.log10(RMIN_MEAS), math.log10(RMAX_MEAS), 100)

SPECTRUM = gF1('spec', 'x**(-[0])', RMIN_TRUE, RMAX_TRUE)
SPECTRUM.SetParameter(0, 2.75)


def generate():
    """
    """
    logger.info('Generating events...')
    hrtrue = ROOT.TH1F('hrtrue', 'hrtrue', len(BINNING) - 1, BINNING)
    hrmeas = ROOT.TH1F('hrmeas', 'hrmeas', len(BINNING) - 1, BINNING)
    hetatrue = ROOT.TH1F('hetatrue', 'hetatrue', 1000, -1./RMIN_MEAS,
                         1./RMIN_MEAS)
    hetameas = ROOT.TH1F('hetameas', 'hetameas', 1000, -1./1./RMIN_MEAS,
                         1./RMIN_MEAS)
    for i in range(NUM_EVENTS):
        rtrue = SPECTRUM.GetRandom()
        etatrue = 1./rtrue
        res = math.sqrt(0.1**2 + (rtrue/MDR)**2)
        etameas = random.gauss(etatrue, etatrue*res)
        rmeas = 1./etameas
        hrtrue.Fill(rtrue)
        hrmeas.Fill(rmeas)
        hetatrue.Fill(1./rtrue)
        hetameas.Fill(1./rmeas)
    logger.info('Writing output file %s...' % ROOT_FILE_PATH)
    outputFile = ROOT.TFile(ROOT_FILE_PATH, 'RECREATE')
    for h in [hrtrue, hrmeas, hetatrue, hetameas]:
        h.Write()
    outputFile.Close()


if not os.path.exists(ROOT_FILE_PATH):
    generate()

inputFile = ROOT.TFile(ROOT_FILE_PATH)
hrtrue = inputFile.Get('hrtrue')
hrtrue.SetLineColor(ROOT.kGray)
hrtrue.SetXTitle('Rigidity [GV]')
hrtrue.SetYTitle('dN/dR')
hrtrue.SetTitle('True')
hrmeas = inputFile.Get('hrmeas')
hrmeas.SetTitle('Measured')
hetatrue = inputFile.Get('hetatrue')
hetatrue.SetLineColor(ROOT.kGray)
hetatrue.SetXTitle('#eta [GV^{-1}]')
hetatrue.SetYTitle('dN/d#eta')
hetameas = inputFile.Get('hetameas')

c1 = gResidualCanvas('rigidity_spec', Logx = True, Logy = True)
c1.cd(2)
ROOT.gPad.SetLogy(False)
hratio = hrmeas.Clone()
hratio.Divide(hrtrue)
hratio.SetXTitle(hrtrue.GetXaxis().GetTitle())
hratio.SetYTitle('Measured/True     ')
hratio.SetMinimum(0)
hratio.SetMaximum(2.9)
hratio.GetYaxis().SetNdivisions(508)
c1.drawPlots(hrtrue, hratio)
c1.cd(1)
hrmeas.Draw('same')
y = 1e4
a = ROOT.TArrow(MDR, 10*y, MDR, y, 0.02, '->')
store(a)
a.Draw()
l = gLegend(0.2, 0.5, entries = [hrtrue, hrmeas])
l.vstretch(1.6)
l.Draw()
c1.annotate(MDR, 10.5*y, 'MDR', ndc = False, align = 21)
c1.Update()
c1.save()


c2 = gCanvas('spillover')
hetatrue.GetXaxis().SetRangeUser(-0.003, 0.003)
hetatrue.GetXaxis().SetNdivisions(509)
hetatrue.Draw()
hetameas.Draw('same')
a = ROOT.TArrow(1./MDR, 14000, 1./MDR, 8000, 0.02, '->')
store(a)
a.Draw()
c2.annotate(1./MDR, 15000, '#frac{1}{MDR}', ndc = False, align = 21)
l = ROOT.TLine(0, 0, 0, 1.05*hetatrue.GetMaximum())
l.SetLineStyle(7)
store(l)
l.Draw()
l = gLegend(0.2, 0.75, entries = [hrtrue, hrmeas])
l.Draw()
shadow = hetameas.Clone()
for i in range(shadow.GetNbinsX()):
    if shadow.GetBinCenter(i) > 0:
        shadow.SetBinContent(i, 0)
shadow.SetFillStyle(3004)
shadow.SetFillColor(ROOT.kBlack)
shadow.Draw('same')
c2.annotate(0.35, 0.25, 'Spillover')
c2.Update()
c2.save()
