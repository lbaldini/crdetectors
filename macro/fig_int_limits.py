#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gF1 import gF1


GAMMA = 2.75

c = gCanvas('int_limits', Logx = True, Logy = False)
f = gF1('f', '1 - x**(1 - [0])', 1, 100)
f.SetParameter(0, GAMMA)
f.GetXaxis().SetTitle('k')
f.GetYaxis().SetTitle('r')
f.Draw()
c.annotate(0.75, 0.8, '#Gamma = %.2f' % GAMMA)
c.Update()
c.save()
