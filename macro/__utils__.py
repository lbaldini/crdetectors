#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy
import math

from __ROOT__ import *
from gGraphErrors import gGraphErrors



def gratio(name, g1, g2, xmax = None, xmin = None, numPoints = 10, **kwargs):
    """ Divide two TGraph objects.
    """
    if xmin is None:
        xmin = max(g.GetXaxis().GetXmin() for g in [g1, g2])
    if xmax is None:
        xmax = min(g.GetXaxis().GetXmax() for g in [g1, g2])
    gout = gGraphErrors(name, **kwargs)
    for x in numpy.logspace(math.log10(xmin), math.log10(xmax), numPoints):
        gout.SetNextPoint(x, g1.Eval(x)/g2.Eval(x))
    return gout

def gscale(graph, yscale, xscale = 1, **kwargs):
    """ Scale the x and y values of a TGraph.
    """
    x = ROOT.Double()
    y = ROOT.Double()
    name = '%s_scaled' % (graph.GetName())
    gout = gGraphErrors(name, **kwargs)
    for i in range(graph.GetN()):
        graph.GetPoint(i, x, y)
        gout.SetPoint(i, x*xscale, y*yscale)
        gout.SetPointError(i, graph.GetErrorX(i)*xscale,
                           graph.GetErrorY(i)*yscale)
    return gout

def gepscale(graph, power, escale = 1, **kwargs):
    """ Scale the x and y values of a TGraph.
    """
    x = ROOT.Double()
    y = ROOT.Double()
    name = '%s_escaled' % (graph.GetName())
    gout = gGraphErrors(name, **kwargs)
    for i in range(graph.GetN()):
        graph.GetPoint(i, x, y)
        yscale = (x/escale)**power
        gout.SetPoint(i, x, y*yscale)
        gout.SetPointError(i, graph.GetErrorX(i), graph.GetErrorY(i)*yscale)
    return gout

def gmul(name, *graphs, **kwargs):
    """ Multiply TGraph objects.
    """
    x = ROOT.Double()
    y = ROOT.Double()
    gout = gGraphErrors(name, **kwargs)
    xmin = max(g.GetXaxis().GetXmin() for g in graphs)
    xmax = min(g.GetXaxis().GetXmax() for g in graphs)
    first = graphs[0]
    for i in range(graphs[0].GetN()):
        first.GetPoint(i, x, y)
        if x >= xmin and x <= xmax:
            val = 1.
            for g in graphs:
                val *= g.Eval(x)
            gout.SetNextPoint(x, val)
    return gout

def gadd(name, *graphs, **kwargs):
    """ Add TGraph objects.
    """
    x = ROOT.Double()
    y = ROOT.Double()
    gout = gGraphErrors(name, **kwargs)
    xmin = max(g.GetXaxis().GetXmin() for g in graphs)
    xmax = min(g.GetXaxis().GetXmax() for g in graphs)
    first = graphs[0]
    for i in range(graphs[0].GetN()):
        first.GetPoint(i, x, y)
        if x <= xmax:
            val = 0.
            for g in graphs:
                val += g.Eval(x)
            gout.SetNextPoint(x, val)
    return gout

