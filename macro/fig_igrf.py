#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gCoastline import gCoastline


GROUND_FILE = ROOT.TFile('data/igrf-11_maps_alt0_2013.root')
ALT_FILE = ROOT.TFile('data/igrf-11_maps_alt500_2013.root')
NUM_CONTS = 10
COASTLINE = gCoastline(LineColor = ROOT.kGray + 1, LineWidth = 1)


c1 = gCanvas('sea_level_mag_field', RightMargin = 0.07)
h1 = GROUND_FILE.Get('h_B_abs')
h1.SetContour(NUM_CONTS)
h1.SetMinimum(0.2)
h1.SetMaximum(0.7)
h1c = h1.Clone()
h1c.SetContour(5*NUM_CONTS)
h1c.SetLineWidth(1)
h1c.SetLineColor(ROOT.kGray)
h1c.Draw('cont3')
c1.annotate(0.99, 0.95, 'Surface magnetic field [G]', angle = 90,
            size = TEXT_SIZE, align = 31)
COASTLINE.Draw()
h1.Draw('cont3,same')
annotateContours(h1, NUM_CONTS, x1 = 0.18, y1 = 0.05, x2 = 0.8, y2 = 0.9,
                 fmt = '%.2f')
c1.Update()
c1.save()

c2 = gCanvas('high_alt_mc_ilwain_L', RightMargin = 0.07)
h2 = ALT_FILE.Get('h_mc_ilwain_L')
h2.SetContour(NUM_CONTS)
h2.SetMinimum(0)
h2.SetMaximum(10)
h2c = h2.Clone()
h2c.SetContour(5*NUM_CONTS)
h2c.SetLineWidth(1)
h2c.SetLineColor(ROOT.kGray)
h2c.Draw('cont3')
c1.annotate(0.99, 0.95, 'McIlwain L @ h = 500 km', angle = 90,
            size = TEXT_SIZE, align = 31)
COASTLINE.Draw()
h2.Draw('cont3,same')
annotateContours(h2, NUM_CONTS, x1 = 0.6, y1 = 0.16, x2 = 0.85, y2 = 0.87,
                 fmt = '%.1f', dxbox = 0.0175)
c2.Update()
c2.save()

c3 = gCanvas('high_alt_rigidity_cutoff', RightMargin = 0.07)
h3 = ALT_FILE.Get('h_rigidity_cutoff')
h3.SetContour(NUM_CONTS)
h3.SetMinimum(0)
h3.SetMaximum(20)
h3c = h3.Clone()
h3c.SetContour(4*NUM_CONTS)
h3c.SetLineWidth(1)
h3c.SetLineColor(ROOT.kGray)
h3c.Draw('cont3')
c1.annotate(0.99, 0.95, 'Vertical rigidity cutoff @ h = 500 km [GV]',
            angle = 90, size = TEXT_SIZE, align = 31)
COASTLINE.Draw()
h3.Draw('cont3,same')
annotateContours(h3, NUM_CONTS, x1 = 0.4, y1 = 0.16, x2 = 0.95, y2 = 0.87,
                 fmt = '%.0f', dxbox = 0.0125)
c3.Update()
c3.save()
