#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from gGraphErrors import gGraphErrors
from gF1 import gF1

import numpy


CUT_VALUE = 0.7


fsig = gF1('sig', '[0]*x**[1]', 0, 1)
fbkg = gF1('bkg', '[0]*exp(-x/[1])', 0, 1)

fsig.SetParameters(1, 3)
fbkg.SetParameters(1, 0.2)

for f in [fsig, fbkg]:
    f.SetParameter(0, 1./f.Integral(0, 1))
    f.GetXaxis().SetTitle('Discriminating variable [a. u.]')
    f.GetYaxis().SetTitle('Probability density function [a. u.^{-1}]')

def getRejectionPower(x):
    """
    """
    try:
        return fsig.Integral(x, 1)/fbkg.Integral(x, 1)
    except ZeroDivisionError:
        return None


def getROCPoint(x):
    """
    """
    truepos = fsig.Integral(x, 1)
    falsepos = fbkg.Integral(x, 1)
    totpos = falsepos + truepos
    totneg = 2. - totpos
    try:
        return (falsepos/totneg, truepos/totpos)
    except ZeroDivisionError:
        return (None, None)



c1 = gCanvas('discr_variable')
fsig.Draw()
fbkg.Draw('same')
fsig2 = fsig.Clone()
fsig2.SetRange(CUT_VALUE, 1)
fsig2.SetFillColor(ROOT.kBlack)
fsig2.SetFillStyle(3004)
fsig2.Draw('fc,same')
fbkg2 = fbkg.Clone()
fbkg2.SetRange(CUT_VALUE, 1)
fbkg2.SetFillColor(ROOT.kBlack)
fbkg2.SetFillStyle(3003)
fbkg2.Draw('fc,same')
l = ROOT.TLine(CUT_VALUE, 0, CUT_VALUE, fsig.Eval(CUT_VALUE))
l.SetLineStyle(7)
l.Draw()
c1.annotate(0.23, 0.75, 'Background')
c1.annotate(0.78, 0.75, 'Signal')
c1.annotate(CUT_VALUE, 0.5, 'Cut value', align = 21)
c1.Update()
c1.save()


c2 = gCanvas('rejection_power')
h2 = gH1F('h2', 'h2', 150, 0, 1, XTitle = 'Signal efficiency',
          YTitle = 'Rejection power', Maximum = 150)
g2 = gGraphErrors('g2')
for x in numpy.linspace(0, 1, 500):
    s = fsig.Integral(x, 1)
    b = fbkg.Integral(x, 1)
    try:
        rpwr = s/b
        g2.SetNextPoint(s, rpwr)
    except ZeroDivisionError:
        pass
h2.Draw()
g2.Draw('lsame')
c2.Update()
c2.save()


#c3 = gCanvas('roc_curve')
#g3 = gGraphErrors('g3')
#for x in numpy.linspace(0.0, 1, 100):
#    _x, _y = getROCPoint(x)
#    if _x is not None:
#        g3.SetNextPoint(_x, _y)
#g3.Draw('al')
#c3.Update()
#c3.save()
