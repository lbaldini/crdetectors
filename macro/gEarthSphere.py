#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *

import array


class gEarthSphere(ROOT.TGeoSphere):
    
    """ Small utility class describing the Earth's sphere.
    """

    def __init__(self, **kwargs):
        """ Constructor.
        """
        ROOT.TGeoSphere.__init__(self, 0, 1)
        self.__RotPadding = kwargs.get('rotpadding', 1.8)
        self.__EquPadding = kwargs.get('equpadding', 1.8)
        self.RotAxis = ROOT.TPolyLine3D()
        self.RotAxis.SetNextPoint(0, 0, -self.__RotPadding)
        self.RotAxis.SetNextPoint(0, 0, self.__RotPadding)
        self.Axis0 = ROOT.TPolyLine3D()
        self.Axis0.SetNextPoint(1, 0, 0)
        self.Axis0.SetNextPoint(self.__EquPadding, 0, 0)
        self.Axis90 = ROOT.TPolyLine3D()
        self.Axis90.SetNextPoint(0, 1, 0)
        self.Axis90.SetNextPoint(0, self.__EquPadding, 0)
        self.Axis180 = ROOT.TPolyLine3D()
        self.Axis180.SetNextPoint(-1, 0, 0)
        self.Axis180.SetNextPoint(-self.__EquPadding, 0, 0)
        self.Axis270 = ROOT.TPolyLine3D()
        self.Axis270.SetNextPoint(0, -1, 0)
        self.Axis270.SetNextPoint(0, -self.__EquPadding, 0)

    def __getPadCoordinates(self, x, y, z):
        """ Return the (x, y) coordinates on the pad corresponding to the 
        (x, y, z) coordinates in the real world.
        """
        pw = array.array('d', [x, y, z])
        pn = array.array('d', [0.0, 0.0, 0.0])
        ROOT.gPad.GetView().WCtoNDC(pw, pn)
        x = pn[0]
        y = pn[1]
        z = pn[2]
        return (x, y)

    def annotatePad(self, x, y, z, text, size = LABEL_TEXT_SIZE,
                    align = 11, color = ROOT.kBlack, angle = 0):
        """ Annotate the parent pad.
        """
        x, y = self.__getPadCoordinates(x, y, z)
        ROOT.gPad.annotate(x, y, text, size, False, align, color, angle)

    def drawRotationAxis(self, labels = True):
        """ Draw the N-S rotation axis.
        """
        self.RotAxis.Draw()
        if labels:
            self.annotatePad(0, 0, self.__RotPadding, 'N', align = 21)
            self.annotatePad(0, 0, -self.__RotPadding, 'S', align = 23)

    def drawEquatorialAxes(self, labels = True):
        """ Draw the four equatorial axes.
        """
        for axis in [self.Axis0, self.Axis90, self.Axis180, self.Axis270]:
            axis.Draw()
        if labels:
            pad = 0.15
            x, y, z = self.__EquPadding, 0, 0
            self.annotatePad(x, y, z, '0#circ', align = 12)
            self.annotatePad(x - pad, y + pad, z, 'E', align = 22,
                             size = SMALL_TEXT_SIZE)
            self.annotatePad(x - pad, y - pad, z, 'W', align = 22,
                             size = SMALL_TEXT_SIZE)
            x, y, z = 0, self.__EquPadding, 0
            self.annotatePad(x, y, z, '90#circ', align = 21)
            self.annotatePad(x - pad, y - pad, z, 'E', align = 22,
                             size = SMALL_TEXT_SIZE)
            self.annotatePad(x + pad, y - pad, z, 'W', align = 22,
                             size = SMALL_TEXT_SIZE)
            x, y, z = -self.__EquPadding, 0, 0
            self.annotatePad(x, y, z, '180#circ', align = 32)
            self.annotatePad(x + pad, y - pad, z, 'E', align = 22,
                             size = SMALL_TEXT_SIZE)
            self.annotatePad(x + pad, y + pad, z, 'W', align = 22,
                             size = SMALL_TEXT_SIZE)
            x, y, z = 0, -self.__EquPadding, 0
            self.annotatePad(x, y, z, '270#circ', align = 23)
            self.annotatePad(x + pad, y + pad, z, 'E', align = 22,
                             size = SMALL_TEXT_SIZE)
            self.annotatePad(x - pad, y + pad, z, 'W', align = 22,
                             size = SMALL_TEXT_SIZE)

    def Draw(self, opts = '', **kwargs):
        """ Draw the actual Earth sphere.
        """
        ROOT.TGeoSphere.Draw(self, opts)
        zoom = kwargs.get('zoom', 0.8)
        view = ROOT.gPad.GetView()
        view.ZoomView(0, zoom)
        view.AdjustPad()
        return view

    def DrawSide(self, opts = '', labels = True, **kwargs):
        """ Draw the side view.
        """
        view = self.Draw(opts, **kwargs)
        view.Side()
        self.drawRotationAxis(labels)
        return view

    def DrawFront(self, opts = '', labels = True, **kwargs):
        """ Draw the side view.
        """
        view = self.Draw(opts, **kwargs)
        view.Front()
        self.drawRotationAxis(labels)
        return view

    def DrawTop(self, opts = '', labels = True, **kwargs):
        """ Draw the top view.
        """
        view = self.Draw(opts, **kwargs)
        view.Top()
        self.drawEquatorialAxes(labels)
        return view



if __name__ == '__main__':
    from gCanvas import gCanvas
    c = gCanvas('c')
    earth = gEarthSphere(equpadding = 2.5)
    view = earth.DrawTop(zoom = 0.5)
    c.Update()
