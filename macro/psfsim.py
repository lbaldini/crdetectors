#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import random

from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1


PITCH = 0.0228
SIGMA_HIT = PITCH/math.sqrt(12.)
LAYER_SPACING = 3.5
LINE = ROOT.TF1('line', '[0] + [1]*x')


def getDirErr(numLayers):
    """
    """
    g = ROOT.TGraphErrors()
    for i in range(numLayers):
        x = i*LAYER_SPACING
        y = random.gauss(0., SIGMA_HIT)
        g.SetPoint(i, x, y)
        g.SetPointError(i, 0., SIGMA_HIT)
    g.Fit('line', 'Q')
    slope = LINE.GetParameter(1)
    return math.degrees(slope)


n = 20
h = gH1F('h', 'h', 100, -0.2, 0.2)
for evt in range(10000):
    err = getDirErr(n)
    h.Fill(err)
h.Draw()

print h.GetRMS()
delta = SIGMA_HIT/LAYER_SPACING
print math.degrees(delta/math.sqrt(n*(n+1)*(2*n+1)/6. - n*(n+1)**2/4.))
print math.degrees(delta/math.sqrt(n**3/12.))
