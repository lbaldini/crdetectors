#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *


class gGraphEnvelope:

    """
    """

    def __init__(self, glist):
        """
        """
        self.GraphLo = glist[0].Clone('glo')
        self.GraphHi = glist[0].Clone('ghi')
        x = ROOT.Double()
        y = ROOT.Double()
        for i in xrange(self.GraphLo.GetN()):
            vals = []
            for g in glist:
                g.GetPoint(i, x, y)
                vals.append(float(y))
            self.GraphLo.SetPoint(i, x, min(vals))
            self.GraphHi.SetPoint(i, x, max(vals))

    def Draw(self):
        """
        """
        self.GraphLo.Draw('lsame')
        self.GraphHi.Draw('lsame')
