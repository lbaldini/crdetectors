#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import __csv__

from __ROOT__ import *
from __pdg__ import *
from __labels__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gH1F import gH1F
from gLegend import gLegend




c = gCanvas('tracer_signal', Logx = True)
h = gH1F('frame', 'frame', 100, 1e-1, 6e3, Minimum = 0, Maximum = 3,
         XTitle = AXIS_TITLE_KIN_ENERGY_N, YTitle = 'Signal [MIP/Z^{2}]')
h.Draw()
g1 = __csv__.read('data/tracer_cher.csv', 'cher')
g2 = __csv__.read('data/tracer_dedx.csv', 'dedx', LineStyle = 7)
g3 = __csv__.read('data/tracer_trd.csv', 'tdr')
g1.Draw('c,same')
g2.Draw('c,same')
g3.Draw('l,same')
c.annotate(3, 2.3, 'Cherenkov #times 2', ndc = False)
c.annotate(30, 1.3, 'dE/dx', ndc = False)
c.annotate(1000, 2.5, 'TRD', ndc = False)
c.Update()
c.save()
