#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from gDetectorElements import *
from gCanvas import gCanvas

c = gCanvas('plane_detector')
geo = ROOT.TGeoManager('Geometry manager', 'Geometry manager')
plane = gDetectorPlane()
geo.SetTopVolume(plane)
geo.CloseGeometry()
geo.SetVisLevel(4)
geo.SetVisOption(0)
plane.Draw()
view3d = ROOT.gPad.GetView()
view3d.SetView(230, 70, 0, ROOT.Long())
view3d.ZoomView(0, 1.1)
drawReferenceSystem()
drawSkyDir()
c.Update()
c.save()

