#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy
import random

from __ROOT__ import *
from __constants__ import *
from __pdg__ import *
from __logging__ import logger
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend
from gGraphErrors import gGraphErrors


ROOT_FILE_PATH = 'data/edisp.root'
EMIN_TRUE = 20
EMAX_TRUE = 500000
EMIN_MEAS = 50
EMAX_MEAS = 5000
ERES = 0.3
NUM_EVENTS = 100000000
BINNING = numpy.logspace(math.log10(EMIN_MEAS), math.log10(EMAX_MEAS), 100)

SPECTRUM = gF1('spec', 'x**(-[0])', EMIN_TRUE, EMAX_TRUE)
SPECTRUM.SetParameter(0, 2.75)

def generate():
    """
    """
    logger.info('Generating events...')
    hetrue = ROOT.TH1F('hetrue', 'hetrue', len(BINNING) - 1, BINNING)
    hemeas = ROOT.TH1F('hemeas', 'hemeas', len(BINNING) - 1, BINNING)
    for i in range(NUM_EVENTS):
        etrue = SPECTRUM.GetRandom()
        emeas = random.gauss(etrue, etrue*ERES)
        hetrue.Fill(etrue)
        hemeas.Fill(emeas)
    logger.info('Writing output file %s...' % ROOT_FILE_PATH)
    outputFile = ROOT.TFile(ROOT_FILE_PATH, 'RECREATE')
    for h in [hetrue, hemeas]:
        h.Write()
    outputFile.Close()


#generate()

c1 = gCanvas('energy_redistribution')
h1 = gH1F('h1', 'h1', 3, 0, 3, Minimum = 0, Maximum = 250,
          XTitle = 'Energy [a. u.]', YTitle = 'Counts/bin')
h1.GetXaxis().SetLabelOffset(10)
h1.GetYaxis().SetLabelOffset(10)
h1.SetBinContent(1, 200)
h1.SetBinContent(2, 100)
h1.SetBinContent(3, 20)
f1 = gF1('f1', 'gaus', 0, 3, LineColor = ROOT.kGray)
f1.SetParameters(1.*h1.GetBinContent(2), 1.5, 0.5)
h1.Draw()
f1.Draw('same')
h1.Draw('same')
h1c = h1.Clone()
h1c.SetLineStyle(7)
n = 0.5*(1 - 0.68)*h1.GetBinContent(2)
h1c.SetBinContent(1, h1.GetBinContent(1) + n)
h1c.SetBinContent(2, h1.GetBinContent(2) - 2*n)
h1c.SetBinContent(3, h1.GetBinContent(3) + n)
h1c.Draw('same')
y = -18
c1.annotate(0.5, y, 'i - 1', ndc = False, align = 21)
c1.annotate(1.5, y, 'i', ndc = False, align = 21)
c1.annotate(2.5, y, 'i + 1', ndc = False, align = 21)
y = 150
al = ROOT.TArrow(1.5, y, 0.5, y, 0.02, '->')
al.Draw()
ar = ROOT.TArrow(1.5, y, 2.5, y, 0.02, '->')
ar.Draw()
dy = 10
c1.annotate(1.5, y - dy, '-0.32 n_{i}', ndc = False, align = 23)
c1.annotate(0.5, y + dy, '+0.16 n_{i}', ndc = False, align = 21)
c1.annotate(2.5, y + dy, '+0.16 n_{i}', ndc = False, align = 21)
c1.Update()
c1.save()


inputFile = ROOT.TFile(ROOT_FILE_PATH)
hetrue = inputFile.Get('hetrue')
hetrue.SetLineColor(ROOT.kGray)
hetrue.SetXTitle('Energy [GeV]')
hetrue.SetYTitle('dN/dE')
hetrue.SetTitle('True')
hemeas = inputFile.Get('hemeas')
hemeas.SetTitle('Measured')

c2 = gResidualCanvas('energy_spec', Logx = True, Logy = True)
c2.cd(2)
ROOT.gPad.SetLogy(False)
hratio = hemeas.Clone()
hratio.Divide(hetrue)
hratio.SetXTitle(hetrue.GetXaxis().GetTitle())
hratio.SetYTitle('Measured/True     ')
hratio.SetMinimum(0.81)
hratio.SetMaximum(1.19)
hratio.GetYaxis().SetNdivisions(506)
c2.drawPlots(hetrue, hratio)
c2.cd(1)
hemeas.Draw('same')
l = gLegend(0.2, 0.55, entries = [hetrue, hemeas])
l.vstretch(1.6)
l.Draw()
c2.Update()
c2.save()


c3 = gCanvas('edisp_pl_offset')
h3 = gH1F('h3', 'h3', 100, 0, 0.6, Minimum = 0, Maximum = 0.35,
          XTitle = '#sigma_{E}/E', YTitle = 'Offset')
h3.Draw()
foffset = gF1('foffset', '0.5*([0] - 1)*([0] - 2)*x**2')
for gamma in [2.25, 2.5, 2.75, 3, 3.25]:
    foffset.SetParameter(0, gamma)
    foffset.DrawCopy('same')
    x = 0.4
    der = math.degrees((gamma - 1)*(gamma - 2)*x)
    c3.annotate(x, 0.005 + foffset.Eval(x), '#Gamma = %.2f ' % gamma,
                ndc = False, size = SMALL_TEXT_SIZE, align = 21, angle = der)
c3.Update()
c3.save()
