#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import sys
import datetime

from __settings__ import *
sys.path.append(CRDETECTORS_ROOT)

from __ROOT__ import *
from __logging__ import logger


def str2datetime(text):
    """ 1997/08/01-000000
    """
    d, t = text.split('-')
    year, month, day = [int(item) for item in d.split('/')]
    hours = int(t[0:2])
    mins = int(t[2:4])
    secs = int(t[4:6])
    try:
        return datetime.datetime(year, month, day, hours, mins, secs)
    except:
        logger.error('Error processing text %s...' % text)
        logger.error('Going back by one day :-(')
        return datetime.datetime(year, month, day - 1, hours, mins, secs)


class gDataSet:

    """ Class describing a data set.
    """
    
    def __init__(self, entry, uniqueId, **kwargs):
        """ Constructor.

        Initialize a data set and set the firts entry.
        """
        self.Quantity = entry['QUANTITY_NAME']
        self.SubExpName = entry['SUB-EXP_NAME']
        self.ExpName = self.SubExpName.split('(')[0].strip()
        self.EAxis = entry['EAXIS_TYPE']
        self.Url = entry['ADS_URL_FOR_PAPER_REF']
        # Horrible tweaks.
        self.Url = self.Url.replace('%26', '&').replace('A+A', 'A&A')
        self.Phi = entry['phi']
        self.Datimes = entry['DATIMES']
        timeRanges = self.Datimes.split(';')
        start = timeRanges[0].split(':')[0]
        stop = timeRanges[-1].split(':')[1] 
        self.StartDate = str2datetime(start)
        self.StopDate = str2datetime(stop)
        self.UniqueId = uniqueId
        self.Name = 'dataset_%d' % self.UniqueId
        self.Title = '[%4s] %s (%s)' %\
                     (self.UniqueId, self.SubExpName, self.Quantity)
        self.__Entries = []
        self.addEntry(entry)

    def __str__(self):
        """ String formatting.
        """
        return '[%4s] %s (%s, %.3f--%.3f)' %\
            (self.UniqueId, self.SubExpName, self.Quantity,
             self.getEAxisMinimum(), self.getEAxisMaximum())

    def __cmp__(self, other):
        """ Sorting operator.
        """
        if self.Quantity > other.Quantity:
            return 1
        elif self.Quantity == other.Quantity:
            if self.StartDate > other.StartDate:
                return 1
            else:
                return -1
        else:
            return -1

    def getEAxisMinimum(self):
        """ Get the minimum value on the x-axis.
        """
        return min([entry['EBIN_LOW'] for entry in self.__Entries])

    def getEAxisMaximum(self):
        """ Get the maximum value on the x-axis.
        """
        return max([entry['EBIN_HIGH'] for entry in self.__Entries])

    def addEntry(self, entry):
        """ Add an entry to the data set.
        """
        if entry['EAXIS_TYPE'] != self.EAxis:
            logger.error('Mixing EAXIS_TYPE for %s' % self)
            logger.error('Skipping entry...')
            return 
        self.__Entries.append(entry)

    def Write(self):
        """ Write the ROOT object to file.
        
        At this point the graph is filled and we can set the axis titles.
        """
        g = ROOT.TGraphAsymmErrors()
        g.SetName(self.Name)
        g.SetTitle(self.Title)
        g.SetMarkerStyle(20)
        for i, entry in enumerate(self.__Entries):
            dyhi = math.sqrt(entry['ERR_STAT+']**2 + entry['ERR_SYST+']**2)
            dylo = math.sqrt(entry['ERR_STAT-']**2 + entry['ERR_SYST-']**2)
            g.SetPoint(i, entry['<E>'], entry['QUANTITY_VALUE'])
            g.SetPointEYhigh(i, dyhi)
            g.SetPointEYlow(i, dylo)
        g.GetXaxis().SetTitle(self.EAxis)
        g.GetYaxis().SetTitle(self.Quantity)
        g.Write()
        



if __name__ == '__main__':
    from gDbEntry import gDbEntry
    from gCanvas import gCanvas

    dbf = open(CRDB_TEXT_FILE_PATH)
    line = dbf.next()
    while line.startswith('#'):
        line = dbf.next()
    entry = gDbEntry(line)
    dataset = gDataSet(entry, 0)
    line = dbf.next()
    entry = gDbEntry(line)
    while entry['QUANTITY'] == dataset.Quantity:
        dataset.addEntry(entry)
        line = dbf.next()
        entry = gDbEntry(line)
    dbf.close()
    c = gCanvas('c')
    dataset.Draw('ap')
    c.Update()
    print(dataset)
    print(dataset.ExpName)
    print(dataset.Datimes)
    print(dataset.StartDate)
    print(dataset.StopDate)
