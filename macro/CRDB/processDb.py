#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import pickle

from __settings__ import *
sys.path.append(CRDETECTORS_ROOT)

from __ROOT__ import *
from __logging__ import *
from gDbEntry import gDbEntry
from gDataSet import gDataSet


def process():
    """
    """
    CR_DATASET_LIST = []
    logger.info('Opening input file %s...' % CRDB_TEXT_FILE_PATH)
    inputFile = open(CRDB_TEXT_FILE_PATH)
    # Skip the comment lines.
    line = inputFile.next()
    while line.startswith('#'):
        line = inputFile.next()
    # Now the actual file content.
    dataset = None
    for line in inputFile:
        entry = gDbEntry(line)
        if dataset is None or entry['QUANTITY_NAME'] != dataset.Quantity or \
           entry['SUB-EXP_NAME'] != dataset.SubExpName:
            dataset = gDataSet(entry, len(CR_DATASET_LIST))
            logger.info('Created new data set %s' % dataset)
            CR_DATASET_LIST.append(dataset)
        else:
            dataset.addEntry(entry)
    inputFile.close()
    logger.info('Input file closed.')
    logger.info('Opening output file %s...' % CRDB_ROOT_FILE_PATH)
    outputRootFile = ROOT.TFile(CRDB_ROOT_FILE_PATH, 'RECREATE')
    for dataset in CR_DATASET_LIST:
        dataset.Write()
    outputRootFile.Close()
    logger.info('Done, ROOT file closed.')
    logger.info('Opening output file %s...' % CRDB_PICKLE_FILE_PATH)
    outputPickleFile = open(CRDB_PICKLE_FILE_PATH, 'w')
    pickle.dump(CR_DATASET_LIST, outputPickleFile)
    outputPickleFile.close()
    logger.info('Done, pickle file closed.')
 


if __name__ == '__main__':
    process()
