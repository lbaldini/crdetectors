#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os


CRDB_ROOT = os.path.abspath(os.path.dirname(__file__))
CRDB_TEXT_FILE_PATH = os.path.join(CRDB_ROOT, 'usine_all_database.dat')
CRDB_ROOT_FILE_PATH = os.path.join(CRDB_ROOT, 'usine_all_database.root')
CRDB_PICKLE_FILE_PATH = os.path.join(CRDB_ROOT, 'usine_all_database.pickle')
CRDETECTORS_ROOT = os.path.abspath(os.path.join(CRDB_ROOT, '..'))

def dbsplit(line):
    """
    """
    return line.strip('\n# ').split()

""" Peek at the database file and parse the header to grab the field names.
"""
dbf = open(CRDB_TEXT_FILE_PATH)
line = dbf.next()
while not line.startswith('# Format: USINE code'):
    line = dbf.next()
line = dbf.next()
CRDB_FIELDS = []
while 'Col.' in line:
    field = line.strip().split(' - ')[1]
    field = field.split(':')[0]
    field = field.split('(')[0]
    field = field.split('[')[0]
    field = field.strip().replace(' ', '_')
    CRDB_FIELDS.append(field)
    line = dbf.next()
while line.startswith('#'):
    line = dbf.next()
CRBD_NUMERIC_FIELDS = []
for i, item in enumerate(dbsplit(line)):
    try:
        item = float(item)
        CRBD_NUMERIC_FIELDS.append(CRDB_FIELDS[i])
    except:
        pass
dbf.close()






if __name__ == '__main__':
    print(CRDB_ROOT)
    print(CRDETECTORS_ROOT)
    print(CRDB_TEXT_FILE_PATH)
    print(CRDB_FIELDS)
    print(CRBD_NUMERIC_FIELDS)
