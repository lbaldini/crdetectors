#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity, getEGBIntensity
from gLegend import gLegend


CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
CRDB_PICKLE_FILE = open('CRDB/usine_all_database.pickle')
CR_MODELS_ROOT_FILE = ROOT.TFile('data/cr_models.root')


def printDataSets():
    """
    """
    from CRDB.__settings__ import CRDB_PICKLE_FILE_PATH
    CR_DATASET_LIST = pickle.load(open(CRDB_PICKLE_FILE_PATH))
    CR_DATASET_LIST.sort()
    for ds in CR_DATASET_LIST:
        if 'CREAM' in ds.ExpName:
            logger.info(ds)


def loadDataset(uid, title, A, **kwargs):
    """
    """
    g1 = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g1.SetMarkerStyle(kwargs.get('markerStyle', 20))
    g1.SetMarkerSize(kwargs.get('markerSize', 1.2))
    g1.SetTitle(title)
    g2 = __utils__.gscale(g1, 1./A, A)
    g2.SetMarkerStyle(kwargs.get('markerStyle', 20))
    g2.SetMarkerSize(kwargs.get('markerSize', 1.2))
    g2.SetTitle(title)
    return g1, g2


H, H_et = loadDataset(492, 'H', 1., markerStyle = 20)
He, He_et = loadDataset(493, 'He', 4., markerStyle = 25)
C, C_et = loadDataset(497, 'C', 12., markerStyle = 24)
N, N_et = loadDataset(499, 'N', 14., markerStyle = 22)
O, O_et = loadDataset(501, 'O', 16., markerStyle = 23)
Mg, Mg_et = loadDataset(504, 'Mg', 24., markerStyle = 28)
Si, Si_et = loadDataset(506, 'Si', 28., markerStyle = 26)
Fe, Fe_et = loadDataset(508, 'Fe', 56., markerStyle = 21)


#H.Print('all')
#He.Print('all')

f = ROOT.TF1('f', '[0]*x**[1]')
H.Fit(f)

r1 = H.Eval(20000)/He.Eval(20000)
r2 = H_et.Eval(20000)/He_et.Eval(20000)
print r1, r2, r1/r2, 4**1.66


c1 = gVertCanvas('cream_ek', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 30, 3e5, Minimum = 1e-12, Maximum = 1e-3,
          XTitle = 'Kinetic energy/nucleon [GeV/nucleon]',
          YTitle = 'Flux [nucleons m^{-2} s^{-1} sr^{-1} GeV^{-1}]')
h1.Draw()
l1 = gLegend(0.2, 0.6)
l1.vstretch(0.75)
for g in [H, He, C, N, O, Mg, Si, Fe]:
    g.Draw('psame')
    l1.AddEntry(g, g.GetTitle(), 'p')
l1.Draw()
c1.Update()


c2 = gVertCanvas('cream_ekn', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 1000, 30, 3e5, Minimum = 1e-12, Maximum = 1e-3,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Flux [m^{-2} s^{-1} sr^{-1} GeV^{-1}]')
h2.Draw()
l2 = gLegend(0.2, 0.6)
l2.vstretch(0.75)
for g in [H_et, He_et, C_et, N_et, O_et, Mg_et, Si_et, Fe_et]:
    g.Draw('psame')
    l2.AddEntry(g, g.GetTitle(), 'p')
l2.Draw()
c2.Update()
