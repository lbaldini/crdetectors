#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F



ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
DATA_SET_UIDS = [136, 456, 833]
DATA_SET_MARKERS = []


def printDataSets():
    """
    """
    import pickle
    from CRDB.__settings__ import CRDB_PICKLE_FILE_PATH
    CR_DATASET_LIST = pickle.load(open(CRDB_PICKLE_FILE_PATH))
    for ds in CR_DATASET_LIST:
        if ds.Quantity == 'H':
            print ds


printDataSets()

c1 = gCanvas('protons', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 100, 0.1, 250000, Minimum = 1e-11, Maximum = 1e4,
          XTitle = 'Kinetic energy [GeV]', YTitle = 'Flux [GeV^{-1} s^{-1}]')
h1.Draw()
for uid in DATA_SET_UIDS:
    g = ROOT_FILE.Get('dataset_%d' % uid)
    g.Draw('psame')
c1.Update()


