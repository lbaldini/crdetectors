#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy
import os

from __ROOT__ import *
from __logging__ import logger
from __constants__ import SECS_PER_YEAR
from __labels__ import *
from gGDEModel import gGDEModel
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors


DGE_STAT_FILE_PATH = 'data/gdestat.root'
STAT_HIST_NAME = 'hstat'
ENERGY_THRESHOLD = 10 # GeV
PSF_CONT_68 = 0.15    # degrees
DELTA_OMEGA = math.pi*((math.radians(PSF_CONT_68))**2.)
MINIMUM_COUNTS = 25
LIVETIME = 10. #years


GDE = gGDEModel()
GC_SPECTRUM = GDE.spectrum(91, 46)
GC_INT_SPECTRUM = GC_SPECTRUM.getIntegralGraphPL()


def writeStatHist():
    """
    """
    logger.info('Recreating stat histogram...')
    h = GDE.histogram(0).Clone()
    h.Clear()
    h.SetNameTitle(STAT_HIST_NAME, 'Integral counts above %.2f GeV' %\
                   ENERGY_THRESHOLD)
    h.GetZaxis().SetTitle('')
    for i in range(1, h.GetNbinsX() + 1):
        for j in range(1, h.GetNbinsY() + 1):
            counts = GDE.spectrum(i, j).getIntegralGraphPL().Eval(10)
            counts *= DELTA_OMEGA
            h.SetBinContent(i, j, counts)
    f = ROOT.TFile(DGE_STAT_FILE_PATH, 'RECREATE')
    h.Write()
    f.Close()
    logger.info('Done.')


if not os.path.exists(DGE_STAT_FILE_PATH):
    writeStatHist()


# Plot of the GDE intensity around 10 GeV across the sky.
c1 = gCanvas('gde_model', Logz = True)
c1.colz()
model = GDE.histogram(17).Clone()
model.Scale(1./SECS_PER_YEAR)
model.SetXTitle('Galactic longitude [#circ]')
model.SetYTitle('Galactic latitude [#circ]')
model.SetZTitle(axisTitleIntensity())
model.Draw('cont4z')
energy = float(model.GetTitle().split()[0].strip())
c1.annotate(0.2, 0.85, '%.2f GeV' % energy, color = ROOT.kWhite)
c1.Update()
c1.save()


# Energy spectrum of the DGE intensity at the galactic center.
c2 = gCanvas('gde_spectrum', Logx = True, Logy = True)
GC_SPECTRUM.Scale(1./SECS_PER_YEAR)
GC_SPECTRUM.GetYaxis().SetTitle(axisTitleIntensity())
GC_SPECTRUM.SetLineStyle(7)
GC_SPECTRUM.Draw('alp')
c2.annotate(0.7, 0.87, '(L, B) = (0, 0)')
c2.Update()
c2.save()


# Now the integral spectrum above 10 GeV within one PSF 68% containment angle.
c3 = gCanvas('gde_statistics', Logz = True)
c3.colz()
f = ROOT.TFile(DGE_STAT_FILE_PATH)
hstat = f.Get(STAT_HIST_NAME)
title = 'Integral flux > 10 GeV within #theta_{68} [m^{-2} year^{-1}]'
hstat.GetZaxis().SetTitle(title)
hstat.Draw('cont4z')
c1.annotate(0.2, 0.85, '#theta_{68} = %.2f#circ' % PSF_CONT_68,
            color = ROOT.kWhite)
c3.Update()
c3.save()


# 
c4 = gCanvas('gde_statsys', Logx = True, Logy = True)
h2 = ROOT.TH2F('h2', 'h2', 100, 0.1, 10, 100, 0.02, 30)
h2.SetXTitle('Acceptance [m^{2} sr]')
h2.SetYTitle('PSF 68% containment [#circ]')
h2.Draw()

def constant(energy):
    """
    """
    return MINIMUM_COUNTS*4*180**2/LIVETIME/(math.pi**2)/\
        GC_INT_SPECTRUM.Eval(energy)

c10 = constant(10)
c100 = constant(100)
f2 = ROOT.TF1('f10', '([0]/x)**0.5', 0.1, 10)
f2.SetLineWidth(2)
f2.SetParameter(0, c10)
c4.annotate(0.2, 1.1*f2.Eval(0.2), 'E_{0} = 10 GeV', ndc = False, angle = -14)
f2.DrawCopy('same')
f2.SetParameter(0, c100)
c4.annotate(0.2, 1.1*f2.Eval(0.2), 'E_{0} = 100 GeV', ndc = False, angle = -14)
f2.DrawCopy('same')
m = ROOT.TMarker(2, 0.15, 20)
m.Draw()
c4.annotate(2.0, 0.15, ' LAT ', ndc = False, align = 11)
c4.annotate(0.2, 0.86, 'T_{obs} = %d years' % LIVETIME)
c4.annotate(0.2, 0.25, 'Statistics-limited')
c4.annotate(0.7, 0.8, 'PSF-limited')
c4.Update()
c4.save()
