#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *


class gTextMarker:

    """ Small Utiliti class describing a marker accompanied by a piece
    of text (e.g., for indicating a detector on a canvas.)
    """

    def __init__(self, x, y, text, **kwargs):
        """ Constructor.
        """
        markerStyle = kwargs.get('markerStyle', 20)
        markerSize = kwargs.get('markerSize', 1)
        color = kwargs.get('color', ROOT.kBlack)
        offset = kwargs.get('offset', 0)
        xoffset = offset or kwargs.get('xoffset', 0)
        yoffset = offset or kwargs.get('yoffset', 0)
        self.__Marker = ROOT.TMarker(x, y, markerStyle)
        self.__Marker.SetMarkerSize(markerSize)
        self.__Marker.SetMarkerColor(color)
        if ROOT.gPad.GetLogx():
            x *= xoffset
        else:
            x += xoffset
        if ROOT.gPad.GetLogy():
            y *= yoffset
        else:
            y += yoffset
        self.__Label = ROOT.TLatex(x, y, text)
        self.__Label.SetTextColor(color)

    def Draw(self):
        """
        """
        self.__Marker.Draw()
        self.__Label.Draw()


if __name__ == '__main__':
    from gCanvas import gCanvas
    c = gCanvas('c')
    h = ROOT.TH1F('h', 'h', 100, 0, 1)
    h.Draw()
    m = gTextMarker(0.5, 0.5, 'Test', color = ROOT.kRed)
    m.Draw()
    c.Update()
