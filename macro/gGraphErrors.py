#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy
import math

from __ROOT__ import *
from gRootObject import gRootObject


class gGraphErrors(ROOT.TGraphErrors, gRootObject):

    """ Wrapper around the ROOT.TGraphErrors object.
    """

    DEFAULT_OPTIONS = {'MarkerStyle': 20,
                       'MarkerSize' : 1.2,
                       'FillColor'  : ROOT.kWhite,
                       'FillStyle'  : 0
                       }

    def __init__(self, name, title = None, **kwargs):
        """ Constructor.
        """
        ROOT.TGraphErrors.__init__(self)
        title = title or name
        self.SetNameTitle(name, title)
        gRootObject.init(self, **kwargs)

    def AdjustXaxis(self):
        """ To be implemented.
        """
        pass

    def Fill(self, graph):
        """ Set the data points from another graph.

        TODO: shall we reset the graph first?
        """
        x = ROOT.Double()
        y = ROOT.Double()
        for i in range(graph.GetN()):
            graph.GetPoint(i, x, y)
            self.SetPoint(i, x, y)
        if isinstance(graph, ROOT.TGraphErrors):
            for i in range(graph.GetN()):
                self.SetPointError(i, graph.GetErrorX(i), graph.GetErrorY(i))
        self.GetXaxis().SetTitle(graph.GetXaxis().GetTitle())
        self.GetYaxis().SetTitle(graph.GetYaxis().GetTitle())

    def Clone(self, name):
        """ Clone the graph.
        """
        graph = gGraphErrors(name)
        graph.Fill(self)
        return graph

    def GetResiduals(self, function, name = 'gres', **kwargs):
        """ Return a graph of the residuals with respect to a generic function.
        """
        graph = gGraphErrors(name, **kwargs)
        x = ROOT.Double()
        y = ROOT.Double()
        for i in range(self.GetN()):
            self.GetPoint(i, x, y)
            graph.SetPoint(i, x, y - function.Eval(x))
            if isinstance(self, ROOT.TGraphErrors):
                graph.SetPointError(i, 0, self.GetErrorY(i))
        graph.GetXaxis().SetTitle(self.GetXaxis().GetTitle())
        graph.GetYaxis().SetTitle('Residuals')
        graph.GetYaxis().SetNdivisions(508)
        return graph

    def Scale(self, factor):
        """ Return a graph of the residuals with respect to a generic function.
        """
        divisions = self.GetYaxis().GetNdivisions()
        xtitle = self.GetXaxis().GetTitle()
        ytitle = self.GetYaxis().GetTitle()
        x = ROOT.Double()
        y = ROOT.Double()
        for i in range(self.GetN()):
            self.GetPoint(i, x, y)
            self.SetPoint(i, x, y*factor)
            if isinstance(self, ROOT.TGraphErrors):
                self.SetPointError(i, self.GetErrorX(i),
                                   factor*self.GetErrorY(i))
        self.GetXaxis().SetTitle(xtitle)
        self.GetYaxis().SetTitle(ytitle)
        self.GetYaxis().SetNdivisions(divisions)
                
    def SetXRange(self, xmin, xmax, divisions = 510):
        """ Set the range of the y axis.
        """
        self.GetXaxis().SetRangeUser(xmin, xmax)
        self.GetXaxis().SetNdivisions(divisions)

    def SetYRange(self, ymin, ymax, divisions = 510):
        """ Set the range of the y axis.
        """
        self.GetYaxis().SetRangeUser(ymin, ymax)
        self.GetYaxis().SetNdivisions(divisions)

    def SetNextPoint(self, x, y, dx = 0., dy = 0.):
        """ Set the next point (and associated error in the graph).
        """
        i = self.GetN()
        self.SetPoint(i, x, y)
        self.SetPointError(i, dx, dy)

    def Rebin(self, name, n = 2, **kwargs):
        """
        """
        graph = gGraphErrors(name, **kwargs)
        x = ROOT.Double()
        y = ROOT.Double()
        _x = 0
        _y = 0
        _n = 0
        for i in range(self.GetN()):
            self.GetPoint(i, x, y)
            _x += x
            _y += y
            _n += 1
            if _n == 2: 
                graph.SetNextPoint(_x/float(_n), _y/float(_n))
                _x = 0
                _y = 0
                _n = 0
            #if isinstance(self, ROOT.TGraphErrors):
            #    graph.SetPointError(i, 0, self.GetErrorY(i))
        graph.GetXaxis().SetTitle(self.GetXaxis().GetTitle())
        graph.GetYaxis().SetTitle(self.GetYaxis().GetTitle())
        return graph

    def Draw(self, opts = 'ap'):
        """ Overloaded method overriding the nonsense ROOT default.
        """
        ROOT.TGraph.Draw(self, opts)

    def extrapolatePL(self, leverArm, extrArm, numPoints = 10):
        """ Extrapolate at the highest energy using a PL fit.
        """
        x = ROOT.Double()
        y = ROOT.Double()
        self.GetPoint(self.GetN() - 1, x, y)
        xend = float(x)
        xpivot = 2*xend/leverArm
        ypivot = self.Eval(xpivot)
        xl = xend/leverArm
        xh = xend*extrArm
        f = ROOT.TF1('ffit_%s' % self.GetName(), '[0]*(x/[1])**([2])', xl, xh)
        f.FixParameter(0, ypivot)
        f.FixParameter(1, xpivot)
        f.SetParameter(2, -2.75)
        self.Fit(f, 'R0')
        for i in range(self.GetN()):
            self.GetPoint(i, x, y)
            if x > xpivot:
                self.SetPoint(i, x, f.Eval(x))
            self.SetPointError(i, 0, 0)
        for x in numpy.logspace(math.log10(xend), math.log10(xh), numPoints):
            self.SetNextPoint(x, f.Eval(x))

    def IntegralPL(self, idx1, idx2):
        """ Integrate a TGraph under the hypothesis that it's locally well
        described by a power law.
        """
        x = ROOT.Double()
        y = ROOT.Double()
        integral = 0
        for i in range(idx1, idx2):
            self.GetPoint(i, x, y)
            x1 = float(x)
            y1 = float(y)
            self.GetPoint(i + 1, x, y)
            x2 = float(x)
            y2 = float(y)
            try:
                gamma = math.log10(y1/y2)/math.log10(x1/x2)
                prefactor = y1/(x1**gamma)
                integral += prefactor/(gamma+1)*(x2**(gamma+1) - x1**(gamma+1))
            except:
                pass
        return integral

    def getIntegralGraphPL(self, stopFrac = 0.2):
        """
        """
        g = gGraphErrors('%s_int' % self.GetName())
        x = ROOT.Double()
        y = ROOT.Double()
        self.GetPoint(self.GetN() - 1, x, y)
        xmax = float(x)
        for i in range(0, self.GetN() - 1):
            self.GetPoint(i, x, y)
            if x/xmax > stopFrac:
                break
            g.SetNextPoint(float(x), self.IntegralPL(i, self.GetN() - 1))
        return g



if __name__ == '__main__':
    from gCanvas import gCanvas
    from gF1 import gF1
    
    c = gCanvas('c', Logx = True, Logy = True)
    g = gGraphErrors('g')
    f = gF1('f', '[0]*x**[1]', 1, 1000000)
    f.SetParameters(1, -3)
    for x in numpy.logspace(0, 6, 100):
        g.SetNextPoint(x, f.Eval(x))
    g.Draw()
    gint = g.getIntegralGraphPL()
    gint.Draw('lsame')
    f.SetParameters(0.5, -2)
    f.Draw('same')
    c.Update()
