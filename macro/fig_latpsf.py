#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1


EMIN = 0.03
EMAX = 300
THICK_FRONT = 0.03
THICK_BACK = 0.18
NUM_FRONT = 7
NUM_BACK = 3
STRIP_PITCH = 0.0228
SIGMA_HIT = STRIP_PITCH/math.sqrt(12)
LAYER_SPACING = 3.5
SQRT2 = math.sqrt(2)
ASYMPT_FRONT = math.degrees(SQRT2*STRIP_PITCH/(LAYER_SPACING*NUM_FRONT**1.5))
ASYMPT_BACK = math.degrees(SQRT2*STRIP_PITCH/(LAYER_SPACING*NUM_BACK**1.5))

LAT_PERF_FILE = ROOT.TFile('data/perfPlots_P6_V3_SOURCE.root')
LAT_PSF_FRONT = LAT_PERF_FILE.Get('gPsf68Energy_P6_V3_SOURCEFront')
LAT_PSF_BACK = LAT_PERF_FILE.Get('gPsf68Energy_P6_V3_SOURCEBack')
LAT_PSF_FRONT.SetMarkerStyle(24)
LAT_PSF_FRONT.SetTitle('Fermi-LAT (front section)')
LAT_PSF_BACK.SetMarkerStyle(25)
LAT_PSF_BACK.SetTitle('Fermi-LAT (back section)')

# Convert to GeV.
_x = ROOT.Double()
_y = ROOT.Double()
for g in [LAT_PSF_FRONT, LAT_PSF_BACK]:
    for i in range(g.GetN()):
        g.GetPoint(i, _x, _y)
        g.SetPoint(i, _x/1000., _y)

formula = getMSFormula(ELECTRON_MASS)
formula = formula.replace('x', '(0.5*x)')
formula = 'sqrt((%s)**2 + [1]**2)' % formula
ffront = gF1('ffront', formula, EMIN, EMAX)
ffront.SetParameters(THICK_FRONT, ASYMPT_FRONT)
fback = gF1('fback', formula, EMIN, EMAX)
fback.SetParameters(THICK_BACK, ASYMPT_BACK)
fkl = gF1('fkl', '5*(x/0.01)**-1', EMIN, EMAX)
fkl.SetLineStyle(7)

c1 = gCanvas('lat_psf', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 100, EMIN, EMAX, XTitle = 'Energy [GeV]',
          YTitle = 'PSF 68% containmnent [#circ]', Minimum = 0.01, Maximum = 20)
h1.Draw()
LAT_PSF_FRONT.Draw('psame')
LAT_PSF_BACK.Draw('psame')
ffront.Draw('same')
fback.Draw('same')
fkl.Draw('same')
l = gLegend(0.5)
l.AddEntry(LAT_PSF_BACK, options = 'p')
l.AddEntry(LAT_PSF_FRONT, options = 'p')
l.AddEntry(ffront, 'Naive model')
l.AddEntry(fkl, 'Kinematic limit')
l.Draw()
c1.annotate(50, 0.02, 'Front', ndc = False)
c1.annotate(50, 0.15, 'Back', ndc = False)
c1.Update()
c1.save()
