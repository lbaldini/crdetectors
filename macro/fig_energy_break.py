import numpy
import os
import math
import random

from __ROOT__ import *
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1
from __logging__ import logger


ROOT_FILE_PATH = 'data/energy_break.root'
EMIN_TRUE = 10
EMAX_TRUE = 10000
EMIN_MEAS = 50
EMAX_MEAS = 1000
ERES = 0.4
NUM_EVENTS = 100000000
BINNING = numpy.logspace(math.log10(EMIN_MEAS), math.log10(EMAX_MEAS), 100)

E_BREAK = 230
INDEX_LO = -2.85
INDEX_HI = -2.67
SPECTRUM = gF1('spec', '((x/[0])**[1])*(x < [0]) + ((x/[0])**[2])*(x > [0])',
               EMIN_TRUE, EMAX_TRUE)
SPECTRUM.SetParameter(0, E_BREAK)
SPECTRUM.SetParameter(1, INDEX_LO)
SPECTRUM.SetParameter(2, INDEX_HI)


def generate():
    """
    """
    logger.info('Generating events...')
    hetrue = ROOT.TH1F('hetrue', 'hetrue', len(BINNING) - 1, BINNING)
    hemeas = ROOT.TH1F('hemeas', 'hemeas', len(BINNING) - 1, BINNING)
    for i in range(NUM_EVENTS):
        etrue = SPECTRUM.GetRandom()
        emeas = random.gauss(etrue, etrue*ERES)
        hetrue.Fill(etrue)
        hemeas.Fill(emeas)
    logger.info('Writing output file %s...' % ROOT_FILE_PATH)
    outputFile = ROOT.TFile(ROOT_FILE_PATH, 'RECREATE')
    for h in [hetrue, hemeas]:
        h.Write()
    outputFile.Close()


if not os.path.exists(ROOT_FILE_PATH):
    generate()


inputFile = ROOT.TFile(ROOT_FILE_PATH)
hetrue = inputFile.Get('hetrue')
hemeas = inputFile.Get('hemeas')
for i in range(1, hetrue.GetNbinsX() + 1):
    e = hetrue.GetBinCenter(i)
    w = hetrue.GetBinWidth(i)
    hetrue.SetBinContent(i, hetrue.GetBinContent(i)/w*e**2.75)
    hemeas.SetBinContent(i, hemeas.GetBinContent(i)/w*e**2.75)

hetrue.SetLineColor(ROOT.kGray)
hetrue.SetXTitle('Energy [GeV]')
hetrue.SetYTitle('dN/dE')
hetrue.SetTitle('True')
hemeas.SetTitle('Measured')

c1 = gResidualCanvas('energy_break', Logx = True, Logy = True)
c1.cd(2)
ROOT.gPad.SetLogy(False)
heatio = hemeas.Clone()
heatio.Divide(hetrue)
heatio.SetXTitle(hetrue.GetXaxis().GetTitle())
heatio.SetYTitle('Measured/True     ')
heatio.SetMinimum(0)
heatio.SetMaximum(2.9)
heatio.GetYaxis().SetNdivisions(508)
c1.drawPlots(hetrue, heatio)
c1.cd(1)
hemeas.Draw('same')
l = gLegend(0.2, 0.85, entries = [hetrue, hemeas])
l.vstretch(1.6)
l.Draw()
c1.Update()
c1.save()
