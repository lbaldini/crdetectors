#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __pdg__ import *
from __geometry2d__ import *
from gCanvas import gCanvas
from gPhysicalCanvas import gPhysicalCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend


REFRACTION_INDEX = 1.5
BETA = 1.


fbeta = ROOT.TF1('fbeta', getCherenkovFormulaBeta(), 0.1, 1)
fbeta.SetParameters(1, REFRACTION_INDEX)
fbeta.GetXaxis().SetTitle('#beta')
fbeta.GetYaxis().SetTitle('d^{2}N/dxd#lambda [a. u.]')

fbetagamma = ROOT.TF1('fbeta', getCherenkovFormulaBetaGamma(), 0.1, 100)
fbetagamma.SetParameters(1, REFRACTION_INDEX)
fbetagamma.GetXaxis().SetTitle('#beta#gamma')
fbetagamma.GetYaxis().SetTitle('d^{2}N/dxd#lambda [a. u.]')

c1 = gCanvas('cherenkov_beta', Logx = False)
fbeta.Draw()
c1.annotate(0.2, 0.85, 'n = %.1f' % REFRACTION_INDEX)
c1.Update()
c1.save()

c2 = gCanvas('cherenkov_betagamma', Logx = True)
fbetagamma.Draw()
c2.annotate(0.2, 0.85, 'n = %.1f' % REFRACTION_INDEX)
c2.Update()
c2.save()

c3 = gPhysicalCanvas('cherenkov_sketch', 2, 1.5)
x0 = 0.8
numCircles = 15
for i in range(1, numCircles + 1):
    dx = 0.85*i/numCircles
    radius = dx/REFRACTION_INDEX/BETA
    x = x0 - dx
    if i < numCircles:
        circle(x, 0, radius, LineWidth = 1, LineColor = ROOT.kGray)
    else:
        circle(x, 0, radius)
        ctheta = 1./REFRACTION_INDEX/BETA
        stheta = (1 - ctheta**2)**0.5
        theta = math.degrees(math.acos(ctheta))
        line(x, 0, x + radius*ctheta, radius*stheta)
        line(x0, 0, x + radius*ctheta, radius*stheta, extend2 = 0.5)
        line(x0, 0, x + radius*ctheta, -radius*stheta, extend2 = 0.5)
        marker(x0, 0)
        marker(x, 0)
        c3.annotate(x0 - 0.5*dx, -0.02, '#betact', ndc = False, align = 23)
        c3.annotate(x + 0.5*radius*ctheta, 0.5*radius*stheta + 0.1,
                    '#frac{ct}{n}', ndc = False, align = 21)
        circle(x, 0, 0.1, 0, theta)
        c3.annotate(x + 0.3*radius*ctheta, 0.1*radius*stheta, '#theta_{c}',
                    ndc = False)
        c3.annotate(-0.75, -0.02, '#splitline{Charged}{ particle}',
                    ndc = False, align = 23)
        c3.annotate(x + 1.05*radius*ctheta, 1.05*radius*stheta,
                    'Wavefront', ndc = False, angle = -90 + theta, align = 21)
line(-x0, 0, x0, 0)
c3.Update()
c3.save()
