#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from gCanvas import *



class gVertCanvas(gCanvas, gRootObject):

    """ A skinny vertical canvas.
    """

    def __init__(self, name, title = None, **kwargs):
        """ Conctructor.
        """
        gCanvas.__init__(self, name, title, **kwargs)
        self.SetWindowSize(self.GetWindowWidth(), 1000)
        self.SetBottomMargin(0.078)
        self.SetTopMargin(0.024)

    def Update(self):
        """
        """
        gCanvas.Update(self)
        for item in self.GetListOfPrimitives():
            if isinstance(item, ROOT.TH1F) or \
               isinstance(item, ROOT.TGraph):
                item.GetYaxis().SetTitleOffset(1.88)
                item.GetXaxis().SetTitleOffset(1.22)



if __name__ == '__main__':
    from gH1F import gH1F
    c = gVertCanvas('ctest')
    h = gH1F('h1', 'h1', 100, 0, 1, XTitle = 'Something [a. u.]',
             YTitle = 'Something else [a. u.]')
    h.Draw()
    c.annotate(0.5, 0.5, 'Test annotation')
    c.Update()

    c2 = gCanvas('c2')
    h2 =  gH1F('h2', 'h2', 100, 0, 1, XTitle = 'Something [a. u.]',
               YTitle = 'Something else [a. u.]')
    h2.Draw()
    c2.Update()
