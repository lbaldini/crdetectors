#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gEarthSphere import gEarthSphere
from TJI.gTrajectoryTracer import TrajectoryTracer


EARTH = gEarthSphere(rotpadding = 1.5)
LAT = 0.
LON = 0.
ALT = 500.
ENERGY = 25.
ZENITH_ANGLES = [90, 85, 80, 75, 70]

etracer = TrajectoryTracer('e-', startAltitude = ALT)
ptracer = TrajectoryTracer('e+', startAltitude = ALT)

c = gCanvas('geoelepos')
EARTH.DrawTop(zoom = 0.9)

for zenith in ZENITH_ANGLES:
    for azimuth in [-90, 90]: 
        te = etracer.getTrajectory(ENERGY, LAT, LON, zenith, azimuth)
        te.Draw()
        te.SetLineColor(ROOT.kGray)
        tp = ptracer.getTrajectory(ENERGY, LAT, LON, zenith, azimuth)
        tp.Draw()
c.Update()
c.save()
