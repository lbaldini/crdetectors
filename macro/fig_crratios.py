#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity
from gLegend import gLegend


CR_MODELS_ROOT_FILE = ROOT.TFile('data/cr_models.root')

protons = CR_MODELS_ROOT_FILE.Get('gcomb_protons')
electrons = CR_MODELS_ROOT_FILE.Get('gcomb_all_electrons')
positrons = CR_MODELS_ROOT_FILE.Get('gcomb_positrons')
antiprotons = CR_MODELS_ROOT_FILE.Get('gcomb_antiprotons')
helium = CR_MODELS_ROOT_FILE.Get('gcomb_helium')
carbon = CR_MODELS_ROOT_FILE.Get('gcomb_C')
oxygen = CR_MODELS_ROOT_FILE.Get('gcomb_O')
iron = CR_MODELS_ROOT_FILE.Get('gcomb_Fe')
allgamma = CR_MODELS_ROOT_FILE.Get('gcomb_allgamma')
egb = CR_MODELS_ROOT_FILE.Get('gcomb_egb')

CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
pos_frac = CRDB_ROOT_FILE.Get('dataset_140')


x = ROOT.Double()
y = ROOT.Double()

c1 = gVertCanvas('cr_ratios', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 0.5, 20000, Minimum = 1e-6, Maximum = 0.25,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Ratio of differential intensities')
h1.Draw()
gele_prot = __utils__.gratio('ele_prot', electrons, protons, xmax = 5000)
gele_prot.Draw('csame')
gpos_prot = __utils__.gratio('pos_prot', positrons, protons, xmax = 500)
gpos_prot.Draw('csame')
gpbar_ele = __utils__.gratio('pbar_ele', antiprotons, electrons, xmax = 500)
for i in range(gpbar_ele.GetN()):
    gpbar_ele.GetPoint(i, x, y)
    corr = 1 - pos_frac.Eval(x)
    gpbar_ele.SetPoint(i, x, y*corr)
gpbar_ele.Draw('csame')
gallgam_prot = __utils__.gratio('allgam_prot', allgamma, protons, xmax = 1000)
gallgam_prot.Draw('csame')
gegb_prot = __utils__.gratio('egb_prot', egb, protons, xmax = 500)
gegb_prot.Draw('csame')
gpbar_prot = __utils__.gratio('pbar_prot', antiprotons, protons, xmax = 300)
gpbar_prot.Draw('csame')


gele_prot.GetPoint(gele_prot.GetN() - 1, x, y)
c1.annotate(x, y, '(e^{+} + e^{-}) / p', ndc = False, align = 23)
gpos_prot.GetPoint(gpos_prot.GetN() - 1, x, y)
c1.annotate(x, y, ' e^{+} / p', ndc = False, align = 12)
gpbar_ele.GetPoint(gpbar_ele.GetN() - 1, x, y)
c1.annotate(x, y, ' #bar{p} / e^{-}', ndc = False, align = 12)
gallgam_prot.GetPoint(gallgam_prot.GetN() - 1, x, y)
c1.annotate(x, y, ' #gamma (all sky) / p', ndc = False, align = 12)
gegb_prot.GetPoint(gegb_prot.GetN() - 1, x, y)
c1.annotate(x, y, ' #gamma (EGB) / p', ndc = False, align = 12)
gpbar_prot.GetPoint(gpbar_prot.GetN() - 1, x, y)
c1.annotate(x, y, ' #bar{p} / p', ndc = False, align = 12)

c1.Update()
c1.save()
