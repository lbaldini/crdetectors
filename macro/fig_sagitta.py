#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from gF1 import gF1


TKR_NUM_PLANES = 6
TKR_TOP = 0.6
TKR_BOTTOM = 0.1
TKR_HEIGHT = TKR_TOP - TKR_BOTTOM
TKR_LEFT = 0.5
TKR_RIGHT = 0.85
TKR_HOR_MID = 0.5*(TKR_LEFT + TKR_RIGHT)
TKR_VER_MID = 0.5*(TKR_TOP + TKR_BOTTOM)
TKR_WIDTH = TKR_RIGHT - TKR_LEFT
RHO = 0.6



c1 = gCanvas('magnetic_spectrometer')
ar = c1.GetWw()/float(c1.GetWh())
c1.Range(0, 0, 1, 1./ar)
# TKR planes.
for i in range(TKR_NUM_PLANES):
    y = TKR_TOP - i*float(TKR_HEIGHT)/(TKR_NUM_PLANES - 1)
    l = ROOT.TLine(TKR_LEFT, y, TKR_RIGHT, y)
    store(l)
    l.Draw()
# Circumference.
x = TKR_HOR_MID - RHO
y = TKR_VER_MID
phi0 = math.asin(0.5*TKR_HEIGHT/RHO)
phimin = 360. - math.degrees(phi0)
phimax = 360. + math.degrees(phi0)
circ = ROOT.TArc(x, y, RHO, phimin, phimax)
circ.SetLineStyle(7)
circ.SetFillStyle(0)
store(circ)
circ.Draw('same')
m = ROOT.TMarker(x, y, 20)
store(m)
m.Draw()
a = ROOT.TArc(x, y, 0.09, 360, phimax)
a.SetNoEdges()
a.SetFillStyle(0)
store(a)
a.Draw()
a = ROOT.TArc(x, y, 0.1, 360, phimax)
a.SetNoEdges()
a.SetFillStyle(0)
store(a)
a.Draw()
c1.annotate(x, y + 0.01, 'O', ndc = False, align = 21)
c1.annotate(x + 0.5*RHO, y + 0.11, '#rho', ndc = False, align = 21)
c1.annotate(x + 0.15, y + 0.035, '#frac{#theta}{2}', ndc = False, align = 22)
a = ROOT.TArc(x, y, RHO, phimin, phimax)
a.SetNoEdges()
a.SetFillStyle(0)
store(a)
a.Draw()
for i in range(TKR_NUM_PLANES):
    dy = i*float(TKR_HEIGHT)/(TKR_NUM_PLANES - 1)
    y = TKR_TOP - dy
    x = TKR_HOR_MID - RHO*(1 - math.cos(math.asin((dy - 0.5*TKR_HEIGHT)/RHO)))
    m = ROOT.TMarker(x, y, 20)
    store(m)
    m.Draw()
dr = 0.5
dx = -dr*math.sin(phi0)
dy = -dr*math.cos(phi0)
l = ROOT.TLine(x, y, x + dx, y + dy)
store(l)
l.Draw()
y = TKR_TOP
x = TKR_HOR_MID - RHO*(1 - math.cos(math.asin((-0.5*TKR_HEIGHT)/RHO)))
dr = 0.5
dx = -dr*math.sin(phi0)
dy = dr*math.cos(phi0)
l = ROOT.TLine(x, y, x + dx, y + dy)
store(l)
l.Draw()
l = ROOT.TLine(x, TKR_TOP, x, TKR_BOTTOM)
l.SetLineStyle(7)
store(l)
l.Draw()
l = ROOT.TLine(TKR_HOR_MID - RHO, TKR_VER_MID, TKR_HOR_MID, TKR_VER_MID)
l.SetLineStyle(7)
store(l)
l.Draw()
c1.annotate(x + 0.5*(TKR_HOR_MID - x), TKR_VER_MID + 0.0175, 's',
            ndc = False, align = 22)
a = ROOT.TArrow(TKR_RIGHT + 0.05, TKR_TOP, TKR_RIGHT + 0.05, TKR_BOTTOM,
                0.02, '<->')
store(a)
c1.annotate(TKR_RIGHT + 0.075, TKR_VER_MID, 'L',
            ndc = False, align = 22)
a.Draw()
bc = ROOT.TArc(0.3, 0.65, 0.02)
bc.SetFillStyle(0)
bc.Draw()
m = ROOT.TMarker(0.3, 0.65, 20)
m.Draw()
c1.annotate(0.33, 0.65, 'B', ndc = False)
c1.Update()
c1.save()


c2 = gCanvas('sagitta_vs_bp', Logx = True, Logy = True)
emin = 1.
emax = 10000.
h2 = gH1F('h2', 'h2', 100, emin, emax, XTitle = 'Rigidity [GV]',
          YTitle = 'Sagitta [mm]',
          Minimum = 1e-3, Maximum = 100)
h2.Draw()
f = gF1('f', '37.5*[0]/x', emin, emax)
bp = 0.43*(0.445**2)
f.SetParameter(0, bp)
c2.annotate(0.16, 0.38, 'PaMeLa (%.3f T m^{2})' % bp)
f.DrawCopy('same')
bp = 0.86
f.SetParameter(0, bp)
f.Draw('same')
c2.annotate(0.36, 0.75, 'AMS-02 superconductive (%.2f T m^{2})' % bp)
c2.Update()
c2.save()

