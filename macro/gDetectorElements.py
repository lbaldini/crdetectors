#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import array
import math

from __ROOT__ import *
from gLatex import gLatex


def getPadCoordinates(x, y, z):
    """ Return the (x, y) coordinates on the pad corresponding to the 
    (x, y, z) coordinates in the real world.
    """
    pw = array.array('d', [x, y, z])
    pn = array.array('d', [0.0, 0.0, 0.0])
    ROOT.gPad.GetView().WCtoNDC(pw, pn)
    x = pn[0]
    y = pn[1]
    z = pn[2]
    return (x, y)


class gLine(ROOT.TPolyLine3D):

    """ Class representing a simple line
    """

    LENGTH = 100

    def __init__(self, x0, y0, z0, x1, y1, z1, lineStyle = 1):
        """ Constructor.
        """
        ROOT.TPolyLine3D.__init__(self)
        self.X0 = x0
        self.Y0 = y0
        self.Z0 = z0
        self.X1 = x1
        self.Y1 = y1
        self.Z1 = z1
        self.SetPoint(0, self.X0, self.Y0, self.Z0)
        self.SetPoint(1, self.X1, self.Y1, self.Z1)
        self.SetLineStyle(lineStyle)


class gAxis(ROOT.TPolyLine3D):

    """ Class representing an axis.
    """

    def __init__(self, x0, y0, z0, xdir, ydir, zdir, label = None,
                 labelPadding = 1.05, lineStyle = 1, length = 75):
        """ Constructor.
        """
        length = length or self.LENGTH
        ROOT.TPolyLine3D.__init__(self)
        norm = (xdir**2 + ydir**2 + zdir**2)**0.5
        self.X0 = x0
        self.Y0 = y0
        self.Z0 = z0
        self.XDir = xdir/norm
        self.YDir = ydir/norm
        self.ZDir = zdir/norm
        self.X1 = x0 + length*self.XDir
        self.Y1 = y0 + length*self.YDir
        self.Z1 = z0 + length*self.ZDir
        self.SetPoint(0, self.X0, self.Y0, self.Z0)
        self.SetPoint(1, self.X1, self.Y1, self.Z1)
        if label:
            x = self.X0 + labelPadding*length*self.XDir
            y = self.Y0 + labelPadding*length*self.YDir
            z = self.Z0 + labelPadding*length*self.ZDir
            x, y = getPadCoordinates(x, y, z)
            self.Label = gLatex(x, y, label, TextAlign = 22)
        else:
            self.Label = None
        self.SetLineStyle(lineStyle)
        
    def Draw(self):
        """ Draw the axis.
        """
        ROOT.TPolyLine3D.Draw(self)
        if self.Label is not None:
            self.Label.Draw()



class gDetectorPlane(ROOT.TGeoVolume):

    """ Class describing a detector plane.
    """

    SIDE = 100.
    COLOR = ROOT.kGray

    def __init__(self, thickness = 0.1):
        """ Constructor.
        """
        self.Shape = ROOT.TGeoBBox('Detector plane', self.SIDE/2.,
                                   self.SIDE/2., thickness/2.)
        ROOT.TGeoVolume.__init__(self, 'Detector plane', self.Shape)
        self.SetLineColor(self.COLOR)



class gTracker(ROOT.TGeoVolume):
    
    """
    """

    SIDE = gDetectorPlane.SIDE
    COLOR = gDetectorPlane.COLOR

    def __init__(self, numPlanes = 5, spacing = 7.5):
        """
        """
        self.Height = numPlanes*spacing
        self.Shape = ROOT.TGeoBBox('Tracker shape', self.SIDE/2.,
                                   self.SIDE/2., self.Height/2.)
        ROOT.TGeoVolume.__init__(self, 'Tracker volume', self.Shape)
        self.Plane = gDetectorPlane()
        for i in range(numPlanes):
            dz = i*spacing - 0.5*self.Height
            trans  = ROOT.TGeoTranslation(0.0, 0.0, dz)
            self.AddNode(self.Plane, i, trans)



class gCalorimeter(ROOT.TGeoVolume):
    
    """ Class describing a calorimeter.
    """

    SIDE = gDetectorPlane.SIDE
    COLOR = gDetectorPlane.COLOR

    def __init__(self, thickness = 20):
        """ Constructor.
        """
        self.Thickness = thickness
        self.Shape = ROOT.TGeoBBox('Calorimeter', self.SIDE/2.,
                                   self.SIDE/2., thickness/2.)
        ROOT.TGeoVolume.__init__(self, 'Calorimeter', self.Shape)
        self.SetLineColor(self.COLOR)



class gSimpleDetector(ROOT.TGeoVolume):
    
    """
    """

    SIDE = gDetectorPlane.SIDE
    HEIGHT = 100

    def __init__(self):
        """
        """
        self.Shape = ROOT.TGeoBBox('Detector shape', self.SIDE/2.,
                                   self.SIDE/2., self.HEIGHT/2.)
        ROOT.TGeoVolume.__init__(self, 'Detector volume', self.Shape)
        self.Cal = gCalorimeter()
        dz = -(0.5*self.Cal.Thickness + 2)
        trans  = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.Cal, 1, trans)
        self.Tkr = gTracker()
        dz = 0.5*self.Tkr.Height
        trans  = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.Tkr, 2, trans)
        self.Top = gDetectorPlane(2.5)
        dz = 0.5*self.Tkr.Height + 30
        trans  = ROOT.TGeoTranslation(0.0, 0.0, dz)
        self.AddNode(self.Top, 3, trans)



def drawReferenceSystem(xlength = 75, ylength = 75, zlength = 55):
    """ Draw the reference system.
    """
    xaxis = gAxis(0, 0, 0, -1, 0, 0, '+x', length = xlength)
    yaxis = gAxis(0, 0, 0, 0, -1, 0, '+y', length = ylength)
    zaxis = gAxis(0, 0, 0, 0, 0, 1, '+z', length = zlength)
    xaxis.Draw()
    yaxis.Draw()
    zaxis.Draw()

def drawSkyDir(xdir = -2., ydir = -1., zdir = 1.8, length = 90,
               angleSteps = 20):
    """ Draw a direction in the sky to illustrate the coordinate system
    """
    skydir = gAxis(0, 0, 0, xdir, ydir, zdir, '#hat{v}', length = length)
    xyproj = gLine(0, 0, 0, skydir.X1, skydir.Y1, 0, lineStyle = 2)
    zproj  = gLine(skydir.X1, skydir.Y1, skydir.Z1,
                   xyproj.X1, xyproj.Y1, xyproj.Z1, lineStyle = 2)
    skydir.Draw()
    xyproj.Draw()
    zproj.Draw()
    phi = math.atan(ydir/xdir)
    theta = math.pi/2. - math.atan(zdir/(xdir**2+ydir**2)**0.5)
    # Draw the phi angle
    phiLine1 = ROOT.TPolyLine3D()
    phiLine2 = ROOT.TPolyLine3D()
    r1 = 20
    r2 = 22.5
    x, y = getPadCoordinates(-1.5*r2, -0.5*r2, 0)
    ROOT.gPad.annotate(x, y, '#phi', ndc = False)
    for i in xrange(angleSteps):
        p = phi*i/float(angleSteps - 1)
        x = -r1*math.cos(p)
        y = -r1*math.sin(p)
        phiLine1.SetPoint(i, x, y, 0)
        x = -r2*math.cos(p)
        y = -r2*math.sin(p)
        phiLine2.SetPoint(i, x, y, 0)
    phiLine1.Draw()
    phiLine2.Draw()
    store(phiLine1)
    store(phiLine2)
    # Draw the theta angle
    thetaLine = ROOT.TPolyLine3D()
    r = 20
    x, y = getPadCoordinates(-0.65*r, -0.25*r, 1.1*r)
    ROOT.gPad.annotate(x, y, '#theta', ndc = False)
    for i in xrange(angleSteps):
        t = theta*i/float(angleSteps - 1)
        x = -r*math.cos(phi)*math.sin(t)
        y = -r*math.sin(phi)*math.sin(t)
        z = r*math.cos(t)
        thetaLine.SetPoint(i, x, y, z)
    thetaLine.Draw()
    store(thetaLine)

