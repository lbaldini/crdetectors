
import math

RAD_TO_DEG = 180./math.pi
DEG_TO_RAD = 1./RAD_TO_DEG

SECS_PER_YEAR = 3.15569e7


SI_DENSITY = 2.329       # g/cm^3
SI_X0 = 21.82/SI_DENSITY # cm
