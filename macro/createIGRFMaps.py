#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os

from IGRF.IGRF import IGRF
from __ROOT__ import *
from __logging__ import logger


# Grid size for the maps [deg]
GRID_SIZE = 1.

# Base settings.
LON_MIN = -180.
LON_MAX = 180.
LON_BINS = int((LON_MAX - LON_MIN)/GRID_SIZE)
LON_GRID = float(LON_MAX - LON_MIN)/LON_BINS
LAT_MIN = -90.
LAT_MAX = 90.
LAT_BINS = int((LAT_MAX - LAT_MIN)/GRID_SIZE)
LAT_GRID = float(LAT_MAX - LAT_MIN)/LAT_BINS

# Default variables.
VAR_LIST = ['mc_ilwain_L', 'B_abs', 'rigidity_cutoff']


def dump(altitude = 0., year = 2013):
    logger.info('Dumping IGRF maps for alt = %.2f km, year %.2f.' %\
                (altitude, year))
    # Create the base histogram.
    h = ROOT.TH2F('h', 'h', LON_BINS, LON_MIN, LON_MAX,
                  LAT_BINS, LAT_MIN, LAT_MAX)
    h.SetXTitle('Geographic longitude [#circ]')
    h.SetYTitle('Geographic latitude [#circ]')
    # And clone it into the necessary flavors.
    hDict = {}
    for var in VAR_LIST:
        hDict[var] = h.Clone('h_%s' % var)
    # Fill the histograms.
    logger.info('Filling histograms...')
    model = IGRF()
    for i in range(LON_BINS):
        lon = LON_MIN + LON_GRID*(i + 0.5)
        for j in range(LAT_BINS):
            lat = LAT_MIN + LAT_GRID*(j + 0.5)
            model.compute(lat, lon, altitude, year)
            for var in VAR_LIST:
                hDict[var].SetBinContent(i + 1, j + 1, model[var])            
    # Write the output file.
    outputFilePath = 'igrf-11_maps_alt%d_%d.root' % (altitude, year)
    outputFilePath = os.path.join('data', outputFilePath)
    logger.info('Writing output file %s...' % outputFilePath)
    outputFile = ROOT.TFile(outputFilePath, 'RECREATE')
    for h in hDict.values():
        h.Write()
    outputFile.Close()
    logger.info('Done, bye!')



dump(0.)
dump(500.)
