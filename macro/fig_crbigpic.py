#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import __csv__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity
from gLegend import gLegend
from gGraphErrors import gGraphErrors
from  __labels__ import *


CR_MODELS_ROOT_FILE = ROOT.TFile('data/cr_models.root')
CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
SECS_PER_YEAR = 31556926.


def loadModel(name, A = 1):
    """
    """
    g = CR_MODELS_ROOT_FILE.Get('gcomb_%s' % name)
    g = __utils__.gscale(g, 1./A, 1.e9*A)
    return g


def loadDataset(uid, **kwargs):
    """
    """
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(kwargs.get('markerStyle', 24))
    g = __utils__.gscale(g, 1, 1e9)
    return g


def loadCsv(filePath, name, **kwargs):
    """ Load a UHECR cvs file and do the necessary massaging.
    """
    g = __csv__.read(filePath, name)
    g = __utils__.gepscale(g, -2.6)
    g = __utils__.gscale(g, 10**23.4)
    g.SetMarkerStyle(kwargs.get('MarkerStyle', 20))
    g.SetMarkerSize(kwargs.get('MarkerSize', 1.))
    g.Draw('psame')
    return g


c1 = gVertCanvas('cr_big_picture', Logx = True, Logy = True)
h = gH1F('h', 'h', 1000, 1e8, 0.9e21, Minimum = 1e-30, Maximum = 1e4,
         XTitle = AXIS_TITLE_KIN_ENERGY,
         YTitle = axisTitleIntensity(LABEL_KIN_ENERGY))
h.Draw()
tibet = loadCsv('data/uhecr_tibet.csv', 'tibet', MarkerStyle = 25)
kaskade = loadCsv('data/uhecr_kaskade_grande.csv', 'kaskade', MarkerStyle = 20)
auger = loadCsv('data/uhecr_auger.csv', 'auger', MarkerStyle = 24)




protons = loadModel('protons')
helium = loadModel('helium', 4)
carbon = loadModel('C', 12)
oxygen = loadModel('O', 16)
iron = loadModel('Fe', 56)
total = __utils__.gadd('total', protons, helium, carbon, oxygen, iron,
                       LineStyle = 7)
total2 = gGraphErrors('total2', LineStyle = 7)
x = ROOT.Double()
y = ROOT.Double()
for i in range(total.GetN()):
    total.GetPoint(i, x, y)
    if x < 2e14:
        total2.SetNextPoint(x, y)
total2.Draw('csame')

ppamela = loadDataset(873)
pcream = loadDataset(492)
#ppamela.Draw('psame')
#pcream.Draw('psame')

# Now start annotating.
x = 1e16
y = 1e-9
arrow = ROOT.TArrow(x, y, x, 10*kaskade.Eval(x), 0.02, '->')
arrow.Draw()
text = '~1 particle m^{-2} sr^{-1} year^{-1} above 10^{16} eV'
c1.annotate(x, 2*y, text, ndc = False, align = 21)
l1uhe = gLegend(0.7, 0.55)
l1uhe.AddEntry(tibet, 'Tibet', 'p')
l1uhe.AddEntry(kaskade, 'Kaskade', 'p')
l1uhe.AddEntry(auger, 'Auger', 'p')
l1uhe.vstretch(0.75)
l1uhe.Draw()
l1he = gLegend(0.26, 0.94)
#l1he.AddEntry(total2, 'p + He + C + O + Fe', 'l')
l1he.AddEntry(total2, '#splitline{Sum of individual}{CR species}', 'l')
l1he.Draw()
line = ROOT.TLine(1e15, 1e-30, 1e15, 1e4)
line.SetLineStyle(7)
line.Draw()
c1.Update()
c1.save()



c2 = gVertCanvas('cr_big_picture_weighted', Logx = True, Logy = True)
power = 2.
h2 = gH1F('h2', 'h2', 1000, 1e8, 0.9e21, Minimum = 1e-7, Maximum = 1e4,
          XTitle = 'Kinetic energy [eV]',
          YTitle = 'E^{2} #times Flux [m^{-2} s^{-1} sr^{-1} GeV^{-1}]')
h2.Draw()
ppamelaw = __utils__.gepscale(ppamela, power, 1e9)
pcreamw = __utils__.gepscale(pcream, power, 1e9)
total2w = __utils__.gepscale(total2, power, 1e9, LineStyle = 7)
tibetw = __utils__.gepscale(tibet, power, 1e9, MarkerStyle = 25)
kaskadew = __utils__.gepscale(kaskade, power, 1e9, MarkerStyle = 20)
augerw = __utils__.gepscale(auger, power, 1e9, MarkerStyle = 24)
total2w.Draw('csame')
tibetw.Draw('psame')
kaskadew.Draw('psame')
augerw.Draw('psame')
ppamelaw.Draw('psame')
pcreamw.Draw('psame')
print ppamelaw.Eval(10.6)/total2w.Eval(10.6)
c2.Update()

