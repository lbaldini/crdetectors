#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import __utils__

from __ROOT__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend

h = gH1F('h', 'h', 100, 0, 5, Minimum = 0, Maximum = 1, XTitle = 'R [GV]',
         YTitle = '#beta')
f = gF1('f', '(1 + ([0]*0.938/([1]*x))**2)**-0.5', 0, 10)

def box(x, y):
    """
    """
    b = ROOT.TBox(x - 0.2, y - 0.025, x + 0.2, y + 0.025)
    b.SetFillColor(ROOT.kWhite)
    b.Draw()
    store(b)


c = gCanvas('isotopes_rbeta')
h.Draw()
f.SetParameters(3, 2)
f.SetLineStyle(1)
f.DrawCopy('same')
x = 1.5
box(x, f.Eval(x))
c.annotate(x, f.Eval(x), '^{3}He', ndc = False, align = 22)
f.SetParameters(4, 2)
f.SetLineStyle(7)
f.DrawCopy('same')
box(x, f.Eval(x))
c.annotate(x, f.Eval(x), '^{4}He', ndc = False, align = 22)
f.SetParameters(10, 4)
f.SetLineStyle(1)
f.DrawCopy('same')
x = 3
box(x - 0.1, f.Eval(x))
c.annotate(x - 0.1, f.Eval(x), '^{10}Be', ndc = False, align = 22)
f.SetLineStyle(7)
f.SetParameters(11, 4)
f.DrawCopy('same')
c.annotate(x + 0.5, f.Eval(x), '^{11}Be', ndc = False, align = 22)
c.Update()
c.save()
