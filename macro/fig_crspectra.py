#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import __csv__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity, getEGBIntensity
from gLegend import gLegend
from  __labels__ import *


def loadCsv(filePath, name, **kwargs):
    """ Load a UHECR cvs file and do the necessary massaging.
    """
    g = __csv__.read(filePath, name)
    g = __utils__.gscale(g, 1e7, 1e-3)
    g.SetMarkerStyle(kwargs.get('MarkerStyle', 20))
    g.SetMarkerSize(kwargs.get('MarkerSize', 1.))
    g.Draw('psame')
    return g


CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
CRDB_PICKLE_FILE = open('CRDB/usine_all_database.pickle')
CR_MODELS_ROOT_FILE = ROOT.TFile('data/cr_models.root')

CR_DATASET_LIST = pickle.load(CRDB_PICKLE_FILE)

H_DATASETS = [142, 492, 873]
HBAR_DATASETS = [878]
POS_DATASETS = [607, 655]
ALL_ELE_DATASETS = [141, 610, 609, 880, 983]
HE_DATASETS = [143, 493, 875]
C_DATASETS = [145, 497, 909, 415, 310, 617]
N_DATASETS = [499, 587, 618, 311, 222, 196]
O_DATASETS = [910, 501, 899, 148, 416, 588, 619, 223]
FE_DATASETS = [508, 906, 892, 291, 526]


protons = CR_MODELS_ROOT_FILE.Get('gcomb_protons')
electrons = CR_MODELS_ROOT_FILE.Get('gcomb_all_electrons')
positrons = CR_MODELS_ROOT_FILE.Get('gcomb_positrons')
antiprotons = CR_MODELS_ROOT_FILE.Get('gcomb_antiprotons')
helium = CR_MODELS_ROOT_FILE.Get('gcomb_helium')
carbon = CR_MODELS_ROOT_FILE.Get('gcomb_C')
oxygen = CR_MODELS_ROOT_FILE.Get('gcomb_O')
iron = CR_MODELS_ROOT_FILE.Get('gcomb_Fe')
allgamma = CR_MODELS_ROOT_FILE.Get('gcomb_allgamma')
egb = CR_MODELS_ROOT_FILE.Get('gcomb_egb')


c1 = gVertCanvas('cr_spectra_single_charge', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY))
h1.Draw()
l1 = gLegend(0.57, 0.92)
l1.vstretch(0.75)
for g in [protons, electrons, positrons, antiprotons]:
    g.SetMarkerStyle(24)
    g.SetLineStyle(7)
    g.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l1.AddEntry(g, 'Protons')
for uid in ALL_ELE_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(25)
    g.Draw('psame')
    store(g)
l1.AddEntry(g, 'Electrons + positrons')
for uid in POS_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(24)
    g.Draw('psame')
    store(g)
l1.AddEntry(g, 'Positrons')
for uid in HBAR_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(21)
    g.Draw('psame')
    store(g)
l1.AddEntry(g, 'Anti-protons')
l1.Draw()
c1.Update()
c1.save()
refs = []
for uid in H_DATASETS + ALL_ELE_DATASETS + POS_DATASETS + HBAR_DATASETS:
    ds = CR_DATASET_LIST[uid]
    url = ds.Url
    if url not in refs:
        refs.append(url)
print(('%s' % refs).replace('\'', ''))


c2 = gVertCanvas('cr_spectra_metals', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY_N,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY_N))
h2.Draw()
l2 = gLegend(0.57, 0.92)
l2.vstretch(0.75)
for g in [protons, helium, carbon, iron]:
    g.SetMarkerStyle(24)
    g.SetLineStyle(7)
    g.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l2.AddEntry(g, 'Protons')
for uid in HE_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(25)
    g.Draw('psame')
    store(g)
l2.AddEntry(g, 'Helium')
for uid in C_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(24)
    g.Draw('psame')
    store(g)
l2.AddEntry(g, 'Carbon')
for uid in FE_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(21)
    g.Draw('psame')
    store(g)
l2.AddEntry(g, 'Iron')
l2.Draw()
c2.Update()
c2.save()
refs = []
for uid in H_DATASETS + HE_DATASETS + C_DATASETS + FE_DATASETS:
    ds = CR_DATASET_LIST[uid]
    url = ds.Url
    if url not in refs:
        refs.append(url)
print(('%s' % refs).replace('\'', ''))


c3 = gVertCanvas('cr_spectra_gamma', Logx = True, Logy = True)
h3 = gH1F('h3', 'h3', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY))
h3.Draw()
l3 = gLegend(0.57, 0.92)
l3.vstretch(0.75)
for g in [protons, antiprotons, allgamma, egb]:
    g.SetMarkerStyle(24)
    g.SetLineStyle(7)
    g.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l3.AddEntry(g, 'Protons')
for uid in HBAR_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(21)
    g.Draw('psame')
    store(g)
l3.AddEntry(g, 'Antiprotons')
g1 = getAllSkyGammaIntensity()
g1.SetMarkerStyle(24)
g1.Draw('psame')
g2 = getEGBIntensity()
g2.SetMarkerStyle(25)
g2.Draw('psame')
l3.AddEntry(g1, 'Gamma rays (all sky)')
l3.AddEntry(g2, 'Gamma rays (EGB)')
l3.Draw()
c3.Update()
c3.save()
for uid in H_DATASETS + HBAR_DATASETS:
    ds = CR_DATASET_LIST[uid]
    url = ds.Url
    if url not in refs:
        refs.append(url)
print(('%s' % refs).replace('\'', ''))


c4 = gVertCanvas('cr_spectra_p_he_ekn', Logx = True, Logy = True)
h4 = gH1F('h4', 'h4', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY_N,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY_N))
h4.Draw()
l4 = gLegend(0.57, 0.92)
l4.vstretch(0.75)
for g in [protons, helium]:
    g.SetMarkerStyle(24)
    g.SetLineStyle(7)
    g.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l4.AddEntry(g, 'Protons')
for uid in HE_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(25)
    g.Draw('psame')
    store(g)
l4.AddEntry(g, 'Helium')
l4.Draw()
c4.Update()
c4.save()
print helium.Eval(1000)/protons.Eval(1000)


c5 = gVertCanvas('cr_spectra_p_he_ek', Logx = True, Logy = True)
h5 = gH1F('h5', 'h5', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY))
h5.Draw()
l5 = gLegend(0.57, 0.92)
l5.vstretch(0.75)
protons.Draw('csame')
A = 4
helium2 = __utils__.gscale(helium.Clone(), 1./A, A)
helium2.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l5.AddEntry(g, 'Protons')
for uid in HE_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g = __utils__.gscale(g.Clone(), 1./A, A)
    g.SetMarkerStyle(25)
    g.Draw('psame')
    store(g)
l5.AddEntry(g, 'Helium')
l5.Draw()
c5.Update()
c5.save()


c6 = gVertCanvas('cr_spectra_limb', Logx = True, Logy = True)
h6 = gH1F('h6', 'h6', 1000, 0.5, 1000000, Minimum = 1e-12, Maximum = 1e4,
          XTitle = AXIS_TITLE_KIN_ENERGY,
          YTitle = axisTitleIntensity(LABEL_KIN_ENERGY))
h6.Draw()
l6 = gLegend(0.5, 0.92)
l6.vstretch(0.75)
for g in [protons, allgamma, egb]:
    g.SetMarkerStyle(24)
    g.SetLineStyle(7)
    g.Draw('csame')
for uid in H_DATASETS:
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    g.SetMarkerStyle(20)
    g.Draw('psame')
    store(g)
l6.AddEntry(g, 'Protons')
g1 = getAllSkyGammaIntensity()
g1.SetMarkerStyle(24)
g1.Draw('psame')
g2 = getEGBIntensity()
g2.SetMarkerStyle(25)
g2.Draw('psame')
g3 = loadCsv('data/fermi_limb.csv', 'limb', MarkerStyle = 21)
# Horrible hack for the errors.
n = g3.GetN() - 1
x = ROOT.Double()
y = ROOT.Double()
g3.GetPoint(n, x, y)
g3.SetPointError(n, 0, 0.7*y)
n -= 1
g3.GetPoint(n, x, y)
g3.SetPointError(n, 0, 0.5*y)
n -= 1
g3.GetPoint(n, x, y)
g3.SetPointError(n, 0, 0.3*y)
n -= 1
g3.GetPoint(n, x, y)
g3.SetPointError(n, 0, 0.15*y)
g3.Draw('psame')
l6.AddEntry(g3, 'Gamma rays (Earth limb)')
l6.AddEntry(g1, 'Gamma rays (all sky)')
l6.AddEntry(g2, 'Gamma rays (EGB)')
l6.Draw()
c6.Update()
c6.save()
