#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy

from __ROOT__ import *
from __logging__ import logger
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gF1 import gF1
from gH1F import gH1F
from gLegend import gLegend


GAMMA = 2.75
E_MIN = 10
E_MAX = 100e3
PIVOT = 1e3
DELTA_INDEX = 0.2
LE_SCALE_ERR = 0.05
LE_SCALE_ERR_END = 300
HE_SCALE_ERR = 0.15
HE_SCALE_ERR_START = 3000
MAX_ACC_ERR = 0.1
ERES_ERR = 0.3
SPEC_COLOR = ROOT.kGray + 1

def ratioEres(gamma, eres):
    """
    """
    return 1 + 0.5*(gamma - 1)*(gamma - 2)*(eres**2.)

def ratioScale(gamma, scale):
    """
    """
    return 1 + (gamma - 1)*scale


frame = gH1F('frame', 'frame', 100, E_MIN, E_MAX, XTitle = 'Energy [GeV]',
             YTitle = 'E^{%.2f} #times J(E) [a. u.]' % GAMMA, Minimum = 0.65,
             Maximum = 1.65)

specRef = gF1('fref', '1', E_MIN, E_MAX)
specRef.SetLineWidth(3)
specRef.SetLineStyle(7)
specRef.SetLineColor(SPEC_COLOR)
const = gF1('const', '[0]', E_MIN, E_MAX)
const.SetLineColor(ROOT.kBlack)

bplFormula = '(x < [0])*(x/[0])**(-[1]) + (x > [0])*(x/[0])**([1]) + [2]'

specPlus = gF1('fplus', bplFormula, E_MIN, E_MAX)
specPlus.SetParameters(PIVOT, 0.5*DELTA_INDEX, 0.001)
specPlus.SetLineWidth(3)
specPlus.SetLineColor(SPEC_COLOR)
specMinus = gF1('fminus', bplFormula, E_MIN, E_MAX)
specMinus.SetParameters(PIVOT, -0.5*DELTA_INDEX, -0.001)
specMinus.SetLineWidth(3)
specMinus.SetLineColor(SPEC_COLOR)

c = gCanvas('spectral_sys', Logx = True)
frame.Draw()
specRef.Draw('same')
c.annotate(15, 0.98, '#Gamma = 2.75', ndc = False, align = 13,
           color = SPEC_COLOR)
specPlus.Draw('same')
c.annotate(20, 1.52, '#Gamma = 2.85', ndc = False, color = SPEC_COLOR)
c.annotate(5e4, 1.52, '#Gamma = 2.65', ndc = False, align = 31,
           color = SPEC_COLOR)

pad = 0.02
dsFormula = '([0] - 1)*((x < [3])*[1] + (x > [3])*(x < [4])*([1] + ([2] - [1])*log10(x/[3])/log10([4]/[3])) + (x > [4])*[2])'


fsplus = gF1('fsplus', '1 + %s' % dsFormula, E_MIN, E_MAX)
fsplus.SetLineStyle(7)
fsplus.SetParameters(GAMMA, LE_SCALE_ERR, HE_SCALE_ERR, LE_SCALE_ERR_END,
                     HE_SCALE_ERR_START)
fsminus = gF1('fsminus', '1 - %s' % dsFormula, E_MIN, E_MAX)
fsminus.SetLineStyle(7)
fsminus.SetParameters(GAMMA, LE_SCALE_ERR, HE_SCALE_ERR, LE_SCALE_ERR_END,
                     HE_SCALE_ERR_START)
fsplus.Draw('same')
fsminus.Draw('same')

const.SetLineStyle(7)
for s in [HE_SCALE_ERR, -HE_SCALE_ERR]:
    ratio = ratioScale(GAMMA, s)
    text = '#Deltas_{E} = %d%%' % (100*s)
    if s > 0:
        text = text.replace('= ', '= +')
        c.annotate(20000, -pad + ratio, text, ndc = False,
                   size = SMALLER_TEXT_SIZE, align = 13)
    else:
        c.annotate(20000, pad + ratio, text, ndc = False,
                   size = SMALLER_TEXT_SIZE)
   
    
pad = 0.01
const.SetLineStyle(9)
ratio = ratioEres(GAMMA, ERES_ERR)
const.SetParameter(0, ratio)
const.DrawCopy('same')
c.annotate(15, ratio - pad, '#DeltaE/E = %d%%' % (100*ERES_ERR), ndc = False,
           size = SMALLER_TEXT_SIZE, align = 13)

const.SetLineStyle(2)
for err in [MAX_ACC_ERR, -MAX_ACC_ERR]:
    ratio = 1 + err
    const.SetParameter(0, ratio)
    const.DrawCopy('same')
    text = '#DeltaG/G = %d%%' % (-100*err)
    if err < 0:
        text = text.replace('= ', '= +')
    c.annotate(4000, pad + ratio, text, ndc = False, size = SMALLER_TEXT_SIZE)

totsysplus = gGraphErrors('totsysplus')
totsysminus = gGraphErrors('totsysminus')
for g in [totsysplus, totsysminus]:
    g.SetLineWidth(3)
deres = ratioEres(GAMMA, ERES_ERR) - 1
dacc = MAX_ACC_ERR

for x in numpy.logspace(math.log10(E_MIN), math.log10(E_MAX), 100):
    des = 1 - fsplus.Eval(x)
    yp = 1 + math.sqrt(deres**2 + dacc**2 + des**2)
    ym = 1 - math.sqrt(dacc**2 + des**2)
    totsysplus.SetNextPoint(x, yp)
    totsysminus.SetNextPoint(x, ym)
totsysplus.Draw('lsame')
totsysminus.Draw('lsame')
c.Update()
c.save()
