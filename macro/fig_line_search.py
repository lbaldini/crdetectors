#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math
import numpy

from __ROOT__ import *
from __logging__ import logger
from __constants__ import SECS_PER_YEAR
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend
from gTextMarker import gTextMarker


OFFSET = 1.05

f = ROOT.TF1('f', 'x/([0]**2)', 0.5, 200.)
f.SetLineWidth(2)
f.SetLineStyle(7)

c1 = gCanvas('line_search_sensitivity', Logx = True, Logy = True)
h1 = ROOT.TH2F('h1', 'h1', 100, 0.5, 200, 100, 0.5, 30)
h1.SetXTitle('Exposure factor [m^{2} sr year]')
h1.SetYTitle('Energy resolution [%]')
h1.Draw()
for p0 in [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8]:
    f.SetParameter(0, p0)
    x = 5*p0
    c1.annotate(x, 1.05*f.Eval(x), '%.2f' % p0, ndc = False, angle = 50,
                size = SMALLER_TEXT_SIZE)
    f.DrawCopy('same')
LAT = gTextMarker(2*10, 10, 'LAT', offset = OFFSET)
LAT.Draw()
DAMPE = gTextMarker(0.2*3, 1.5, 'DAMPE', offset = OFFSET)
DAMPE.Draw()
CALET = gTextMarker(0.12*5, 2, 'CALET', offset = OFFSET)
CALET.Draw()
HERD = gTextMarker(3*10, 1, 'HERD', offset = OFFSET)
HERD.Draw()
Gamma400 = gTextMarker(0.48*7, 1, 'Gamma-400', offset = OFFSET)
Gamma400.Draw()
Gamma400CC = gTextMarker(3.4*7, 2, 'Gamma-400 (CC)', offset = OFFSET)
Gamma400CC.Draw()
c1.Update()
c1.save()
