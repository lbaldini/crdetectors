#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import pickle
import re

# This is for the pickling to work.
import sys
sys.path.append('CRDB')

from __logging__ import logger
from CRDB.__settings__ import *
from CRDB.gDataSet import gDataSet


TIME_FORMAT = '%Y %b %d'


logger.info('Reading input file %s...' % CRDB_PICKLE_FILE_PATH)
inputPickleFile = open(CRDB_PICKLE_FILE_PATH)
CR_DATASET_LIST = pickle.load(inputPickleFile)
logger.info('Done.')

CR_DATASET_LIST.sort()


outputFilePath = '../tables/cr_measurements.tex'
outputFile = open(outputFilePath, 'w')


def _write(text, endline = True):
    """ Convenience local function to write to the output file.
    """
    if endline:
        text = '%s\n' % text
    outputFile.write(text)

q = None
for ds in CR_DATASET_LIST:
    if q is None or ds.Quantity != q:
        print('--- %s' % ds.Quantity)
        quant = ds.Quantity
        matches = re.findall('[0-9]*', quant)
        nums = []
        for text in matches:
            if text.isdigit():
                if text not in nums:
                    nums.append(text)
        for num in nums:
            quant = quant.replace(num, ('$^{%s}$' % num))
        quant = quant.replace('ELECTRON', 'Electrons')
        quant = quant.replace('POSITRON', 'Positrons')
        col = '\\multicolumn{4}{c}{%s}' % quant
        col = '\\tabrowspace%s\\\\\\tabrowspace' % col
        _write('\\tabularnewline\\hline\n%s\n\\hline' % col)
    
    if ds.EAxis == 'EKN':
        units = 'GeV/n'
    elif ds.EAxis == 'EK':
        units = 'GeV'
    elif ds.EAxis == 'R':
        units = 'GV'
    else:
        units = ds.EAxis

    def _format(val):
        """
        """
        if val < 1:
            return '%.2f' % val
        elif val < 100:
            return '%.1f' % val
        else:
            return '%d' % val

    emin = _format(ds.getEAxisMinimum())
    emax = _format(ds.getEAxisMaximum())
    erange = '\\makebox[0.15\\textwidth]{%s--%s\\hfill}[%s]' %\
             (emin, emax, units)
    start = ds.StartDate.strftime(TIME_FORMAT)
    stop = ds.StopDate.strftime(TIME_FORMAT)
    expName = ds.ExpName.replace('&', '\\&')
    ref = '{\\cite{%s}}' % ds.Url
    if '-' in ref:
        ref = '--'
    if False:
        ref += '\\hfill[%d]' % ds.UniqueId
    row = (expName, erange, start, stop, ref)
    fmt = '%s & %s & %s--%s & %s\\\\'
    logger.info(ds.SubExpName)
    _write(fmt % row)
    q = ds.Quantity

logger.info('Closing output file %s...' % outputFilePath)
outputFile.close()
