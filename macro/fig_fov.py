#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __constants__ import DEG_TO_RAD
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1


LAT_PERF_FILE = ROOT.TFile('data/perfPlots_P7SOURCE_V6.root')
LAT_AEFF_VS_THETA = LAT_PERF_FILE.Get('gAeffTheta_P7SOURCE_V6Total')
_x = ROOT.Double()
_y = ROOT.Double()
LAT_AEFF_VS_THETA.GetPoint(0, _x, _y)
aeffnorm = float(_y)
for i in range(LAT_AEFF_VS_THETA.GetN()):
    LAT_AEFF_VS_THETA.GetPoint(i, _x, _y)
    LAT_AEFF_VS_THETA.SetPoint(i, math.cos(math.radians(_x)), _y/aeffnorm)
LAT_AEFF_VS_THETA.SetMarkerStyle(24)
LAT_AEFF_VS_THETA.SetTitle('Fermi-LAT')

EGRET_AEFF_VS_THETA = gGraphErrors('egret_aeff', 'EGRET', MarkerStyle = 20)
for line in open('data/egret_aeff.csv'):
    x, y = [float(item) for item in line.strip('\n').split(',')]
    EGRET_AEFF_VS_THETA.SetNextPoint(math.cos(math.radians(x)), y)
# This one is to close the integral.
EGRET_AEFF_VS_THETA.SetNextPoint(math.cos(math.radians(40)), -0.01)


def getFoV(thetamax):
    """
    """
    return math.pi*(1 - math.cos(math.radians(thetamax)))


THETA_MAX_VALUES = [90, 75, 30]
DETECTOR_DICT = {'Fermi-LAT': (75, 2.4),
                 #http://heasarc.gsfc.nasa.gov/docs/cgro/egret/egret_tech.html
                 'EGRET': (30, 2*math.pi*EGRET_AEFF_VS_THETA.Integral()),
                 #'AMS-02': (15, 0.05/0.5),
                 #'PaMeLa': (18, 0.00215/(0.13*0.13))
             }


c1 = gCanvas('fov')
h1 = gH1F('h1', 'h1', 100, 0, 1, Minimum = 0, Maximum = 1, XTitle = 'cos#theta',
          YTitle = 'A_{eff}/A_{eff}^{#perp}')
f1 = gF1('f1', '(x - [0])/(1 - [0])', 0, 1)
h1.Draw()
for i, thetamax in enumerate(THETA_MAX_VALUES):
    ct = math.cos(math.radians(thetamax))
    f1.SetParameter(0, ct)
    f1.DrawCopy('same')
    y = 0.4
    x = y*(1 - ct) + ct
    c1.annotate(x, y, '%d#circ' % thetamax, ndc = False, align = 32)
LAT_AEFF_VS_THETA.Draw('psame')
EGRET_AEFF_VS_THETA.Draw('psame')
l = gLegend()
l.AddEntry(LAT_AEFF_VS_THETA, options = 'p')
l.AddEntry(EGRET_AEFF_VS_THETA, options = 'p')
l.Draw()
c1.Update()
c1.save()


c2 = gCanvas('fov_vs_thetamax', Logy = True)
h2 = gH1F('h2', 'h2', 100, 0, 90, Minimum = 0.01, Maximum = 10,
          XTitle = '#theta_{max} [#circ]', YTitle = 'Field of view [sr]')
f2 = gF1('f2', '3.1415*(1 - cos(x*3.1415/180))', 0, 90)
h2.Draw()
f2.Draw('same')
for detector, (thetamax, fov) in DETECTOR_DICT.items():
    m = ROOT.TMarker(thetamax, fov, 20)
    m.SetMarkerSize(1.2)
    store(m)
    m.Draw()
    c2.annotate(thetamax, 1.2*fov, detector, ndc = False, align = 21)
c2.Update()
c2.save()


c22 = gCanvas('fov_vs_thetamax_frac', Logy = True)
h22 = gH1F('h2', 'h2', 100, 0, 90, Minimum = 0.001, Maximum = 0.5,
           XTitle = '#theta_{max} [#circ]',
           YTitle = 'Fraction of the sky seen at any time')
f22 = gF1('f2', '0.25*(1 - cos(x*3.1415/180))', 0, 90)
h22.Draw()
f22.Draw('same')
c22.Update()
c22.save()


c3 = gCanvas('theta_dist')
h3 = gH1F('h3', 'h3', 100, 0, 90, Minimum = 0, Maximum = 0.06,
          XTitle = '#theta [#circ]', YTitle = 'dN/d#theta [#circ^{-1}]')
norm = '(0.5 + 0.5*cos([0]*%f)**2 - cos([0]*%f)' % (DEG_TO_RAD, DEG_TO_RAD)
expr = '(cos(x*%f) - cos([0]*%f))*sin(x*%f)/%s)' %\
       (DEG_TO_RAD, DEG_TO_RAD, DEG_TO_RAD, norm)
expr = '%f*(%s)' % (DEG_TO_RAD, expr)
f3 = gF1('f3', expr, 0, 90)
h3.Draw()
offsets = [-0.003, 0.001, 0.002]
for i, thetamax in enumerate(THETA_MAX_VALUES):
    f3.SetParameter(0, thetamax)
    f3.DrawCopy('same')
    print(f3.Integral(0, thetamax))
    x = 0.5*thetamax
    y = f3.Eval(x) + offsets[i]
    c3.annotate(x, y, '%d#circ' % thetamax, ndc = False)
c3.Update()
c3.save()


c4 = gCanvas('pathlength')
h4 = gH1F('h4', 'h4', 100, 0, 90, Minimum = 0, Maximum = 2.5,
          XTitle = '#theta_{max} [#circ]', YTitle = '<t>/t_{0}')
norm = '(0.5 + 0.5*cos(x*%f)**2 - cos(x*%f)' % (DEG_TO_RAD, DEG_TO_RAD)
expr = '(1 + cos(x*%f)*(log(abs(cos(x*%f))) - 1))/%s)' %\
       (DEG_TO_RAD, DEG_TO_RAD, norm)
f4 = gF1('f4', expr, 0, 90)
h4.Draw()
f4.Draw('same')
c4.Update()
c4.save()

"""
c5 = gCanvas('test', Logy = True)
thetamax = 75
f3.SetParameter(0, thetamax)
f3.SetRange(0, thetamax)
h5 = gH1F('h5', 'h5', 1000, 0, 100)
for i in range(10000):
    theta = f3.GetRandom()
    h5.Fill(1./math.cos(math.radians(theta)))
h5.Draw()
print(h5.GetMean())
c5.Update()
"""
