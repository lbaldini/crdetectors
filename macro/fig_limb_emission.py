#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy
import math
import __csv__

from __ROOT__ import *
from gCanvas import gCanvas
from gF1 import gF1
from gH1F import gH1F
from gGraphErrors import gGraphErrors

R_EARTH = 6371
ALTITUDE = 565
RHO_0 = 1.24e2
R = R_EARTH + ALTITUDE
Z0 = 7.0
AIR_X0 = 37.
LAMBDA_P = 85.

Z_FORMULA = '(%f**2 + x**2 + 2*%f*x*cos(3.1415*[0]/180))**0.5 - %f' %\
            (R, R, R_EARTH)
FORMULA = '%f*exp(-(%s)/%f)' % (RHO_0, Z_FORMULA, Z0)


f = gF1('f', FORMULA, 0, R_EARTH)

g = gGraphErrors('g')
for theta in numpy.linspace(110.5, 113.3, 100):
    f.SetParameter(0, theta)
    col = f.Integral(0, R_EARTH)
    g.SetNextPoint(theta, col)
g.GetXaxis().SetTitle('#theta_{zenith} [#circ]')
g.GetYaxis().SetTitle('Integrated column density [g cm^{-2}]')

fX0 = gF1('fX0', '%s' % AIR_X0, 0, R_EARTH, LineStyle = 7)
flp = gF1('flp', '%s' % LAMBDA_P, 0, R_EARTH, LineStyle = 7)


c1 = gCanvas('limb_col_profile', Logy = True)
h1 = gH1F('h1', 'h1', 100, 0, 5000, Minimum = 1e-15, Maximum = 1000,
          XTitle = 'x [km]', YTitle = 'Atmospheric density @ h(x) [g cm^{-3}]')
h1.Draw()
for theta in [111, 112, 113]:
    f.SetParameter(0, theta)
    f.DrawCopy('same')
    x = 2450 + 150*(theta - 111)
    c1.annotate(x, 2*f.Eval(x), '#theta_{z} = %d#circ' % theta,
                ndc = False, align = 21)
c1.annotate(0.2, 0.85, 'h_{det} = %d km' % ALTITUDE)
c1.Update()
c1.save()


c2 = gCanvas('limb_col_density', Logy = True)
g.Draw('al')
fX0.Draw('same')
flp.Draw('same')
c2.annotate(112.9, 0.8*AIR_X0, 'X_{0}', ndc = False, align = 13)
c2.annotate(112.9, 1.5*LAMBDA_P, '#lambda_{p}', ndc = False, align = 11)
c2.annotate(0.2, 0.85, 'h_{det} = %d km' % ALTITUDE)
x = 111.9
y = 10
a = ROOT.TArrow(x, y, x-1, y, 0.02, '->')
a.Draw()
l = ROOT.TLine(x, g.Eval(110.5), x, g.Eval(113.3))
l.SetLineStyle(7)
l.Draw('same')
c2.annotate(x - 0.1, 0.75*y, 'Thin-target regime', ndc = False, align = 33)
g = __csv__.read('data/fermi_limb_profile.csv', 'limb_profile')
x = ROOT.Double()
y = ROOT.Double()
for i in range(g.GetN()):
    g.GetPoint(i, x, y)
    g.SetPoint(i, 180 - x, 0.45*y)
g.Draw('psame')
c2.Update()
c2.save()
