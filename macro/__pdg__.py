#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


#from __ROOT__ import *
from __constants__ import RAD_TO_DEG


ELECTRON_MASS = 0.000511 # GeV
PROTON_MASS = 0.938      # GeV

CSI_EC = 11.17 # MeV
BGO_EC = 10.1  # MeV
PB_EC  = 7.2   # MeV



def getBetaFormula(mass):
    """
    """
    return '(x/sqrt(x**2 + %f**2))' % mass


def getMSFormula(mass, space = True, z = 1):
    """ Return the ROOT formula for the multiple scattering angel (in degrees)
    as a function of the momentum p (in GeV).

    The mass is given in GeV.
    """
    beta = getBetaFormula(mass)
    formula = '%f*0.0136*%d*sqrt([0])*(1 + 0.038*log([0]))/(x * %s)' %\
              (RAD_TO_DEG, z, beta)
    if space:
        formula = '%f*%s' % (2.**0.5, formula)
    return formula


def getCherenkovFormulaBeta():
    """
    """
    return '(x > 1./[1])*([0]*(1. - 1./((x**2)*([1]**2))))'


def getCherenkovFormulaBetaGamma():
    """
    """
    return '(x > sqrt(1/([1]**2 - 1)))*([0]*(1.-(1 + x**2)/((x**2)*([1]**2))))'
