#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from gCanvas import gCanvas
from gResidualCanvas import gResidualCanvas
from gLegend import gLegend
from gH1F import gH1F
from gF1 import gF1
from __logging__ import logger


frame = ROOT.TH1F('frame', 'frame', 100, 0, 4)
frame.SetMinimum(0.95)
frame.SetMaximum(1.01)
frame.GetYaxis().SetNdivisions(509)
frame.SetXTitle('Time since launch [years]')
frame.SetYTitle('Calorimeter light yield')


c = gCanvas('cal_light_yield')
frame.Draw()
g = ROOT.TGraph()
g.SetMarkerStyle(20)
g.SetMarkerSize(1.2)
for line in open('data/cal_light_yield.txt'):
    x, y = [float(item) for item in line.split()]
    g.SetPoint(g.GetN(), x, y)
g.Draw('p')
c.Update()
c.save()
