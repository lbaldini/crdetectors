#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import glob
import numpy

from __ROOT__ import *
from __labels__ import *
from gCanvas import gCanvas
from gH1F import gH1F
from gLegend import gLegend

MARKERS = [(20, 1.2), (24, 1), (21, 1), (25, 1), (22, 1.2)]


def MeV2GeV(h, yscale = None):
    """
    """
    binning = [h.GetBinLowEdge(i) for i in range(1, h.GetNbinsX() + 2)]
    binning = numpy.array(binning)*0.001
    hname = '%s_GeV' % h.GetName()
    htitle = h.GetTitle()
    hnew = ROOT.TH1F(hname, htitle, len(binning) - 1, binning)
    for i in range(1, h.GetNbinsX() + 1):
        hnew.SetBinContent(i, h.GetBinContent(i))
    if yscale is not None:
        hnew.Scale(yscale)
    return hnew


h = gH1F('frame', 'frame', 100, 1, 52, Minimum = 0.75e-3, Maximum = 1e1,
         XTitle = AXIS_TITLE_ENERGY, YTitle = axisTitleIntensity())

c = gCanvas('cre_broadband_spectra', Logx = True, Logy = True)
h.Draw()
fileList = glob.glob('data/allele_spec_1p*.root')
fileList.sort()
lmax = 1
l = gLegend(0.62, 0.9)
for i, filePath in enumerate(fileList):
    f = ROOT.TFile(filePath)
    spec = f.Get('flux')
    spec = MeV2GeV(spec, 1e3)
    mstyle, msize = MARKERS[i]
    spec.SetMarkerStyle(mstyle)
    spec.SetMarkerSize(msize)
    spec.SetLineStyle(7)
    spec.Draw('pc,same')
    store(f)
    store(spec)
    lmin = lmax
    lmax = float(filePath.split('.')[0].split('_')[-1].replace('p', '.'))
    spec.SetTitle('%.2f < L < %.2f' % (lmin, lmax))
    l.AddEntry(spec)
l.Draw()
c.Update()
c.save()
