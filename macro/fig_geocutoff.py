#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy
import os

from __ROOT__ import *
from gCanvas import gCanvas
from gEarthSphere import gEarthSphere
from TJI.gTrajectoryTracer import TrajectoryTracer


LAT = 0.
LON = 0.
ALT = 500.

NUM_ZENITH_BINS = 30
NUM_AZIMUTH_BINS = 72
HIST_FILE_NAME = 'data/geocutoff.py'

def createHist():
    """
    """
    zbinning = numpy.linspace(0, 90, NUM_ZENITH_BINS + 1)
    abinning = numpy.linspace(0, 360, NUM_AZIMUTH_BINS + 1)
    h = ROOT.TH2F('h', 'h', NUM_AZIMUTH_BINS, abinning,
                  NUM_ZENITH_BINS, zbinning)
    tracer = TrajectoryTracer('p+', startAltitude = ALT)
    for i in range(1, NUM_AZIMUTH_BINS + 1):
        for j in range(1, NUM_ZENITH_BINS + 1):
            azimuth = (i - 0.5)*360./NUM_AZIMUTH_BINS
            zenith = (j - 0.5)*90./NUM_ZENITH_BINS
            cutoff = tracer.getRigidityCutoff(LAT, LON, zenith, azimuth)
            print zenith, azimuth, cutoff
            h.SetBinContent(i, j, cutoff)
    outputFile = ROOT.TFile(HIST_FILE_NAME, 'RECREATE')
    h.Write()
    outputFile.Close()

if not os.path.exists(HIST_FILE_NAME):
    createHist()
inputFile = ROOT.TFile(HIST_FILE_NAME)
h = inputFile.Get('h')
h.SetMinimum(0)
h.SetMaximum(50)
h.SetXTitle('Azimuthal angle [#circ]')
h.SetYTitle('Zenithal angle [#circ]')
h.SetZTitle('Rigidity cutoff [GV]')
c = gCanvas('geomagnetic_cutoff')
c.colz(True)
h.Draw('col,cont3,z')
c.Update()
c.save()
