#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy
import math
import __stat__

from __ROOT__ import *
from __logging__ import logger
from gCanvas import gCanvas
from gH1F import gH1F
from gF1 import gF1
from gGraphErrors import gGraphErrors


MARKERS = [24, 20, 25, 21]
CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')



class gBaseSpectrum:

    """ Small convenience class to interpolate the error bars for a
    TGraph object.
    """

    def __init__(self, uid, points = None):
        """ Constructor.
        """
        logger.info('Creating base spectrum for uid %s...' % uid)
        self.GraphCenter = gGraphErrors('gcenter_%d' % uid)
        self.GraphHigh = gGraphErrors('ghigh_%d' % uid)
        self.GraphLow = gGraphErrors('glow_%d' % uid)
        for g in [self.GraphHigh, self.GraphLow]:
            g.SetLineStyle(7)
        x = ROOT.Double()
        y = ROOT.Double()
        g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
        points = points or range(g.GetN())
        self.Xmin = None
        self.Xmax = None
        for i in points:
            g.GetPoint(i, x, y)
            if self.Xmin is None or x < self.Xmin:
                self.Xmin = float(x)
            if self.Xmax is None or x > self.Xmax:
                self.Xmax = float(x)
            logger.info('Adding point %d: %.3e, %.3e' % (i, x, y))
            dx = g.GetErrorX(i)
            dy = g.GetErrorY(i)
            logx = math.log10(x)
            self.GraphCenter.SetNextPoint(logx, math.log10(y))
            self.GraphHigh.SetNextPoint(logx, y + dy)
            self.GraphLow.SetNextPoint(logx, y - dy)

    def value(self, x):
        """ Eval the best value at a specific coordinate.

        Return none if the coordinate is out of range.
        """
        if x >= 0.9*self.Xmin and x <= 1.1*self.Xmax:
            logx = math.log10(x)
            return 10**self.GraphCenter.Eval(logx)
        return None

    def error(self, x):
        """Eval the error at a specific coordinate.

        Return none if the coordinate is out of range.
        """
        if x >= 0.9*self.Xmin and x <= 1.1*self.Xmax:
            logx = math.log10(x)
            return 0.5*(self.GraphHigh.Eval(logx) - \
                        self.GraphLow.Eval(logx))
        return None




class gCombinedSpectrum(gGraphErrors):

    """
    """

    def __init__(self, name, uids, **kwargs):
        """ Constructor.
        """
        self.Name = name
        gGraphErrors.__init__(self, 'gcomb_%s' % name, name, **kwargs)
        self.Z = kwargs.get('z', 1)
        self.__combine(uids)
        self.__extrapolate(kwargs.get('fitLeverArm', 10),
                           kwargs.get('extrArm', 10))
       
    def __combine(self, uids, numPoints = 100):
        """ Combine the data sets into a unique TGraph.
        """
        logger.info('Combining data sets into %s...' % self.Name)
        datasets = [gBaseSpectrum(uid) for uid in uids]
        xmin = min(g.Xmin for g in datasets)
        xmax = max(g.Xmax for g in datasets)
        for x in numpy.logspace(math.log10(xmin), math.log10(xmax), numPoints):
            values = [g.value(x) for g in datasets]
            errors = [g.error(x) for g in datasets]
            val, err = __stat__.weightedAverage(values, errors)
            if val is not None and err is not None:
                val /= self.Z
                err /= self.Z
                x *= self.Z
                logger.info('Adding point: %.3e, %.3e (%.3e)' % (x, val, err))
                self.SetNextPoint(x, val, 0, err)

    def __extrapolate(self, leverArm, extrArm, numPoints = 10):
        """
        """
        logger.info('Extrapolating combined data set %s...'  % self.Name)
        self.extrapolatePL(leverArm, extrArm, numPoints)




if __name__ == '__main__':
    c = gCanvas('combined_spectrum', Logx = True, Logy = True)
    s = gCombinedSpectrum('protons', [136, 456, 825])
    h = gH1F('frame', 'frame', 100, 1, 5000000, Minimum = 1e-14,
             Maximum = 1e4)
    h.Draw()
    s.Draw('lsame')
    g1 = CRDB_ROOT_FILE.Get('dataset_136')
    g2 = CRDB_ROOT_FILE.Get('dataset_456')
    g3 = CRDB_ROOT_FILE.Get('dataset_825')
    g1.SetMarkerStyle(21)
    g2.SetMarkerStyle(25)
    g3.SetMarkerStyle(24)
    g1.Draw('psame')
    g2.Draw('psame')
    g3.Draw('psame')
    c.Update()
