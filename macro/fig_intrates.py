#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import math

from __ROOT__ import *
from __constants__ import SECS_PER_YEAR
from gCanvas import gCanvas
from gH1F import gH1F

import __utils__


INPUT_ROOT_FILE = ROOT.TFile('data/cr_models.root')

protons = INPUT_ROOT_FILE.Get('gcomb_protons_int')
electrons = INPUT_ROOT_FILE.Get('gcomb_all_electrons_int')
positrons = INPUT_ROOT_FILE.Get('gcomb_positrons_int')
antiprotons = INPUT_ROOT_FILE.Get('gcomb_antiprotons_int')
iron = INPUT_ROOT_FILE.Get('gcomb_Fe_int')
allgamma = INPUT_ROOT_FILE.Get('gcomb_allgamma_int')
egb = INPUT_ROOT_FILE.Get('gcomb_egb_int')


CAL_GEO_FACTORS = [0.001, 0.01, 0.1, 1., 10., 100.]
CAL_GEO_FACTORS_LABELS = str(CAL_GEO_FACTORS).strip('][').split(', ')
SPEC_GEO_FACTORS = [0.001, 0.01, 0.1, 1.,]
SPEC_GEO_FACTORS_LABELS = str(SPEC_GEO_FACTORS).strip('][').split(', ')


c1 = gCanvas('cr_int_rate_protons', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 9.99, 1e7, Minimum = 9.99, Maximum = 1e9,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Number of events above a given energy')
h1.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(protons, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(protons, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    x = 1e3
    y = 1.3*g.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c1.annotate(x, y, text, ndc = False, angle = -45, size = SMALLER_TEXT_SIZE)
c1.annotate(0.73, 0.83, 'Protons')
c1.Update()
c1.save()


c2 = gCanvas('cr_int_rate_electrons', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 1000, 9.99, 1e5, Minimum = 9.99, Maximum = 1e8,
          XTitle = 'Energy [GeV]',
          YTitle = 'Number of events above a given energy')
h2.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(electrons, ef)
    if gf == 1.:
        print 'Estimated Fermi 12-month counts in the last bin: %d' %\
            ((g.Eval(772)- g.Eval(1000))*0.9*0.86*0.92)
        print 'Actual counts: 1039'
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(electrons, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    x = 1e2
    y = 1.3*g.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c2.annotate(x, y, text, ndc = False, angle = -42, size = SMALLER_TEXT_SIZE)
c2.annotate(0.63, 0.83, 'Electrons + positrons')
c2.Update()
c2.save()


c21 = gCanvas('cr_int_rate_electrons_eres', Logx = True, Logy = True)
eres = 0.01
h21 = gH1F('h21', 'h21', 1000, 9.99, 1e4, Minimum = 9.99, Maximum = 1e8,
          XTitle = 'Energy [GeV]',
           YTitle = 'Number of events within E #pm %d%%' % (eres*100))
h21.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(electrons, ef)
    g2 = g.Clone('ginteleeres')
    e = ROOT.Double()
    n = ROOT.Double()
    for _i in range(g.GetN()):
        g.GetPoint(_i, e, n)
        emin = e*(1 - eres)
        emax = e*(1 + eres)
        g2.SetPoint(_i, e, g.Eval(emin) - g.Eval(emax))
    g2.Draw('csame')
    store(g2)
    x = 0.2e2
    y = 1.3*g2.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c21.annotate(x, y, text, ndc = False, angle = -35, size = SMALLER_TEXT_SIZE)
c21.annotate(0.63, 0.83, 'Electrons + positrons')
c21.Update()
c21.save()


c3 = gCanvas('cr_int_rate_antiprotons', Logx = True, Logy = True)
h3 = gH1F('h3', 'h3', 1000, 9.99, 1e4, Minimum = 9.99, Maximum = 1e5,
          XTitle = 'Kinetic energy [GeV]',
          YTitle = 'Number of events above a given energy')
h3.Draw()
for i, gf in enumerate(SPEC_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(antiprotons, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(antiprotons, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    x = 4e1*math.log10(10.*gf/SPEC_GEO_FACTORS[0])
    y = 1.2*g.Eval(x)
    text = '%s m^{2} sr year' % SPEC_GEO_FACTORS_LABELS[i]
    c3.annotate(x, y, text, ndc = False, angle = -45, size = SMALLER_TEXT_SIZE)
c3.annotate(0.73, 0.83, 'Antiprotons')
c3.Update()
c3.save()


c4 = gCanvas('cr_int_rate_positrons', Logx = True, Logy = True)
h4 = gH1F('h4', 'h4', 1000, 9.99, 1e4, Minimum = 9.99, Maximum = 1e5,
          XTitle = 'Energy [GeV]',
          YTitle = 'Number of events above a given energy')
h4.Draw()
for i, gf in enumerate(SPEC_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(positrons, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(positrons, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    x = 6e1*math.log10(10.*gf/SPEC_GEO_FACTORS[0])
    y = 1.1*g.Eval(x)
    text = '%s m^{2} sr year' % SPEC_GEO_FACTORS_LABELS[i]
    c4.annotate(x, y, text, ndc = False, angle = -42, size = SMALLER_TEXT_SIZE)
c4.annotate(0.73, 0.83, 'Positrons')
c4.Update()
c4.save()


c5 = gCanvas('cr_int_rate_iron', Logx = True, Logy = True)
h5 = gH1F('h5', 'h5', 1000, 9.99, 1e5, Minimum = 9.99, Maximum = 1e6,
          XTitle = 'Kinetic energy/nucleon [GeV/nucleon]',
          YTitle = 'Number of events above a given energy')
h5.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(iron, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(iron, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    x = 4e1*math.log10(10.*gf/CAL_GEO_FACTORS[0])
    y = 1.3*g.Eval(x)
    text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
    c5.annotate(x, y, text, ndc = False, angle = -45, size = SMALLER_TEXT_SIZE)
c5.annotate(0.73, 0.83, 'Iron')
c5.Update()
c5.save()


c6 = gCanvas('cr_int_rate_allgamma', Logx = True, Logy = True)
h6 = gH1F('h6', 'h6', 1000, 9.99, 1e5, Minimum = 9.99, Maximum = 1e5,
          XTitle = 'Energy [GeV]',
          YTitle = 'Number of events above a given energy')
h6.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(allgamma, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(allgamma, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    if i > 1:
        x = 4e1*math.log10(10.*gf/CAL_GEO_FACTORS[0])
        y = 1.2*g.Eval(x)
        text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
        c6.annotate(x, y, text, ndc = False, angle = -50,
                    size = SMALLER_TEXT_SIZE)
c6.annotate(0.53, 0.83, 'Gamma-ray all-sky intensity')
c6.Update()
c6.save()


c7 = gCanvas('cr_int_rate_egb', Logx = True, Logy = True)
h7 = gH1F('h7', 'h7', 1000, 9.99, 1e4, Minimum = 9.99, Maximum = 1e5,
          XTitle = 'Energy [GeV]',
          YTitle = 'Number of events above a given energy')
h7.Draw()
for i, gf in enumerate(CAL_GEO_FACTORS):
    ef = gf*SECS_PER_YEAR
    g = __utils__.gscale(egb, ef)
    g.Draw('csame')
    store(g)
    g = __utils__.gscale(egb, ef, LineStyle = 7)
    g.extrapolatePL(2.5, 100)
    g.Draw('csame')
    store(g)
    if i > 1:
        x = 2e1*math.log10(10.*gf/CAL_GEO_FACTORS[0])
        y = 1.2*g.Eval(x)
        text = '%s m^{2} sr year' % CAL_GEO_FACTORS_LABELS[i]
        c6.annotate(x, y, text, ndc = False, angle = -42,
                    size = SMALLER_TEXT_SIZE)
c7.annotate(0.45, 0.83, 'Isotropic gamma-ray background')
c7.Update()
c7.save()
