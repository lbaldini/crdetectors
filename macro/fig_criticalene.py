#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from __pdg__ import *
from gCanvas import gCanvas
from gLegend import gLegend
from gF1 import gF1


f1 = gF1('fce_solid', '710/(x + 0.92)', 1, 100, Title = 'Solids')
f1.GetXaxis().SetTitle('Z')
f1.GetYaxis().SetTitle('E_{c} [MeV]')
f2 = gF1('fce_gas', '610/(x + 1.24)', 1, 100, LineStyle = 7, Title = 'Gases')



def drawElement(symbol, z, pad = 2):
    """
    """
    val = f1.Eval(z)
    a = ROOT.TArrow(z, pad*val, z, val, 0.02, '->')
    a.Draw()
    store(a)
    ROOT.gPad.annotate(z, pad*val, '%s' % symbol, ndc = False, align = 21)


c = gCanvas('critical_energy', Logy = True, Logx = True)
f1.Draw()
f2.Draw('same')
l = gLegend(0.7, 0.9, [f1, f2])
l.Draw()
drawElement('Pb', 82)
drawElement('CsI', 54)
drawElement('Si', 14)
drawElement('C', 6)
drawElement('W', 74, pad = 3)
c.Update()
c.save()
