#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import numpy

from __ROOT__ import *
from gH1F import gH1F
from gCanvas import gCanvas
from gLegend import gLegend


BINNING = numpy.array([0., 200., 500., 1000., 2000., 3000., 4000., 5200.])
VALUES_1 = [15.4, 15.5, 15.6, 15.9, 17.3, 19.8, 34.4]
VALUES_2 = [11.1, 10.4, 10.3, 12.1, 13.3, 16.5, 27.2]


h1 = ROOT.TH1F('h1', 'h1', len(BINNING) - 1, BINNING)
h1.SetMarkerStyle(24)
h1.SetMarkerSize(1.2)
h1.SetMinimum(0)
h1.SetMaximum(40)
h1.SetXTitle('Altitude [km]')
h1.SetYTitle('Discharge rate [cm^{-3} s^{-1}]')
h1.SetTitle('Apparatus 1')
for i, value in enumerate(VALUES_1):
    h1.SetBinContent(i + 1, value)
    h1.SetBinError(i + 1, 0.01)
h2 = ROOT.TH1F('h2', 'h2', len(BINNING) - 1, BINNING)
h2.SetMarkerStyle(25)
h2.SetMarkerSize(1.)
h2.SetTitle('Apparatus 2')
for i, value in enumerate(VALUES_2):
    h2.SetBinContent(i + 1, value)
    h2.SetBinError(i + 1, 0.01)


c1 = gCanvas('hess_electroscope')
h1.Draw('e')
h2.Draw('esame')
l1 = gLegend(0.2, 0.8, [h1, h2])
l1.Draw()
c1.Update()
c1.save()


