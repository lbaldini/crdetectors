#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
sys.path.append('CRDB')

import __utils__
import pickle

from gCombinedSpectrum import gCombinedSpectrum
from __ROOT__ import *
from gCanvas import gCanvas
from gVertCanvas import gVertCanvas
from gH1F import gH1F
from __logging__ import logger
from __gamma__ import getAllSkyGammaIntensity
from gLegend import gLegend
from CRDB.__settings__ import CRDB_PICKLE_FILE_PATH


CRDB_ROOT_FILE = ROOT.TFile('CRDB/usine_all_database.root')
CR_DATASET_LIST = pickle.load(open(CRDB_PICKLE_FILE_PATH))


def printDataSets():
    """
    """
    CR_DATASET_LIST.sort()
    for quant in ['ELECTRON', 'ELECTRON+POSITRON']:
        logger.info('Searching datasets for %s...' % quant)
        for ds in CR_DATASET_LIST:
            if ds.Quantity == quant:
                logger.info(ds)
#printDataSets()

c1 = gCanvas('cr_allelectron_spectrum', Logx = True, Logy = True)
h1 = gH1F('h1', 'h1', 1000, 10, 10000, Minimum = 10, Maximum = 500,
          XTitle = 'Energy [GeV]',
          YTitle = 'E^{3} #times J(E) [m^{-2} s^{-1} GeV^{2} sr^{-1}]')
h1.Draw()
l1 = gLegend(0.2, 0.5)
markers = [20, 21, 22, 25, 24, 28]
for i, uid in enumerate([609, 610, 611, 141, 472, 880]):
    ds = CR_DATASET_LIST[uid]
    print ds.Url
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    gs = __utils__.gepscale(g, 3, MarkerStyle = markers[i])
    gs.Draw('psame')
    store(gs)
    l1.AddEntry(gs, '%s' % ds.ExpName)
l1.Draw()
c1.Update()
c1.save()


c2 = gCanvas('cr_proton_spectrum', Logx = True, Logy = True)
h2 = gH1F('h2', 'h2', 1000, 10, 100000, Minimum = 5e3, Maximum = 0.6e5,
          XTitle = 'Energy [GeV]',
          YTitle = 'E^{2.75} #times J(E) [m^{-2} s^{-1} GeV^{1.75} sr^{-1}]')
h2.Draw()
l2 = gLegend(0.2, 0.8)
markers = [24, 25, 20]
for i, uid in enumerate([142, 492, 873]):
    ds = CR_DATASET_LIST[uid]
    print ds.Url
    g = CRDB_ROOT_FILE.Get('dataset_%d' % uid)
    gs = __utils__.gepscale(g, 2.75, MarkerStyle = markers[i])
    gs.Draw('psame')
    store(gs)
    l2.AddEntry(gs, '%s' % ds.ExpName)
l2.Draw()
c2.Update()
c2.save()


