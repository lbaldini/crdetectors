#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from __ROOT__ import *
from gCanvas import gCanvas
from gGraphErrors import gGraphErrors
from gGraphEnvelope import gGraphEnvelope
from gF1 import gF1

# Ideal gas constant [J/mole/deg]
R = 8.314

# Average molecular mass
MU = 29

# Pressure at sea level [kPa] 
P_0 = 101.325

# Scale height [km]
Z_0 = 7.0

# Density at sea level [g/cm^3]
DRY_AIR_DENSITY = 1.24e-3

# Dry air radiation length [cm]
DRY_AIR_X0 = 36.62/DRY_AIR_DENSITY

# Lead density [g/cm^3]
LEAD_DENSITY = 11.350

# Lead radiation length [cm]
LEAD_X0 = 6.37/LEAD_DENSITY

# Relevant lengths.
TROPOPAUSE = 12.0
STRATOPAUSE = 50.0
MESOPAUSE = 90.0

# Credits
CREDITS = 'http://ccmc.gsfc.nasa.gov/modelweb/models/nrlmsise00.php'


def readFile(filePath):
    gd = gGraphErrors('gd')
    gd.SetLineWidth(1)
    gd.SetLineColor(ROOT.kGray)
    gt = gGraphErrors('gt')
    gt.SetLineWidth(1)
    gt.SetLineColor(ROOT.kGray)
    inputFile = open(filePath)
    for i in range(28):
        inputFile.next()
    for line in inputFile:
        line = line.strip('\n')
        if not line.startswith('-') and len(line):
            data = [float(item) for item in line.split()]
            h = data[0]
            d = data[4]
            t = data[5]
            gd.SetNextPoint(h, d)
            gt.SetNextPoint(h, t)
    gd.GetXaxis().SetTitle('Altitude [km]')
    gd.GetYaxis().SetTitle('Atmospheric density [g cm^{-3}]')
    gt.GetXaxis().SetTitle('Altitude [km]')
    gt.GetYaxis().SetTitle('Temperature [#circK]')
    return (gd, gt)


import glob
gdlist = []
gtlist = []
for filePath in glob.glob('data/nrlmsise00*.txt'):
    gd, gt = readFile(filePath)
    gdlist.append(gd)
    gtlist.append(gt)


fp = gF1('fp', '[0]*exp(-x/[1])', 0, 300)
fp.SetParameters(P_0, Z_0)
fp.GetXaxis().SetTitle('Altitude [km]')
fp.GetYaxis().SetTitle('Atmospheric pressure [kPa]')

fd = gF1('fp', '[0]*exp(-x/[1])', 0, 300)
fd.SetParameters(DRY_AIR_DENSITY, Z_0)
fd.GetXaxis().SetTitle('Altitude [km]')
fd.GetYaxis().SetTitle('Atmospheric density [g cm^{-3}]')

fb = gF1('fp', '[0]*exp(-x/[1])', 0, 50)
fb.SetParameters(100000*DRY_AIR_DENSITY*Z_0, Z_0)
fb.GetXaxis().SetTitle('Altitude [km]')
fb.GetYaxis().SetTitle('Atmospheric overburden [g cm^{-2}]')


ct = gCanvas('atmospheric_temperature', Logy = False)
g = gtlist[0]
g.Draw('al')
g.GetXaxis().SetRangeUser(0, 300)
g.GetYaxis().SetRangeUser(100, 1200)
for g in gtlist[1:]:
    g.Draw('lsame')
env = gGraphEnvelope(gtlist)
env.Draw()
y = 500
ypad = 20
a1 = ROOT.TArrow(TROPOPAUSE, y, TROPOPAUSE,
                 env.GraphHi.Eval(TROPOPAUSE) + ypad, 0.02)
a1.Draw()
ct.annotate(TROPOPAUSE, y, 'Tropopause', ndc = False, angle = 60)
y = 450
a2 = ROOT.TArrow(STRATOPAUSE, y, STRATOPAUSE,
                 env.GraphHi.Eval(STRATOPAUSE) + ypad, 0.02)
a2.Draw()
ct.annotate(STRATOPAUSE, y, 'Stratopause', ndc = False, angle = 60)
y = 600
a3 = ROOT.TArrow(MESOPAUSE, y, MESOPAUSE,
                 env.GraphHi.Eval(MESOPAUSE) + ypad, 0.02)
a3.Draw()
ct.annotate(MESOPAUSE, y, 'Mesopause', ndc = False, angle = 60)
#ct.annotate(0.15, 0.9, 'Credits: %s' % CREDITS, size = SMALLEST_TEXT_SIZE)
ct.Update()
ct.save()

cp = gCanvas('atmospheric_pressure', Logy = True)
fp.Draw()
cp.Update()
cp.save()

cd = gCanvas('atmospheric_density', Logy = True)
g = gdlist[0]
g.SetMinimum(1e-15)
g.SetMaximum(1e-2)
g.Draw('al')
g.GetXaxis().SetRangeUser(0, 300)
for g in gdlist[1:]:
    g.Draw('lsame')
fd.Draw('same')
g = gdlist[0]
lines = []
for x in [TROPOPAUSE, STRATOPAUSE, MESOPAUSE]:
    l = ROOT.TLine(x, g.GetMinimum(), x, g.GetMaximum())
    l.SetLineStyle(7)
    lines.append(l)
    l.Draw()
x = 0.5*(TROPOPAUSE + STRATOPAUSE)
cd.annotate(x, 1e-12, 'Stratosphere', ndc = False, angle = 90, align = 12)
x = 0.5*(STRATOPAUSE + MESOPAUSE)
cd.annotate(x, 1e-14, 'Mesosphere', ndc = False, angle = 90, align = 12)
x = 0.5*(MESOPAUSE + 300)
cd.annotate(x, 1e-10, 'Thermosphere', ndc = False, angle = 90, align = 12)
cd.Update()
cd.save()

cb = gCanvas('atmospheric_overburden', Logy = True)
fb.SetMinimum(0.5)
fb.SetMaximum(2000)
fb.Draw()
box = ROOT.TBox(30, 0.5, 42, 2000)
box.SetFillStyle(3004)
box.SetFillColor(ROOT.kBlack)
box.Draw('l')
cb.annotate(0.5, 0.7, 'Typical balloon float altitude')
cb.Update()
cb.save()

t = 270.
z0 = R*t/9.81/MU
print('Isothermal scale height at %d deg K: %.2f km' % (t, z0)) 
print('Pressure at sea level: %.2f kPa' % P_0)
print('Density at sea level: %.3e g cm^-3' % DRY_AIR_DENSITY)
print('Best scale height: %.2f km' % (Z_0)) 
print('Overburden at sea level: %.2f' % (DRY_AIR_DENSITY*Z_0*100000))
print('Equivalent lead thickness: %.2f' %\
      (Z_0*100000*DRY_AIR_DENSITY/LEAD_DENSITY))
print('Equivalent air X0: %.2f' % (Z_0*100000/DRY_AIR_X0))
print('Equivalent lead X0: %.2f' %\
      (Z_0*100000*DRY_AIR_DENSITY/LEAD_DENSITY/LEAD_X0))
