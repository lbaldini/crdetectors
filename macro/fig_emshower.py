#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2014 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *
from __geometry2d__ import *
from gPhysicalCanvas import gPhysicalCanvas

WIDTH = 8
HEIGHT = 8
XPAD = 2.5


c = gPhysicalCanvas('toy_em_shower', WIDTH, HEIGHT)
eterminals = []
gterminals = []
for t in range(6):
    y = 0.5*HEIGHT - t - 1
    hline(0, y, 0.8*WIDTH, LineStyle = 7, LineColor = ROOT.kGray + 1,
          LineWidth = 1)
    c.annotate(0.4*WIDTH + 0.2, y, 't = %d' % t, ndc = False)
    n = 2**t
    if t == 0:
        line(0, y, 0, y - 1)
        eterminals = [0]
    else:
        dx = XPAD/float(2**t)
        _eterm = []
        _gterm = []
        for x in eterminals:
            line(x, y, x + dx, y - 1)
            line(x, y, x - dx, y - 1, curly = True)
            _eterm.append(x + dx)
            _gterm.append(x - dx)
        for x in gterminals:
            line(x, y, x + dx, y - 1)
            line(x, y, x - dx, y - 1)
            _eterm.append(x + dx)
            _eterm.append(x - dx)
        eterminals = _eterm
        gterminals = _gterm
c.annotate(0, 0.5*HEIGHT - 0.9, 'e^{-}', ndc = False)
c.annotate(-0.4*XPAD, 0.5*HEIGHT - 2.5, '#gamma', align = 22, ndc = False)
c.annotate(0.4*XPAD, 0.5*HEIGHT - 2.5, 'e^{-}', align = 22, ndc = False)
c.annotate(-0.7*XPAD, 0.5*HEIGHT - 3.5, 'e^{+}', align = 22, ndc = False)
c.annotate(-0.3*XPAD, 0.5*HEIGHT - 3.5, 'e^{-}', align = 22, ndc = False)
c.Update()
c.save()
