#!/usr/bin/env python
# *********************************************************************
# * Copyright (C) 2013 Luca Baldini (luca.baldini@pi.infn.it)         *
# *                                                                   *
# * For the license terms see the file LICENCE, distributed           *
# * along with this software.                                         *
# *********************************************************************
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from __ROOT__ import *


class gCoastline:
    
    """ Small class encapsulating a coastline.
    """

    def __init__(self, filePath = 'data/wlc_coastline.dat', **kwargs):
        """ Constructor.
        """
        self.PolyList = []
        if filePath is not None:
            self.read(filePath, **kwargs)

    def __PolyLine(self, **kwargs):
        """ Create a new custom polyline.
        """
        l = ROOT.TPolyLine()
        l.SetLineWidth(kwargs.get('LineWidth', 1))
        l.SetLineColor(kwargs.get('LineColor', ROOT.kBlack))
        self.PolyList.append(l)
        return l

    def read(self, filePath, **kwargs):
        """ Read an input data file.
        """
        self.PolyList = []
        for line in file(filePath):
            if line.startswith('>'):
                l = self.__PolyLine(**kwargs)
            else:
                x, y = [float(item) for item in line.strip('\n').split()]
                l.SetNextPoint(x, y)

    def Draw(self, opts = ''):
        """ Draw the coastline.
        """
        for line in self.PolyList:
            line.Draw()



if __name__ == '__main__':
    from gCanvas import gCanvas
    c = gCanvas('c')
    h = ROOT.TH2F('h', 'h', 360, -180, 180, 180, -90, 90)
    h.Draw()
    cl = gCoastline()
    cl.Draw()
    c.Update()
    
