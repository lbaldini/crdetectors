\section{Basic formalism and notation}%
\label{sec:notation}

A significant part of cosmic-ray physics is about \emph{energy spectra}.
That said, you should be wary when you happen to hear a cosmic-ray physicist
pronouncing the word \emph{spectrum}: it might indicate all sort of things.
On the $x$-axis you might find the particle (total or kinetic) energy, the
energy per nucleon, the momentum or the rigidity. On the $y$-axis you might
find a differential or integral flux or intensity, possibly multiplied by a
power of the variable plotted on the $x$-axis. We shall not refrain ourselves
from using the term spectrum in this somewhat loose sense, and the reader is
advised that sometimes it's pretty easy to get confused---so always pay
attention to the axis labels!

\begin{figure*}[!htb]
  \includegraphics[width=0.5\linewidth]{cr_spectra_p_he_ekn}%
  \includegraphics[width=0.5\linewidth]{cr_spectra_p_he_ek}
  \caption{Differential intensities, plotted as a function of kinetic energy
    per nucleon (left) and total kinetic energy (right) for the two most
    abundant CR species: protons and He nuclei. While the He intensity is a
    factor of $\sim 10$ smaller than that of protons at, say,
    $\sim 1$~TeV/nucleon, the two are comparable, at $\sim 1$~TeV, when binned
    in total kinetic energy. See next section for more details about the data
    points and the dashed lines.}
  \label{fig:cr_spectra_p_he}
\end{figure*}

As a mere illustration, figure~\ref{fig:cr_spectra_p_he} shows how the proton
and helium spectra look different, relative to each other, depending on whether
the differential intensity is plotted as a function of the kinetic energy per
nucleon or the total kinetic energy. We note, in passing, that the first
choice is the one customarily adopted in this energy range and, clearly, the
one that we have in mind when we make statements such as
``\emph{protons account for $\sim 90\%$ of cosmic-rays}''. That said,
the second representation is not necessarily less meaningful---e.g., when
dealing with the energy measurement in a calorimetric experiment.
We'll come back to this shortly.

In this section we introduce some basic terminology and notation that
we shall use in the following.

\begin{table}[htb!]
  \begin{tabular*}{\linewidth}{l @{\extracolsep{\fill}} lll}
    \hline
    Symbol & Description & Units\\
    \hline
    \hline
    \Z & Atomic number & --\\
    \A & Mass number & --\\
    \density & Density & [\densityunits]\\
    \grammage & Grammage & [\grammageunits]\\
    \hline
    \E & Energy & [GeV]\\
    \Ek & Kinetic energy & [GeV]\\
    \Ekn & Kinetic energy/nucleon & [\Eknunits]\\
    \p & Momentum & [\punits]\\
    \R & Rigidity & [\Runits]\\
    \hline
    \flux & Differential flux & [\fluxunits]\\
    \intensity & Differential intensity & [\intensityunits]\\
    & & [\intensityunitsn]\\
    \anisotropy & Dipole anisotropy & --\\
    \hline
  \end{tabular*}
  \caption{Summary table of some useful quantities, along with the
    corresponding symbols and measurement units we shall use throughout this
    write-up.}
  \label{tab:basic_notation}
\end{table}


\subsection{Fluxes and Intensities}%
\label{sec:fluxes_intensites}

The term \emph{(differential) flux} indicates the number of particles per unit
time and energy crossing the unit vector area toward a given direction in the
sky and is customarily measured in \fluxunits. We shall indicate differential
fluxes with \flux\ throughout this manuscript.

The concept of differential flux essentially applies to \emph{point source}
studies, where the incoming particles all arrive from the same direction.
On the other hand, an isotropic flux of charged particles or photons is more
conveniently characterized by its \emph{intensity} \intensity\ (number of
particles per unit time, energy, area and solid angle) which is typically
measured in \intensityunits.

The distinction between differential fluxes and intensities is connected with
the dispersive nature of gamma-ray astronomy---in gamma rays you observe
a different patch of the sky at any time, while cosmic rays are approximately
the same in all directions. As we shall see in section~\ref{sec:irfs}, the
consequences are far reaching in terms of describing the instrumental
sensitivity. We shall try and stick to this nomenclature religiously throughout
the manuscript.

Sometimes it is handy to work with quantities related to the number of events
detected \emph{above} a given energy---\emph{integral} fluxes and intensities
are useful concepts that will be widely used in the following.

Depending on the situation, differential and integral fluxes and intensities
can be expressed as a function of energy, energy per nucleon, momentum and
rigidity%
\footnote{As a rule of thumb remember that, if a differential quantity is
  plotted as a function of a given variable $x$ (be it energy, momentum,
  rigidity or whatever), it is generally understood that the derivative is
  taken with respect to $x$.}%
. Trivial as this might seem, there is a few subtleties involved in
the conversion between different representation that we shall discuss in the
next section. 


\subsection{Energy, momentum and all that}%
\label{sec:energy_momentum}

The total energy \E, kinetic energy \Ek\ and momentum \p\ of a particle or
nucleus are related to each other (through the rest mass $m$) by the well
known relativistic formul\ae
\begin{align}
  \p &= mc\beta\gamma\\
  \E^2 &= m^2 c^4 + \p^2 c^2 \\
  \Ek &= \E - mc^2,
\end{align}
that allow to switch from one variable to another when needed. When dealing
with ultra-relativistic particles (i.e., when $E \gg mc^2$, which is not
uncommon at all, in this context) energy, kinetic energy and momentum are
really the same thing---modulo the speed of light $c$---and one does not need
to bother about the differences. But for, e.g., protons and heavier nuclei
below $\sim 10$~GeV the spectra do look different depending on the variable
they are binned in (see figure~\ref{fig:cr_spectra_p_he}).

Since we are at it, here is a few other relativistic formul\ae\ that we shall
occasionally use in the following---they express $\beta$, $\gamma$ and
$\beta\gamma$ as a function of momentum, total energy and kinetic energy:
\begin{align}
  \beta & =
  \frac{pc}{\sqrt{m^2c^4 + \p^2c^2}} = 
  \sqrt{1 - \frac{m^2c^4}{{\E}^2}} =
  \frac{\sqrt{\Ek^2 + 2\Ek mc^2}}{\Ek + mc^2}\\
  \gamma & = 
  \sqrt{1 + \frac{\p^2c^2}{m^2c^4}} =
  \frac{\E}{mc^2} = 
  1 + \frac{\Ek}{mc^2}\\
  \beta\gamma &=
  \frac{\p}{mc} =
  \sqrt{\frac{{\E}^2}{m^2c^4} - 1} =
  \sqrt{\frac{{\Ek}^2 + 2\Ek mc^2}{m^2c^4}}.
\end{align}

The rigidity $R$ is essentially the ratio between the momentum and the
charge \Z\ (measured in units of the electron charge $e$) of a
particle\footnote{Don't get confused by the extra factors: a proton ($Z = 1$)
with a momentum $\p = 1$~GeV/c has rigidity of 1~GV.}:
\begin{align}
  \R = \frac{\p c}{\Z e}
\end{align}
Since we shall deal with magnetic fields---and it is easy to realize that
particles with the same rigidity behave the same way in a magnetic field---this
is a useful concept.

Finally, the CR spectra for He and heavier nuclei are more conveniently
expressed as a function of the kinetic energy per nucleon
\begin{align}
  \Ekn = \frac{\Ek}{\A}.
\end{align}
The kinetic energy per nucleon is a useful concept because, from the standpoint
of the hadronic interactions, a nucleus with mass number \A\ and kinetic energy
\Ek\ behaves, to a large extent, as the \emph{superposition} of \A\ nucleons
with kinetic energy $\Ek/\A$. (As a matter of fact, this is equally true---and
relevant---in the interstellar medium, in the Earth atmosphere and in the
calorimeter of a particle-physics detector.) It goes without saying that 
the kinetic energy per nucleon is conserved in spallation processes, and that
it's also the basic quantity determining whether a nucleus is relativistic or
not (which is the reason why differential intensities for different charged
species look more similar to each other in shape when binned in \Ekn).

Before we move on, we note that translating a differential intensity expressed
in kinetic energy per nucleon into the same intensity expressed in (total)
kinetic energy (or vice-versa) is slightly more complicated than scaling the
$x$-axis by a factor \A---one has also to scale the $y$-axis by a factor
$1/\A$, as by \emph{differential} we really mean \emph{differential in whatever
variable we have on the $x$-axis}. If the original differential intensity is
reasonably well described by a power-law with a spectral index $\Gamma$, a
scale factor \A\ on the $x$-axis is equivalent to a factor $\A^\Gamma$ on the
$y$-axis and the whole thing is effectively equivalent to multiplying the data
points by factor $\A^{\Gamma -1}$:
\begin{align}
  \intensity(\Ek) \approx \A^{\Gamma -1} \intensity(\Ekn).
\end{align}
Is is not by chance that the two He spectra in figure~\ref{fig:cr_spectra_p_he}
differ by a factor of $\sim 10$ in the high-energy regime, as
$4^{1.75} \approx 10$.


\subsection{Other observables}%
\label{sec:other_observables}

Charged-particle energy spectra (i.e. differential intensities) are not only
interesting \emph{per se}, but also in relation to each other. This aspect
customarily goes under the name of cosmic-ray \emph{chemical composition},
which is a crucial piece of information, as it probes the diffusion processes
in the Galaxy and provides an indirect measurement of the material traversed
by cosmic rays in their random walk from the source to the observer.
The isotopical composition, accessible by magnetic spectrometers, is an
interesting sub-chapter of this topic (more on this in section~\ref{sec:tof}).

On a related note, CR arrival directions generally bear no real memory of the
source---except, possibly, for the highest energies (see, e.g.,
\cite{2008arXiv0808.0417L}), which are not really of interest, in this context.
Nonetheless the anisotropy on large angular scales is another interesting
observable, especially for the leptons (since they rapidly loose energy due to
radiation they probe the nearby galactic space). In the somewhat na\"ive
scenario where the intensity is dominated by a single (nearby) source, one
expects a dipole anisotropy by the Firck's law. More generally, in a multipole
analysis one could expect detectable signatures on medium to large scales due
to the stochastic nature of the sources.

Anisotropies at all angular scales are also interesting in gamma rays, both in
connection with the study of the galactic diffuse emission and the contribution
of source populations to the extra-galactic diffuse emission.

As gamma rays \emph{do} point back to their sources, the position and
morphology of point/extended sources are yet other relevant observables.
We shall come back to this later in the write-up.

We end the list mentioning the exciting perspective of measuring gamma-ray
polarization in the pair production regime.


\subsection{The grammage}

When a particle traverses a homogeneous slab of a material---say some
compressible gas at a given pressure---the average number of scattering centers
it encounters is proportional to the product between the density \density\
and the thickness $l$ of the slab. Doubling one and reducing the other by $1/2$
does not really change anything: the quantity characterizing the amount of
material traversed is the \emph{grammage}
\begin{align}
  \grammage = \density l,
\end{align}
which is measured in g~cm$^{-2}$ in the cgs system. This quantity is also
referred to as the \emph{mass per unit area}, as
\begin{align}
  \density = \frac{m}{V} = \frac{m}{lS},
\end{align}
and therefore
\begin{align}
  \grammage = \frac{m}{S}.
\end{align}

There are many situations in which the grammage is a useful concept, e.g., in
characterizing the integrated column density that cosmic rays traverse
in their random walk from the source to the observer or the residual 
atmosphere at the typical balloon floating altitude. In both cases it is
really the product of the density times the path length (or, more precisely,
the integral of the density along the path, as the density is not constant)
that matters%
\footnote{Interestingly enough, in this case the enormous differences in 
  the basic length scales play out in such a way that the answer is pretty much
  the same ($\sim 5$~\grammageunits) in both situations.
  More on this in section~\ref{sec:atmospheric_grammage}}.

The grammage is not the only quantity measured in g~cm$^{-2}$ we shall encounter
in the following. The radiation and interaction length of materials---as we
shall see in sections~\ref{sec:em_showers} and \ref{sec:had_showers} these are
the typical length scales over which electromagnetic and hadronic showers
develop---are customarily measured in the same units, as they tend to be
smaller for denser material. In this case the mass scaling law is only
approximate and measuring these quantities in \grammageunits\ underlines the
intrinsic differences. One can then recover the actual length scale (in~cm)
just dividing by the density. We shall see in section~\ref{sec:earth_limb} how
comparing the grammage and the radiation length in a variable-density
environment (the atmosphere of the Earth) turns out to be handy.
