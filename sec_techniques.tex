\section{Experimental Techniques}%
\label{sec:techniques}

In this section we briefly review some of the most widely used experimental
techniques (mind this largely relies on the content of the previous section). 


\subsection{Tracking detectors}

The simplest---and yet most widely used in practical
implementations---configuration for a tracking stage of a modern experiment is
a stack of parallel detection planes. The basic building block of such an
instrument is a position-sensitive detector, i.e., a device capable of
measuring the position of passage of the particle---in one or both dimensions.

At the top level, the two main figures of merit of a tracking detectors
are the tracking efficiency (i.e., the efficiency of correctly reconstructing
the track of the incoming particle) and the spatial and/or angular resolution.
At a more fundamental level, these performance figure derive largely
from the hit efficiency and resolution of the position-sensitive detector used,
though in practice there are many more considerations involved (e.g., we shall
see in sections~\ref{sec:lat_psf} and \ref{sec:ams_mdr} two examples of
interplay between the hit resolution and the multiple scattering in practical
tracking applications.)

Other relevant figures of merit of tracking systems are, in no particular order:
triggering capabilities, time response, dead time per event, radiation
tolerance, robustness, reliability, power consumption.


\subsubsection{Position-sensitive detectors}

There are literally too many position-sensitive detectors used in tracking
applications to list them here, and we refer the reader to~\cite{PDG} and
references therein for a comprehensive review.
Roughly speaking, most tracking detectors exploit the ionization produced by
charged particles as they traverse matter. In order for the electron-ion
(or electron-hole) pairs not to recombine immediately a suitable electric field
is necessary---and drifting under the effect of this electric field the charges
induce on the readout electrodes a signal that is generally amplified,
processed and digitized by some form of readout electronics.

At a microscopic level, the amount of primary free charge that is created as a
particle traverse a medium is determined by the effective ionization energy.
This determines the counting statistic of the process and, as a consequence,
the energy resolution (in applications where this is relevant). The average
ionization energy is smaller, e.g., for semiconductor detectors relative to
gaseous detectors, though it should be noted that in the latter case avalanche
processes can be exploited to multiply the primary ionization
\emph{in the detector medium}.

The drift velocity of the ionization toward the readout electrodes is another
fundamental characteristic of a position-sensitive detector, as it determines
the time profile of the charge signal at the input of the readout electronics.
In broad terms, gas detectors are generally slower than semiconductor
detectors.

The hit efficiency is defined as the efficiency of signaling the passage of a
(minimum ionizing) particle and usually runs not too much below $100\%$ within
the active area for a decent modern detector. (We note, in passing, that it
does not really make sense to quote an efficiency without quoting the
corresponding noise level, or the rate of spurious hits. Here we really mean
``$\sim 100\%$ hit efficiency at a reasonable noise level'' whatever than means
in any specific context.) Hit resolutions of $\sim 10~\mu$m are not uncommon
in modern silicon-strip trackers.

The positional measurement capabilities are usually achieved by segmenting
the readout electrode(s), e.g., in strips or pixels. We note, in passing,
that the number of readout channels in a pixel detector scales as the square
of the active surface (as opposed to strip detectors, for which the scaling is
linear), and therefore pixel detectors are not particularly fashionable for use
in space due to power constraints (there are, however, exception, see
e.g.~\cite{2007NIMPA.570..286P,2003ICRC....4.1857Z}).
The \emph{hit resolution} denotes the resolution on the point where the
particle hit the detector and it is obviously related to the level of
segmentation of the sensor. For a strip detector with pitch $p$ it is given by
\begin{align}
  \sigma_{\rm hit} = \frac{p}{\sqrt{12}}
\end{align}
(i.e., the standard deviation of a uniform continuous distribution) if a single
strip is hit and can be significantly better is more strips are hit and
the pulse-height information is available to baricenter the position.


\subsubsection{Silicon detectors}%
\label{sec:silicon_detectors}

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{si_detector_size}
  \caption{Silicon surface vs. number of readout channels for a compilation
    of tracking detectors operated at accelerators, in space and on
    balloons.}
  \label{fig:si_detector_size}
\end{figure}

Semiconductor detectors---and very especially silicon detectors---are widely
used in modern high-energy physics experiments (see
figure~\ref{fig:si_detector_size}), as integrated circuit technology allows
the realization on large scale of high-density electrode structures, with
excellent spatial resolution and very fast intrinsic response time.
All the last-generation space-based experiments where state-of the-art tracking
capabilities are needed exploit silicon (strip, mostly) detectors
(see, e.g., table~\ref{tab:exp_si_trackers}). In this section we shall
briefly review some of the basic related concepts and we refer the reader
to~\cite{2012NIMPA.666...25H} for a comprehensive review of silicon tracking
detectors in high-energy physics.

\begin{table}[htb!]
  \begin{tabular}{p{0.2\linewidth}p{0.18\linewidth}p{0.17\linewidth}%
      p{0.18\linewidth}p{0.17\linewidth}}
    \hline
    Experiment & Area [m$^2$] & Channels & Pitch [$\mu$m] & Power [W]\\
    \hline
    \hline
    AGILE & 3.5 & 36,864 & 242 & 15\\
    AMS-02 & 6.2 & 196,608 & 110/208 & 140\\
    \Fermi-LAT & 73.0 & 884,736 & 228 & 160\\
    PaMeLa & 0.134 & 36864 & 50/50 & 37 \\
    \hline
  \end{tabular}
  \caption{Main features of some of the silicon trackers built for space-based
    cosmic-ray and gamma-ray detectors. For the magnetic spectrometers the
    two figures for the strip pitch indicate the values for the
    bending/non-bending view, respectively.}
  \label{tab:exp_si_trackers}
\end{table}

Silicon detectors are essentially $p$-$n$ junction diodes operated at reverse
bias. The bias voltage has the primary purpose of increasing the depth of the
so-called depletion region, defining the active volume of the detector. Full
depletion can generally be achieved with 100--200~V bias over a typical
thickness of 300--400~$\mu$m.

One of the primary advantages of silicon detectors over other typologies of
sensors is the small (3.6~eV) average energy needed to create an electron-hole
pair---roughly an order of magnitude less than what is usually required in a
gaseous detector. For the same radiation energy this translates, as mentioned
in a the previous section, into a corresponding increase in terms of primary
charge carriers created. In addition they feature very fast intrinsic
response times (of the order of ns) and can be reliably produced with active
surfaces of tens (or hundreds) of m$^2$. They are self-triggering and
do not require consumables (e.g., gas), which makes them ideal candidates for
long-term operation in space.


\subsection{Magnetic Spectrometers}%
\label{sec:magnetic_spectrometers}

Momentum is typically measured by measuring the deflection of the
charged-particle trajectory in a magnetic field. There are actually two
slightly different concepts of \emph{magnetic spectrometers}: those measuring
the deflection angle and those measuring the sagitta. While this distinction
is partially artificial, the most notable difference is that the tracking
detectors are arranged in two arms \emph{outside} of the deflecting magnet in
the first case, while they are placed \emph{inside} the magnet in the latter.
Placing the tracking stage inside the magnet is key to make the spectrometer
compact and therefore this is typically the solution adopted in space.
We shall see, though, that AMS-02 in the permanent magnet configuration is
actually an interesting hybrid between the two concepts.

Figure~\ref{fig:magnetic_spectrometer} illustrates the basic principle
of a magnetic spectrometer measuring the sagitta.
The curvature radius for a particle with charge $z$ (in units of the electron
charge $e$) and transverse momentum $p$ (in the following we shall neglect the
trivial drift in the longitudinal direction) in a magnetic field $B$ is
\begin{align}
  \rho = \frac{p}{zeB} = \frac{R}{cB} =
  \frac{R~[{\rm GV}]}{0.3 B~[{\rm T}]}~{\rm m}.
\end{align}
In this context the rigidity $R$ is a convenient quantity to work with, as
particles with the same rigidity behave the same way in a magnetic field,
irrespectively of their charge. If $\rho$ is measured and $B$ is known, one
can recover $R$---and, assuming that the charge of the particle is also known,
the momentum $p$.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{magnetic_spectrometer}
  \caption{Sketch of the momentum measurement in a uniform magnetic field
    (in the bending view).
    Here and in the following of this section $L$ is the height of the
    region permeated by the magnetic field, $\rho$ is the radius of
    curvature of the track, $\theta_{\rm B}$ is the deflection angle, and $s$ is
    the sagitta of the track.}
  \label{fig:magnetic_spectrometer}
\end{figure}

One can see in figure~\ref{fig:magnetic_spectrometer} that $L$ and the
deflection angle $\theta$ are related
\begin{align}
  \frac{L}{2} = \rho\sin\left(\frac{\theta_{\rm B}}{2}\right) \approx
  \frac{\rho\theta_{\rm B}}{2}
\end{align}
(the latter holding in small-angle approximation). The bending angle
$\theta_{\rm B}$ therefore reads
\begin{align}\label{eq:theta_bending}
  \theta_{\rm B} \approx \frac{L}{\rho} = \frac{cBL}{R} =
  \frac{0.3 B~[{\rm T}]~L~[{\rm m}]}{R~[{\rm GV}]}~{\rm rad}.
\end{align}
The details of the measurement depend somewhat on the detector setup (e.g.,
the number of measurements and relative spacing of the tracking planes, not to
mention the pattern recognition and track-fitting algorithms) but
essentially what we measure is the sagitta $s$%
\footnote{Another commonly used exemplificative metrics is the maximum
  track displacement $\Delta_b$ in the bending plane for normal incidence,
  which can be shown to be $\Delta_b = 4s$. We prefer $s$ as its geometrical
  meaning is independent of the incidence angle (though we are mainly
  interested in the orders of magnitude here, so they are effectively
  the same thing).}
of the trajectory
\begin{align}
  s & = \rho\left[1 - \cos\left(\frac{\theta_{\rm B}}{2}\right)\right] \approx
  \frac{\rho\theta_{\rm B}^2}{8} \approx \frac{L^2}{8\rho} = \frac{cBL^2}{8R} =
  \nonumber \\
  & = \frac{37.5~B~[{\rm T}]~L^2~[{\rm m^2}]}{R~[{\rm GV}]}~{\rm mm}.
\end{align}
It goes without saying that the expression we derived is typically used in the
opposite direction, i.e., one measures $s$ to deduce $R$:
\begin{align}\label{eq:spectro_rigidity}
  R = \frac{cBL^2}{2s} = 
  \frac{37.5~B~[{\rm T}]~L^2~[{\rm m^2}]}{s~[{\rm mm}]}~{\rm GV}.
\end{align}
The quantity $BL^2$ is customarily referred to as the \emph{bending power}
and it is one of the basic figures of merit for a magnetic spectrometer,
as it determines the sagitta of the track.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{sagitta_vs_bp}
  \caption{Trajectory sagitta as a function of rigidity
    (see figure~\ref{fig:magnetic_spectrometer}).
    The two lines correspond to two illustrative values of the bending power,
    corresponding to PaMeLa and the original design of AMS-02---with the
    superconductive magnet). We have not used the AMS-02 design
    that is operating on the ISS as the two external tracker planes do not
    quite fit in our simplified cartoon---and the original AMS-02 design
    is still representative of the current technological bleeding edge---but
    we shall come back to that in section~\ref{sec:ams_mdr}.
    Note that the plot uses the small-angle approximation that might be
    inaccurate at small rigidity.}
  \label{fig:sagitta_vs_bp}
\end{figure}

Figure~\ref{fig:sagitta_vs_bp} shows the value of the trajectory sagitta as a
function of the particle rigidity for two representative values of the bending
power. At a rigidity of $\sim 1$~TV the track displacement that one aims
measuring, with the magnetic field that can be reasonably achieved, is between
$\sim 5$ and $\sim 50~\mu$m, i.e., at the limit of a typical solid-state
tracking detector. We anticipate that this is indeed the limiting factor for
the maximum detectable rigidity, and in the high-momentum regime the relative
uncertainty on the rigidity increases linearly with the rigidity itself
\begin{align}
  \frac{\sigma_R}{R} = \frac{\sigma_s}{s} \propto{R},
\end{align}
as the uncertainty on the sagitta $\sigma_s$ is a constant dictated by the
detector and the sagitta itself $s$ goes like $1/R$. This is one of the most
notable differences between spectrometers and calorimeters (see
section~\ref{sec:calorimetry}).


\subsection{Scintillators}

Scintillators utilize the ionization produced by charged particles (or by
gamma-rays converting into an electron positron pair within the material)
into light that can be in turn converted into an electric signal by a
photodetector such as a photomultiplier tube (PMT) or a pin diode.

Scintillating materials are broadly divided into \emph{organic} (most notably,
plastic) and \emph{inorganic}---the main difference being that
the first have a relatively low density (and are therefore widely employed,
e.g., in trigger, anti-coincidence and time of flight systems), while the
latter are used in applications, such as calorimetry (see
section~\ref{sec:calorimetry}), where a high stopping power is required.

The choice of a particular material for a particular application is in general
dictated by several different properties, such as the light yield, the
duration and wavelength spectrum of the output light pulse, the value of the
radiation and/or interaction lengths. We refer the reader to~\cite{PDG} and
reference therein for a more in-depth discussion.


\subsection{Calorimetry}
\label{sec:calorimetry}

Calorimeters are instruments in which the particles to be measured are
fully or partially absorbed and their energy is transformed into a
measurable signal.

Compared to magnetic spectrometers, calorimeters have the double advantage
that the energy resolution improves as $1/\sqrt{E}$ at high energy (rather than
deteriorating linearly with the particle momentum) and the characteristic
dimensions (i.e., the shower depth) scale only logarithmically with the
particle energy. In addition, calorimeters are also sensitive to neutral
particles and provide position/direction information and particle
identification capabilities (though, strictly, speaking, the latter apply to
spectrometers, too).


\subsubsection{Electromagnetic calorimeters}
\label{sec:em_cal}

Electromagnetic calorimeter can be homogeneous or sampling. Homogeneous
calorimeters typically feature an excellent energy resolution, but can be less
easily segmented with a potentially detrimental effect on the position
measurement and particle identification. It is fair to say that the thickness
(in terms of radiation lengths) is one of the basic figures of merit in this
context, as it determines the shower containment, as shown in
figure~\ref{fig:shower_containment}.

\begin{figure}[htb]
  \includegraphics[width=\linewidth]{shower_containment}
  \caption{Average shower containment as a function of the shower
    depth (in homogeneous BGO) for electrons of different energy:
    $1$, $10$, $100$~GeV, $1$ and $10$~TeV.}
  \label{fig:shower_containment}
\end{figure}

\begin{table}
  \begin{tabular}{p{0.25\linewidth}p{0.22\linewidth}p{0.25\linewidth}%
      p{0.2\linewidth}}
    \hline
    Experiment & Material & Depth [$X_0$] & Mass [kg]\\
    \hline
    \hline
    AMS-02 & Pb/fibers & 17 & 638\\
    ATIC & BGO & 22.6 & $\sim 500$\\
    CREAM & W/fibers & 20 & 380\\
    \Fermi-LAT & CsI(Tl) & 8.6 (10.1) & 1350\\
    PaMeLa & W/Si & 15.3 & 110\\
    \hline
  \end{tabular}
  \caption{Material and on-axis depth (in radiation lengths) for the
    calorimeters of some recent space- and balloon-borne experiments.
    The \Fermi-LAT being a pair-conversion telescope, its tracker is
    $1.5~X_0$ thick and effectively acts as a pre-shower, bringing the
    total thickness of the instrument to $10.1~X_0$ for normal incidence
    (see also the comments in section~\ref{sec:theta_instr_distr}).}
  \label{tab:exp_calorimeters}
\end{table}

The energy resolution of an electromagnetic calorimeter is usually
parameterized as
\begin{align}\label{eq:ecal_eres}
  \frac{\sigma_E}{E} = \frac{a}{\sqrt{E}} \oplus \frac{b}{E} \oplus c.
\end{align}
The stochastic term $a$ is due to the intrinsic fluctuations
related to the physical development of the shower. For homogeneous
calorimeters, assuming that the shower is entirely contained, the total
energy deposited does not fluctuate and the stochastic term is smaller
than one would expect based on counting statistics by the so called Fano
factor. In this case $a$ is typically of the order of a few \%~GeV$^{-1/2}$.
For sampling calorimeter the sampling fluctuations increase the stochastic term
and $a$ is more likely to be of the order of $5$--$20\%$~GeV$^{-1/2}$. 
The noise term $b$ is due to the electronic noise of the readout
chain and can dominate at low energy, especially when one operates at
high rate (i. e. when a large bandwidth is needed).
The constant term $c$ includes the effect of response
nonuniformities and represents the value at which the energy resolution
levels off when the stochastic term becomes negligible.

In real life there are additional contributions to the energy resolution 
of an electromagnetic calorimeter, such as the longitudinal and lateral
leakage, the upstream energy losses in other detectors and the effect of the
cracks and dead regions. Figure~\ref{fig:calorimeter_eres} shows that in
practice these effect can become dominant for relatively thin calorimeters.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{calorimeter_eres}
  \caption{On-axis energy resolution, as a function of the energy, for the
    \Fermi-LAT~\cite{2012ApJS..203....4A} and
    AMS-02~\cite{2013NIMPA.714..147A}. AMS-02 features a $17~X_0$ sampling
    calorimeter whose energy resolution can be parameterized according
    to~\eqref{eq:ecal_eres} with $a = 10.4\%$ and $c = 1.4\%$.
    The \Fermi-LAT features a homogeneous calorimeter, $8.6~X_0$ thick on axis
    (and a $1.5~X_0$ tracker converter effectively acting as a pre-shower), and
    the energy resolution at high energy is dominated by the shower leakage,
    which can only be partially compensated by using the imaging capabilities
    of the detector.}
  \label{fig:calorimeter_eres}
\end{figure}


\subsubsection{Hadronic calorimeters}%
\label{sec:had_cal}

From the standpoint of calorimetry, the main peculiarity of hadronic showers
is that some fraction of the energy contained in the non-electromagnetic
component does not contribute to the signal (this is what is usually called
\emph{invisible} energy). Endothermic spallation losses are responsible for
the vast majority of the invisible energy, as the nuclear binding energy of
the released protons, neutrons and heavier aggregates has to be supplied by
the shower particles that induce the reactions---and obviously does not
contribute to the signal. Furthermore nuclear recoils, neutron captures
producing delayed gamma rays, muons and neutrinos, all contribute to the
invisible energy.

In addition to that a calorimeter has a different response to
the electromagnetic and the non-electromagnetic components of a hadronic
shower, in terms of how much of the relative energy is actually translated
into \emph{visible signal}. The ratio of the efficiencies for conversion into
detector signal of the two components is customarily referred to as $h/e$, and
is characteristic of the material and detector configuration.

As a consequence of these basic facts, typical resolutions for hadronic
calorimeters are intrinsically worst than those of the best homogeneous
electromagnetic calorimeters (30--40\% being a representative figure).
It should also be noted that, being the nuclear
interaction length typically much larger than the radiation length, hadronic 
calorimeters need to be (geometrically) thick---and heavy. This is the main
reason why hadronic calorimetry, at least in the strict sense the term is used
in high-energy physics at accelerators, is not very fashionable in space.
One interesting detector concept---used, e.g., in the
ATIC~\cite{2009BRASP..73..564P} and CREAM~\cite{2011ApJ...728..122Y}
experiments---is that of exploiting a thick carbon\footnote{As shown in
figure~\ref{fig:rad_int_len}, low-$Z$ materials feature a comparatively short
nuclear interaction length, and therefore allow to realize relatively thick
(in the sense of $\lambda_I$) targets with reasonable weights.}
passive target to promote nuclear interaction of the hadrons and then
recovering the particle energy by measuring the electromagnetic component of
the shower.


\subsection{Particle identification}

There are traditionally four methods for particle identification in high-energy
physics: measurement of $dE/dx$, time of flight (TOF) systems, \cheren\
detectors and transition radiation detectors (TRD). In addition to that,
modern electromagnetic imaging calorimeters generally provide a high
discrimination power for the specific purpose of separating photon- or
electron/positron-initiated showers from hadronic showers---so we shall
briefly introduce this topic of discussion, too.

Incidentally, all of these techniques are exploited in the AMS-02 experiment
operating on the ISS, so the suite of contributions presented by the AMS-02
collaboration at the $33^{th}$ International Cosmic-Ray Conference is
potentially a good source of information on the state of the art of particle
identification in space. We refer the reader to~\cite{2012NIMPA.666..148L}
for a more accelerator-oriented review.


\subsubsection{Measurement of $dE/dx$}

The particle identification is based on the simultaneous measurement of the
momentum $p$ and the mean energy loss $dE/dx$ and relies on the fact that
the Bethe-Bloch formula is a universal function of $\beta\gamma = p/m$ for
all particle species (and therefore it's different for different masses at the
same momentum).

Particle identification systems (e.g. to discriminate protons from pions) based
on the mean energy loss have been successfully implemented up to reasonably
high energies, well in the relativistic rise (below the saturation point due
to the density effect) where typical $dE/dx$ differences are of the order of
$10\%$ or so. Since the typical width of the Landau curve for a single sample
is generally much larger than this differences, many independent measurements
are needed to achieve the required level of accuracy. Typically, due to the
long right tail of the Landau curve, some sort of truncated mean is the
simplest and most robust estimator of the average energy loss.

In the context of space-based cosmic-ray detectors the $dE/dx$ particle
identification technique is customarily used to identify the nuclear species
by measuring the atomic number $Z$ through the $Z^2$ dependence, as illustrated
in figure~\ref{fig:dedxcharge}.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{dedxcharge}
  \caption{Ionization energy loss, as a function of the rigidity, for protons
    and He nuclei in silicon. The values are calculated according
    to~\eqref{eq:stopping_power_full} without any density correction.
    In real life the separation power of the $dE/dx$ technique is determined
    by the Landau fluctuations, determining how closely the actual events
    cluster around the average line.}
  \label{fig:dedxcharge}
\end{figure}


\subsubsection{Time of flight}%
\label{sec:tof}

The time of flight method relies on the simultaneous measurement of the
particle momentum and velocity through the direct measurement of the time
$T$ it takes to the particles to traverse a distance $L$:
\begin{align}
  \beta c = \frac{L}{T}
\end{align}
We can invert the expression $p = m\gamma\beta c$ to obtain the mass of the
particle as a function of momentum and velocity:
\begin{align}\label{eq:tof_mass}
  m = \frac{p}{\gamma\beta c} = \frac{p\sqrt{1 - \beta^2}}{\beta c} =
  \frac{p}{c}\sqrt{\frac{1}{\beta^2} - 1}.
\end{align}
By the standard error propagation we get the relative error on the mass
measurement:
\begin{align}\label{eq:tof_mres}
  \frac{\sigma_m}{m} = \frac{\sigma_p}{p} \oplus
  \gamma^2 \frac{\sigma_\beta}{\beta}.
\end{align}
In practice $L$ is known with a good enough precision that the error on $\beta$
is essentially determined by the time resolution of the system. For
$L \sim 1.5$~m and $\sigma_T/T \sim 100$~ps (which are somewhat representative
of the AMS-02 detector), one can achieve a relative accuracy on the velocity
of the order of a few~\%. Depending on the actual momentum dependence of the
momentum resolution and the value of $\gamma$, the mass resolution can
be dominated by either one of the two terms.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{isotopes_rbeta}
  \caption{Relation between velocity and rigidity for different isotopes,
    according to~\eqref{eq:beta_r_isotopes}.}
  \label{fig:isotopes_rbeta}
\end{figure}

One practical application of this technique is the measurement of
cosmic ray isotopical composition (e.g. the ratio of the differential
intensities of $^3$He/$^4$He). This is usually achieved by measuring
independently the particle rigidity $R$ with a magnetic spectrometer and its
velocity $\beta$ with a TOF system (in addition to the charge $Z$, that can be
inferred by means of $dE/dx$ measurements in both the spectrometer and the TOF).
In this context \eqref{eq:tof_mass} is more conveniently written as
\begin{align}\label{eq:beta_r_isotopes}
  A = \frac{ZeR\sqrt{1 - \beta^2}}{m_p\beta c^2},
\end{align}
where $A$ is the mass number of the nucleus and $m_p$ is the mass of the
nucleon. Equivalently, for fixed $Z$, the velocity and momentum for nuclei with
different mass numbers (i.e., isotopes) will be related to each other by
\begin{align}
  \beta = \left[ 1 + \left(\frac{A m_p c^2}{ZeR}\right)^2 \right]^{-\frac{1}{2}},
\end{align}
as illustrated in figure~\ref{fig:isotopes_rbeta} for the two cases of
$^3$He vs. $^4$He and $^{10}$Be vs. $^{11}$Be.

In any practical application the finite mass resolution given
by~\eqref{eq:tof_mres} will cause events to be distributed around the
theoretical line with a finite dispersion---at the level that an event-by-event
mass separation could be impossible. Still in these case a template fitting
of the data to the separate isotopical mass distribution can in principle
allow to recover the composition, at least at intermediate rigidities, where the
curves are best separated. We refer the reader to~\cite{2011ApJ...736..105A}
for a detailed description of the light nuclei isotopical composition
measurement by the AMS-01 experiment.


\subsubsection{\cheren\ detectors}

As explained in section~\ref{sec:cherenckov_rad}, the emission of \cheren\
radiation is a threshold process taking place when a particle moves in a medium
with index of refraction $n$ with velocity $\beta > 1/n$.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{beta_vs_r}
  \caption{Relativistic $\beta$ factor, as a function of the particle rigidity,
    for electrons and protons. For illustration purposes, the threshold
    corresponding to a representative index of refraction $n = 1.05$ is shown.}
  \label{fig:beta_vs_r}
\end{figure}

In their simplest form, \cheren\ detectors exploit the existence of this
threshold acting as \emph{digital counters} that allow to distinguish, e.g.,
electrons and protons over relatively large energy (or rigidity) ranges, as
illustrated in figure~\ref{fig:beta_vs_r}. This was done in many of the early
experiments aimed at measuring the positron component of cosmic rays, e.g.,
in~\cite{1968ApJ...152..783F,1969ApJ...158..771F,1975ApJ...198..493D,
  1974PhRvL..33...34B,1975ApJ...199..669B}.
Since $\beta$ approaches $\sim 1$ for $\sim 10$~GV
protons, it is difficult to effectively exploit this technique past a few GV.

The steep slope of the intensity of the \cheren\ signal near threshold (see,
e.g., figure~\ref{fig:cherenkov_betagamma}) allows quite an accurate velocity
measurement (though in a relatively small window). This can be used to
complement TOF system in mass measurements (see~\ref{sec:tof}) and, by
using a clever arrangement of radiators with different indices of refraction,
for actual spectral measurements~\cite{2011ApJ...742...14O}.

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{rich_sketch}
  \caption{Functional sketch of a ring imaging \cheren\ detector. A cone
    of \cheren\ radiation produced as the charged particle traverse the
    radiator is imaged as a ring on the photon detection plane, which provides
    a measurement of both the intensity of the radiation itself and of the
    \cheren\ angle $\theta_c$.}
  \label{fig:rich_sketch}
\end{figure}

The most advanced \cheren\ detectors, such as ring-imaging \cheren\ detectors
(RICH) exploit all the properties of the \cheren\ effect, including the
intensity of the signal \emph{and} the angular aperture of the light cone
(see figure~\ref{fig:rich_sketch} for a simplified, illustrative sketch).


\subsubsection{Transition radiation detectors}

Even a brief summary of the basic considerations going into a practical
implementation of a transition radiation detector (see, e.g, \cite{PDG} and
\cite{2012NIMPA.666..130A}) are beyond the scope of this review.
Typically many layers of radiator (i.e., optical interfaces) are needed to
achieve a reasonable signal yield, interleaved with suitable radiation
detection elements---usually gas detectors. Furthermore, since the
transition radiation is emitted in the forward direction at a fairly small
angle, it effectively overlaps with the ionization due to the specific
energy loss $dE/dx$, and the latter is also an important ingredient of the mix.

As pointed out in section~\ref{sec:transition_rad}, a relativistic
$\gamma$ factor of the order of $10^3$ is needed for the spectrum of the
transition radiation to extend in the X-ray domain---i.e., where the emitted
photons can be effectively detected on top of the energy deposited by
ionization. Figure~\ref{fig:gamma_vs_r} shows that a relatively large rigidity
window---roughly speaking between $\sim 1$~GV and $\sim 1$~TV---exists where
the transition radiation can potentially be exploited to discriminate
electrons and positrons from protons. (Incidentally, this extends to much
higher energies than those accessible to the \cheren\ technique discussed in
the previous section).

\begin{figure}[htb!]
  \includegraphics[width=\linewidth]{gamma_vs_r}
  \caption{Relativistic $\gamma$ factor, as a function of the particle rigidity,
    for electrons and protons.}
  \label{fig:gamma_vs_r}
\end{figure}

To put things in context, the TRD of the AMS-02 experiment operating on the
ISS provides a proton rejection factor (at 90\% electron efficiency) in
excess of 100 between 1 and 600~GV, peaking around $2 \times 10^4$ at
$\sim 10$~GV~\cite{2013NuPhS.243...12T}.


\subsubsection{Electron-proton separation in imaging calorimeters}

As mentioned in section~\ref{sec:had_showers}, the differences between
electromagnetic and hadronic showers are customarily exploited to discriminate
high-energy photons, electrons and positrons from hadrons.

As shown in figure~\ref{fig:rad_int_len}, for the typical materials used in
electromagnetic calorimeters the interaction length $\lambda_I$ is 20--30 times
larger than the radiation length $X_0$ (when expressed in g~cm$^{-2}$, i.e.,
normalized to the density).
This means that the first interaction is on average at a much larger depth
for a hadronic shower than for an electromagnetic one---something like
$\sim 20$--30~cm vs. $\sim 1$~cm. Incidentally, since electromagnetic
calorimeters are typically \emph{thin} (less than one~$\lambda_I$) from the
standpoint of hadrons, a significant fraction of high-energy protons just pass
through without interacting---i.e., behaving as minimum ionizing particles.
(It goes without saying that they are easy to discriminate from photons and
electrons.)

In addition to that electromagnetic showers are typically \emph{well behaved}
in that the vast majority of the energy is contained within a few Moliere radii
from the shower axis and, fluctuations aside, the shower maximum is located at
a predictable position along the axis. In contrast hadronic showers are
generally much wider (due to secondaries from inelastic nuclear interactions
spreading out with non-negligible transverse momentum) and feature larger
fluctuations in both the longitudinal and the transverse development.
